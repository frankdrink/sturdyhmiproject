package com.sturdy.hmi.adapter;

/**
 * Created by franklin on 2019/8/9.
 */

public class MenuListItem {
    private String web;
    private Integer imageId;
    private boolean checked = false;

    public MenuListItem(String web,Integer imageId,boolean checked)
    {
        this.web=web;
        this.imageId=imageId;
        this.checked=checked;
    }
    public boolean isChecked() {
        return checked;
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public String getTitle() {
        return web;
    }
    public void setTitle(String title) {
        this.web = title;
    }
    public Integer getImageId() {
        return imageId;
    }
    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}
