package com.sturdy.hmi;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.filebrowser.FileBrowserWithCustomHandler;
import com.sturdy.filebrowser.models.FileItem;
import com.sturdy.filebrowser.utils.UIUtils;
import com.sturdy.filechooser.Content;
import com.sturdy.filechooser.StorageChooser;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.FileUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.view.DashedLineView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class SteriRecordReaderActivity extends BaseToolBarActivity {
    private Handler handler = new Handler();
    private StorageChooser.Builder builder = new StorageChooser.Builder();
    private StorageChooser chooser;
    TableLayout tableLayout;
    String mRecordData[];

    private Runnable updateTableRunnable = new Runnable() {
        @Override
        public void run() {
            try{
                tableLayout.removeAllViews();
                String fileString = GlobalClass.getInstance().globalStdRecordSaveString;
                if (fileString!="") {
                    mRecordData = fileString.split("\\n");
                    for(String line: mRecordData) {
                        TableRow row= new TableRow(mActivity);
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                                TableRow.LayoutParams.WRAP_CONTENT);
                        row.setLayoutParams(lp);
                        TextView tv1 = new TextView(mActivity);
                        tv1.setGravity(Gravity.LEFT);
                        tv1.setTextSize(28);
                        tv1.setTypeface(Typeface.MONOSPACE,Typeface.NORMAL);
                        tv1.setText(line);
                        tv1.setTextColor(Color.BLACK);
                        row.addView(tv1);
                        tableLayout.addView(row,new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.WRAP_CONTENT,
                                TableLayout.LayoutParams.WRAP_CONTENT));
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            handler.postDelayed(updateTableRunnable, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterireport);
        mActivity = this;

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);

        Button mNavTitleView = mNavTopBar.addLeftTextButton(Constants.instance().fetchValueString(saveSelectMenuNameKey), R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        registerBaseActivityReceiver();

        tableLayout = (TableLayout) findViewById(R.id.tableLayout1);

        handler.postDelayed(updateTableRunnable, 10);

    }


}
