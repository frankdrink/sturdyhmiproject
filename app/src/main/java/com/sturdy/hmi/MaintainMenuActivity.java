package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.filebrowser.FileBrowser;
import com.sturdy.filebrowser.FileChooser;
import com.sturdy.filechooser.Content;
import com.sturdy.filechooser.StorageChooser;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.NotificationDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import java.io.File;
import java.util.ArrayList;

import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class MaintainMenuActivity extends BaseToolBarActivity {
    private Activity mActivity;
    private StorageChooser.Builder builder = new StorageChooser.Builder();
    private StorageChooser chooser;
    private DatabaseHelper mDatabaseHelper;
    ListView list;
    String[] Menu_L = {
            "Sensor",
            "Altitude",
            "Cycle counter",
            "Pipe heater",
            "Optional Setting",
            "Software Update",
            "Firmware Update",
            "Reset admin password",
            "Default setting"

    } ;
    String[] Menu_B = {
            "Sensor",
            "Altitude",
            "Cycle counter",
            "Software Update",
            "Firmware Update",
            "Reset admin password",
            "Default setting"

    } ;
    Integer[] imageId = {
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain,
            R.drawable.setting_card_icon_maintain
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterilization_menu);
        mActivity = this;
        mDatabaseHelper=new DatabaseHelper(mActivity);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        registerBaseActivityReceiver();
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Maintain", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        SteriMenuList listAdapter=null;
        try {
            if ((GlobalClass.getInstance().MachineModel.equals("30L")) || (GlobalClass.getInstance().MachineModel.equals("60L"))) {
                listAdapter = new SteriMenuList(this, Menu_L, imageId);
            } else {
                listAdapter = new SteriMenuList(this, Menu_B, imageId);
            }
        }catch (Exception e){
            listAdapter = new SteriMenuList(this, Menu_L, imageId);
        }
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent();
                String programName;
                if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {

                    switch (position) {
                        case 0:
                            intent.setClass(MaintainMenuActivity.this, CalSensorActivity.class);
                            break;
                        case 1:
                            intent.setClass(MaintainMenuActivity.this, AltitudeSettingActivity.class);
                            break;
                        case 2:
                            intent.setClass(MaintainMenuActivity.this, CycleCounterSettingActivity.class);
                            break;
                        case 3:
                            intent.setClass(MaintainMenuActivity.this, PipeheaterSettingActivity.class);
                            break;
                        case 4:
                            intent.setClass(MaintainMenuActivity.this, OptionalSettingActivity.class);
                            break;
                        case 5: {
                            Content c = new Content();
                            c.setCreateLabel("Create");
                            c.setInternalStorageText("My Storage");
                            c.setCancelLabel("Cancel");
                            c.setSelectLabel("Select");
                            c.setOverviewHeading("Choose Drive");

                            builder.withActivity(mActivity)
                                    .withFragmentManager(getFragmentManager())
                                    .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                                    .withContent(c);
                            builder.withMemoryBar(true);
//                        builder.filter(StorageChooser.FileType.APK);
                            builder.allowCustomPath(true);
                            builder.setType(StorageChooser.FILE_PICKER);
                            chooser = builder.build();

                            chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                                @Override
                                public void onSelect(String path) {
                                    File file = new File(path);
                                    installApk(file);
//                                Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show();
                                }
                            });
                            chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                                @Override
                                public void onCancel() {
//                                Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                                }
                            });
//                        chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
//                            @Override
//                            public void onDone(ArrayList<String> selectedFilePaths) {
//                                for(String s: selectedFilePaths) {
////                            Log.e(TAG, s);
//                                }
//                            }
//                        });
                            chooser.show();
                        }

                        return;
                        case 6: {
                            Content c = new Content();
                            c.setCreateLabel("Create");
                            c.setInternalStorageText("My Storage");
                            c.setCancelLabel("Cancel");
                            c.setSelectLabel("Select");
                            c.setOverviewHeading("Choose Drive");

                            builder.withActivity(mActivity)
                                    .withFragmentManager(getFragmentManager())
                                    .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                                    .withContent(c);
                            builder.withMemoryBar(true);
                            builder.allowCustomPath(true);
                            builder.setType(StorageChooser.FILE_PICKER);
                            chooser = builder.build();

                            chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                                @Override
                                public void onSelect(String path) {
                                    Intent intent = new Intent();
                                    intent.setClass(MaintainMenuActivity.this, DFUProgressActivity.class);
                                    intent.putExtra("path", path);
                                    startActivity(intent);
                                    overridePendingTransition(0, 0);
                                }
                            });
                            chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                                @Override
                                public void onCancel() {
//                                Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
                                @Override
                                public void onDone(ArrayList<String> selectedFilePaths) {
                                    for (String s : selectedFilePaths) {
//                            Log.e(TAG, s);
                                    }
                                }
                            });
                            chooser.show();
                        }
                        return;

                        case 7:
                            new NotificationDialog(mActivity)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("Notification")
                                    .setContentText("Confirm to reset admin password?")
                                    .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            closeAllActivities();

                                            dialog.dismiss();

                                        }
                                    })
                                    .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            User user = new User();
                                            user.setName("Admin");
                                            user.setUserLevel(1);
                                            user.setPassword("0000");
                                            user.setBarcode("");
                                            mDatabaseHelper.updateUser(user);
                                            NoticeDialog ndialog = new NoticeDialog(getApplicationContext());
                                            ndialog.setContentImageRes(R.drawable.setting_icon_power_saving);
                                            ndialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                                            ndialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                                            ndialog.setAnimationEnable(true);
                                            ndialog.setTitleText("Notification");
                                            ndialog.setContentText("Reset Admin Password");
                                            ndialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                                @Override
                                                public void onClick(NoticeDialog dialog) {

                                                    dialog.dismiss();
                                                }
                                            }).show();
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;

                        case 8:
                            new NotificationDialog(mActivity)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("Notification")
                                    .setContentText("Are you sure to restore the factory settings? Except cycle time, the others will be reset to factory settings")
                                    .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();

                                        }
                                    })
                                    .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            Intent intent = new Intent();
                                            intent.setClass(MaintainMenuActivity.this, CycleProgressActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(0, 0);
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;

                    }
                }else{
                    switch (position) {
                        case 0:
                            intent.setClass(MaintainMenuActivity.this, CalSensorActivity.class);
                            break;
                        case 1:
                            intent.setClass(MaintainMenuActivity.this, AltitudeSettingActivity.class);
                            break;
                        case 2:
                            intent.setClass(MaintainMenuActivity.this, CycleCounterSettingActivity.class);
                            break;
                        case 3: {
                            Content c = new Content();
                            c.setCreateLabel("Create");
                            c.setInternalStorageText("My Storage");
                            c.setCancelLabel("Cancel");
                            c.setSelectLabel("Select");
                            c.setOverviewHeading("Choose Drive");

                            builder.withActivity(mActivity)
                                    .withFragmentManager(getFragmentManager())
                                    .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                                    .withContent(c);
                            builder.withMemoryBar(true);
//                        builder.filter(StorageChooser.FileType.APK);
                            builder.allowCustomPath(true);
                            builder.setType(StorageChooser.FILE_PICKER);
                            chooser = builder.build();

                            chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                                @Override
                                public void onSelect(String path) {
                                    File file = new File(path);
                                    installApk(file);
//                                Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show();
                                }
                            });
                            chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                                @Override
                                public void onCancel() {
//                                Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                                }
                            });
//                        chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
//                            @Override
//                            public void onDone(ArrayList<String> selectedFilePaths) {
//                                for(String s: selectedFilePaths) {
////                            Log.e(TAG, s);
//                                }
//                            }
//                        });
                            chooser.show();
                        }

                        return;
                        case 4: {
                            Content c = new Content();
                            c.setCreateLabel("Create");
                            c.setInternalStorageText("My Storage");
                            c.setCancelLabel("Cancel");
                            c.setSelectLabel("Select");
                            c.setOverviewHeading("Choose Drive");

                            builder.withActivity(mActivity)
                                    .withFragmentManager(getFragmentManager())
                                    .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                                    .withContent(c);
                            builder.withMemoryBar(true);
                            builder.allowCustomPath(true);
                            builder.setType(StorageChooser.FILE_PICKER);
                            chooser = builder.build();

                            chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                                @Override
                                public void onSelect(String path) {
                                    Intent intent = new Intent();
                                    intent.setClass(MaintainMenuActivity.this, DFUProgressActivity.class);
                                    intent.putExtra("path", path);
                                    startActivity(intent);
                                    overridePendingTransition(0, 0);
                                }
                            });
                            chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                                @Override
                                public void onCancel() {
//                                Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
                                @Override
                                public void onDone(ArrayList<String> selectedFilePaths) {
                                    for (String s : selectedFilePaths) {
//                            Log.e(TAG, s);
                                    }
                                }
                            });
                            chooser.show();
                        }
                        return;

                        case 5:
                            new NotificationDialog(mActivity)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("Notification")
                                    .setContentText("Confirm to reset admin password?")
                                    .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();

                                        }
                                    })
                                    .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            User user = new User();
                                            user.setName("Admin");
                                            user.setUserLevel(1);
                                            user.setPassword("0000");
                                            user.setBarcode("");
                                            mDatabaseHelper.updateUser(user);
                                            NoticeDialog ndialog = new NoticeDialog(getApplicationContext());
                                            ndialog.setContentImageRes(R.drawable.setting_icon_power_saving);
                                            ndialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                                            ndialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                                            ndialog.setAnimationEnable(true);
                                            ndialog.setTitleText("Notification");
                                            ndialog.setContentText("Reset Admin Password");
                                            ndialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                                @Override
                                                public void onClick(NoticeDialog dialog) {

                                                    dialog.dismiss();
                                                }
                                            }).show();
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;

                        case 6:
                            new NotificationDialog(mActivity)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("Notification")
                                    .setContentText("Are you sure to restore the factory settings? Except cycle time, the others will be reset to factory settings")
                                    .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();

                                        }
                                    })
                                    .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(NotificationDialog dialog) {
                                            Intent intent = new Intent();
                                            intent.setClass(MaintainMenuActivity.this, CycleProgressActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(0, 0);
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;

                    }
                }
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        startShowTitleClock(rightTimeButton);
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
    }

    private void installApk(File file) {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
        Intent intent = new Intent("android.intent.action.VIEW");

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.d("installApk", "installApk");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }
}
