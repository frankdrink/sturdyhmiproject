package com.sturdy.springindicator;


public interface TabClickListener {

    public boolean onTabClick(int position);

}
