package com.sturdy.hmi;


import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StartSterInformationFragmentA extends Fragment {

    RecyclerView recyclerView;
    ListView list;

    private TextView mPM25;
    private TextView mPaperAQI;
    private TextView mPaperAQIDesc;
    private TextView mPaperArea;
    private TextView mPaperDivider1;
    private TextView mPaperDivider2;
    private TextView mPaperFeedbackDesc;
    private EditText mPaperFeedbackMessage;
    private TextView mPaperFeedbackTitle;
    private ViewGroup mPaperLayout;
    private ViewGroup mPaperListLayout;
    private TextView mPaperProposal;
    private TextView mPaperQuality;
    private TextView mPaperShareFrom;
    private TextView mPaperTime;
    private TextView mPaperTitle;
    private TextView mBy;
    private TextView mCity;
    private TextView mAQI;
    private ListView mListView;

    private ImageButton mPrint;
    private MediaPlayer mPrintPlayer;
    private MediaPlayer mRipPlayer;
    private ImageButton mShare;
    private TextView mStudio;
    private TextView mValue;
    private ViewGroup mrl;
    private String vAQI;
    private String vBeforeCity;
    private String vCity;
    private String vPM25;
    private String vQuality;
    private String vShare = "今日%s空气质量指数(AQI)：%s，等级【%s】；PM2.5 浓度值：%s μg/m3。%s。（请关注博客：http://blog.csdn.net/weidi1989 ）";
    private String vTime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.machinestatefragment, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        LayerDrawable bottomBorder = getBorders(
                Color.argb(255,242,242,242), // Background color
                Color.BLACK, // Border color
                0, // Left border in pixels
                0, // Top border in pixels
                0, // Right border in pixels
                1 // Bottom border in pixels
        );
        TableRow tableRow1_3 = (TableRow) view.findViewById(R.id.tableRow1_3);
        tableRow1_3.setBackground(bottomBorder);
        TableRow tableRow1_4 = (TableRow) view.findViewById(R.id.tableRow1_4);
        tableRow1_4.setBackground(bottomBorder);
    }

    private String wrapFont(String paramString) {
        return paramString + " ";
    }

    protected LayerDrawable getBorders(int bgColor, int borderColor,
                                       int left, int top, int right, int bottom){
        // Initialize new color drawables
        ColorDrawable borderColorDrawable = new ColorDrawable(borderColor);
        ColorDrawable backgroundColorDrawable = new ColorDrawable(bgColor);

        // Initialize a new array of drawable objects
        Drawable[] drawables = new Drawable[]{
                borderColorDrawable,
                backgroundColorDrawable
        };

        // Initialize a new layer drawable instance from drawables array
        LayerDrawable layerDrawable = new LayerDrawable(drawables);

        // Set padding for background color layer
        layerDrawable.setLayerInset(
                1, // Index of the drawable to adjust [background color layer]
                left, // Number of pixels to add to the left bound [left border]
                top, // Number of pixels to add to the top bound [top border]
                right, // Number of pixels to add to the right bound [right border]
                bottom // Number of pixels to add to the bottom bound [bottom border]
        );

        // Finally, return the one or more sided bordered background drawable
        return layerDrawable;
    }

    private void initFontface() {
//        Typeface typeface = Typeface.createFromAsset(getAssets(),
//                "fonts/LCD.ttf");
//        mCity.setTypeface(typeface);
//        mAQI.setTypeface(typeface);
//        mPM25.setTypeface(typeface);
//        mValue.setTypeface(typeface);
//        mBy.setTypeface(typeface);
//        mStudio.setTypeface(typeface);
    }
}