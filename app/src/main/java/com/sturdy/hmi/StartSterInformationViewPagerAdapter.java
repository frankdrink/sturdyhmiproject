package com.sturdy.hmi;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sturdy.hmi.StartSterInformationFragmentA;
import com.sturdy.hmi.StartSterInformationFragmentB;


public class StartSterInformationViewPagerAdapter extends FragmentPagerAdapter {

    public StartSterInformationViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new StartSterInformationFragmentA();
        }
        else if (position == 1)
        {
            fragment = new StartSterInformationFragmentB();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Machine State";
        }
        else if (position == 1)
        {
            title = "Operational Info.";
        }
        return title;
    }
}
