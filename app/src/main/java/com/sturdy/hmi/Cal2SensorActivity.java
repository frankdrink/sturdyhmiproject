package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.UnitRadioListAdapter;
import com.sturdy.hmi.view.QuantityView;
import com.stx.xhb.commontitlebar.CustomTitleBar;

import java.util.Calendar;
import java.util.List;

public class Cal2SensorActivity extends BaseToolBarActivity {
    private Activity mActivity;
    UnitRadioListAdapter adapter1;
    UnitRadioListAdapter adapter2;
    int saveTempFormatBase;
    int savePresFormatBase;
    List<String> data1;
    List<String> data2;
    QuantityView chamberTempQuantityView;
    QuantityView bhTempQuantityView;
    QuantityView sgTempQuantityView;
    QuantityView chamberPresTempQuantityView;
    QuantityView bhPresTempQuantityView;



    List<MainMenuSettingElement> settingElementList;
    Handler handler;

    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cal2sensor);
        mActivity = this;
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Sensor", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        chamberTempQuantityView = (QuantityView) findViewById(R.id.chamber_temp_quantityView);
        chamberTempQuantityView.setReadOnly(true);

        bhTempQuantityView = (QuantityView) findViewById(R.id.bh_temp_quantityView);
        bhTempQuantityView.setReadOnly(true);

        sgTempQuantityView = (QuantityView) findViewById(R.id.sg_temp_quantityView);
        sgTempQuantityView.setReadOnly(true);

        chamberPresTempQuantityView = (QuantityView) findViewById(R.id.chamber_pres_quantityView);
        chamberPresTempQuantityView.setReadOnly(true);

        bhPresTempQuantityView = (QuantityView) findViewById(R.id.bh_pres_quantityView);
        bhPresTempQuantityView.setReadOnly(true);



        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        DrawMeButton mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(Cal2SensorActivity.this, CalibrationMenuActivity.class));
                    overridePendingTransition(0, 0);
                    finish();

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }




}
