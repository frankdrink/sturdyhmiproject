package com.sturdytheme.framework.picker;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sturdytheme.framework.popup.ConfirmPopup;
import com.sturdytheme.framework.widget.WheelView;


@SuppressWarnings("WeakerAccess")
public abstract class WheelPicker extends ConfirmPopup<View> {
    protected float lineSpaceMultiplier = WheelView.LINE_SPACE_MULTIPLIER;
    protected int textPadding = WheelView.TEXT_PADDING;
    protected int textSize = WheelView.TEXT_SIZE;
    protected Typeface typeface = Typeface.DEFAULT;
    protected int textColorNormal = WheelView.TEXT_COLOR_NORMAL;
    protected int textColorFocus = WheelView.TEXT_COLOR_FOCUS;
    protected int labelTextColor = WheelView.TEXT_COLOR_FOCUS;
    protected int offset = WheelView.ITEM_OFF_SET;
    protected boolean cycleDisable = true;
    protected boolean useWeight = true;
    protected boolean textSizeAutoFit = true;
    protected WheelView.DividerConfig dividerConfig = new WheelView.DividerConfig();

    public WheelPicker(Activity activity) {
        super(activity);
    }

    /**
     * 可用於設置每項的高度，範圍為2-4
     */
    public final void setLineSpaceMultiplier(@FloatRange(from = 2, to = 4) float multiplier) {
        lineSpaceMultiplier = multiplier;
    }

    /**
     * Use {@link #setTextPadding(int)} instead
     */
    @Deprecated
    public void setPadding(int textPadding) {
        this.textPadding = textPadding;
    }

    /**
     * 可用於設置每項的寬度，單位為dp
     */
    public void setTextPadding(int textPadding) {
        this.textPadding = textPadding;
    }

    /**
     * 設置文字大小
     */
    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    /**
     * 設置文字顏色
     */
    public void setTextColor(@ColorInt int textColorFocus, @ColorInt int textColorNormal) {
        this.textColorFocus = textColorFocus;
        this.textColorNormal = textColorNormal;
    }

    /**
     * 設置文字顏色
     */
    public void setTextColor(@ColorInt int textColor) {
        this.textColorFocus = textColor;
    }

    public void setLabelTextColor(int labelTextColor) {
        this.labelTextColor = labelTextColor;
    }

    /**
     * 設置分隔陰影是否可見
     */
    public void setShadowVisible(boolean shadowVisible) {
        if (null == dividerConfig) {
            dividerConfig = new WheelView.DividerConfig();
        }
        dividerConfig.setShadowVisible(shadowVisible);
    }

    /**
     * 設置分隔陰影顏色及透明度
     */
    public void setShadowColor(@ColorInt int color) {
        setShadowColor(color, 100);
    }

    /**
     * 設置分隔陰影顏色及透明度
     */
    public void setShadowColor(@ColorInt int color, @IntRange(from = 1, to = 255) int alpha) {
        if (null == dividerConfig) {
            dividerConfig = new WheelView.DividerConfig();
        }
        dividerConfig.setShadowColor(color);
        dividerConfig.setShadowAlpha(alpha);
    }

    /**
     * 設置分隔線是否可見
     */
    public void setDividerVisible(boolean visible) {
        if (null == dividerConfig) {
            dividerConfig = new WheelView.DividerConfig();
        }
        dividerConfig.setVisible(visible);
    }

    /**
     * @deprecated use {@link #setDividerVisible(boolean)} instead
     */
    @Deprecated
    public void setLineVisible(boolean visible) {
        setDividerVisible(visible);
    }

    /**
     * @deprecated use {@link #setDividerColor(int)} instead
     */
    @Deprecated
    public void setLineColor(@ColorInt int color) {
        setDividerColor(color);
    }

    /**
     * 設置分隔線顏色
     */
    public void setDividerColor(@ColorInt int lineColor) {
        if (null == dividerConfig) {
            dividerConfig = new WheelView.DividerConfig();
        }
        dividerConfig.setVisible(true);
        dividerConfig.setColor(lineColor);
    }

    /**
     * 設置分隔線長度比例
     */
    public void setDividerRatio(float ratio) {
        if (null == dividerConfig) {
            dividerConfig = new WheelView.DividerConfig();
        }
        dividerConfig.setRatio(ratio);
    }

    /**
     * 設置分隔線配置項，設置null將隱藏分割線及陰影
     */
    public void setDividerConfig(@Nullable WheelView.DividerConfig config) {
        if (null == config) {
            dividerConfig = new WheelView.DividerConfig();
            dividerConfig.setVisible(false);
            dividerConfig.setShadowVisible(false);
        } else {
            dividerConfig = config;
        }
    }

    /**
     * @deprecated use {@link #setDividerConfig(WheelView.DividerConfig)} instead
     */
    @Deprecated
    public void setLineConfig(WheelView.DividerConfig config) {
        setDividerConfig(config);
    }

    /**
     * 設置選項偏移量，可用來要設置顯示的條目數，範圍為1-5。
     * 1顯示3條、2顯示5條、3顯示7條……
     */
    public void setOffset(@IntRange(from = 1, to = 5) int offset) {
        this.offset = offset;
    }

    /**
     * 設置是否禁用循環
     */
    public void setCycleDisable(boolean cycleDisable) {
        this.cycleDisable = cycleDisable;
    }

    /**
     * 是否使用比重來平分佈局
     */
    public void setUseWeight(boolean useWeight) {
        this.useWeight = useWeight;
    }

    /**
     * 條目內容過長時是否自動減少字號來適配
     */
    public void setTextSizeAutoFit(boolean textSizeAutoFit) {
        this.textSizeAutoFit = textSizeAutoFit;
    }

    /**
     * 得到選擇器視圖，可內嵌到其他視圖容器
     */
    @Override
    public View getContentView() {
        if (centerView == null) {
            centerView = makeCenterView();
        }
        return centerView;
    }

    protected WheelView createWheelView() {
        WheelView wheelView = new WheelView(activity);
        wheelView.setLineSpaceMultiplier(lineSpaceMultiplier);
        wheelView.setTextPadding(textPadding);
        wheelView.setTextSize(textSize);
        wheelView.setTypeface(typeface);
        wheelView.setTextColor(textColorNormal, textColorFocus);
        wheelView.setDividerConfig(dividerConfig);
        wheelView.setOffset(offset);
        wheelView.setCycleDisable(cycleDisable);
        wheelView.setUseWeight(useWeight);
        wheelView.setTextSizeAutoFit(textSizeAutoFit);
        return wheelView;
    }

    protected WheelView createWheelView(boolean bCycle) {
        WheelView wheelView = new WheelView(activity);
        wheelView.setLineSpaceMultiplier(lineSpaceMultiplier);
        wheelView.setTextPadding(textPadding);
        wheelView.setTextSize(textSize);
        wheelView.setTypeface(typeface);
        wheelView.setTextColor(textColorNormal, textColorFocus);
        wheelView.setDividerConfig(dividerConfig);
        wheelView.setOffset(offset);
        wheelView.setCycleDisable(bCycle);
        wheelView.setUseWeight(useWeight);
        wheelView.setTextSizeAutoFit(textSizeAutoFit);
        return wheelView;
    }

    protected TextView createLabelView() {
        TextView labelView = new TextView(activity);
        labelView.setLayoutParams(new ViewGroup.LayoutParams(WRAP_CONTENT, WRAP_CONTENT));
        labelView.setTextColor(labelTextColor);
        labelView.setTextSize(textSize);
        return labelView;
    }

}
