package com.sturdy.chart.provider.pie;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.DisplayMetrics;

import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.PieData;

/**
 * Created by Frank on 2019/10/9.
 * 3D餅圖
 */

public class Pie3DProvider extends PieProvider {
    private int borderWidth = 20;

    //提供攝像頭
    private Camera camera = new Camera();

    public Pie3DProvider(Context context){
        //拉遠攝像頭Z軸
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float newZ = -displayMetrics.density * 6;
        camera.setLocation(0,0,newZ);
    }
    @Override
    public boolean calculationChild(ChartData<PieData> chartData) {
        return true;
    }

    /**
     * 變形開始
     *
     */
    @Override
    protected void matrixRectStart(Canvas canvas, Rect rect) {
        canvas.save();
        camera.save();
        canvas.translate(rect.centerX(),rect.centerY());
        camera.rotateX(60);
        camera.applyToCanvas(canvas);
        canvas.translate(-rect.centerX(),-rect.centerY());
        canvas.clipRect(rect);
        if(rotateHelper != null && rotateHelper.isRotate()){
            canvas.rotate((float) rotateHelper.getStartAngle(),rect.centerX(),rect.centerY());
        }

    }

    @Override
    protected void drawProvider(Canvas canvas, Rect zoomRect, Rect rect, Paint paint) {

        super.drawProvider(canvas, zoomRect, rect, paint);
        PointF centerPoint = getCenterPoint();
        paint.setStrokeWidth(borderWidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.parseColor("#80FFFFFF"));
        int radius = getCenterRadius();
        canvas.drawCircle(centerPoint.x,centerPoint.y,radius*getCenterCirclePercent()+borderWidth/2,paint);
        canvas.drawCircle(centerPoint.x,centerPoint.y,radius-borderWidth/2,paint);
    }

    /**
     * 變形結束
     *
     */
    @Override
    protected void matrixRectEnd(Canvas canvas, Rect rect) {
        camera.restore();
        super.matrixRectEnd(canvas, rect);

    }


}
