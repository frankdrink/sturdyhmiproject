package com.sturdy.chart.matrix;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import com.sturdy.chart.core.base.BaseChart;
import com.sturdy.chart.listener.ChartGestureObserver;
import com.sturdy.chart.listener.Observable;
import com.sturdy.chart.listener.OnChartChangeListener;

import java.util.List;

/**
 * 圖表放大縮小協助類
 */

public class MatrixHelper extends Observable<ChartGestureObserver> implements ITouch, ScaleGestureDetector.OnScaleGestureListener {

    /**
     * 最大縮放比
     */
    private static final int MAX_ZOOM = 10;
    /**
     * 最小縮放比
     */
    public static final int MIN_ZOOM = 1;
    /**
     * 當前縮放比
     */
    private float zoom = MIN_ZOOM; //縮放比例  不得小於1
    /**
     * X軸位移的距離
     */
    private int translateX; //以左上角為準，X軸位移的距離
    /**
     * y軸位移的距離
     */
    private int translateY;//以左上角為準，y軸位移的距離
    /**
     * 縮放手勢
     */
    private ScaleGestureDetector mScaleGestureDetector;
    /**
     * 移動手勢
     */
    private GestureDetector mGestureDetector;
    /**
     * 是否可以縮放
     */
    private boolean isCanZoom;
    /**
     * 是否正在縮放
     */
    private boolean isScale; //是否正在縮放
    /**
     * 原始圖表大小
     */
    private Rect originalRect; //原始大小
    /**
     * 縮放之後的大小
     */
    private Rect zoomRect;
    /**
     * 點擊X，y點
     */
    private float mDownX, mDownY;
    /**
     * 屏幕的手指點個數
     */
    private int pointMode;
    /**
     * 寬縮放比
     */
    private float widthMultiple =1;
    /**
     * 滾動輔助
     */
    private Scroller scroller;
    /**
     * 最小速率
     */
    private int mMinimumVelocity;
    /**
     * 是否正在飛滾
     */
    private boolean isFling;
    /**
     * 飛滾速率
     */
    private float flingRate = 0.8f; //速率
    /**
     * 圖表縮放監聽
     */
    private OnChartChangeListener listener;

    /**
     * 縮放移動手勢輔助類構造方法
     * @param context context
     */
    public MatrixHelper(Context context) {
        mScaleGestureDetector = new ScaleGestureDetector(context, this);
        mGestureDetector = new GestureDetector(context, new OnChartGestureListener());
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        scroller = new Scroller(context);
        isCanZoom = false;
    }

    /**
     * 處理手勢
     */
    @Override
    public boolean handlerTouchEvent(MotionEvent event) {
        if (isCanZoom) {
            mScaleGestureDetector.onTouchEvent(event);
        }
        mGestureDetector.onTouchEvent(event);
        return true;
    }



    /**
     * 判斷是否需要接收觸摸事件
     */
    @Override
    public void onDisallowInterceptEvent(BaseChart chart, MotionEvent event) {
        if (!isCanZoom) {
            return;
        }
        ViewParent parent = chart.getParent();
        if (zoomRect == null || originalRect == null) {
            parent.requestDisallowInterceptTouchEvent(false);
            return;
        }
        switch (event.getAction()&MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                pointMode = 1;
                //ACTION_DOWN的時候，趕緊把事件hold住
                mDownX = event.getX();
                mDownY = event.getY();
                if(originalRect.contains((int)mDownX,(int)mDownY)){ //判斷是否落在圖表內容區中
                    parent.requestDisallowInterceptTouchEvent(true);
                }else{
                    parent.requestDisallowInterceptTouchEvent(false);
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                pointMode += 1;
                parent.requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                if (pointMode > 1) {
                    parent.requestDisallowInterceptTouchEvent(true);
                    return;
                }
                float disX = event.getX() - mDownX;
                float disY = event.getY() - mDownY;
                boolean isDisallowIntercept = true;
                if (Math.abs(disX) > Math.abs(disY)) {
                    if ((disX > 0 && toRectLeft()) || (disX < 0 && toRectRight())) { //向右滑動
                        isDisallowIntercept = false;
                    }
                } else {
                   /* if ((disY > 0 && toRectTop()) || (disY < 0 && toRectBottom())) {*/
                    isDisallowIntercept = false;
                   /* }*/
                }
                parent.requestDisallowInterceptTouchEvent(isDisallowIntercept);
                break;
            case MotionEvent.ACTION_POINTER_UP:
                pointMode -= 1;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                pointMode = 0;
                parent.requestDisallowInterceptTouchEvent(false);
        }

    }

    private boolean toRectLeft() {
        return translateX <= -(zoomRect.width() - originalRect.width()) / 2;
    }

    private boolean toRectRight() {
        return translateX >= (zoomRect.width() - originalRect.width()) / 2;
    }

    private boolean toRectBottom() {

        return translateY >= (zoomRect.height() - originalRect.height()) / 2;
    }

    private boolean toRectTop() {
        return translateY <= -(zoomRect.height() - originalRect.height()) / 2;
    }

    @Override
    public void notifyObservers(List<ChartGestureObserver> observers) {

    }

    public void setScrollNext(int distanceX,int distanceY) {
        translateX += distanceX;
        translateY += distanceY;
        notifyViewChanged();
    }

    private float tempScale = MIN_ZOOM; //縮放比例  不得小於1
    private int tempTranslateX; //以左上角為準，X軸位移的距離
    private int tempTranslateY;//以左上角為準，y軸位移的距離
    class OnChartGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            translateX += distanceX;
            translateY += distanceY;
            notifyViewChanged();
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        /**
         * 飛滾
         */
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if(Math.abs(velocityX) >mMinimumVelocity || Math.abs(velocityY) >mMinimumVelocity) {
                scroller.setFinalX(0);
                scroller.setFinalY(0);
                tempTranslateX = translateX;
                tempTranslateY = translateY;
                scroller.fling(0,0,(int)velocityX,(int)velocityY,-50000,50000
                        ,-50000,50000);
                isFling = true;
                startFilingAnim(false);
            }

            return true;
        }

        //雙擊
        @Override
        public boolean onDoubleTap(MotionEvent e) {
//            if (isCanZoom) {
//                float oldZoom = zoom;
//                if (isScale) { //縮小
//                    zoom = zoom / 1.5f;
//                    if (zoom < 1) {
//                        zoom = MIN_ZOOM;
//                        isScale = false;
//                    }
//                } else { //放大
//                    zoom = zoom * 1.5f;
//                    if (zoom > MAX_ZOOM) {
//                        zoom = MAX_ZOOM;
//                        isScale = true;
//                    }
//                }
//                float factor = zoom / oldZoom;
//                resetTranslate(factor);
//                notifyObservers(observables);
//            }

            return true;
        }

        //單擊
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            notifyViewChanged();
            for (ChartGestureObserver observer : observables) {
//                observer.onClick(e.getX(), e.getY());
            }
            return true;
        }
    }
    private Point startPoint = new Point(0, 0);
    private Point endPoint = new Point();
    private TimeInterpolator interpolator = new DecelerateInterpolator();
    private PointEvaluator evaluator= new PointEvaluator();

    /**
     * 開始飛滾
     * @param doubleWay 雙向飛滾
     */
    private void startFilingAnim(boolean doubleWay) {

        int scrollX =Math.abs(scroller.getFinalX());
        int scrollY =Math.abs(scroller.getFinalY());
        if(doubleWay){
            endPoint.set((int) (scroller.getFinalX() * flingRate),
                    (int) (scroller.getFinalY() * flingRate));
        }else {
            if (scrollX > scrollY) {
                endPoint.set((int) (scroller.getFinalX() * flingRate), 0);
            } else {
                endPoint.set(0, (int) (scroller.getFinalY() * flingRate));
            }
        }
        final ValueAnimator valueAnimator = ValueAnimator.ofObject(evaluator,startPoint,endPoint);
        valueAnimator.setInterpolator(interpolator);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if(isFling) {
                    Point point = (Point) animation.getAnimatedValue();
                    translateX = tempTranslateX - point.x;
                    translateY = tempTranslateY - point.y;
                    notifyViewChanged();
                }else{
                    animation.cancel();
                }
            }
        });
        int duration = (int)(Math.max(scrollX,scrollY)*flingRate)/2;
        valueAnimator.setDuration(duration>300 ?300:duration);
        valueAnimator.start();
    }

    public void notifyViewChanged(){
        if(listener != null) {
            listener.onTableChanged(translateX, translateY);
        }
    }

    /**
     * 縮放開始
     */
    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        tempScale = this.zoom;

        return true;
    }

    /**
     * 正在縮放
     */
    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        float oldZoom = zoom;
        float scale = detector.getScaleFactor();
        this.zoom = (float) (tempScale * Math.pow(scale, 2));
        float factor = zoom / oldZoom;
        resetTranslate(factor);
        notifyObservers(observables);
        if (this.zoom > MAX_ZOOM) {
            this.zoom = MAX_ZOOM;
            return true;
        } else if (this.zoom < 1) {
            this.zoom = 1;
            return true;
        }
        return false;
    }

    /**
     * 縮放結束
     */
    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }


    /**
     * 重新計算偏移量
     * * @param factor
     */
    private void resetTranslate(float factor) {

        translateX = (int) (translateX * factor);
        translateY = (int) (translateY * factor);
    }

    /**
     * 獲取圖片內容的縮放大小
     *
     * @param providerRect 內容的大小
     * @return 縮放後內容的大小
     */
    public Rect getZoomProviderRect(Rect providerRect) {
        originalRect = providerRect;
        Rect scaleRect = new Rect();
        int oldw = providerRect.width();
        int oldh = providerRect.height();
        int multipleWidth = (int) (oldw *widthMultiple);

        int newWidth = (int) (multipleWidth * zoom);
        int newHeight = (int) (oldh * zoom);
        int offsetX = (int) (oldw*(zoom-1))/2;
        int offsetY =(int) (oldh*(zoom-1))/2;
        int maxTranslateLeft = (int)Math.abs(oldw*(zoom-1) / 2);
        int maxTranslateRight = newWidth - oldw - offsetX+1000;
        int maxTranslateY = Math.abs(newHeight - oldh) / 2;
        if(translateX >maxTranslateRight){
            translateX = maxTranslateRight;
        }
        if(translateX < -maxTranslateLeft){
            translateX = -maxTranslateLeft;
        }
        if (Math.abs(translateY) > maxTranslateY) {
            translateY = translateY > 0 ? maxTranslateY : -maxTranslateY;
        }

        scaleRect.left = providerRect.left - offsetX - translateX;
        scaleRect.right=scaleRect.left+newWidth;
        scaleRect.top = providerRect.top - offsetY - translateY;
        scaleRect.bottom = scaleRect.top+newHeight;
        zoomRect = scaleRect;
        return scaleRect;
    }

    /**
     * 獲取寬的倍數
     * <p>一旦設置倍數，寬度會變大相應倍數，這樣就可以橫向滑動了。</p>
     */
    public float getWidthMultiple() {
        return widthMultiple;
    }

    /**
     * 設置寬的倍數
     * <p>一旦設置倍數，寬度會變大相應倍數，這樣就可以橫向滑動了。</p>
     * @param widthMultiple 寬的倍數
     */
    public void setWidthMultiple(float widthMultiple) {
        this.widthMultiple = widthMultiple;
    }

    /**
     * 是否可以縮放
     * @return 是否可以縮放
     */
    public boolean isCanZoom() {
        zoom = 1f;
        return isCanZoom;

    }

    /**
     * 設置是否可以縮放
     * @param canZoom 是否可以縮放
     */
    public void setCanZoom(boolean canZoom) {
        isCanZoom = canZoom;
    }

    /**
     * 獲取當前縮放比例
     * @return 縮放比例
     */
    public float getZoom() {
        return zoom;
    }

    /**
     * 設置縮放滾動監聽
     * @return 縮放滾動監聽
     */
    public OnChartChangeListener getOnTableChangeListener() {
        return listener;
    }
    /**
     * 獲取縮放滾動監聽
     * @param onTableChangeListener 縮放滾動監聽
     */
    public void setOnTableChangeListener(OnChartChangeListener onTableChangeListener) {
        this.listener = onTableChangeListener;
    }

}
