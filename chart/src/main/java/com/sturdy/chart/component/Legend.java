package com.sturdy.chart.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;

import com.sturdy.chart.component.base.ILegend;
import com.sturdy.chart.component.base.PercentComponent;
import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.ColumnData;
import com.sturdy.chart.data.style.FontStyle;
import com.sturdy.chart.listener.OnClickLegendListener;
import com.sturdy.chart.provider.component.point.ILegendPoint;
import com.sturdy.chart.provider.component.point.IPoint;
import com.sturdy.chart.provider.component.point.LegendPoint;

import java.util.List;

/**
 * 圖表圖例
 */

public class Legend<C extends ColumnData> extends PercentComponent<ChartData<C>> implements ILegend<C> {

    private FontStyle fontStyle;
    private ILegendPoint point;
    private int padding = 10;
    private PointF pointF;
    private boolean isSelectColumn = true;
    private OnClickLegendListener<C> onClickLegendListener;
    private boolean isDisplay = true;
    private String mTempValue = "";
    private String mTempETValue = "";
    private String mTempFTValue = "";
    private String mPresValue = "";
    private String str;

    public Legend(){
        LegendPoint p = new LegendPoint();
        p.getPointStyle().setWidth(p.getPointStyle().getWidth()*2);
        point = p;
        fontStyle = new FontStyle();
    }

    @Override
    public void computeRect(Rect chartRect) {
        if(isDisplay) {
            super.computeRect(chartRect);
        }
    }

    /**
     * 繪製
     * @param canvas 畫布
     * @param chartData 數據
     * @param paint 畫筆
     */
    @Override
    public void draw(Canvas canvas, ChartData<C> chartData, Paint paint) {

        if(isDisplay) {
            paint.setTextAlign(Paint.Align.LEFT);
            Rect legendRect = getRect();
            List<C> columnDataList = chartData.getColumnDataList();
            int maxLegendNameLength = 0;
            int columnDataSize = columnDataList.size();
            String maxLengthColumnName = null;
            for (int i = 0; i < columnDataSize; i++) {
                ColumnData columnData = columnDataList.get(i);
                String name = columnData.getName();
                if (maxLegendNameLength < name.length()) {
                    maxLengthColumnName = name;
                    maxLegendNameLength = name.length();
                }
            }
            fontStyle.setTextSize(22);
            fontStyle.fillPaint(paint);
            int textWidth = (int) paint.measureText(maxLengthColumnName);//文本長度
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float textHeight = fontMetrics.descent - fontMetrics.ascent;
            int maxLegendLength = (int) (point.getWidth() + padding * 3 + textWidth);
            int columnSize = legendRect.width() / maxLegendLength; //列
            columnSize = columnSize > 0 ? columnSize : 1;
            int rowSize = columnDataSize / columnSize;
            rowSize = rowSize > 0 ? rowSize : 1;
            int perHeight = (int) (textHeight + padding);
            int perWidth = legendRect.width() / columnSize;
            int offsetY = (legendRect.height() - perHeight * rowSize) / 2;
            offsetY = offsetY > 0 ? offsetY : 0;
            int offsetX = columnDataSize < columnSize ? (columnSize - columnDataSize) * perWidth / 2 : 30;
            for (int i = 0; i < columnDataList.size(); i++) {
                int column = i % columnSize;
                int row = i / columnSize;
                int startX = offsetX + legendRect.left + column * perWidth + (perWidth - maxLegendLength) / 2;
                int startY = legendRect.top + offsetY + row * perHeight;
                C columnData = columnDataList.get(i);
                String name = columnData.getName();
                float pointWidth = point.getWidth();
                float pointHeight = point.getHeight();
                if (pointF != null && isClickRect(startX - pointWidth, startX + perWidth, startY - padding / 2, startY + perHeight + padding / 2)) {
                    if (isSelectColumn) {
                        columnData.setDraw(!columnData.isDraw());
                    }
                    pointF = null;
                    if (onClickLegendListener != null) {
                        onClickLegendListener.onClickLegend(columnData, this);
                    }
                }
                paint.setColor(columnData.getColor());
                if (i==0) {
                    drawPoint(canvas, columnData.isDraw(), startX-10, (int) (startY - textHeight / 2 + pointHeight / 2)-10, paint);
                }else if (i==1){
                    drawPoint(canvas, columnData.isDraw(), startX-30, (int) (startY - textHeight / 2 + pointHeight / 2)-10, paint);
                }else if (i==2){
                    if (mTempETValue.length()>1)
                        drawPoint(canvas, columnData.isDraw(), startX-10, (int) (startY - textHeight / 2 + pointHeight / 2)-10, paint);
                }else if (i==3){
                    if (mTempFTValue.length()>1)
                        drawPoint(canvas, columnData.isDraw(), startX-30, (int) (startY - textHeight / 2 + pointHeight / 2)-10, paint);
                }
                startX += pointWidth + padding;
//                drawText(canvas, startX-28, startY-22, name, paint);
                if (i==0) {
                    drawText(canvas, startX-10, startY-5, mTempValue, paint);
                }else if (i==1){
                    drawText(canvas, startX-30, startY-5, mPresValue, paint);
                }else if (i==2){
                    drawText(canvas, startX-10, startY-5, mTempETValue, paint);
                }else if (i==3){
                    drawText(canvas, startX-30, startY-5, mTempFTValue, paint);
                }

            }
        }
    }


    @Override
    public void setFontStyle(FontStyle fontStyle) {
        this.fontStyle = fontStyle;
    }

    /**
     * Determine whether to click or not rect
     */

    private  boolean isClickRect(float left, float right, float top, float bottom){
        if(pointF != null) {
            return  pointF.x >= left && pointF.x <= right && pointF.y >= top && pointF.y <= bottom;
        }
        return false;
    }


    /**
     * 繪製文字
     */
    private void drawText(Canvas canvas, int startX, int startY, String  content, Paint paint) {
        fontStyle.fillPaint(paint);
        canvas.drawText(content, startX, startY, paint);
    }

    private void drawPoint(Canvas canvas,boolean isDraw,int x, int y, Paint paint){
        float w = point.getWidth();
        x += w/2;
        point.drawPoint(canvas,x,y,0,!isDraw,paint);

    }




    @Override
    public FontStyle getFontStyle() {
        return fontStyle;
    }


    @Override
    public void onClickLegend(PointF pointF) {
        this.pointF = pointF;
    }


    @Override
    public void setOnClickLegendListener(OnClickLegendListener<C> onClickLegendListener) {
        this.onClickLegendListener = onClickLegendListener;
    }

    @Override
    public int getPadding() {
        return padding;
    }
    @Override
    public void setPadding(int padding) {
        this.padding = padding;
    }

    public IPoint getPoint() {
        return point;
    }

    public void setPoint(ILegendPoint point) {
        this.point = point;
    }
    @Override
    public void setSelectColumn(boolean selectColumn) {
        isSelectColumn = selectColumn;
    }

    @Override
    public void setDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    @Override
    public void setCurrentValue(String temp,String pres,String tempET,String tempFT) {
        this.mTempValue = temp;
        this.mTempETValue = tempET;
        this.mPresValue = pres;
        this.mTempFTValue = tempFT;

    }
}
