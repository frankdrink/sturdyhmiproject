package com.sturdy.hmi;

/**
 * Created by franklin on 2019/10/24.
 */


import android.os.Environment;

import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.wifilibrary.WiFiManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dma.xch.hmi.api.HmiClient;

public class GlobalClass{
    public Constants.MACHINE_STATE MachineState;
    public String MachineModel;                             // 30L,60L,30B,60B
    public HmiClient client=null;
    public HashMap<Integer,Integer> portMap = null;
    public boolean isStartSterillizationActivity=false;
    public boolean isPrintSignature=false;
    public boolean isFloatingTemp=false;
    public boolean isETemp=false;
    public char cLoginUserID=0x00;
    public int Steri_Temp;
    public int CB_DissolTemp;
    public int CB_DissolTime;
    public int CB_InsulationTemp;
    public int CB_InsulationTime;
    public int CB_CTTemp;
    public int CB_FTTemp;
    public int CB_ETTemp;
    public int CB_Reservation;
    public boolean CB_FanCooling;
    public boolean CB_FloatingSensor;
    public boolean CB_PressurizedCooling;
    public boolean CB_VacuumPump;
    public int CB_ExhaustLevel;
    public List<char[]> globalArrayList;
    public List<RecordStepClass> globalRecordStepList;
    public String globalCloudRecordSaveString;
    public String globalStdRecordSaveString;
    public String globalDtlRecordSaveString;
    public String globalStdCSVRecordSaveString;
    public int CB_LeakageRate;
    public int[] CB_LeakageBar = new int[4];
    public int[] CB_LeakageTime = new int[4];
    public char[] globalStageList;
    public String CB_Model;
    public String CB_Firmware;
    public String CB_SN;
    public long programStartTime;
    public long programEndTime;
    public int CB_PreVacuumCount;
    public boolean CB_PreVacuum;
    public int CB_SteriTemp;
    public int CB_Bar;
    public int CB_SteriTime;
    public int CB_DryTime;
    public int CB_WasteType;
    public boolean CB_SteriSuccess;
    public char CB_DoorLock;
    public char CB_CleanWaterLevel;                 // 清水箱水位
    public int CB_ErrorCode;                        // 控制版
    public int HM_WiFiStatus;                       // WiFi 0:none 1:
    public int HM_KeyboardStatus;                   // Keyboard 0:none 1:
    public int HM_USBStatus;                   // Keyboard 0:none 1:
    public WiFiManager mWiFiManager;

    public int CalSelectTemp;
    public int CalState;
    public boolean OpenSleepMode=false;
    public int SleepModeTime=1000;
    // DFU RecvBuffer
    static int BUFFER_SIZE = 2048;
    public byte[] dfuRecvBuffer = new byte[BUFFER_SIZE];
    public int dfuBufferPos = 0;
    public boolean isSerialServiceRun = false;
    public ArrayList<char[]> mSendCmdQueue = null;
    public String steriSN;
    public String saveStdRecordFileName;
    public String saveDtlRecordFileName;
    public String saveStdCSVRecordFileName;

    private GlobalClass() {
        globalArrayList = new ArrayList<char[]>();
        globalRecordStepList = new ArrayList<RecordStepClass>();
        globalStageList = new char[30];
    }

    private static GlobalClass instance;

    public static GlobalClass getInstance() {
        if (instance == null) instance = new GlobalClass();
        return instance;
    }

    public void writeDfuBuffer(char[]recv) {
        for(int i=0;i < recv.length;i++){
            dfuRecvBuffer[dfuBufferPos]=(byte)recv[i];
            dfuBufferPos++;
        }
    }

    public byte[] readDfuRecvData(int count) {
        try {
//                byte[] buffer = mSerialPort.readBytes(count, timeout);
//
//                if (mDebug)
//                    System.out.println("read bytes 0x" + Hex.encodeHexString( buffer ));
//
//                return buffer;
            char[] recv;
            byte[] buffer=null;
            boolean bRecv = false;
            for (int i=0;i<100;i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                buffer = recvDfuBuffer(count);
                if (buffer!=null) break;
            }
            if (buffer!=null) {
                String receviceMsg = asHex(buffer);
                appendLog(receviceMsg);
            }else{
//                    buffer = new byte[1];
//                    buffer[0] = 0x79;
                appendLog("null");
            }
            return buffer;
        } catch (Exception e) {
        }
        return null;
    }

    public byte readDfuRecvData() {
        try {
//                byte[] buffer = mSerialPort.readBytes(count, timeout);
//
//                if (mDebug)
//                    System.out.println("read bytes 0x" + Hex.encodeHexString( buffer ));
//
//                return buffer;
            char[] recv;
            byte buffer=-1;
            boolean bRecv = false;
            for (int i=0;i<100;i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                buffer = recvDfuBuffer();
                if (buffer!=-1) break;
            }
            if (buffer!=-1) {
                String receviceMsg = asHex(buffer);
                appendLog(receviceMsg);
            }else{
//                    buffer = new byte[1];
//                    buffer[0] = 0x79;
                appendLog("null");
            }
            return buffer;
        } catch (Exception e) {
            appendLog(e.toString());
        }
        return -1;
    }

    private byte[] recvDfuBuffer(int recvCount) {
        if (dfuBufferPos-recvCount<0) return null;
        dfuBufferPos-=recvCount;
        byte[] recvBuffer = new byte[recvCount];
        java.lang.System.arraycopy(dfuRecvBuffer, 0, recvBuffer, 0, recvCount);
        byte[] result = new byte[BUFFER_SIZE];
        java.lang.System.arraycopy(dfuRecvBuffer, recvCount, result, 0, BUFFER_SIZE-recvCount);
        java.lang.System.arraycopy(result, 0, dfuRecvBuffer, 0, BUFFER_SIZE);

        return recvBuffer;
    }

    private byte recvDfuBuffer() {
        int recvCount = 1;
        if (dfuBufferPos-recvCount<0) return -1;
        dfuBufferPos-=recvCount;
        byte recvBuffer;
        recvBuffer = dfuRecvBuffer[0];
        byte[] result = new byte[BUFFER_SIZE];
        java.lang.System.arraycopy(dfuRecvBuffer, recvCount, result, 0, BUFFER_SIZE-recvCount);
        java.lang.System.arraycopy(result, 0, dfuRecvBuffer, 0, BUFFER_SIZE);

        return recvBuffer;
    }

    public void appendLog(String text)
    {
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "dfu_uart.txt");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void saveRecordData(String text)
    {
        if (saveStdRecordFileName==null) return;
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveStdRecordFileName);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
//            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static String asHex (byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public static String asHex (byte buf) {
        byte[] hex = new byte[1];
        hex[0] = buf;
        StringBuffer strbuf = new StringBuffer(hex.length * 2);
        int i;
        for (i = 0; i < hex.length; i++) {
            if (((int) hex[i] & 0xff) < 0x10)
                strbuf.append("0");
            strbuf.append(Long.toString((int) hex[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public int getKeyFromMap(HashMap<Integer,Integer> portMap, int port){
        int portNum = -1;
        Set<Integer> keySet = portMap.keySet();
        Iterator<Integer> iterator = keySet.iterator();
        while(iterator.hasNext()){
            int key = iterator.next();
            int portVal = portMap.get(key);
            if(portVal==port){
                portNum = key;
            }
        }
        return portNum;
    }
}

