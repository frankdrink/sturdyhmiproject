package com.sturdy.hmi;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.view.circleprogressview.CircleProgressView;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.adapter.CycleListAdapter;
import com.sturdy.hmi.adapter.EditListAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import android.provider.Settings.System;
import static com.sturdy.hmi.Constants.BrightnessKey;
import static com.sturdy.hmi.Constants.SoundKey;

public class USBProgressActivity extends BaseToolBarActivity {
    private Activity mActivity;
    Handler handler;
    private CircleProgressView cpv;
    TextView descTV;
    String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_usbprogress);
        registerBaseActivityReceiver();
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        cpv = findViewById(R.id.cpv);
        cpv.setShowTick(false);
        cpv.setTurn(false);
        cpv.setProgressColor(0xff0277bd);
//        cpv.showAnimation(100,3000);
        descTV =  (TextView)findViewById(R.id.description_textview);

//
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetCSDNLogoTask().execute( "" );

    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class GetCSDNLogoTask extends AsyncTask<String,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            publishProgress(0);//将会调用onProgressUpdate(Integer... progress)方法
            int totalCount = 3600*4;
            for (int i=0;i<totalCount;i++) {
                appendLog("S00-00 045:43  135.0  2.297");
                appendLog("S00-00 045:43  135.0  2.297");
                appendLog("S00-00 045:43  135.0  2.297");
                appendLog("S00-00 045:43  135.0  2.297");
                float progress = (float)i/(float)totalCount*(float)100;

                publishProgress((int)progress);
            }

            publishProgress(100);
            //mImageView.setImageBitmap(result); // 不能在背景處理UI
            return (Boolean.TRUE);
        }

        protected void onProgressUpdate(Integer... progress) {
            cpv.setProgress((int)progress[0]);
            int totalCount = 3600*4;
            float nowCount = (float)progress[0] / (float)100 * (float)totalCount;
            str = String.format("%d / %d\nData transferring...",(int)nowCount,totalCount);
            descTV.setText(str);
        }

        protected void onPostExecute(Boolean aBoolean) {
            str = "100 %\nData transferring...";
            descTV.setText(str);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeAllActivities();
                }
            }, 3000);
        }

        protected void onPreExecute () {//在 doInBackground(Params...) 之前

        }

        protected void onCancelled () {
        }

    }

    public void appendLog(String text)
    {
        File logFile = new File("/storage/udisk3", "P20191118.Detial");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return;
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
