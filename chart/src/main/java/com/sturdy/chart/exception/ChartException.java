package com.sturdy.chart.exception;

/**
 * Created by Frank 2020/01/01
 */

public class ChartException extends RuntimeException {

    public ChartException(String message) {
        super(message);
    }
}
