package com.sturdy.hmi;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.chart.core.LineChart;
import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.LineData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.filechooser.StorageChooser;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.Service.SerialPortService;
import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.FileUtils;
import com.stx.xhb.commontitlebar.CustomTitleBar;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.AutoAddWaterKey;
import static com.sturdy.hmi.Constants.BuzzerVolumeKey;
import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.ExhaustTempKey;
import static com.sturdy.hmi.Constants.HMI_ERROR_BROADCAST;
import static com.sturdy.hmi.Constants.HMI_RMSLEEP_BROADCAST;
import static com.sturdy.hmi.Constants.HMI_SLEEP_BROADCAST;
import static com.sturdy.hmi.Constants.MACHINE_STATE.IDLE;
import static com.sturdy.hmi.Constants.NextAirFilterKey;
import static com.sturdy.hmi.Constants.NextExhaustFilterKey;
import static com.sturdy.hmi.Constants.NextGasketServiceKey;
import static com.sturdy.hmi.Constants.NextServiceCycleKey;
import static com.sturdy.hmi.Constants.PipeheaterKey;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.SleepTimeKey;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;
import static java.net.Proxy.Type.HTTP;

public class MainActivity extends BaseToolBarActivity implements GridViewAdapter.OnItemClickListener{
    Handler handler = new Handler();
    private RecyclerView listView;
    private RecyclerView gridView;
    private GridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;
    List<MainMenuSettingElement> settingElementList;
    private DatabaseHelper mDatabaseHelper;
    private Activity mActivity;
    private String settext;
    private char[] Error_char = new char[2];

    private LineChart lineChart;
    //    private BaseCheckDialog<ChartStyle> chartDialog;
    List<String> chartYDataList;
    ArrayList<Double> tempList1,tempList2,tempETList;
    ArrayList<Double> presList;
    ChartData<LineData> chartData2;
    List<LineData> ColumnDatas;
    private int addCount = 0;

    private BroadcastReceiver mFileChooserReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            String filePath = intent.getStringExtra("BROADCAST_SELECTED_FILE");
            intent = new Intent();
            intent.setClass(MainActivity.this,RecordReaderActivity.class);
            intent.putExtra("fileName",filePath);
            startActivity(intent);
        }
    };

    private Runnable mSleepTimeRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if ((GlobalClass.getInstance().isStartSterillizationActivity)||(GlobalClass.getInstance().MachineState==Constants.MACHINE_STATE.CB_DFU)||(GlobalClass.getInstance().MachineState==Constants.MACHINE_STATE.DYNAMIC_CAL)) {
                handler.removeCallbacks(mSleepTimeRunnable);
                int sleepTime = Constants.instance().fetchValueInt(SleepTimeKey,0)*1000*60;
                if (sleepTime>0)
                    handler.postDelayed(mSleepTimeRunnable, sleepTime);
            }else{
                if (!GlobalClass.getInstance().OpenSleepMode) {
                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this,SleepModeActivity.class);
                    startActivity(intent);
                    handler.removeCallbacks(mSleepTimeRunnable);
                }
            }
        }
    };

    private BroadcastReceiver mRestartSleepReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            handler.removeCallbacks(mSleepTimeRunnable);
            if ((GlobalClass.getInstance().isStartSterillizationActivity)||(GlobalClass.getInstance().MachineState==Constants.MACHINE_STATE.CB_DFU)||(GlobalClass.getInstance().MachineState==Constants.MACHINE_STATE.DYNAMIC_CAL)) {

            }else {
                int sleepTime = Constants.instance().fetchValueInt(SleepTimeKey, 0) * 1000 * 60;
                if (sleepTime > 0)
                    handler.postDelayed(mSleepTimeRunnable, sleepTime);
            }
        }
    };

    private BroadcastReceiver mRemoveSleepReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            handler.removeCallbacks(mSleepTimeRunnable);
        }
    };

    private BroadcastReceiver mShowErrorDialog = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            GlobalClass.getInstance().CB_ErrorCode = intent.getIntExtra("Error",0);
            String contentStr;
            Error_char = CharUtils.fromShort((short)GlobalClass.getInstance().CB_ErrorCode);

            GlobalClass.getInstance().programStartTime = System.currentTimeMillis()/1000;
            programEndTime=0;
//            new Thread(new Runnable(){
//                @Override
//                public void run() {
//                    if (GlobalClass.getInstance().CB_ErrorCode>0) {
//                        HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//
//                        try {
//                            List<NameValuePair> params = new ArrayList<NameValuePair>();
//                            params.add(new BasicNameValuePair("programName","Message "+GlobalClass.getInstance().CB_ErrorCode));
//                            params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
//                            params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
//                            params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                            params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
//                            myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                            HttpResponse response = new DefaultHttpClient().execute(myPost);
//                            String er = response.toString();
//                        } catch (Exception e) {
//                            String ee = e.toString();
//                            e.printStackTrace();
//                        }
//                    }
//
//                }
//            }).start();


            switch (GlobalClass.getInstance().CB_ErrorCode) {
                case 0:
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
//                            if (isTopActivity("DoorOpenActivity")) {
                                new PreScanDialog(mActivity)
                                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                        .setAnimationEnable(true)
                                        .setTitleText("Post scan")
                                        .setContentText("Post scan is activated, please scan first and then will be back to main page")
                                        .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                            @Override
                                            public void onClick(PreScanDialog dialog) {
                                                dialog.dismiss();
                                                Intent intent = new Intent(MainActivity.this, PostBarCodeScanActivity.class);
                                                intent.putExtra("StartSteri",1);
                                                startActivity(intent);
                                            }
                                        })
                                        .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                            @Override
                                            public void onClick(PreScanDialog dialog) {
                                                if (GlobalClass.getInstance().isPrintSignature) {
                                                    String printData = "\n\n\r\n" +
                                                            "Signature:______________________\n\n\n\r\n";
                                                    sendToServicePrinter(printData.toCharArray(),printData);
                                                    saveDtlToCache(printData.toCharArray());
                                                }
                                                GlobalClass.getInstance().MachineState = IDLE;
                                                GlobalClass.getInstance().isPrintSignature = false;
                                                dialog.dismiss();
                                                closeAllActivities();
                                            }
                                        })
                                        .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                            @Override
                                            public void onClick(PreScanDialog dialog) {
                                                if (GlobalClass.getInstance().isPrintSignature) {
                                                    String printData = "\n\n\r\n" +
                                                            "Signature:______________________\n\n\n\r\n";
                                                    sendToServicePrinter(printData.toCharArray(),printData);
                                                    saveDtlToCache(printData.toCharArray());
                                                }
                                                GlobalClass.getInstance().MachineState = IDLE;
                                                GlobalClass.getInstance().isPrintSignature = false;
                                                dialog.dismiss();
                                                closeAllActivities();
                                            }
                                        }).show();
//                            }
                        }
                    });
                    break;
                case 2:
                    contentStr = "Code 2\nEmergency stop";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_emergency_stop);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 3:
                    contentStr = "Code 3\nStop operation\nWait......";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_stop);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 10:
                    contentStr = "Code 10\nmaintenance service";
//                    new Thread(new Runnable(){
//                        @Override
//                        public void run() {
//                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
//                            try {
//                                List<NameValuePair> params = new ArrayList<NameValuePair>();
//                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
//                                params.add(new BasicNameValuePair("title","Code 10 Gasket Aging"));
//                                params.add(new BasicNameValuePair("description","The door gasket has exceeded the number or time of use, please replace it, or contact your service technician for replacement and maintenance."));
//                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                                HttpResponse response = new DefaultHttpClient().execute(myPost);
//                            } catch (Exception e) {
//                                String ee = e.toString();
//                                e.printStackTrace();
//                            }
//                        }
//                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 11:
                    contentStr = "Code 11\nExhaust Filter service";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 12:
                    contentStr = "Code 12\nAir Filter service";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 13:
                    contentStr = "Code 13\nGasket service";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 101:
                    contentStr = "Code 101\nmaintenance service";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 101 Solid State Relay (SSR) is Abnormal"));
                                params.add(new BasicNameValuePair("description","The solid state relay (SSR) works abnormally, please contact service technician to troubleshoot."));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
                case 110:
                    contentStr = "Code 110\nAbsolute pressure sensor fault";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 110 Abnormal Environmental Pressure Sensor"));
                                params.add(new BasicNameValuePair("description","The pressure is sensed to a value beyond the use range, please contact service technician for troubleshooting"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 111:
                    contentStr = "Code 111\nPressure sensor fault";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 111 Abnormal Pressure Sensor in the Chamber"));
                                params.add(new BasicNameValuePair("description","The pressure is sensed to a value beyond the use range, please contact service technician for troubleshooting"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 120:
                    contentStr = "Code 120\nCJC fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 121:
                    contentStr = "Code 121\nTemp sensor T1 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 122:
                    contentStr = "Code 122\nTemp sensor T2 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 123:
                    contentStr = "Code 123\nTemp sensor T3 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 124:
                    contentStr = "Code 124\nTemp sensor T4 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 125:
                    contentStr = "Code 125\nTemp sensor T5 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 126:
                    contentStr = "Code 126\nTemp sensor T6 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 127:
                    contentStr = "Code 127\nTemp sensor T7 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 130:
                    contentStr = "Code 130\nKeyboard fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 140:
                    contentStr = "Code 140\nAir Filter block";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 140 Air Filter is Abnormal"));
                                params.add(new BasicNameValuePair("description","The chamber negative pressure back pressure is too slow, please confirm the user’s air filter or pressure sensor and the piping can work."));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 150:
                    contentStr = "Code 150\nFAN 1 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 151:
                    contentStr = "Code 151\nFAN 2 fault";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 160 System Cooling Fan is Abnormal"));
                                params.add(new BasicNameValuePair("description","The fan transmits is an abnormal signal, please confirm whether if each fan of the machine works abnormally, if not, please replace it"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 152:
                    contentStr = "Code 152\nFAN 3 fault";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 160:
                    contentStr = "Code 160\nSG heater abnormal";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 160 Steam Generator Failure"));
                                params.add(new BasicNameValuePair("description","The steam generator heating is abnormal, please confirm whether the water source is normal, and contact the service technician for troubleshooting."));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 161:
                    contentStr = "Code 161\nBand heater abnormal";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","The chamber B-heater is abnormal"));
                                params.add(new BasicNameValuePair("description","ode 161 The B-heating rate of the chamber heater is not good. Please confirm whether the voltage of the operating environment is abnormal, and check whether the wiring of the heater has bad contact or the value of the current. If there is a bad situation, please replace it."));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 200:
                    contentStr = "Code 200\nAltritude over";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 201:
                    contentStr = "Code 201\r\n";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 202:
                    contentStr = "Code 202\r\n";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 210:
                    contentStr = "Code 210\nOver heat";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 211:
                    contentStr = "Code 211\nOver pressure";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 220:
                    contentStr = "Code 220\nVacuum abnormal";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 220 Poor Airtight Cavity"));
                                params.add(new BasicNameValuePair("description","Please confirm whether the vacuum efficiency or pipeline is abnormal, if there is any abnormality, please troubleshoot"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_leakage);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 223:
                    contentStr = "Code 223\nDrain Filter is Abnormal";

                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_leakage);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 224:
                    contentStr = "Code 224\nPost vacuum abnormal";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_leakage);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 230:
                    contentStr = "Code 230\nPressure too high";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 231:
                    contentStr = "Code 231\nPressure too low";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 232:
                    contentStr = "Code 232\r\n";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {

                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 233:
                    contentStr = "Code 233\nExhaust over time";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 233 Drain Filter is Abnormal"));
                                params.add(new BasicNameValuePair("description","The drain rate is too slow, please clean the foreign matter on the drain filter. If the abnormality cannot be resolved after cleaning, please contact the service technician for troubleshooting."));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 240:
                    contentStr = "Code 240\nPre-heat over time";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Code 240 Intake Solenoid Valve is Abnormal"));
                                params.add(new BasicNameValuePair("description","Too much steam enters the Chamber, causing the temperature to rise too fast. Please confirm whether the solenoid valve is working or there are foreign objects in the valve"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 242:
                    contentStr = "Code 242\nLow temp during sterillizer step";

                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 243:
                    contentStr = "Code 243\r\n";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_low_water_level);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 246:
                    contentStr = "Code 246\nSterillizer temp over rang";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_error);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 302:
                    contentStr = "Code 302\nLeakage fail";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_leakage);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 304:
                    contentStr = "Code 304\nChamber temp. higher than 40°C";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_leakage);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 400:
                    contentStr = "Code 400\nLow water in the tank";
                    runOnUiThread(new Runnable() {
                    public void run()
                    {
                        NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                        dialog.setContentImageRes(R.drawable.error_msg_icon_low_water_level);
                        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                        dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                        dialog.setAnimationEnable(true);
                        dialog.setTitleText("Notification");
                        dialog.setContentText(contentStr);
                        dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                    @Override
                                    public void onClick(NoticeDialog dialog) {
                                        char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                        sendToService(data);
                                        dialog.dismiss();
                                    }
                                }).show();
                        }
                    });
                    break;
                case 401:
                    contentStr = "Code 401\nLow water in the SG";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_low_water_level);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 403:
                    contentStr = "Code 403\nWaste water tank full water level";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_low_water_level);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 404:
                    contentStr = "Code 404\nFull water in the tank";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_fulltank);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 420:
                    contentStr = "Code 420\nHigh conductivity";
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            //HttpClient client = new DefaultHttpClient();

                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_notification");
                            try {
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                                params.add(new BasicNameValuePair("name","Prime "+GlobalClass.getInstance().MachineModel));
                                params.add(new BasicNameValuePair("title","Abnormal Water Supply Filter"));
                                params.add(new BasicNameValuePair("description","The quality of the sterilized water is not good, please replace the filter or contact the service technician to help replace it"));
                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                                HttpResponse response = new DefaultHttpClient().execute(myPost);
                            } catch (Exception e) {
                                String ee = e.toString();
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_low_water_level);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
                case 600:
                    contentStr = "Code 600\nDoor open";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_unlock);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x04,0x53,Error_char[0],Error_char[1]};
                                    sendToService(data);
                                    dialog.dismiss();
                                }
                            }).show();
                        }
                    });
                    break;
            }
        }
    };

    private StorageChooser.Builder builder = new StorageChooser.Builder();
    private StorageChooser chooser;

    private static String URI = "MODEL";


    String[] homeSettingName = {
            "Unwrapped 121",
            "Unwrapped 134",
            "Wrapped 121",
            "Wrapped 134",
            "Instrument 121",
            "Instrument 134",
            "Prion",
            "Flash",
            "Dry",
            "Customization",
            "Leakage test",
            "Helix test",
            "B&D test"
    };

    String[] homeSettingName_30L = {
            "Liquid 1",
            "Liquid 2",
            "Solid 1",
            "Solid 2",
            "Agar",
            "Dissolution",
            "Dry Only",
            "Waste",
            "User 1",
            "User 2",
            "Leakage test"
    };

    Integer[] homeImageId = {
            R.drawable.home_icon_unwrapped_normal,
            R.drawable.home_icon_unwrapped_normal,
            R.drawable.home_icon_wrapped_normal,
            R.drawable.home_icon_wrapped_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_prion_normal,
            R.drawable.home_icon_flash_normal,
            R.drawable.home_icon_dry_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_leakage_test_normal,
            R.drawable.home_icon_helix_test_normal,
            R.drawable.home_icon_b_d_test_normal
    };

    Integer[] homeImageId_30L = {
            R.drawable.home_icon_liquid_normal,
            R.drawable.home_icon_liquid_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_agar_normal,
            R.drawable.home_icon_dissollution_normal,
            R.drawable.home_icon_dry_normal,
            R.drawable.home_icon_waste_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_leakage_test_normal
    };


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0xf0) {
                    char[] ch10 = new char[10];
                    System.arraycopy(recv, 2, ch10, 0, 10);
                    GlobalClass.getInstance().CB_Model = CharUtils.charToString(ch10);
                    char[] ch8 = new char[8];
                    System.arraycopy(recv, 12, ch8, 0, 8);
                    GlobalClass.getInstance().CB_Firmware = CharUtils.charToString(ch8);
                    char[] f3_data = new char[8];
                    f3_data[0] = 0x07;
                    f3_data[1] = 0xf3;
                    f3_data[2] = Constants.instance().fetchValueInt(AutoAddWaterKey) > 0?(char)1:0;
                    f3_data[3] = Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0?(char)1:0;
                    f3_data[4] = Constants.instance().fetchValueInt(ExhaustTempKey) > 0?(char)1:0;
                    f3_data[5] = Constants.instance().fetchValueInt(VacuumpumpKey) > 0?(char)1:0;
                    f3_data[6] = Constants.instance().fetchValueInt(PipeheaterKey) > 0?(char)1:0;
                    f3_data[7] = Constants.instance().fetchValueInt(BuzzerVolumeKey) > 0?(char)1:0;

                    sendToService(f3_data);
                }else if (recv[1] == 0xf4) {
                    char[] ch14 = new char[14];
                    System.arraycopy(recv, 2, ch14, 0, 14);
                    String sn =CharUtils.charToString(ch14).trim();
                    GlobalClass.getInstance().CB_SN = sn;
                }else if (recv[1] == 0x73) {
                    char[] ch = new char[recv.length];
                    System.arraycopy(recv, 0, ch, 0, recv.length);
                }else if (recv[1] == 0x60){
                    if (recv[2] == 0x02) {  // 移除開門畫面
                        GlobalClass.getInstance().isStartSterillizationActivity = false;
                        if ((Constants.instance().fetchValueInt(postScanControl))==1) {
                            closeAllActivities();
                            Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
                            broadcast.putExtra("Error",0);
                            sendBroadcast(broadcast);
                            Intent sleepbroadcast = new Intent(HMI_SLEEP_BROADCAST);
                            sendBroadcast(sleepbroadcast);
                        }else {
                            closeAllandPrintActivities();
                            Intent sleepbroadcast = new Intent(HMI_SLEEP_BROADCAST);
                            sendBroadcast(sleepbroadcast);
                        }
                    }else if (recv[2] == 0x03) { // 顯示開門畫面
                        sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
                        intent = new Intent(MainActivity.this, DoorOpenActivity.class);
                        startActivity(intent);
                    }else if (recv[2] == 0x04) { // 顯示排壓中請等待畫面
                        sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
                        intent = new Intent(MainActivity.this, ExhaustWaitDoorOpenActivity.class);
                        startActivity(intent);
                    }
                }

            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    protected void closeAllandPrintActivities(){
        if (GlobalClass.getInstance().isPrintSignature) {
            String printData = "\n\n\r\n" +
                    "Signature:______________________\n\n\n\r\n";
            sendToServicePrinter(printData.toCharArray(),printData);
            saveDtlToCache(printData.toCharArray());
        }
        GlobalClass.getInstance().MachineState = IDLE;
        GlobalClass.getInstance().isPrintSignature = false;
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }



    private boolean isTopActivity(String activityName){
        ActivityManager manager = (ActivityManager) mActivity.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
        String cmpNameTemp = null;
        if(runningTaskInfos != null){
            cmpNameTemp = runningTaskInfos.get(0).topActivity.toString();
        }
        if(cmpNameTemp == null){
            return false;
        }
        return cmpNameTemp.equals(activityName);
    }

    private boolean isMainActivityAlive(Context context, String activityName){
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (info.topActivity.toString().equals(activityName) || info.baseActivity.toString().equals(activityName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        GlobalClass.getInstance().MachineModel = "60L";//Settings.System.getString(this.getContentResolver(),URI);//"30B";
        GlobalClass.getInstance().CB_SN = "210830001-001";
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        if (!GlobalClass.getInstance().isSerialServiceRun) {
            Intent Serialintent = new Intent(MainActivity.this, SerialPortService.class);
            startService(Serialintent);
        }
        float currentCount = 0;
        float totalCount = 0;
//        Constants.instance().storeValueInt(SleepTimeKey,2);

        mActivity = this;
//        Toast.makeText(this, Settings.System.getString(this.getContentResolver(),
//                URI), Toast.LENGTH_LONG).show();

        if (Settings.System.getInt(this.getContentResolver(),NextExhaustFilterKey,0)==0)
            Settings.System.putInt(this.getContentResolver(),NextExhaustFilterKey, 250);
        if (Settings.System.getInt(this.getContentResolver(),NextAirFilterKey,0)==0)
            Settings.System.putInt(this.getContentResolver(),NextAirFilterKey, 500);
        if (Settings.System.getInt(this.getContentResolver(),NextGasketServiceKey,0)==0)
            Settings.System.putInt(this.getContentResolver(),NextGasketServiceKey, 2000);
        if (Settings.System.getInt(this.getContentResolver(),NextServiceCycleKey,0)==0)
            Settings.System.putInt(this.getContentResolver(),NextServiceCycleKey, 3000);


//        String str = "--------------------------------\r\n" +
//                "Model:null\r\n" +
//                "Ver.\r\n" +
//                "null_null\r\n" +
//                "SN:null\r\n" +
//                "--------------------------------\r\n" +
//                "Program:\r\n" +
//                "Wrapped 121\r\n" +
//                "Pre-Vacuum: 4\r\n" +
//                "Ster. Temp: 116 'C\r\n" +
//                "Ster. Time:  15 m 30 s\r\n" +
//                "Dry   Time:  15 m  0 s\r\n" +
//                "--------------------------------\r\n" +
//                "Date:Jul.02.2020\r\n" +
//                "Time:16:35:49\r\n" +
//                "Cycle Counter:0000016\r\n" +
//                "--------------------------------\r\n" +
//                "Step       Time    Temp.  Pres.\r\n" +
//                "           mmm:ss  'C     bar\r\n" +
//                "Start     \u0000 000:00  75.8  -0.009\r\n" +
//                "Pheat     \u0000 000:10  75.8  -0.009\r\n" +
//                "PV1       \u0000 000:20  75.8  -0.009\r\n" +
//                "H1        \u0000 000:30  75.8  -0.008\r\n" +
//                "EX        \u0000 000:35  75.8  -0.008\r\n" +
//                "PV2       \u0000 000:40  75.8  -0.009\r\n" +
//                "H2        \u0000 000:50  75.8  -0.009\r\n" +
//                "PV3       \u0000 001:01  75.8  -0.008\r\n" +
//                "H3        \u0000 001:10  75.8  -0.008\r\n" +
//                "EX        \u0000 001:16  75.8  -0.009\r\n" +
//                "PV4       \u0000 001:21  75.8  -0.008\r\n" +
//                "H4        \u0000 001:31  75.8  -0.009\r\n" +
//                "S000-00   \u0000 001:31  75.8  -0.009\r\n" +
//                "S002-00   \u0000 003:31  75.8  -0.009\r\n" +
//                "S002-29   \u0000 004:01  75.8  -0.009\r\n" +
//                "EX        \u0000 004:06  75.8  -0.009\r\n" +
//                "D1        \u0000 004:11  75.8  -0.008\r\n" +
//                "Program complete\r\n" +
//                "--------------------------------\r\n" +
//                "Ster. Temp: 75.8 - 75.8'C \r\n" +
//                "Ster. Pres: -0.011 - -0.008 bar\r\n" +
//                "Ster. Time:  15:30\r\n" +
//                "Total Time:  04min 17sec\r\n" +
//                "--------------------------------\r\n";
//        FileUtils.saveStdRecordFile("test.std",str.getBytes());


        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        GlobalClass.getInstance().MachineState = IDLE;
        mDatabaseHelper=new DatabaseHelper(mActivity);
        mDatabaseHelper.getWritableDatabase();
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        gridView = (RecyclerView) findViewById(R.id.grid);


        ImageButton mInfoButton = (ImageButton)findViewById(R.id.main_info_button);
        mInfoButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
                return false;
            }
        });

        DrawMeImageButton mMenuButton = (DrawMeImageButton)findViewById(R.id.main_menu_button);
        mMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        DrawMeImageButton mSleepButton = (DrawMeImageButton)findViewById(R.id.main_sleep_button);
        mSleepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ImageSpan mImageSpan = new ImageSpan(MainActivity.this, R.drawable.pop_icon_unlock);
//                String contentStr = "Please confirm that the Sterilization device has been placed.\n(Press@to unlock the door.)";
//                SpannableString mSpannableString = new SpannableString(contentStr);
//                mSpannableString.setSpan(mImageSpan, contentStr.indexOf('@') , contentStr.indexOf('@')+1 , 0);
//                new Notice2Dialog(mActivity)
//                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
//                        .setAnimationEnable(true)
//                        .setTitleText("Notification")
//                        .setContentText(mSpannableString,Gravity.LEFT)
//                        .setConfirmListener("Confirm", new Notice2Dialog.OnConfirmListener() {
//                            @Override
//                            public void onClick(Notice2Dialog dialog) {
//                                dialog.dismiss();
//                            }
//                        }).show();




                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this,SleepModeActivity.class);
                    startActivity(intent);

            }
        });

//        listView.setHasFixedSize(true);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "ListView"
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(horizontalManager);
//        listViewAdapter = new ListViewAdapter(this, corporations);
//        listView.setAdapter(listViewAdapter);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridView.setLayoutManager(layoutManager);
        setMainMenuElement();
        gridViewAdapter = new GridViewAdapter(this, recyclerViewButton, recyclerViewButton_press);
        gridViewAdapter.setOnItemClickListener(this);
        gridView.setAdapter(gridViewAdapter);
        gridView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gridView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTouch(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is touch!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        final IntentFilter myFileFilter = new IntentFilter("com.sturdy.filebrowser.FILE_SELECTED_BROADCAST");
        registerReceiver(mFileChooserReceiver, myFileFilter);
        final IntentFilter myErrorFilter = new IntentFilter(HMI_ERROR_BROADCAST);
        registerReceiver(mShowErrorDialog, myErrorFilter);
        final IntentFilter mySleepFilter = new IntentFilter(HMI_SLEEP_BROADCAST);
        registerReceiver(mRestartSleepReceiver, mySleepFilter);
        final IntentFilter myRemoveSleepFilter = new IntentFilter(HMI_RMSLEEP_BROADCAST);
        registerReceiver(mRemoveSleepReceiver, myRemoveSleepFilter);
        createDocuments();

        int sleepTime = Constants.instance().fetchValueInt(SleepTimeKey,0)*1000*60;
        if (sleepTime>0)
            handler.postDelayed(mSleepTimeRunnable, sleepTime);

        if (Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0)>Settings.System.getInt(this.getContentResolver(),NextExhaustFilterKey,0)) {
            Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
            broadcast.putExtra("Error",11);
            sendBroadcast(broadcast);
        }
        if (Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0)>Settings.System.getInt(this.getContentResolver(),NextAirFilterKey,0)) {
            Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
            broadcast.putExtra("Error",12);
            sendBroadcast(broadcast);
        }
        if (Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0)>Settings.System.getInt(this.getContentResolver(),NextGasketServiceKey,0)) {
            Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
            broadcast.putExtra("Error",13);
            sendBroadcast(broadcast);
        }
        if (Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0)>Settings.System.getInt(this.getContentResolver(),NextServiceCycleKey,0)) {
            Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
            broadcast.putExtra("Error",10);
            sendBroadcast(broadcast);
        }


        new Thread(new Runnable(){
            @Override
            public void run() {
                //HttpClient client = new DefaultHttpClient();

                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
                try {
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = "0";//tsLong.toString();
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("programName","Standby"));
                    params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
                    params.add(new BasicNameValuePair("programEndtime",ts));
                    params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
                    params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
                    myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
                    HttpResponse response = new DefaultHttpClient().execute(myPost);
                    String er = response.toString();
                } catch (Exception e) {
                    String ee = e.toString();
                    e.printStackTrace();
                }
            }
        }).start();


        if ((GlobalClass.getInstance().MachineModel.equals("30B"))||(GlobalClass.getInstance().MachineModel.equals("60B"))) {
            Constants.instance().storeValueInt(VacuumpumpKey,1);
        }

        HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("programName","Standby"));
            params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
            params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
            params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
            params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
            myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
            HttpResponse response = new DefaultHttpClient().execute(myPost);
            String er = response.toString();
        } catch (Exception e) {
            String ee = e.toString();
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        setMainMenuElement();
        if (gridView!=null && gridViewAdapter!=null){
            gridViewAdapter.resetAdapter(recyclerViewButton, recyclerViewButton_press);
            gridViewAdapter.notifyDataSetChanged();
        }
        TextView mCycleCounterTV = (TextView) findViewById(R.id.menu_work_count_view);
        int iCycleCounter = Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0);
        int iNextService = Settings.System.getInt(this.getContentResolver(),NextServiceCycleKey,0);
        settext = String.format("%06d/%06d",iCycleCounter,iNextService);
        mCycleCounterTV.setText(settext);
        GlobalClass.getInstance().isFloatingTemp = false;
        GlobalClass.getInstance().isPrintSignature = false;
    }


    void createDocuments () {
        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        boolean isPresent = true;
        if (!docsFolder.exists()) {
            isPresent = docsFolder.mkdir();
        }
    }

    @Override
    public void onItemClick(int position) {
        Constants.instance().storeValueString(saveSelectMenuNameKey, settingElementList.get(position).getName());
        Constants.instance().storeValueInt(saveSelectMenuIdKey, settingElementList.get(position).getId());

        DatabaseHelper helper = new DatabaseHelper(mActivity);
        helper.getCustomerDao().deleteAllBarCode();

        if ((Constants.instance().fetchValueInt(preIDControl))==1) {
            Constants.instance().storeValueString(userLoginCheckPoint, "PreID");
            this.startActivity(new Intent(MainActivity.this, UserLoginActivity.class));
        }else{


            Intent intent = new Intent();
            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                switch (settingElementList.get(position).getId()) {
                    case 0:
                        intent.setClass(this, Liquid1EditActivity.class);
                        intent.putExtra("name", "Liquid 1");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 1:
                        intent.setClass(this, Liquid2EditActivity.class);
                        intent.putExtra("name", "Liquid 2");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 2:
                        intent.setClass(this, Solid1EditActivity.class);
                        intent.putExtra("name", "Solid 1");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 3:
                        intent.setClass(this, Solid2EditActivity.class);
                        intent.putExtra("name", "Solid 2");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 4:
                        intent.setClass(this, AgarEditActivity.class);
                        intent.putExtra("name", "Agar");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 5:
                        intent.setClass(this, DissolutionEditActivity.class);
                        intent.putExtra("name", "Dissolution");
                        intent.putExtra("temp", "60");
                        GlobalClass.getInstance().Steri_Temp = 60;
                        break;
                    case 6:
                        intent.setClass(this, DryOnlyEditActivity.class);
                        intent.putExtra("name", "Dry Only");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 7:
                        intent.setClass(this, WasteEditActivity.class);
                        intent.putExtra("name", "Waste");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 8:
                        intent.setClass(this, User1EditActivity.class);
                        intent.putExtra("name", "User 1");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 9:
                        intent.setClass(this, User2EditActivity.class);
                        intent.putExtra("name", "User 2");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 10:
                        intent.setClass(this, LeakageTestEditActivity.class);
                        intent.putExtra("name", "Leakage test");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 11:
                        intent.setClass(this, FlashEditActivity.class);
                        intent.putExtra("name", "Flash");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 12:
                        intent.setClass(this, UnwrappedEditActivity.class);
                        intent.putExtra("name", "B&D test");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 13:
                        intent.setClass(this, LatexEditActivity.class);
                        intent.putExtra("name", "Latex");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                }
            }else{
                switch (settingElementList.get(position).getId()) {
                    case 0:
                        intent.setClass(this, UnwrappedEditActivity.class);
                        intent.putExtra("name", "Unwrapped 121");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 1:
                        intent.setClass(this, UnwrappedEditActivity.class);
                        intent.putExtra("name", "Unwrapped 134");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 2:
                        intent.setClass(this, WrappedEditActivity.class);
                        intent.putExtra("name", "Wrapped 121");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 3:
                        intent.setClass(this, WrappedEditActivity.class);
                        intent.putExtra("name", "Wrapped 134");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 4:
                        intent.setClass(this, InstrumentEditActivity.class);
                        intent.putExtra("name", "Instrument 121");
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 5:
                        intent.setClass(this, InstrumentEditActivity.class);
                        intent.putExtra("name", "Instrument 134");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 6:
                        intent.setClass(this, PrionEditActivity.class);
                        intent.putExtra("name", "Prion");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 7:
                        intent.setClass(this, FlashEditActivity.class);
                        intent.putExtra("name", "Flash");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 8:
                        intent.setClass(this, DryEditActivity.class);
                        intent.putExtra("name", "Dry");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 9:
                        intent.setClass(this, CustomizationEditActivity.class);
                        intent.putExtra("name", "Customization");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 10:
                        intent.setClass(this, LeakageTestEditActivity.class);
                        intent.putExtra("name", "Leakage test");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 11:
                        intent.setClass(this, HelixTestEditActivity.class);
                        intent.putExtra("name", "Helix test");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 12:
                        intent.setClass(this, UnwrappedEditActivity.class);
                        intent.putExtra("name", "B&D test");
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                }
            }
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        this.overridePendingTransition(0, 0);

    }



    private void setMainMenuElement() {
        recyclerViewButton = new ArrayList<>();
        recyclerViewButton_press = new ArrayList<>();
        // 取出主畫面的設定資料
        String json = (Constants.instance().fetchValueString(saveMenuSettingKey));
        recyclerViewButton.clear();
        recyclerViewButton_press.clear();
        if (json != null)
        {
            Gson gson = new Gson();
            Type type = new TypeToken<List<MainMenuSettingElement>>(){}.getType();
            settingElementList = new ArrayList<MainMenuSettingElement>();
            settingElementList = gson.fromJson(json, type);
            if (settingElementList.size()>0) {
                for(int i = 0; i < settingElementList.size(); i++)
                {
                    if (settingElementList.get(i).getName().equals("Leakage test")) {
                        if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                            recyclerViewButton.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
                            recyclerViewButton_press.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
                        }
                    }else{
                        recyclerViewButton.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
                        recyclerViewButton_press.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
                    }
//                Log.d(TAG, alterSamples.get(i).getName()+":" + alterSamples.get(i).getX() + "," + alterSamples.get(i).getY());
                }
            }
            return;

        }
        settingElementList = new ArrayList<MainMenuSettingElement>();
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            settingElementList.add(new MainMenuSettingElement(0,homeImageId_30L[0],homeSettingName_30L[0]));
            settingElementList.add(new MainMenuSettingElement(2,homeImageId_30L[2],homeSettingName_30L[2]));
            settingElementList.add(new MainMenuSettingElement(4,homeImageId_30L[4],homeSettingName_30L[4]));
            settingElementList.add(new MainMenuSettingElement(5,homeImageId_30L[5],homeSettingName_30L[5]));
            settingElementList.add(new MainMenuSettingElement(7,homeImageId_30L[7],homeSettingName_30L[7]));
            settingElementList.add(new MainMenuSettingElement(8,homeImageId_30L[8],homeSettingName_30L[8]));
        }else{
            settingElementList.add(new MainMenuSettingElement(2,homeImageId[2],homeSettingName[2]));
            settingElementList.add(new MainMenuSettingElement(3,homeImageId[3],homeSettingName[3]));
            settingElementList.add(new MainMenuSettingElement(6,homeImageId[6],homeSettingName[6]));
            settingElementList.add(new MainMenuSettingElement(7,homeImageId[7],homeSettingName[7]));
            settingElementList.add(new MainMenuSettingElement(10,homeImageId[10],homeSettingName[10]));
            settingElementList.add(new MainMenuSettingElement(11,homeImageId[11],homeSettingName[11]));
        }

        for(int i = 0; i < settingElementList.size(); i++)
        {
            recyclerViewButton.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
            recyclerViewButton_press.add(new RecyclerViewItem(settingElementList.get(i).getIcon(), settingElementList.get(i).getName()));
        }
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }





}
