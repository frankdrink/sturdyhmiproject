package com.sturdy.chart.listener;

import com.sturdy.chart.data.ColumnData;


public interface OnClickColumnListener<C extends ColumnData> {

   void onClickColumn(C c,int position);
}
