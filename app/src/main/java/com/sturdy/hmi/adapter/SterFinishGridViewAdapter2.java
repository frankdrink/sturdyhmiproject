package com.sturdy.hmi.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.sturdy.hmi.R;
import com.sturdy.hmi.RecyclerViewItem;

public class SterFinishGridViewAdapter2 extends RecyclerView.Adapter<SterFinishGridViewAdapter2.ViewHolder> {
    private List<RecyclerViewItem> items,items_press;
    private Activity activity;

    public SterFinishGridViewAdapter2(Activity activity, List<RecyclerViewItem> items, List<RecyclerViewItem> items_press) {
        this.activity = activity;
        this.items = items;
        this.items_press = items_press;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.sterilizationfinish_grid2, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SterFinishGridViewAdapter2.ViewHolder viewHolder,final int position) {
        viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());

        viewHolder.imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    viewHolder.imageView.setBackgroundResource(items_press.get(position).getDrawableId());
//                    onItemClickListener.onItemClick(position);
//
////                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_pressed));
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());
//
////                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_normal));
//                }
                return false;
            }
        });




        viewHolder.textView1.setText(items.get(position).getName());
        viewHolder.textView2.setText(items.get(position).getValue());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageButton imageView;
        private TextView textView1,textView2;

        public ViewHolder(View view) {
            super(view);
            textView1 = (TextView)view.findViewById(R.id.text1);
            textView2 = (TextView)view.findViewById(R.id.text2);
            imageView = (ImageButton) view.findViewById(R.id.image);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
