package com.sturdy.wifilibrary.listener;


public interface OnWifiEnabledListener {

    /**
     * WIFI開關的回調
     *
     * @param enabled true 可用 false 不可用
     */
    void onWifiEnabled(boolean enabled);
}
