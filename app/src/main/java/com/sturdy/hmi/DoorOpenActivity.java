package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.RecyclerViewDisabler;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import static com.sturdy.hmi.Constants.MACHINE_STATE.IDLE;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class DoorOpenActivity extends BaseToolBarActivity{
    Handler handler;
    Runnable run;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_dooropen);
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        new Handler().postDelayed(new Runnable(){
            public void run() {


//                Intent intent = new Intent(DoorOpenActivity.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                overridePendingTransition(0, 0);
            }
        }, 5000);
    }


    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
