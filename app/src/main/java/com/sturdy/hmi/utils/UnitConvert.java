package com.sturdy.hmi.utils;

import com.sturdy.hmi.Constants;

import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.saveTempFormat;

/**
 * Created by franklin on 2019/11/28.
 */

public class UnitConvert {
    public static float fTempConvert(float C) {
        if (C==0) return 0;
        float temp = C / 10;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
            temp = (float)C / 10.0f;
        }else{
            temp = (float)C / 10.0f*(9.0f/5.0f) + 32.0f;
        }
        return temp;
    }

    public static float fPresConvert(float bar) {
        if (bar==0) return 0;
        bar = bar / 1000.0f;
        float pres = 0.0f;
        if (Constants.instance().fetchValueInt(savePresFormat)==0) {
            pres = bar;
        }else if (Constants.instance().fetchValueInt(savePresFormat)==1) {
            pres = bar * 100;
        }else if (Constants.instance().fetchValueInt(savePresFormat)==2) {
            pres = bar * 0.1f;
        }else if (Constants.instance().fetchValueInt(savePresFormat)==3) {
            pres = bar * 14.5f;
        }else if (Constants.instance().fetchValueInt(savePresFormat)==4) {
            pres = bar * 1.0197f;
        }
        return pres;
    }

    public static int TempConvert(int C) {
        int temp;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
            temp = (int)C;
        }else{
            temp = (int)((float)C*((float)9.0f/(float)5.0f) + (float)32.0f);
        }
        return temp;
    }

    public static int TempConvertF(int F) {
        int tempC;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
            tempC = F;
        }else{
            float temp = ((float)F-(float)32.0f)*((float)5.0f/(float)9.0f);
            temp = (float)(Math.round(temp*10))/10;
            tempC = (int)temp;
        }
        return tempC;
    }


    public static String sTempConvert(int unit,float C,boolean showFloat) {
        String str;
        float temp;
        if (showFloat) {
            if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                temp = (float)C/(float)10;
                if (unit==0){
                    str = String.format("%.1f",(float)temp);
                }else if (unit==1) {
                    str = String.format("%.1f°C",(float)temp);
                }else {
                    str = String.format("%.1f'C",(float)temp);
                }
            }else{
                temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                if (unit==0) {
                    str = String.format("%.1f",(float)temp);
                }else if (unit==1) {
                    str = String.format("%.1f°F",(float)temp);
                }else {
                    str = String.format("%.1f'F",(float)temp);
                }
            }

        }else{
            if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                temp = (float)C/(float)10;
                if (unit==0) {
                    str = String.format("%d",Math.round(temp));
                }else if (unit==1) {
                    str = String.format("%d °C",Math.round(temp));
                }else {
                    str = String.format("%d 'C",Math.round(temp));
                }
            }else{
                temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                if (unit==0) {
                    str = String.format("%d",Math.round(temp));
                }else if (unit==1) {
                    str = String.format("%d °F",Math.round(temp));
                }else {
                    str = String.format("%d 'F",Math.round(temp));
                }
            }
        }
        return str;
    }

    public static String sTempConvert(int unit,float C,int floatCount) {
        String str="";
        float temp;
        switch (floatCount) {
            case 1:
                if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                    temp = (float)C/(float)10;
                    temp = (float)(Math.round(temp*100))/100;
                    if (unit==0) {
                        str = String.format("%.1f",(float)temp);
                    }else if (unit==1)
                        str = String.format("%.1f°C",(float)temp);
                    else
                        str = String.format("%.1f'C",(float)temp);
                }else{
                    temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                    if (unit==0)
                        str = String.format("%.1f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.1f°F",(float)temp);
                    else
                        str = String.format("%.1f'F",(float)temp);
                }
                break;
            case 2:
                if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                    temp = (float)C/(float)10;
                    if (unit==0)
                        str = String.format("%.2f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.2f°C",(float)temp);
                    else
                        str = String.format("%.2f'C",(float)temp);
                }else{
                    temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                    if (unit==0)
                        str = String.format("%.2f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.2f°F",(float)temp);
                    else
                        str = String.format("%.2f'F",(float)temp);
                }
                break;
            case 3:
                if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                    temp = (float)C/(float)10;
                    if (unit==0)
                        str = String.format("%.3f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.3f°C",(float)temp);
                    else
                        str = String.format("%.3f'C",(float)temp);
                }else{
                    temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                    if (unit==0)
                        str = String.format("%.3f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.3f°F",(float)temp);
                    else
                        str = String.format("%.3f'F",(float)temp);
                }
                break;
            case 4:
                if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
                    temp = (float)C/(float)10;
                    if (unit==0)
                        str = String.format("%.4f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.4f°C",(float)temp);
                    else
                        str = String.format("%.4f'C",(float)temp);
                }else{
                    temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
                    if (unit==0)
                        str = String.format("%.4f",(float)temp);
                    else if (unit==1)
                        str = String.format("%.4f°F",(float)temp);
                    else
                        str = String.format("%.4f'F",(float)temp);
                }
                break;
        }


        return str;
    }

    public static String sTempConvert(int unit,float C) {
        String str="";
        float temp;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
            temp = (float)C/(float)10;
            if (unit==0) {
                str = String.format("%d",Math.round(temp));
            }else if (unit==1) {
                str = String.format("%d °C",Math.round(temp));
            }else if (unit==2) {    // for Printer
                str = String.format("%d 'C",Math.round(temp));
            }
        }else{
            temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
            temp = (float)(Math.round(temp*10))/10;
            if (unit==0) {
                str = String.format("%d",Math.round(temp));
            }else if (unit==1) {
                str = String.format("%d °F",Math.round(temp));
            }else if (unit==2) {
                str = String.format("%d 'F",Math.round(temp));
            }
        }
        return str;
    }

    public static String sTempConvertP(boolean unit,float C) {
        String str;
        float temp;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0) {
            temp = (float)C/(float)10;
            if (unit)
                str = String.format("%.1f'C",temp);
            else
                str = String.format("%.1f",temp);
        }else{
            temp = (float)C/10.0f*9.0f/5.0f + 32.0f;
            if (unit)
                str = String.format("%.1f'F",temp);
            else
                str = String.format("%.1f",temp);
        }
        return str;
    }


    public static String sPresConvert(int unit,float bar) {
        bar = bar / 1000.0f;
        String str="";
        float pres;
        if (Constants.instance().fetchValueInt(savePresFormat)==0) {
            pres = bar;
            if (unit==1)
                str = String.format("%.3f bar",(float)pres);
            else
                str = String.format("%.3f",(float)pres);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==1) {
            pres = bar * 100;
            if (unit==1)
                str = String.format("%.1f kPa",(float)pres);
            else
                str = String.format("%.1f",(float)pres);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==2) {
            pres = bar * 0.1f;
            if (unit==1)
                str = String.format("%.4f Mpa",(float)pres);
            else
                str = String.format("%.4f",(float)pres);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==3) {
            pres = bar * 14.5f;
            if (unit==1)
                str = String.format("%.2f psi",(float)pres);
            else
                str = String.format("%.2f",(float)pres);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==4) {
            pres = bar * 1.0197f;
            if (unit==1)
                str = String.format("%.3f kgf/cm2",(float)pres);
            else
                str = String.format("%.3f",(float)pres);
        }
        return str;
    }

    public static final String formatTimeH(long millis) {
        long secs = millis;
        return String.format("%02d h", secs / 3600);
    }

    public static final String formatTimeM(long millis) {
        long secs = millis;
        return String.format("%02d m", (secs % 3600) / 60);
    }

    public static final String formatTimeHM(long millis) {
        long secs = millis;
        return String.format("%02dh %02dm", secs / 3600, (secs % 3600) / 60);
    }

    public static final String formatTimeHMS(long millis) {
        long secs = millis;
        return String.format("%02d:%02d:%02d", secs / 3600, (secs % 3600) / 60, secs % 60);
    }
}
