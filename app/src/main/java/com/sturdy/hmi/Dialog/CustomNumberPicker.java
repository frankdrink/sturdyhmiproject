package com.sturdy.hmi.Dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;

/**
 * Created by franklin on 2019/10/30.
 */
public class CustomNumberPicker extends NumberPicker
{
    Activity mContext;
    public CustomNumberPicker(Context context)
    {
        super(context);
        mContext = (Activity)context;
    }

    public CustomNumberPicker(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public void addView(View child)
    {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params)
    {
        super.addView(child, params);
        updateView(child);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params)
    {
        super.addView(child, index, params);
        updateView(child);
    }

    private void updateView(View child)
    {
        if (child instanceof EditText)
        {
            EditText MyEditor = ((EditText) child);
            MyEditor.setTextSize(46);

            //            ((EditText) view).setTextColor(Color.parseColor("#333333"));

        }
    }

    public static void disableSoftInputFromAppearing(EditText editText) {
        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }
    }


}

