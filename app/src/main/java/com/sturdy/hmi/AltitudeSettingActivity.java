package com.sturdy.hmi;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.adapter.AltitudeRadioListAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.UnitRadioListAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.utils.CharUtils;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveAltitudeKey;
import static com.sturdy.hmi.Constants.saveAltitudeModeKey;
import static com.sturdy.hmi.Constants.saveAltitudeUnitKey;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class AltitudeSettingActivity extends BaseToolBarActivity implements NumberDialog.OnNumberDialogDoneListener,AltitudeRadioListAdapter.UpdateMainClass{
    private Activity mActivity;
    AltitudeRadioListAdapter adapter1;
    AltitudeRadioListAdapter adapter2;
    int saveTempFormatBase;
    int savePresFormatBase;
    List<String> data1;
    List<String> data2;

    List<MainMenuSettingElement> settingElementList;
    Handler handler;

    private String timeStr;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0xf5) {
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 10, ch2, 0, 2);
                    int ATBar = CharUtils.charArrayToInt(ch2);
                    double P = (((double)ATBar/(double)1013.25));
                    double pow = Math.pow(P,(double)0.190258751902588);
                    double H = 44300*(1-pow);
                    Constants.instance().storeValueInt(saveAltitudeKey,(int)H);
                    adapter1.notifyDataSetChanged();
//                    char[] ch2 = new char[2];
//                    System.arraycopy(recv, 2, ch2, 0, 2);
//                    int SGTemp = CharUtils.charArrayToInt(ch2);
//                    infoValue[6] = String.format("%.1f℃",(float)SGTemp/(float)10);
//                    System.arraycopy(recv, 4, ch2, 0, 2);
//                    int ChamberTemp = CharUtils.charArrayToInt(ch2);
//                    infoValue[7] = String.format("%.1f℃",(float)ChamberTemp/(float)10);
//                    System.arraycopy(recv, 6, ch2, 0, 2);
//                    int BHTemp = CharUtils.charArrayToInt(ch2);
//                    infoValue[8] = String.format("%.1f℃",(float)BHTemp/(float)10);
//                    System.arraycopy(recv, 8, ch2, 0, 2);
//                    int ChamberBar = CharUtils.charArrayToInt(ch2);
//                    infoValue[9] = String.format("%.3fbar",(float)ChamberBar/(float)1000);
//                    System.arraycopy(recv, 10, ch2, 0, 2);
//                    int ATBar = CharUtils.charArrayToInt(ch2);
//                    infoValue[10] = String.format("%.3fbar",(float)ATBar/(float)1000);
//                    System.arraycopy(recv, 12, ch2, 0, 2);
//                    int FanSpeed = CharUtils.charArrayToInt(ch2);
////                    Random random = new Random();
////                    infoValue[14] = String.format("%d rpm",FanSpeed+random.nextInt(2000));
//                    System.arraycopy(recv, 14, ch2, 0, 2);
//                    int WaterQuality = CharUtils.charArrayToInt(ch2);
//                    infoValue[16] = String.format("%d",WaterQuality);
//                    char WasteWaterLevel = recv[16];
//                    if (GlobalClass.getInstance().CB_CleanWaterLevel==0x00) {
//                        infoValue[17] = "Low";
//                    }else if (GlobalClass.getInstance().CB_CleanWaterLevel==0x01){
//                        infoValue[17] = "Middle";
//                    }else {
//                        infoValue[17] = "High";
//                    }
//                    if (WasteWaterLevel==0x00) {
//                        infoValue[18] = "Low";
//                    }else {
//                        infoValue[18] = "High";
//                    }
//                    tableLayout.removeAllViews();
//                    for (int i=0;i<infoItem.length;i++) {
//                        TableRow row= new TableRow(mActivity);
//                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
//                                TableRow.LayoutParams.WRAP_CONTENT);
//                        row.setLayoutParams(lp);
//                        TextView tv1 = new TextView(mActivity);
//                        tv1.setGravity(Gravity.LEFT);
//                        tv1.setTextSize(32);
//                        tv1.setTypeface(null, Typeface.BOLD);
//                        tv1.setText(infoItem[i]);
//                        TextView tv2 = new TextView(mActivity);
//                        tv2.setGravity(Gravity.RIGHT);
//                        tv2.setTextSize(28);
//                        tv2.setText(infoValue[i]);
//                        row.addView(tv1);
//                        row.addView(tv2);
//                        tableLayout.addView(row,new TableLayout.LayoutParams(
//                                TableLayout.LayoutParams.WRAP_CONTENT,
//                                TableLayout.LayoutParams.WRAP_CONTENT));
//                    }
                }

            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            char[] data = new char[]{0x01, 0xf5};
            sendToService(data);
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Constants.instance().storeValueInt(saveAltitudeKey,1000);
        setContentView(R.layout.activity_datetimesetting);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
        );
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Altitude", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView titleView1 = (TextView) findViewById(R.id.list1_text_view);
        titleView1.setText("Mode");
        TextView titleView2 = (TextView) findViewById(R.id.list2_text_view);
        titleView2.setText("Altitude Unit");

        data1 = new ArrayList<String>();
        data1.add("Auto");
        data1.add("Manual");
        saveTempFormatBase = Constants.instance().fetchValueInt(saveAltitudeModeKey);
        adapter1 = new AltitudeRadioListAdapter(this, data1, "mode", saveTempFormatBase);
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);
        data2 = new ArrayList<String>();
        data2.add("m");
        data2.add("ft");
//        savePresFormatBase = Constants.instance().fetchValueInt(savePresFormat);
        savePresFormatBase = Constants.instance().fetchValueInt(saveAltitudeUnitKey);
        adapter2 = new AltitudeRadioListAdapter(this, data2, "unit", savePresFormatBase);
        ListView lvMain2 = (ListView) findViewById(R.id.list2);
        lvMain2.setAdapter(adapter2);

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        handler = new Handler();
        handler.postAtTime(runnable, 1000);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);

    }

    @Override
    public void updateItemList(final String itemName) {
        if (itemName.equals("editmode")) {
            NumberDialog myDiag =
                    NumberDialog.newInstance(4, 1000);
            myDiag.show(getSupportFragmentManager(), "altitude");
        }else if (itemName.equals("unit0")) {   // m
            Constants.instance().storeValueInt(saveAltitudeUnitKey,0);

            adapter1.notifyDataSetChanged();
        }else if (itemName.equals("unit1")) {   // ft
            // ft =m * 3.2808
            Constants.instance().storeValueInt(saveAltitudeUnitKey,1);
            adapter1.notifyDataSetChanged();

        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {

    }

    @Override
    public void onDone(int value) {
        int iNextService = value;
        Constants.instance().storeValueInt(saveAltitudeKey,value);
        String valueStr = Integer.toString(iNextService);
        data1.set(1,valueStr);
        adapter1.resetAdapter(data1,2);
        adapter1.notifyDataSetChanged();
        char[] data = new char[4];
        char[] ch22 = new char[2];
        data[0] = 0x03;
        data[1] = 0x42;
        ch22 = CharUtils.fromShort((short)value);
        data[2] = ch22[0];
        data[3] = ch22[1];
        sendToService(data);
    }

}
