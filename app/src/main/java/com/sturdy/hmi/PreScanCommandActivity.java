package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.PreScanListAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.ACTION_TO_SERVICE;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;

public class PreScanCommandActivity extends BaseToolBarActivity implements View.OnClickListener {
    private Activity mActivity;
    List<MainMenuSettingElement> settingElementList;
    int PreScanMode;
    TextView maxSelectTextView;
    ListView list;
    List<SwitchButton> switchList;
    List<RadioButton> radioList;

    List<Boolean> listIsChecked;    // 這個用來記錄哪幾個 item 是被打勾的
    String[] commandName = {
            "Prescan 0XD0",
            "全開     0XE7",
            "全關     0XE8",
            "D001    0XD1",
            "D002    0XD2",
            "D003    0XD3",
            "D004    0XD4",
            "D005    0XD5",
            "D006    0XD6",
            "D007    0XD7",
            "D008    0XD8",
            "D009    0XD9",
            "D010    0XDA",
            "D011    0XDB",
            "D012    0XDC",
            "D013    0XDD",
            "D014    0XDE",
            "D015    0XDF",
            "D016    0XE0",
            "D017    0XE1",
            "R001    0XE2",
            "R002    0XE3",
            "R003    0XE4",
            "R004    0XE5",
            "R005    0XE6"
    } ;

    char[] cmdData = new char[]{0xd0, 0xe7, 0xe8, 0xd1, 0xd2 ,0xd3, 0xd4 ,0xd5, 0xd6 ,0xd7, 0xd8 ,0xd9, 0xda ,0xdb, 0xdc ,0xdd, 0xde ,0xdf, 0xe0 ,0xe1, 0xe2 ,0xe3, 0xe4 ,0xe5, 0xe6};


    String[] deviceName = {
            "三色燈",
            "印表機"
    } ;
    Integer[] imageId = {
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_prion_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.steri_list_icon_dry_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.fuction_test_list_icon_leakage_test_normal,
            R.drawable.fuction_test_list_icon_helix_test_normal,
            R.drawable.fuction_test_list_icon_b_d_test_normal
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_prescan_commnad);
        Intent intent = this.getIntent();
        PreScanMode = intent.getIntExtra("prescanmode",0);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("PreScan", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        populateList();

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
    }

    private void populateList(){
//        LinearLayout llgroup = (LinearLayout) findViewById(R.id.linearGroup);
        LinearLayout radiogroup = (LinearLayout) findViewById(R.id.radiogroup);
//        radiogroup.setOrientation(LinearLayout.HORIZONTAL);
        // layout params to use when adding each radio button

        radioList = new ArrayList<>();
        switchList = new ArrayList<>();
        if (PreScanMode==0) {
            for (int i = 0; i < commandName.length; i++){
                LinearLayout linearLayout = new LinearLayout(this);
                LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL
                );
                RadioButton newRadioButton = new RadioButton(this);
                LinearLayout.LayoutParams radioParams = new RadioGroup.LayoutParams(
                        240,
                        RadioGroup.LayoutParams.WRAP_CONTENT);
                radioParams.setMargins(50, 0, 50, 0);
                newRadioButton.setTag(i);
                String label = String.format(" %s",commandName[i]);
                newRadioButton.setTextSize(42);
                newRadioButton.setText(label);
                newRadioButton.setOnClickListener(this);
                newRadioButton.setId(i);
                linearLayout.addView(newRadioButton,radioParams);

                SwitchButton switchButton = new SwitchButton(this);
                switchButton.dontTouch = false;
                LinearLayout.LayoutParams btnLayoutParams = new LinearLayout.LayoutParams(
                        80,
                        40);
                btnLayoutParams.setMargins(0, 0, 0, 0);
                switchList.add(switchButton);
                radioList.add(newRadioButton);
                linearLayout.addView(switchButton,btnLayoutParams);
                radiogroup.addView(linearLayout);
            }
        }else {
            for (int i = 0; i < deviceName.length; i++){
                LinearLayout linearLayout = new LinearLayout(this);
                LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL
                );
                RadioButton newRadioButton = new RadioButton(this);
                LinearLayout.LayoutParams radioParams = new RadioGroup.LayoutParams(
                        220,
                        RadioGroup.LayoutParams.WRAP_CONTENT);
                radioParams.setMargins(50, 0, 50, 0);
                newRadioButton.setTag(i);
                String label = String.format(" %s",deviceName[i]);
                newRadioButton.setTextSize(42);
                newRadioButton.setText(label);
                newRadioButton.setOnClickListener(this);
                newRadioButton.setId(i);
                linearLayout.addView(newRadioButton,radioParams);

                SwitchButton switchButton = new SwitchButton(this);
                switchButton.dontTouch = false;
                LinearLayout.LayoutParams btnLayoutParams = new LinearLayout.LayoutParams(
                        80,
                        40);
                btnLayoutParams.setMargins(0, 0, 0, 0);
                switchList.add(switchButton);
                radioList.add(newRadioButton);
                linearLayout.addView(switchButton,btnLayoutParams);
                radiogroup.addView(linearLayout);
            }
        }
    }

    public void onClick(View view) {
        try {
            String s = ((RadioButton) view).getText().toString();
            int tag =  (int)((RadioButton) view).getTag();
            boolean switch_on = switchList.get(tag).isChecked();
            tag =  (int)((RadioButton) view).getTag();
            for (int i=0;i<radioList.size();i++) {
                if (tag!=i) {
                    radioList.get(i).setChecked(false);
                }
            }
            if (PreScanMode==0) {
                char sw = switch_on ? (char)0x01 : (char)0x00;
                char[] data = new char[]{0x02, cmdData[tag], sw};
                sendToService(data);
            } else {
                char sw = switch_on ? (char)0x01 : (char)0x00;
                char[] data = new char[]{0x00, (char)tag, sw};
                sendToService(data);
            }
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void onRadioButtonClicked(View view)
    {
        // 檢查按鈕是不是被選擇了
        boolean checked = ((RadioButton)view).isChecked();

//        // 透過ID來確認哪一個Radio Button被選擇了
//        switch(view.getId())
//        {
//            case R.id.radioBtn1:
//                if(checked)
//                    // 做被選取要做的事情
//                    break;
//            case R.id.radioBtn2:
//                if (checked)
//                    // 做被選取要做的事情
//                    break;
//        }
    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
