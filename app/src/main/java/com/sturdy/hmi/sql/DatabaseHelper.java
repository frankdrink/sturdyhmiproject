package com.sturdy.hmi.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context ;

    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_USER = "user";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_LEVEL = "user_level";
    private static final String COLUMN_USER_PASSWORD = "user_password";
    private static final String COLUMN_USER_BARCODE = "user_barcode";
    private static final String COLUMN_USER_ORDER = "user_order";

    private static final String COLUMN_DATE="user_date";

    public static final String DATE_FORMAT="YYYY-MM-DD";
    // create table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT,"
            + COLUMN_USER_LEVEL + " TEXT," + COLUMN_USER_PASSWORD + " TEXT," + COLUMN_USER_BARCODE + " TEXT," +  COLUMN_DATE+" TEXT,"+  COLUMN_USER_ORDER+" INTEGER"
            +")";

    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;


    public static final String TABLE_CUSTOMER = "customer" ;
    public static final String COL_ID = "_id" ;
    public static final String COL_FIRST_NAME = "firstName" ;
    public static final String COL_LAST_NAME = "lastName" ;

    private String DROP_CUSTOMER_TABLE = "DROP TABLE IF EXISTS " + TABLE_CUSTOMER;

    private User user;

    /**
     * Constructor
     * 
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context ;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        createCustomerTable(db);

        addAdminUser(db);
    }

    public void addAdminUser(SQLiteDatabase db) {
        user = new User();
        user.setName("Admin");
        user.setUserLevel(0);
        user.setPassword("0000");
        user.setBarcode("");
        user.setUserOrder(0);
        addUser(db,user);

        user = new User();
        user.setName("Super User A");
        user.setUserLevel(1);
        user.setPassword("0000");
        user.setBarcode("");
        user.setUserOrder(1);
        addUser(db,user);

        user = new User();
        user.setName("Super User B");
        user.setUserLevel(1);
        user.setPassword("0000");
        user.setBarcode("");
        user.setUserOrder(2);
        addUser(db,user);
    }

    public static void createCustomerTable(SQLiteDatabase db){
        String createQuery = "Create Table " +
                TABLE_CUSTOMER + " ( " +
                COL_ID + " Integer Primary key AutoIncrement , " +
                COL_FIRST_NAME + " text not null," +
                COL_LAST_NAME + " text not null )" ;
        db.execSQL(createQuery);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_CUSTOMER_TABLE);

        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create user record
     *
     * @param user
     */
    public void addUser(SQLiteDatabase db,User user) {
//        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_LEVEL, Integer.toString(user.getUserLevel()));
        values.put(COLUMN_USER_PASSWORD, user.getPassword());
        values.put(COLUMN_USER_BARCODE, user.getBarcode());
        values.put(COLUMN_USER_ORDER, user.getUserOrder());
        // Inserting Row
        db.insert(TABLE_USER, null, values);
//        db.close();
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_LEVEL, Integer.toString(user.getUserLevel()));
        values.put(COLUMN_USER_PASSWORD, user.getPassword());
        values.put(COLUMN_USER_BARCODE, user.getBarcode());
        values.put(COLUMN_USER_ORDER, user.getUserOrder());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
//        db.close();
    }

    /**
     * This method to update user record
     *
     * @param user
     */
    public void updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_LEVEL, user.getUserLevel());
        values.put(COLUMN_USER_PASSWORD, user.getPassword());
        values.put(COLUMN_USER_BARCODE, user.getBarcode());
        values.put(COLUMN_USER_ORDER, user.getUserOrder());

        // updating row
        db.update(TABLE_USER, values, COLUMN_USER_NAME + " = ?",
                new String[]{user.getName()});
        db.close();
    }
    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public int getUserOrderNumber() {
        List<User> userList = getAllUser();
        int[] intArray = new int[userList.size()];
        for (int i=0;i<userList.size();i++) {
            intArray[i] = userList.get(i).getUserOrder();
        }
        Arrays.sort(intArray);
        for (int i=0;i<intArray.length;i++) {
            if (intArray[i]!=i)
                return i;
        }
        return intArray.length;
    }
    public List<User> getAllUser() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_LEVEL,
                COLUMN_USER_NAME,
                COLUMN_USER_PASSWORD,
                COLUMN_USER_BARCODE,
                COLUMN_USER_ORDER
        };
        // sorting orders

        String sortOrder =
                COLUMN_USER_LEVEL + "," +COLUMN_USER_NAME + " ASC";
        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
                user.setUserLevel(Integer.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_USER_LEVEL))));
                user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_USER_PASSWORD)));
                user.setBarcode(cursor.getString(cursor.getColumnIndex(COLUMN_USER_BARCODE)));
                user.setUserOrder(Integer.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ORDER))));

                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }



    /**
     * This method is to delete user record
     *
     * @param user
     */
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_USER, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @return true/false
     */
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_USER_LEVEL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_USER_LEVEL + " = ?" + " AND " + COLUMN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    /**
     * Method return an object of CustomerDAO
     * @return
     */
    public BarCodeSNDao getCustomerDao(){
        return BarCodeSNDao.getInstance(this);
    }

    /**
     * Method used to process cursor and return corresponding object
     * @param cursor
     * @param cls
     * @return
     */
    public static Object getObjectFromCursor(Cursor cursor, Object cls ){
        Object returnObject = null;
        if(cls.getClass().isInstance(BarCode.class)){
            if (cursor != null && cursor.getCount() > 0){
                BarCode customer = new BarCode();
                customer.setId(cursor.getInt(cursor.getColumnIndex(BarCodeSNDao.COL_ID)));
                customer.setSerialNumber(cursor.getString(cursor.getColumnIndex(BarCodeSNDao.COL_FIRST_NAME)));
                customer.setLastName(cursor.getString(cursor.getColumnIndex(BarCodeSNDao.COL_LAST_NAME)));
                returnObject = customer ;
            }
        }
        return returnObject;
    }

    public Context getContext(){
        return this.context ;
    }
}
