package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class UserTraceabilityActivity extends BaseToolBarActivity {
    private Activity mActivity;
    List<MainMenuSettingElement> settingElementList;
//    TextView maxSelectTextView;
    ListView list1, list2;
    String[] title_01 = {
            "Pre ID control",
            "Post ID control"

    } ;
    Integer[] imageId_01 = {
            R.drawable.user_list_icon_user_normal,
            R.drawable.user_list_icon_user_normal
    };

    String[] title_02 = {
            "Pre scan",
            "Post scan"

    } ;
    Integer[] imageId_02 = {
            R.drawable.tool_bar_icon_barcode_dark,
            R.drawable.tool_bar_icon_barcode_dark
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_usertraceability);
        mActivity = this;
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("User Traceability", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

//        maxSelectTextView = (TextView) findViewById(R.id.max_program_view);

        final boolean[] listChecked1 = new boolean[2];
        final boolean[] listChecked2 = new boolean[2];

        if ((Constants.instance().fetchValueInt(preIDControl))==0)
            listChecked1[0] = false;
        else
            listChecked1[0] = true;

        if ((Constants.instance().fetchValueInt(postIDControl))==0)
            listChecked1[1] = false;
        else
            listChecked1[1] = true;

        if ((Constants.instance().fetchValueInt(preScanControl))==0)
            listChecked2[0] = false;
        else
            listChecked2[0] = true;

        if ((Constants.instance().fetchValueInt(postScanControl))==0)
            listChecked2[1] = false;
        else
            listChecked2[1] = true;

        final MainSettingListAdapter listAdapter1 = new
                MainSettingListAdapter(this, title_01, imageId_01, listChecked1);
        list1=(ListView)findViewById(R.id.list1);
        list1.setClickable(false);
        list1.setAdapter(listAdapter1);
        list1.setBackgroundColor(Color.WHITE);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SwitchButton switchButton=(SwitchButton)view.findViewById(R.id.menu_setting_swich);
                boolean isChecked = !listChecked1[position];
                switchButton.setChecked(isChecked);
                listChecked1[position] = isChecked;
                listAdapter1.setChecked(listChecked1);
                if (position==0) {
                    Constants.instance().storeValueInt(preIDControl,listChecked1[position]? 1 : 0);
                } else {
                    Constants.instance().storeValueInt(postIDControl,listChecked1[position]? 1 : 0);
                }
            }
        });

        final MainSettingListAdapter listAdapter2 = new
                MainSettingListAdapter(this, title_02, imageId_02, listChecked2);
        list2 = (ListView)findViewById(R.id.list2);
        list2.setClickable(false);
        list2.setAdapter(listAdapter2);
        list2.setBackgroundColor(Color.WHITE);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SwitchButton switchButton=(SwitchButton)view.findViewById(R.id.menu_setting_swich);
                boolean isChecked = !listChecked2[position];
                switchButton.setChecked(isChecked);
                listChecked2[position] = isChecked;
                listAdapter2.setChecked(listChecked2);
                if (position==0) {
                    Constants.instance().storeValueInt(preScanControl,listChecked2[position]? 1 : 0);
                } else {
                    Constants.instance().storeValueInt(postScanControl,listChecked2[position]? 1 : 0);
                }
            }
        });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
