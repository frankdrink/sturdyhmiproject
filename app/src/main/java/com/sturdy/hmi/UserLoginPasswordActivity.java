package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.sql.ReturnMessage;
import com.sturdy.hmi.utils.KyEditText;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginAccount;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;
import static com.sturdy.hmi.Constants.userLoginLevel;
import static com.sturdy.hmi.Constants.userLoginPassword;

public class UserLoginPasswordActivity extends BaseToolBarActivity {
    private Activity mActivity;
    private DatabaseHelper mDatabaseHelper;
    private String loginCheckPoint;
    private ListView list;
    private KyEditText userPasswordEdit;
    private TextView login_status_textview;
    private String menuSelectName;
    private int menuSelectId;
    private String loginAccount;
    private String loginPassword;
    private int loginLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_login_password);
        Constants.instance(this.getApplicationContext());
        mActivity = this;
        mDatabaseHelper=new DatabaseHelper(mActivity);

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loginCheckPoint = (Constants.instance().fetchValueString(userLoginCheckPoint));
        String navTitle = "";
        if (loginCheckPoint.equals("PreID")) {
            navTitle = "Pre ID control";
        } else if (loginCheckPoint.equals("PostID")) {
            navTitle = "Post ID control";
        } else if (loginCheckPoint.equals("UserManagement")) {
            navTitle = "User Management";
        }
        Button mNavTitleView = mNavTopBar.addLeftTextButton(navTitle, R.id.topbar_sterilization_right_title, Color.BLACK,38);
//        mNavTopBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
////            }
////        });
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        menuSelectName = (Constants.instance().fetchValueString(saveSelectMenuNameKey));
        menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
        loginAccount = (Constants.instance().fetchValueString(userLoginAccount));
        loginPassword = (Constants.instance().fetchValueString(userLoginPassword));
        loginLevel = (Constants.instance().fetchValueInt(userLoginLevel));
        int imageId = 0;
        switch (loginLevel) {
            case 0:
                imageId =  R.drawable.user_list_icon_top_administrator_normal;
                break;
            case 1:
                imageId =  R.drawable.user_list_icon_administrator_normal;
                break;
            case 2:
                imageId =  R.drawable.user_list_icon_user_normal;
                break;
        }
        TextView userAccountTextview = (TextView) findViewById(R.id.user_account_textview);
        login_status_textview = (TextView) findViewById(R.id.login_status_textview);
        userAccountTextview.setText(loginAccount);
        ImageView level_ImageView = (ImageView) findViewById(R.id.user_level_img);
        level_ImageView.setImageResource(imageId);
        userPasswordEdit = (KyEditText)findViewById(R.id.password_edit);
        userPasswordEdit.setOnEditorActionListener(
                new KyEditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                                ) {

                            if (userPasswordEdit.getText().length()>1){
                                String input = userPasswordEdit.getText().toString();
                                if (loginPassword.equals(input)) {
                                    String programName;
                                    Intent intent = new Intent();
                                    if (loginCheckPoint.equals("PreID")) {
                                        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                                            switch (menuSelectId) {
                                                case 0:
                                                    intent.setClass(mActivity, Liquid1EditActivity.class);
                                                    programName = "Liquid 1";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 1:
                                                    intent.setClass(mActivity, Liquid2EditActivity.class);
                                                    programName = "Liquid 2";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 2:
                                                    intent.setClass(mActivity, Solid1EditActivity.class);
                                                    programName = "Solid 1";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 3:
                                                    intent.setClass(mActivity, Solid2EditActivity.class);
                                                    programName = "Solid 2";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 4:
                                                    intent.setClass(mActivity, AgarEditActivity.class);
                                                    programName = "Agar";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 5:
                                                    intent.setClass(mActivity, DissolutionEditActivity.class);
                                                    programName = "Dissolution";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "60");
                                                    GlobalClass.getInstance().Steri_Temp = 60;
                                                    break;
                                                case 6:
                                                    intent.setClass(mActivity, DryOnlyEditActivity.class);
                                                    programName = "Dry Only";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 7:
                                                    intent.setClass(mActivity, WasteEditActivity.class);
                                                    programName = "Waste";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 8:
                                                    intent.setClass(mActivity, User1EditActivity.class);
                                                    programName = "User 1";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 9:
                                                    intent.setClass(mActivity, User2EditActivity.class);
                                                    programName = "User 2";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 10:
                                                    intent.setClass(mActivity, LeakageTestEditActivity.class);
                                                    programName = "Leakage Test";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 13:
                                                    intent.setClass(mActivity, LatexEditActivity.class);
                                                    programName = "Latex";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name", programName);
                                                    intent.putExtra("temp", "121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                            }
                                        }else{
                                            switch (menuSelectId){
                                                case 0:
                                                    intent.setClass(mActivity,UnwrappedEditActivity.class);
                                                    programName = "Unwrapped 121";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 1:
                                                    intent.setClass(mActivity,UnwrappedEditActivity.class);
                                                    programName = "Unwrapped 134";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 2:
                                                    intent.setClass(mActivity,WrappedEditActivity.class);
                                                    programName = "Wrapped 121";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 3:
                                                    intent.setClass(mActivity,WrappedEditActivity.class);
                                                    programName = "Wrapped 134";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 4:
                                                    intent.setClass(mActivity,InstrumentEditActivity.class);
                                                    programName = "Instrument 121";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","121");
                                                    GlobalClass.getInstance().Steri_Temp = 121;
                                                    break;
                                                case 5:
                                                    intent.setClass(mActivity,InstrumentEditActivity.class);
                                                    programName = "Instrument 134";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 6:
                                                    intent.setClass(mActivity,PrionEditActivity.class);
                                                    programName = "Prion";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 7:
                                                    intent.setClass(mActivity,FlashEditActivity.class);
                                                    programName = "Flash";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;

                                                    break;
                                                case 8:
                                                    intent.setClass(mActivity,DryEditActivity.class);
                                                    programName = "Dry";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;

                                                    break;
                                                case 9:
                                                    intent.setClass(mActivity,CustomizationEditActivity.class);
                                                    programName = "Customization";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;

                                                    break;
                                                case 10:
                                                    intent.setClass(mActivity,LeakageTestEditActivity.class);
                                                    programName = "Leakage test";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 11:
                                                    intent.setClass(mActivity,HelixTestEditActivity.class);
                                                    programName = "Helix test";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                                case 12:
                                                    intent.setClass(mActivity,BDTestEditActivity.class);
                                                    programName = "B&D test";
                                                    Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                                    intent.putExtra("name",programName);
                                                    intent.putExtra("temp","134");
                                                    GlobalClass.getInstance().Steri_Temp = 134;
                                                    break;
                                            }
                                        }
                                    } else if (loginCheckPoint.equals("PostID")) {
                                        char[] data = new char[]{0x01, 0x60};
                                        sendToService(data);
                                        String printData = "--------------------------------\n";
                                        sendToServicePrinter(printData.toCharArray(),printData);
                                        saveDtlToCache(printData.toCharArray());
                                        printData = String.format("Post ID:\n%s\n"
                                                , Constants.instance().fetchValueString(userLoginAccount));
                                        sendToServicePrinter(printData.toCharArray(),printData);
                                        saveDtlToCache(printData.toCharArray());
                                        intent.setClass(mActivity,DoorOpenActivity.class);
                                    } else if (loginCheckPoint.equals("MaintainMenu")) {
                                        intent.setClass(mActivity,MaintainMenuActivity.class);
                                    } else if (loginCheckPoint.equals("UserManagement")) {
                                        intent.setClass(UserLoginPasswordActivity.this, UserManagementSettingActivity.class);
                                    }


                                    startActivity(intent);
                                    overridePendingTransition(0, 0);
                                    finish();
                                }else{
                                    userPasswordEdit.setText("");
                                    login_status_textview.setVisibility(View.VISIBLE);
                                    return true;
                                }

                                return false;
                            }
                        }
                        return false;
                    }
                });

        ImageView showPasswordIV = (ImageView)this.findViewById(R.id.show_password);
        showPasswordIV.setTag(0);
        showPasswordIV.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                if ((int)showPasswordIV.getTag()==0) {
                    showPasswordIV.setTag(1);
                    userPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    showPasswordIV.setImageResource(R.drawable.text_field_icon_visibility);
                }else{
                    showPasswordIV.setTag(0);
                    userPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    showPasswordIV.setImageResource(R.drawable.text_field_icon_invisibility);
                }

            }});
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();



    }


}
