package com.sturdy.hmi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.adapter.MainMenuGridViewAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;

import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.RecyclerTouchListener;

import static com.sturdy.hmi.Constants.HMI_ERROR_BROADCAST;
import static com.sturdy.hmi.Constants.HMI_SLEEP_BROADCAST;

public class SleepModeActivity extends BaseToolBarActivity{

    private RecyclerView listView;
    private RecyclerView gridView;
    private MainMenuGridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        GlobalClass.getInstance().OpenSleepMode = true;
        setContentView(R.layout.activity_sleep_mode);
        TextView timeView =  (TextView)findViewById(R.id.description_textview);
        startShowTitleClock(timeView);
        registerBaseActivityReceiver();
        char[] data = new char[]{0x02,0xf7,0x01};
        sendToService(data);
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    char[] data = new char[]{0x02,0xf7,0x00};
                    sendToService(data);
                    Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
                    sendBroadcast(broadcast);
                    finish();
                }
                return false;
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GlobalClass.getInstance().OpenSleepMode = false;
    }

}
