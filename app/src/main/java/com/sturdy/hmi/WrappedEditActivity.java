package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.List;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;

public class WrappedEditActivity extends BaseToolBarActivity implements SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;

    private String[] pickerTempArrayList;
    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String str;
    private int CB_VacuumCount;
    private int CB_Temp;
    private int CB_SteriTime;
    private int CB_DryTime;
    private char cSteri_Temp;
    private boolean isFirstPrescan = true;


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == cSteri_Temp) {
                    char ch = recv[2];
                    CB_VacuumCount = ch;
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 3, ch2, 0, 2);
                    CB_Temp = CharUtils.charArrayToInt(ch2);
                    ch2 = new char[2];
                    System.arraycopy(recv, 5, ch2, 0, 2);
                    CB_SteriTime = CharUtils.charArrayToInt(ch2);
                    ch2 = new char[2];
                    System.arraycopy(recv, 7, ch2, 0, 2);
                    CB_DryTime = CharUtils.charArrayToInt(ch2);
                    str = Integer.toString(CB_VacuumCount);
                    mItems.set(0, new RecyclerViewClass("Pre-Vacuum", str, R.drawable.parameter_icon_prevacuum,true));
                    str = UnitConvert.sTempConvert(1,CB_Temp);
                    mItems.set(1, new RecyclerViewClass("Ster.Temp", str, R.drawable.parameter_icon_temp,true));
                    str = String.format("%dm %ds",CB_SteriTime/60,CB_SteriTime%60);
                    mItems.set(2, new RecyclerViewClass("Ster.Time", str, R.drawable.parameter_icon_time,true));
                    str = String.format("%dm %ds",CB_DryTime/60,CB_DryTime%60);
                    mItems.set(3, new RecyclerViewClass("Dry Time", str, R.drawable.parameter_icon_time,true));
                    mAdapter.notifyDataSetChanged();
                    GlobalClass.getInstance().Steri_Temp = CB_Temp / 10;
                    GlobalClass.getInstance().CB_PreVacuumCount = CB_VacuumCount;
                    GlobalClass.getInstance().CB_SteriTime = CB_SteriTime;
                    GlobalClass.getInstance().CB_DryTime = CB_DryTime;
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_wrapped_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        if (GlobalClass.getInstance().Steri_Temp==121) {
            cSteri_Temp = 0x02;
        }else{
            cSteri_Temp = 0x03;
        }
        pickerTempArrayList = new String[]{
                "121°C",  "134°C"
        };
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_1_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_2_button, 64);


        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();

        mItems.add(0, new RecyclerViewClass("Pre-Vacuum", "", R.drawable.parameter_icon_prevacuum,false));
        mItems.add(1, new RecyclerViewClass("Ster.Temp", "", R.drawable.parameter_icon_temp,true));
        mItems.add(2, new RecyclerViewClass("Ster.Time", "", R.drawable.parameter_icon_time,false));
        mItems.add(3, new RecyclerViewClass("Dry Time", "", R.drawable.parameter_icon_time,false));

        mAdapter = new SteriEditAdapter(this, mItems);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WrappedEditActivity.this,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WrappedEditActivity.this,WrappedInformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bFieldmodified){
//                    bFieldmodified = false;
//                    mAdapter.setTextViewEditable(bFieldmodified);
//                    mStartSterButton.setText("Start");
//                }else{
//                    startActivity(new Intent(WrappedEditActivity.this, StartSterilizationActivity.class));
//                    overridePendingTransition(0, 0);
//                    finish();
//                }
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(WrappedEditActivity.this, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendGetSterillzationStep();
                }
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        // 取得消毒行程資(Wrapped)
        char[] data = new char[]{0x01,cSteri_Temp};
        sendToService(data);
    }



    @Override
    public void updateItemList(final int position) {
        OptionPicker picker = new OptionPicker(WrappedEditActivity.this, pickerTempArrayList);
                picker.setCanceledOnTouchOutside(false);
                picker.setTopHeight(50);
                picker.setSubmitTextSize(40);
                picker.setDividerRatio(WheelView.DividerConfig.FILL);
                picker.setShadowColor(Color.WHITE, 40);
                picker.setCycleDisable(true);
                picker.setTextSize(32);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
//                        showToast("index=" + index + ", item=" + item);
                        String str = pickerTempArrayList[index];
                        RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                        mItems.set(position,recycler);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                picker.show();
//        mAdapter.notifyItemRemoved(position);
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }


}
