package com.sturdy.filechooser.exceptions;


public class MemoryNotAccessibleException extends Exception {

    public MemoryNotAccessibleException(String message) {
        super(message);
    }
}
