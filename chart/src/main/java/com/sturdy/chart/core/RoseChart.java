package com.sturdy.chart.core;

import android.content.Context;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseRotateChart;
import com.sturdy.chart.data.RoseData;
import com.sturdy.chart.matrix.RotateHelper;
import com.sturdy.chart.provider.rose.RoseProvider;



public class RoseChart extends BaseRotateChart<RoseProvider,RoseData> {


    public RoseChart(Context context) {
        super(context);
    }

    public RoseChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoseChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    protected RoseProvider initProvider(RotateHelper rotateHelper) {
        RoseProvider provider =  new RoseProvider();
        provider.setRotateHelper(rotateHelper);
        return provider;
    }
}
