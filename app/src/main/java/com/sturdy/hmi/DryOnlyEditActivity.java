package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.List;

import com.sturdytheme.framework.picker.DoublePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.preScanControl;

public class DryOnlyEditActivity extends BaseToolBarActivity implements SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;

    private String[] pickerDryTimeArrayList;
    private String[] switchArray = {"ON","OFF"};

    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String str;
    private boolean isFirstPrescan = true;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x10) { // 乾燥時間(60~3600,Word,秒);Vacuum Pump(ON/OFF);
                    char ch = recv[2];
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 3, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DryTime = CharUtils.charArrayToInt(ch2);
                    str = String.format("%d m",GlobalClass.getInstance().CB_DryTime/60);
                    mItems.set(0, new RecyclerViewClass("Dry Time", str, R.drawable.parameter_icon_time,true));
                    if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                        GlobalClass.getInstance().CB_VacuumPump = (recv[5]!=0)?true:false;
                        str = GlobalClass.getInstance().CB_VacuumPump == true ? "ON" : "OFF";
                        mItems.set(1, new RecyclerViewClass("Vacuum Pump", str, R.drawable.parameter_icon_prevacuum,true));
                    }else{
                        mItems.set(1, new RecyclerViewClass("", "", R.drawable.parameter_icon_pressurized_cooling,true));
                    }
                    mAdapter.notifyDataSetChanged();
                    GlobalClass.getInstance().CB_PreVacuumCount = 0;
                    GlobalClass.getInstance().CB_SteriTemp = 0;
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dry_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);

        pickerDryTimeArrayList = new String[60];
        for (int i=0;i<60;i++) {
            pickerDryTimeArrayList[i] = Integer.toString(i+1)+" m";
        }
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
//        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_edit, R.id.topbar_unwrapped_right_1_button, 64);
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_2_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_3_button, 64);


        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();

        mItems.add(0, new RecyclerViewClass("Dry Time", "10 mins", R.drawable.parameter_icon_time,true));
        if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
            mItems.add(1, new RecyclerViewClass("Vacuum Pump", "", R.drawable.parameter_icon_prevacuum,true));
        }else{
            mItems.add(1, new RecyclerViewClass("", "", R.drawable.parameter_icon_prevacuum,true));
        }

        mAdapter = new SteriEditAdapter(this, mItems, true);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(DryOnlyEditActivity.this,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(DryOnlyEditActivity.this,DryOnlyInformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bFieldmodified){
//                    bFieldmodified = false;
//                    mAdapter.setTextViewEditable(bFieldmodified);
//                    mStartSterButton.setText("Start");
//                }else{
//                    startActivity(new Intent(DryEditActivity.this, StartSterilizationActivity.class));
//                    overridePendingTransition(0, 0);
//                    finish();
//                }
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(DryOnlyEditActivity.this, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendDrySetting();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendDrySetting();
                    sendGetSterillzationStep();
                }
            }
        });
        // 取得消毒行程資(Dry Only)
        char[] data = new char[]{0x02,0x10,GlobalClass.getInstance().cLoginUserID};
        sendToService(data);

    }

    void sendDrySetting() {
        char[] data = new char[5];
        char[] ch22 = new char[2];
        data[0] = 0x04;
        data[1] = 0x2c;
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_DryTime);
        data[2] = ch22[0];
        data[3] = ch22[1];
        data[4] = GlobalClass.getInstance().CB_VacuumPump?(char)1:0;
        sendToService(data);
    }

    @Override
    public void updateItemList(final int position) {
       if (position==0) {
            OptionPicker picker = new OptionPicker(DryOnlyEditActivity.this, pickerDryTimeArrayList);
           picker.setSelectedItem(mItems.get(position).getMessage2());
           picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
//                        showToast("index=" + index + ", item=" + item);
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_DryTime = Integer.valueOf(str) * 60;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
       }else if (position==1) {
           OptionPicker picker = new OptionPicker(DryOnlyEditActivity.this, switchArray);
           picker.setSelectedIndex(GlobalClass.getInstance().CB_PressurizedCooling?0:1);
           picker.setCanceledOnTouchOutside(false);
           picker.setTopHeight(50);
           picker.setSubmitTextSize(40);
           picker.setTitleText(mItems.get(position).getMessage1());
           picker.setTitleTextSize(40);
           picker.setDividerRatio(WheelView.DividerConfig.FILL);
           picker.setShadowColor(Color.WHITE, 40);
           picker.setCycleDisable(true);
           picker.setTextSize(32);
           picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
               @Override
               public void onOptionPicked(int index, String item) {
                   GlobalClass.getInstance().CB_VacuumPump = (index==0)?true:false;
                   RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                   mItems.set(position,recycler);
                   mAdapter.notifyDataSetChanged();
               }
           });
           picker.show();
       }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }


}
