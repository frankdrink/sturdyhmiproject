package com.sturdy.hmi;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.Dialog.PayFragment;
import com.sturdy.hmi.Dialog.PayPasswordView;
import com.sturdy.hmi.adapter.CycleListAdapter;
import com.sturdy.hmi.adapter.EditListAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import android.provider.Settings.System;
import static com.sturdy.hmi.Constants.BrightnessKey;
import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.NextAirFilterKey;
import static com.sturdy.hmi.Constants.NextExhaustFilterKey;
import static com.sturdy.hmi.Constants.NextGasketServiceKey;
import static com.sturdy.hmi.Constants.NextServiceCycleKey;
import static com.sturdy.hmi.Constants.RecordModeKey;
import static com.sturdy.hmi.Constants.SoundKey;

public class CycleCounterSettingActivity extends BaseToolBarActivity implements NumberDialog.OnNumberDialogDoneListener,CycleListAdapter.UpdateMainClass, PayPasswordView.InputCallBack{
    private Activity mActivity;
    CycleListAdapter adapter1;
    List<String> data1;
    List<MainMenuSettingElement> settingElementList;
    int iNextExhaustFilter;
    int iNextAirFilter;
    int iNextGasket;
    private PayFragment mPayFragment;

    int enterEModeCount=0;
    int iNextService = 5000;
//    TextView maxSelectTextView;
    Handler handler;
    ListView list1;
    String[] title_L = {
            "Cycle counter",
            "Next Exhaust Filter Service",
            "Next Air Filter Service",
            "Next Gasket Service",
            "Next Service"

    } ;
    Integer[] imageId_L = {
            R.drawable.setting_icon_cycle_counter,
            R.drawable.setting_icon_next_service,
            R.drawable.setting_icon_next_service,
            R.drawable.setting_icon_next_service,
            R.drawable.setting_icon_next_service

    };

    String[] title_B = {
            "Cycle counter",
            "Next Air Filter Service",
            "Next Gasket Service",
            "Next Service"

    } ;
    Integer[] imageId_B = {
            R.drawable.setting_icon_cycle_counter,
            R.drawable.setting_icon_next_service,
            R.drawable.setting_icon_next_service,
            R.drawable.setting_icon_next_service

    };
    private String[] pickerTimeArrayList;
    private ContentResolver cResolver;
    int dialogIndex = 0;
    int BrightnessValue;
    int SoundValue;
    String valueStr;


    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_powersetting);
        mActivity = this;
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        pickerTimeArrayList = new String[]{
                "60 mins",  "100 mins", "150 mins",  "200 mins"
        };
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Cycle counter", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        int CycleCounter = Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0);
        iNextExhaustFilter = Settings.System.getInt(this.getContentResolver(),NextExhaustFilterKey,0);
        iNextAirFilter = Settings.System.getInt(this.getContentResolver(),NextAirFilterKey,0);
        iNextGasket = Settings.System.getInt(this.getContentResolver(),NextGasketServiceKey,0);
        iNextService = Settings.System.getInt(this.getContentResolver(),NextServiceCycleKey,0);
        data1 = new ArrayList<String>();

        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            valueStr = String.format("%06d",CycleCounter);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextExhaustFilter);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextAirFilter);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextGasket);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextService);
            data1.add(valueStr);
            adapter1 = new CycleListAdapter(this, title_L, data1, imageId_L);
        }else{
            valueStr = String.format("%06d",CycleCounter);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextAirFilter);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextGasket);
            data1.add(valueStr);
            valueStr = String.format("%06d",iNextService);
            data1.add(valueStr);
            adapter1 = new CycleListAdapter(this,title_B, data1, imageId_B);
        }
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void updateItemList(final int itemPostion) {
        dialogIndex = itemPostion;
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {

            if (itemPostion == 0) {

            } else if (itemPostion == 1) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextExhaustFilter);
                myDiag.show(getSupportFragmentManager(), "Next ExhaustFilter");
            } else if (itemPostion == 2) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextAirFilter);
                myDiag.show(getSupportFragmentManager(), "Next AirFilter");
            } else if (itemPostion == 3) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextGasket);
                myDiag.show(getSupportFragmentManager(), "Next Gasket");
            } else if (itemPostion == 4) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextService);
                myDiag.show(getSupportFragmentManager(), "Next Service");
            }
        }else{
            if (itemPostion == 0) {

            } else if (itemPostion == 1) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextAirFilter);
                myDiag.show(getSupportFragmentManager(), "Next AirFilter");
            } else if (itemPostion == 2) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextGasket);
                myDiag.show(getSupportFragmentManager(), "Next Gasket");
            } else if (itemPostion == 3) {
                NumberDialog myDiag =
                        NumberDialog.newInstance(6, iNextService);
                myDiag.show(getSupportFragmentManager(), "Next Service");
            }
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
        enterEModeCount++;
        if (enterEModeCount>10) {
            enterEModeCount=0;
            Bundle bundle = new Bundle();
            mPayFragment = new PayFragment();
            mPayFragment.setArguments(bundle);
            mPayFragment.setPaySucessCallBack(CycleCounterSettingActivity.this);
            mPayFragment.show(getSupportFragmentManager(), "pay");
            hideKeyboard(this);
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onDone(int value) {
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            switch (dialogIndex) {
                case 1:
                    iNextExhaustFilter = value;
                    Settings.System.putInt(this.getContentResolver(), NextExhaustFilterKey, iNextExhaustFilter);
                    valueStr = String.format("%06d", iNextExhaustFilter);
                    data1.set(1, valueStr);
                    break;
                case 2:
                    iNextAirFilter = value;
                    Settings.System.putInt(this.getContentResolver(), NextAirFilterKey, iNextAirFilter);
                    valueStr = String.format("%06d", iNextAirFilter);
                    data1.set(2, valueStr);
                    break;
                case 3:
                    iNextGasket = value;
                    Settings.System.putInt(this.getContentResolver(), NextGasketServiceKey, iNextGasket);
                    valueStr = String.format("%06d", iNextGasket);
                    data1.set(3, valueStr);
                    break;
                case 4:
                    iNextService = value;
                    Settings.System.putInt(this.getContentResolver(), NextServiceCycleKey, iNextService);
                    valueStr = String.format("%06d", iNextService);
                    data1.set(4, valueStr);
                    break;
            }
        }else{
            switch (dialogIndex) {
                case 1:
                    iNextAirFilter = value;
                    Settings.System.putInt(this.getContentResolver(), NextAirFilterKey, iNextAirFilter);
                    valueStr = String.format("%06d", iNextAirFilter);
                    data1.set(1, valueStr);
                    break;
                case 2:
                    iNextGasket = value;
                    Settings.System.putInt(this.getContentResolver(), NextGasketServiceKey, iNextGasket);
                    valueStr = String.format("%06d", iNextGasket);
                    data1.set(2, valueStr);
                    break;
                case 3:
                    iNextService = value;
                    Settings.System.putInt(this.getContentResolver(), NextServiceCycleKey, iNextService);
                    valueStr = String.format("%06d", iNextService);
                    data1.set(3, valueStr);
                    break;
            }
        }

        adapter1.resetAdapter(data1);
        adapter1.notifyDataSetChanged();
//        TextView tv=
//                (TextView) findViewById(R.id.textView);
//        tv.setText(Integer.toString(value));
    }

    @Override
    public void onInputFinsh(String result) {
        mPayFragment.dismiss();
//        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        if (result.equals("123456")) {
            startActivity(new Intent(CycleCounterSettingActivity.this, EModeActivity.class));
            overridePendingTransition(0, 0);
        }
    }

}
