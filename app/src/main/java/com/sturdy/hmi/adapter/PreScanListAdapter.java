package com.sturdy.hmi.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sturdy.hmi.R;
import com.sturdy.widget.SwitchButton;

public class PreScanListAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    private boolean checked[];
    private int checkedCount = 0;
    public PreScanListAdapter(Activity context,
                      String[] web, Integer[] imageId, boolean[] checked) {
        super(context, R.layout.mainmenu_setting_list, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;
        this.checked = checked;
    }

    public void setChecked(boolean[] checked) {
        this.checked = checked;

    }

    public int getCheckedCoount() {
        checkedCount = 0;
        for (int i=0;i<checked.length;i++){
            if (this.checked[i])
                checkedCount++;
        }
        return checkedCount;
    }



    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.prescan_cmd_list, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
//        TextView switchTextView = (TextView) rowView.findViewById(R.id.switch_txt);
        txtTitle.setText(web[position]);


        SwitchButton switchButton = (SwitchButton) rowView.findViewById(R.id.menu_setting_swich);

        switchButton.setChecked(checked[position]);
        if (checked[position]){
//            switchTextView.setText("ON");
//            switchTextView.setTextColor(0Xff0277bd);
        } else {
//            switchTextView.setText("OFF");
//            switchTextView.setTextColor(0Xffa9a9a9);
        }
//        if (getCheckedCoount()>=6){
//            if (checked[position]){
//                rowView.setBackgroundColor(0xffffffff);
//            }else{
//                rowView.setBackgroundColor(0xffeeeeee);
//            }
//        }else{
//            rowView.setBackgroundColor(0xffffffff);
//        }
        return rowView;
    }


//    private static class ViewHolder {
//        TextView txtTitle;
//        ImageView imageView;
//        SwitchButton switchButton;
//    }
//
//    @Override
//    public View getView(int position, View view, ViewGroup parent) {
//        ViewHolder holder;
//        if (view == null) {
//            LayoutInflater inflater = context.getLayoutInflater();
//            view = inflater.inflate(R.layout.mainmenu_setting_list, null, true);
//            holder = new ViewHolder();
//            holder.txtTitle = (TextView) view.findViewById(R.id.txt);
////        TextView switchTextView = (TextView) rowView.findViewById(R.id.switch_txt);
//            holder.imageView = (ImageView) view.findViewById(R.id.img);
//            holder.switchButton = (SwitchButton) view.findViewById(R.id.menu_setting_swich);
//
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//
//        holder.txtTitle.setText(web[position]);
//        holder.imageView.setImageResource(imageId[position]);
//        holder.switchButton.setChecked(checked[position]);
//        if (checked[position]){
////            switchTextView.setText("ON");
////            switchTextView.setTextColor(0Xff0277bd);
//        } else {
////            switchTextView.setText("OFF");
////            switchTextView.setTextColor(0Xffa9a9a9);
//        }
//        return view;
//    }

    public boolean isChecked(int position)
    {
        return checked[position];
    }

    public void setChecked(int position,boolean checked)
    {
        this.checked[position] = checked;
    }
}
