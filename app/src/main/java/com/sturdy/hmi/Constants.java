package com.sturdy.hmi;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by franklin on 2019/9/11.
 */

public class Constants {
    public enum MACHINE_MODEL {
        MODEL_30B,
        MODEL_30L
    }

    public enum MACHINE_STATE {
        IDLE,
        STERI_RUN,
        STERI_COMPLETE,
        STERI_FAIL,
        CB_DFU,
        DYNAMIC_CAL
    }
    public static final String HMI_ERROR_BROADCAST = "com.sturdy.hmi.error";
    public static final String HMI_SLEEP_BROADCAST = "com.sturdy.hmi.sleep";
    public static final String HMI_RMSLEEP_BROADCAST = "com.sturdy.hmi.rmsleep";

    public static final String PREF_FILE = "MainMenuSettingElement";
    public static final String machineModelKey = "MachineModel";                        // 目前執行的機型程序
    public static final String saveMenuSettingKey = "mainMenuSetting";                  // 儲存主選單快捷設定
    public static final String saveSelectMenuNameKey = "MenuSelectName";                // 在主選單選擇的行程名稱
    public static final String saveSelectMenuIdKey = "MenuSelectId";                    // 在主選單選擇的行程ID
    public static final String userLoginAccount = "UserAccount";
    public static final String userLoginLevel = "UserLevel";
    public static final String userLoginPassword = "UserPassword";
    public static final String userLoginBarCode = "UserBarCode";

    public static final String userLoginCheckPoint = "UserLoginCheckPoint";
    public static final String preIDControl = "PreIDControl";
    public static final String postIDControl = "PostIDControl";
    public static final String preScanControl = "PreScanControl";
    public static final String postScanControl = "PostScanControl";
    public static final String saveDateFormat = "DateFormat";                           // 0:Y/M/D 1:D/M/Y 2:M/D/Y
    public static final String saveTimeFormat = "TimeFormat";                           // 0: 12H 1: 24H
    public static final String saveTempFormat = "TempFormat";                           // 0: °C 1: °F
    public static final String savePresFormat = "PresFormat";                           // 0: bar 1: kPa 2: Mpa 3: psi 4: kgf/cm²
    public static final String SleepTimeKey = "SleepTime";
    public static final String BrightnessKey = "Brightness";
    public static final String saveRealPrint = "RealPrint";                             // 即時列印
    public static final String SoundKey = "Sound";
    public static final String RecordModeKey = "RecordMode";                            // 0:Standard 1:Detailed only sterilization step 2:Detailed all cycle
    public static final String CycleCounterKey = "Cycle";
    public static final String NextExhaustFilterKey = "NextExhaustFilter";
    public static final String NextAirFilterKey = "NextAirFilter";
    public static final String NextGasketServiceKey = "NextGasket";
    public static final String NextServiceCycleKey = "NextService";
    public static final String AutoAddWaterKey = "Pressurizedcooling";
    public static final String PipeheaterKey = "Pipeheater";
    public static final String VacuumpumpKey = "Vacuumpump";
    public static final String PressuizedcoolingKey = "Pressurizedcooling";
    public static final String ExhaustTempKey = "ExhaustTemp";
    public static final String BuzzerVolumeKey = "BuzzerVolume";
    public static final String saveAltitudeUnitKey = "altitudeUnit";                           // 0: °C 1: °F
    public static final String saveAltitudeModeKey = "altitudeMode";                           // 0: Auto 1: Manual

    public static final String saveAltitudeKey = "altitude";                           // 0: °C 1: °F


    public static final String ACTION_TO_SERVICE = "com.sturdy.hmi.broadcast.action.TO_SERVICE";
    public static final String ACTION_FROM_SERVICE = "com.sturdy.hmi.broadcast.action.FROM_SERVICE";


    static Constants _instance;

    Context context;
    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;

    public static Constants instance(Context context) {
        if (_instance == null) {
            _instance = new Constants();
            _instance.configSessionUtils(context);
        }
        return _instance;
    }

    public static Constants instance() {
        return _instance;
    }

    public void configSessionUtils(Context context) {
        this.context = context;
        sharedPref = context.getSharedPreferences(PREF_FILE, Activity.MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();
    }

    public void storeValueString(String key, String value) {
        sharedPrefEditor.putString(key, value);
        sharedPrefEditor.commit();
    }

    public String fetchValueString(String key) {
        return sharedPref.getString(key, null);
    }

    public void storeValueInt(String key, int value) {
        sharedPrefEditor.putInt(key, value);
        sharedPrefEditor.commit();
    }

    public int fetchValueInt(String key) {
        return sharedPref.getInt(key, 0);
    }

    public int fetchValueInt(String key,int defVaule) {
        return sharedPref.getInt(key, defVaule);
    }

    public String getVersionName(Context context){
        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionName="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionName=packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

}
