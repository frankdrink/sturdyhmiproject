package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeImageButton;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

public class User1InformationActivity extends BaseToolBarActivity {
    private Activity mActivity;
    ListView list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user1_info);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = this.getIntent();
        String name = intent.getStringExtra("name");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        startShowTitleClock(rightTimeButton);
        String str;
        str = String.format("For liquid, solid, dissolution and agar loads,this function allows the operator to define special sterilization cycle (such as temperature and time) within the specification of this autoclave. ");
        TextView despTextView = (TextView) findViewById(R.id.description_textview);
        despTextView.setText(str);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        if (GlobalClass.getInstance().MachineModel.equals("60L")) {
            str = String.format("14,000 g");
            TextView TextView22 = (TextView) findViewById(R.id.TextView22);
            TextView22.setText(str);

            str = String.format("250 ml(18 bottles)\n500 ml(8 bottles)\n1000 ml(6 bottles)\n2000 ml(2 bottles)");
            TextView TextView32 = (TextView) findViewById(R.id.TextView32);
            TextView32.setText(str);

            str = String.format("250 ml(18 bottles)\n500 ml(15 bottles)\n1000 ml(8 bottles)\n2000 ml(3 bottles)");
            TextView TextView42 = (TextView) findViewById(R.id.TextView42);
            TextView42.setText(str);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

}
