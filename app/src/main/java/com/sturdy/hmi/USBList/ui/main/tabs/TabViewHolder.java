package com.sturdy.hmi.USBList.ui.main.tabs;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.sturdy.hmi.R;


public class TabViewHolder {

    private final View rootView;
//    @BindView(android.R.id.list)
    protected ListView list;
//    @BindView(android.R.id.empty)
    protected View empty;
//    @BindView(R.id.count)
    protected TextView count;

    public TabViewHolder(final View rootView) {
//        ButterKnife.bind(this, rootView);

        this.rootView = rootView;
        list = (ListView)rootView.findViewById(android.R.id.list);
        empty = (View)rootView.findViewById(android.R.id.empty);

        this.list.setEmptyView(empty);
        this.list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    public ListView getList() {
        return list;
    }

    public TextView getCount() {
        return count;
    }
}
