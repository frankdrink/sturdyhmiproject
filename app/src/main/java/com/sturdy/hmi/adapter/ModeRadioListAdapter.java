package com.sturdy.hmi.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sturdy.hmi.R;
import com.sturdy.hmi.RadioButton.CheckableRelativeLayout;
import com.sturdy.hmi.RadioButton.InertCheckBox;
import com.sturdy.hmi.RecyclerViewItem;

public class ModeRadioListAdapter extends BaseAdapter {
	private Context ctx;

	private LayoutInflater lInflater;
	private List<String> data, detaldata;
	List<RadioButton> radioList;
	UpdateMainClass updateMainClass;
	private String listKindTag;
	private int selectRadioTag;


	public ModeRadioListAdapter(Context context, List<String> data, List<String> detaldata, String listKind, int selectRadio) {
		ctx = context;
		this.data = data;
		this.detaldata = detaldata;
		this.listKindTag = listKind;
		this.selectRadioTag = selectRadio;
		radioList = new ArrayList<>();
		lInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(context instanceof UpdateMainClass){
			updateMainClass = (UpdateMainClass)context;
		}
	}

	public void resetAdapter(List<String> data, int selectRadio) {
		this.data = data;
		this.selectRadioTag = selectRadio;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	class ViewHolder {
		private RelativeLayout radioRelativeLayout;
		ImageView iconImg;
		RadioButton radioButton;
		TextView mMsg1, mMsg2;
	}

	@Override
	public int getItemViewType(int position) {
//		if (this.listKindTag.equals("date")) {
//			if (position == 3)
//				return 1;
//			else
//				return 0;
//		}else if (this.listKindTag.equals("time")) {
//			if (position == 2)
//				return 1;
//			else
//				return 0;
//		}
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (this.listKindTag.equals("recordmode")) {

				if (convertView == null) {
					convertView = lInflater.inflate(R.layout.mode_choice_items, parent, false);
					holder = new ViewHolder();
					holder.mMsg1 = (TextView) convertView.findViewById(R.id.singleitemId);
					holder.mMsg1.setText(data.get(position));
					holder.mMsg2 = (TextView) convertView.findViewById(R.id.detal_textview);
					holder.mMsg2.setText(detaldata.get(position));
					holder.radioRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.radioRelativeLayout);
					holder.radioButton = (RadioButton) convertView.findViewById(R.id.singleitemCheckBox);
					radioList.add(holder.radioButton);
					convertView.setTag(holder);
					if (this.selectRadioTag==position) {
						holder.radioButton.setChecked(true);
					}else{
						holder.radioButton.setChecked(false);
					}
				}else {
					holder = (ViewHolder) convertView.getTag();
				}

				holder.mMsg1.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (event.getAction() == MotionEvent.ACTION_DOWN) {
							for (int i=0;i<radioList.size();i++) {
								if (i==position) {
									radioList.get(i).setChecked(true);
								}else{
									radioList.get(i).setChecked(false);

								}
							}
							String str = String.format("mode%d",position);
							selectRadioTag = position;
							updateMainClass.updateItemList(str);
						}
						return false;
					}
				});
				holder.mMsg2.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (event.getAction() == MotionEvent.ACTION_DOWN) {
							for (int i=0;i<radioList.size();i++) {
								if (i==position) {
									radioList.get(i).setChecked(true);
								}else{
									radioList.get(i).setChecked(false);

								}
							}
							String str = String.format("mode%d",position);
							selectRadioTag = position;
							updateMainClass.updateItemList(str);
						}
						return false;
					}
				});
				holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					   @Override
					   public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
							if (isChecked) {
								for (int i=0;i<radioList.size();i++) {
									if (i==position) {
										radioList.get(i).setChecked(true);
									}else{
										radioList.get(i).setChecked(false);

									}
								}
								String str = String.format("mode%d",position);
								selectRadioTag = position;
								updateMainClass.updateItemList(str);
							}
					   }
				   }
				);
		}

		return convertView;
	}

	public interface UpdateMainClass{
		void updateItemList(String itemName);
		void updateListBackground(int position, boolean isChecked);
	}
}