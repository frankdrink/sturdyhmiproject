package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.RecyclerTouchListener;

import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class SystemSettingActivity extends BaseToolBarActivity implements GridViewAdapter.OnItemClickListener{

    private RecyclerView listView;
    private RecyclerView gridView;
    private GridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;
    List<MainMenuSettingElement> settingElementList;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_system_setting);
        mDatabaseHelper=new DatabaseHelper(this);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        gridView = (RecyclerView) findViewById(R.id.grid);

        ImageButton backButton =(ImageButton) findViewById(R.id.back_button);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView menuTitleView = (TextView) findViewById(R.id.menu_title_view);
        menuTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        ImageButton mInfoButton = (ImageButton)findViewById(R.id.main_info_button);
        mInfoButton.setVisibility(View.GONE);
        mInfoButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    Intent intent = new Intent(SystemSettingActivity.this, PreScanSettingActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(0, 0);
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

//        listView.setHasFixedSize(true);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "ListView"
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(horizontalManager);
//        listViewAdapter = new ListViewAdapter(this, corporations);
//        listView.setAdapter(listViewAdapter);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridView.setLayoutManager(layoutManager);
        setSystemMenuElement();
        gridViewAdapter = new GridViewAdapter(this, recyclerViewButton, recyclerViewButton_press);
        gridViewAdapter.setOnItemClickListener(this);
        gridView.setAdapter(gridViewAdapter);
        gridView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gridView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTouch(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is touch!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                this.startActivity(new Intent(SystemSettingActivity.this, DateTimeSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 1:
                this.startActivity(new Intent(SystemSettingActivity.this, UnitSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 2:
                this.startActivity(new Intent(SystemSettingActivity.this, LanguageSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 3:
                this.startActivity(new Intent(SystemSettingActivity.this, PrinterSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 4:
                this.startActivity(new Intent(SystemSettingActivity.this, AddWaterSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 5:
                this.startActivity(new Intent(SystemSettingActivity.this, SerialNumberSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 6:
                Constants.instance().storeValueString(userLoginCheckPoint, "MaintainMenu");
                this.startActivity(new Intent(SystemSettingActivity.this, UserLoginActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 7:
                this.startActivity(new Intent(SystemSettingActivity.this, ConnectivityActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 8:
                this.startActivity(new Intent(SystemSettingActivity.this, CleaningActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 9:
                Constants.instance().storeValueString(userLoginCheckPoint, "UserManagement");
                this.startActivity(new Intent(SystemSettingActivity.this, UserLoginActivity.class));
                this.overridePendingTransition(0, 0);
                break;
            case 10:
                this.startActivity(new Intent(SystemSettingActivity.this, MainMenuSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;
            case 11:
                this.startActivity(new Intent(SystemSettingActivity.this, PowerSettingActivity.class));
                this.overridePendingTransition(0, 0);
                break;
        }


    }

    private void setSystemMenuElement() {
        recyclerViewButton = new ArrayList<>();
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_date_time, "Date & Time"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_unit, "Unit"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_language, "Language"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_print, "Printer"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_auto_add_water, "Auto Add Water"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_serial_nunber, "Serial Number"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_calibration, "Maintain"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_connectivity, "Connectivity"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_cleaning_sg, "Cleaning"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_user_management, "User Management"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_home_preset, "Home Preset"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_power, "Power"));

        recyclerViewButton_press = new ArrayList<>();
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_date_time, "Date & Time"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_unit, "Unit"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_language, "Language"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_print, "Printer"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_auto_add_water, "Auto Add Water"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_serial_nunber, "Serial Number"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_calibration, "Maintain"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_connectivity, "Connectivity"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_cleaning_sg, "Cleaning"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_user_management, "User Management"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_home_preset, "Home Preset"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_power, "Power"));

    }

    @Override
    public void onBackPressed()
    {
    }

}
