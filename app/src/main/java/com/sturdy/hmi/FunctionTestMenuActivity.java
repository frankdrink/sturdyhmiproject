package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeImageButton;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class FunctionTestMenuActivity extends BaseToolBarActivity {
    private Activity mActivity;
    ListView list;
    String[] Menu = {
            "Leakage Test",
            "Helix Test",
            "B&D Test"
    } ;

    String[] Menu_30L = {
            "Leakage Test",
            "B&D Test"

    } ;
    Integer[] imageId = {
            R.drawable.fuction_test_list_icon_leakage_test_normal,
            R.drawable.fuction_test_list_icon_helix_test_normal,
            R.drawable.fuction_test_list_icon_b_d_test_normal
    };
    Integer[] imageId_30L = {
            R.drawable.fuction_test_list_icon_leakage_test_normal,
            R.drawable.fuction_test_list_icon_b_d_test_normal
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterilization_menu);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Function test", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        SteriMenuList listAdapter;
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            listAdapter = new
                    SteriMenuList(this, Menu_30L, imageId_30L);
        }else{
            listAdapter = new
                    SteriMenuList(this, Menu, imageId);
        }
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                    Constants.instance().storeValueInt(saveSelectMenuIdKey, position+10);
                    String programName;
                    if ((Constants.instance().fetchValueInt(preIDControl))==1) {
                        Constants.instance().storeValueString(userLoginCheckPoint, "PreID");
                        startActivity(new Intent(FunctionTestMenuActivity.this, UserLoginActivity.class));
                    }else {
                        Intent intent = new Intent();
                        switch (position) {
                            case 0:
                                Constants.instance().storeValueInt(saveSelectMenuIdKey, position+10);
                                intent.setClass(FunctionTestMenuActivity.this, LeakageTestEditActivity.class);
                                programName = "Leakage Test";
                                Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                intent.putExtra("name", programName);
                                intent.putExtra("temp", "121");
                                GlobalClass.getInstance().Steri_Temp = 121;
                                break;
                            case 1:
                                Constants.instance().storeValueInt(saveSelectMenuIdKey, position+11);
                                intent.setClass(FunctionTestMenuActivity.this, BDTestEditActivity.class);
                                programName = "B&D Test";
                                Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                intent.putExtra("name", programName);
                                intent.putExtra("temp", "134");
                                GlobalClass.getInstance().Steri_Temp = 134;
                                break;

                        }
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    }
                }else{
                    Constants.instance().storeValueInt(saveSelectMenuIdKey, position+10);
                    String programName;
                    if ((Constants.instance().fetchValueInt(preIDControl))==1) {
                        Constants.instance().storeValueString(userLoginCheckPoint, "PreID");
                        startActivity(new Intent(FunctionTestMenuActivity.this, UserLoginActivity.class));
                    }else {
                        Intent intent = new Intent();
                        switch (position) {
                            case 0:
                                intent.setClass(FunctionTestMenuActivity.this, LeakageTestEditActivity.class);
                                programName = "Leakage Test";
                                Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                intent.putExtra("name", programName);
                                intent.putExtra("temp", "121");
                                GlobalClass.getInstance().Steri_Temp = 121;
                                break;
                            case 1:
                                intent.setClass(FunctionTestMenuActivity.this, HelixTestEditActivity.class);
                                programName = "Helix Test";
                                Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                intent.putExtra("name", programName);
                                intent.putExtra("temp", "134");
                                GlobalClass.getInstance().Steri_Temp = 134;
                                break;
                            case 2:
                                intent.setClass(FunctionTestMenuActivity.this, BDTestEditActivity.class);
                                programName = "B&D Test";
                                Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                                intent.putExtra("name", programName);
                                intent.putExtra("temp", "134");
                                GlobalClass.getInstance().Steri_Temp = 134;
                                break;

                        }
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    }
                }
            }
        });
        startShowTitleClock(rightTimeButton);


    }


}
