package com.sturdy.chart.matrix;

import android.graphics.Rect;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.ViewParent;

import com.sturdy.chart.core.base.BaseChart;

/**
 * Created by Frank on 2019/10/10.
 */

public class RotateHelper implements ITouch {

    private int mRadius;

    /**
     * 檢測按下到抬起時旋轉的角度
     */
    private float mTmpAngle;
    /**
     * 檢測按下到抬起時使用的時間
     */
    private long mDownTime;

    /**
     * 判斷是否正在自動滾動
     */
    private boolean isFling;
    /**
     * 佈局時的開始角度
     */
    private double mStartAngle = 0;


    /**
     * 當每秒移動角度達到該值時，認為是快速移動
     */
    private static final int FLINGABLE_VALUE = 300;

    /**
     * 如果移動角度達到該值，則屏蔽點擊
     */
    private static final int NOCLICK_VALUE = 3;

    /**
     * 當每秒移動角度達到該值時，認為是快速移動
     */
    private int mFlingableValue = FLINGABLE_VALUE;

    private Handler mHandler = new Handler();
    /**
     * 記錄上一次的x，y坐標
     */
    private float mLastX;
    private float mLastY;
    private  boolean isRotate;
    private Rect originRect;

    /**
     * 自動滾動的Runnable
     */
    private AutoFlingRunnable mFlingRunnable;

    private OnRotateListener listener;

    public RotateHelper(OnRotateListener listener){
        this.listener = listener;
    }

    @Override
    public boolean handlerTouchEvent(MotionEvent event) {
        if(!isRotate()){
            return false;
        }
        float x = event.getX();
        float y = event.getY();

        // Log.e("TAG", "x = " + x + " , y = " + y);

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:

                mLastX = x;
                mLastY = y;
                mDownTime = System.currentTimeMillis();
                mTmpAngle = 0;

                // 如果當前已經在快速滾動
                if (isFling)
                {
                    // 移除快速滾動的回調
                    mHandler.removeCallbacks(mFlingRunnable);
                    isFling = false;
                    return true;
                }

                break;
            case MotionEvent.ACTION_MOVE:

                /**
                 * 獲得開始的角度
                 */
                float start = getAngle(mLastX, mLastY);
                /**
                 * 獲得當前的角度
                 */
                float end = getAngle(x, y);

                // Log.e("TAG", "start = " + start + " , end =" + end);
                // 如果是一、四象限，則直接end-start，角度值都是正值
                if (getQuadrant(x, y) == 1 || getQuadrant(x, y) == 4)
                {
                    mStartAngle += end - start;
                    mTmpAngle += end - start;
                } else
                // 二、三象限，色角度值是付值
                {
                    mStartAngle += start - end;
                    mTmpAngle += start - end;
                }
                // 重新佈局
                listener.onRotate(mStartAngle);

                mLastX = x;
                mLastY = y;

                break;
            case MotionEvent.ACTION_UP:

                // 計算，每秒移動的角度
                float anglePerSecond = mTmpAngle * 1000
                        / (System.currentTimeMillis() - mDownTime);

                // Log.e("TAG", anglePrMillionSecond + " , mTmpAngel = " +
                // mTmpAngle);

                // 如果達到該值認為是快速移動
                if (Math.abs(anglePerSecond) > mFlingableValue && !isFling)
                {
                    // post一個任務，去自動滾動
                    mHandler.post(mFlingRunnable = new AutoFlingRunnable(anglePerSecond));

                    return true;
                }

                // 如果當前旋轉角度超過NOCLICK_VALUE屏蔽點擊
                if (Math.abs(mTmpAngle) > NOCLICK_VALUE)
                {
                    return true;
                }

                break;
        }
        return true;
    }


    @Override
    public void onDisallowInterceptEvent(BaseChart chart, MotionEvent event){
        ViewParent parent = chart.getParent();
        if (!isRotate() || originRect == null) {
            parent.requestDisallowInterceptTouchEvent(false);
            return;
        }
        switch (event.getAction()&MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:

                if(containsPoint(event)){
                    parent.requestDisallowInterceptTouchEvent(true);
                }else {
                    parent.requestDisallowInterceptTouchEvent(false);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                parent.requestDisallowInterceptTouchEvent(false);
                break;
        }
    }

    public boolean containsPoint(MotionEvent event){
        if(originRect != null) {
            float x = event.getX();
            float y = event.getY();
            int centerY = originRect.centerY();
            int centerX = originRect.centerX();
            return x >= centerX - mRadius && x <= centerX + mRadius
                    && y >= centerY - mRadius && y <= centerY + mRadius;
        }
        return false;
    }

    /**
     * 自動滾動的任務
     *
     * @author zhy
     *
     */
    private class AutoFlingRunnable implements Runnable
    {

        private float angelPerSecond;

        public AutoFlingRunnable(float velocity)
        {
            this.angelPerSecond = velocity;
        }

        public void run()
        {
            // 如果小於20,則停止
            if ((int) Math.abs(angelPerSecond) < 20)
            {
                isFling = false;
                return;
            }
            isFling = true;
            // 不斷改變mStartAngle，讓其滾動，/30為了避免滾動太快
            mStartAngle += (angelPerSecond / 30);
            // 逐漸減小這個值
            angelPerSecond /= 1.0666F;
            mHandler.postDelayed(this, 30);
            // 重新佈局
            listener.onRotate(mStartAngle);
        }
    }



    /**
     * 根據觸摸的位置，計算角度
     *
     * @param xTouch
     * @param yTouch
     * @return
     */
    private float getAngle(float xTouch, float yTouch)
    {
        double x = xTouch- originRect.left - (mRadius );
        double y = yTouch- originRect.top - (mRadius );
        return (float) (Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI);
    }

    /**
     * 根據當前位置計算象限
     *
     * @param x
     * @param y
     * @return
     */
    private int getQuadrant(float x, float y)
    {
        int tmpX = (int) (x- originRect.left - mRadius );
        int tmpY = (int) (y- originRect.top - mRadius );
        if (tmpX >= 0)
        {
            return tmpY >= 0 ? 4 : 1;
        } else
        {
            return tmpY >= 0 ? 3 : 2;
        }

    }


    public interface  OnRotateListener{
        void  onRotate(double angle);
    }


    public void setRadius(int radius) {
        this.mRadius = radius;
    }
    public void setOriginRect(Rect originRect){
        this.originRect = originRect;
    }

    public double getStartAngle() {
        return mStartAngle;
    }

    public boolean isRotate() {
        return isRotate;
    }

    public void setRotate(boolean rotate) {
        isRotate = rotate;
    }
}
