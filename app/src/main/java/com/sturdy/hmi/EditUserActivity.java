package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.hmi.adapter.EditUserListAdapter;
import com.sturdy.hmi.adapter.ModeRadioListAdapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.sql.ReturnMessage;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.RecordModeKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginAccount;
import static com.sturdy.hmi.Constants.userLoginBarCode;
import static com.sturdy.hmi.Constants.userLoginLevel;
import static com.sturdy.hmi.Constants.userLoginPassword;

public class EditUserActivity extends BaseToolBarActivity implements EditUserListAdapter.UpdateMainClass{
    private Activity mActivity;
    private DatabaseHelper mDatabaseHelper;
    private List<User> listUsers;
    private EditUserListAdapter listAdapter;
    ListView list;
    String menuSelectName;
    int menuSelectId;
    String[] web = {
            "Admin"
    } ;
    Integer[] imageId = {
            R.drawable.user_list_icon_top_administrator_normal


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_login);
        Constants.instance(this.getApplicationContext());
        mActivity = this;
        mDatabaseHelper=new DatabaseHelper(mActivity);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Edit User", R.id.topbar_sterilization_right_title, Color.BLACK,38);
//        mNavTopBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
////            }
////        });
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.nav_btn_small_add_normal, R.id.topbar_unwrapped_right_1_button, 64);
        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(EditUserActivity.this,AddAccountActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        menuSelectName = (Constants.instance().fetchValueString(saveSelectMenuNameKey));
        menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
        listUsers = new ArrayList<>();
        listUsers.addAll(mDatabaseHelper.getAllUser());
        if (listUsers.size()>0){
            web = new String[listUsers.size()];
            imageId = new Integer[listUsers.size()];
            for (int i=0;i<listUsers.size();i++) {
                web[i] = listUsers.get(i).getName();
                switch (listUsers.get(i).getUserLevel()) {
                    case 0:
                        imageId[i] =  R.drawable.user_list_icon_top_administrator_normal;
                        break;
                    case 1:
                        imageId[i] =  R.drawable.user_list_icon_administrator_normal;
                        break;
                    case 2:
                        imageId[i] =  R.drawable.user_list_icon_user_normal;
                        break;
                }
            }
        }
        listAdapter = new
                EditUserListAdapter(this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
//                Constants.instance().storeValueString(userLoginAccount,listUsers.get(position).getName());
//                Constants.instance().storeValueInt(userLoginLevel,listUsers.get(position).getUserLevel());
//                Constants.instance().storeValueString(userLoginPassword,listUsers.get(position).getPassword());
//                Intent intent = new Intent();
//                intent.setClass(EditUserActivity.this,EditAccountActivity.class);
//                startActivity(intent);
//                overridePendingTransition(0, 0);
            }
        });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onClickOfUpdateDelete(View view) {
//        ImageButton ib = (ImageButton) view;
//        String[] temp = ib.getTag().toString().split(",");
//        int position = Integer.parseInt(temp[1]);
//        if (temp[0].equals("update")) {
//            Cursor c = (Cursor) listAdapter.getItem(position);
//            BarCode customer = (BarCode) DatabaseHelper.getObjectFromCursor(c, BarCode.class);
//            Toast.makeText(this, customer.getSerialNumber() + " " + customer.getLastName(), Toast.LENGTH_SHORT).show();
////            etFirstName.setText(customer.getFirstName());
////            etLastName.setText(customer.getLastName());
////            lvCustomer.setItemChecked(position, true);
//        } else if (temp[0].equals("delete")) {
//            Cursor c = (Cursor) adapter.getItem(position);
//            BarCode customer = (BarCode) DatabaseHelper.getObjectFromCursor(c, BarCode.class);
//            ReturnMessage rm = helper.getCustomerDao().deleteCustomer(customer.getId());
////            tvMessage.setText(rm.message);
//            if (rm.success) {
////                tvMessage.setTextColor(Color.BLUE);
//                Cursor cursor = helper.getCustomerDao().getCustomers(new BarCode());
//                adapter.changeCursor(cursor);
//            } else {
////                tvMessage.setTextColor(Color.RED);
//            }
//
//        }
    }

    @Override
    public void deleteItemList(final int position) {
        mDatabaseHelper.deleteUser(listUsers.get(position));
        listUsers.clear();
        listUsers.addAll(mDatabaseHelper.getAllUser());
        if (listUsers.size()>0){
            web = new String[listUsers.size()];
            imageId = new Integer[listUsers.size()];
            for (int i=0;i<listUsers.size();i++) {
                web[i] = listUsers.get(i).getName();
                switch (listUsers.get(i).getUserLevel()) {
                    case 0:
                        imageId[i] =  R.drawable.user_list_icon_top_administrator_normal;
                        break;
                    case 1:
                        imageId[i] =  R.drawable.user_list_icon_administrator_normal;
                        break;
                    case 2:
                        imageId[i] =  R.drawable.user_list_icon_user_normal;
                        break;
                }
            }
        }
        listAdapter = new
                EditUserListAdapter(this, web, imageId);
        list.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateItemList(int position) {
        Constants.instance().storeValueString(userLoginAccount,listUsers.get(position).getName());
        Constants.instance().storeValueInt(userLoginLevel,listUsers.get(position).getUserLevel());
        Constants.instance().storeValueString(userLoginPassword,listUsers.get(position).getPassword());
        Constants.instance().storeValueString(userLoginBarCode,listUsers.get(position).getBarcode());

        Intent intent = new Intent();
        intent.setClass(EditUserActivity.this,EditAccountActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

}
