package com.sturdy.chart.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;

import com.sturdy.chart.component.base.IChartTitle;
import com.sturdy.chart.component.base.PercentComponent;
import com.sturdy.chart.data.style.FontStyle;

/**
 * 繪製標題
 */

public class ChartTitle extends PercentComponent<String> implements IChartTitle {

    /**
     * 圖表標題最大占比
     */
    private static final float MAX_PERCENT =0.4f;
    /**
     * 標題字體樣式
     */
    private FontStyle fontStyle= new FontStyle();

    private Path path = new Path();

    /**
     * 設置標題占比
     * @param percent 百分比
     */
    @Override
    public void setPercent(float percent) {
        if(percent > MAX_PERCENT){
            percent = MAX_PERCENT;
        }
        super.setPercent(percent);
    }

    /**
     * 繪製標題
     * <p>通過設置標題方位繪製標題</p>
     * @param canvas 畫布
     * @param chartName 圖表標題
     * @param paint 畫筆
     */
    @Override
    public void draw(Canvas canvas, String chartName, Paint paint) {
        fontStyle.fillPaint(paint);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        paint.setTextAlign(Paint.Align.LEFT);
        float textHeight = fontMetrics.descent - fontMetrics.ascent;
        int textWidth = (int)paint.measureText(chartName);
        Rect rect = getRect();
        int startY = rect.centerY();
        int startX = rect.centerX();
        path.rewind();
        switch (direction) {
            case TOP:
            case BOTTOM:
                startY-= textHeight/2;
                startX -=textWidth/2;
                canvas.drawText(chartName, startX, startY, paint);
                break;
            case LEFT:
            case RIGHT:
                path.moveTo(startX,rect.top);
                path.lineTo(startX,rect.bottom);
                canvas.drawTextOnPath(chartName,path,(rect.height()-textWidth)/2,0,paint);
                break;
        }
    }

    /**
     * 獲取標題字體樣式
     * @return 標題字體樣式
     */
    public FontStyle getFontStyle() {
        return fontStyle;
    }
    /**
     * 設置標題字體樣式
     * @param  fontStyle 標題字體樣式
     */
    public void setFontStyle(FontStyle fontStyle) {
        this.fontStyle = fontStyle;
    }
}
