package com.sturdy.chart.listener;


public interface OnChartChangeListener {

    void onTableChanged(float translateX, float translateY);
}
