package com.sturdy.chart.component.axis;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.Gravity;

import com.sturdy.chart.component.base.IAxis;
import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.format.IFormat;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.data.ScaleData;
import com.sturdy.chart.data.style.FontStyle;
import com.sturdy.chart.data.style.LineStyle;
import com.sturdy.chart.matrix.MatrixHelper;


/**
 * 基本軸 負責軸的計算方位和繪製 Copyright By Frank
 */

public abstract class BaseAxis<V> implements IAxis<V> {

    /**
     * 軸線樣式
     */
    protected LineStyle axisStyle = new LineStyle();
    /**
     * 刻度樣式
     */
    protected FontStyle scaleStyle = new FontStyle();
    /**
     * 網格樣式
     */
    protected LineStyle gridStyle = new LineStyle(); //網格樣式
    /**
     * 是否繪製網格
     */
    protected boolean isDrawGrid; //是否繪製網格
    /**
     * 是否顯示軸線
     */
    protected boolean isShowAxisLine = true;
    /**
     * 軸文字位置
     */
    protected int gravity = Gravity.CENTER;
    /**
     * 軸方位（上下左右，橫向軸可以設置上下，豎軸可以設置左右）
     */
    protected int direction;
    /**
     * 是否是線性圖
     * 因為線性圖一般從0開始繪製軸文字，而柱狀圖從每個間隔中間開始繪製
     */
    protected boolean isLine;
    /**
     * 文字格式化
     */
    private IFormat<V> format;
    /**
     * 是否顯示軸
     */
    private boolean isDisplay = true;

    /**
     *設置是否繪製網格
     * @param drawGrid 是否繪製
     */
    public void setDrawGrid(boolean drawGrid) {
        isDrawGrid = drawGrid;
    }

    /**
     * 獲取軸樣式
     * 默認賦予樣式，可以直接get來使用
     * @return 軸樣式
     */
    public LineStyle getAxisStyle() {
        return axisStyle;
    }

    /**
     * 設置軸樣式
     * 如果你對LineStyle提供的樣式不滿意，可以自定義新的LineStyle
     * @param axisStyle 軸樣式
     */
    public void setAxisStyle(LineStyle axisStyle) {
        this.axisStyle = axisStyle;
    }

    /**
     * 獲取刻度字體樣式
     *  默認賦予樣式，可以直接get來使用
     * @return 刻度字體樣式
     */
    public FontStyle getScaleStyle() {
        return scaleStyle;
    }

    /**
     * 設置刻度字體樣式
     * 如果你對FontStyle提供的樣式不滿意，可以自定義新的FontStyle
     * @param scaleStyle 刻度字體樣式
     */
    public void setScaleStyle(FontStyle scaleStyle) {
        this.scaleStyle = scaleStyle;
    }

    /**
     * 獲取網格樣式
     * @return 網格樣式
     */
    public LineStyle getGridStyle() {
        return gridStyle;
    }

    /**
     * 設置網格樣式
     * @param gridStyle 網格樣式
     */
    public void setGridStyle(LineStyle gridStyle) {
        this.gridStyle = gridStyle;
    }

    /**
     * 設置軸偏移方位
     * @param gravity 偏移方位
     */
    public void setGravity(int gravity) {
        this.gravity = gravity;
    }


    /**
     * 是否是線性圖表
     * 因為線性圖一般從0開始繪製軸文字，而柱狀圖從每個間隔中間開始繪製
     * @param isLine 是否是線性圖表
     */
    public void isLine(boolean isLine) {
        this.isLine = isLine;
    }

    /**
     * 是否顯示軸
     * @return 是否顯示軸
     */
    public boolean isDisplay() {
        return isDisplay;
    }


    /**
     * 繪製軸
     * 判斷是否需要繪製以及調用繪製軸刻度和繪製軸線
     * @param canvas 畫布
     * @param rect  繪製範圍
     * @param helper 縮放移動輔助類
     * @param paint  畫筆
     * @param chartData 圖表數據
     */
    @Override
    public void draw(Canvas canvas, Rect rect, MatrixHelper helper, Paint paint, ChartData<? extends BarData> chartData) {
        if(isDisplay) {
            drawScale(canvas, rect, helper, paint, chartData);
            drawAxis(canvas, rect, paint, chartData);
        }
    }

    /**
     * 繪製軸刻度
     * @param canvas 畫布
     * @param rect 繪製範圍
     * @param helper 縮放移動輔助類
     * @param paint 畫筆
     * @param chartData 圖表數據
     */
    protected void drawScale(Canvas canvas, Rect rect, MatrixHelper helper, Paint paint, ChartData<? extends BarData> chartData) {
        ScaleData scaleData = chartData.getScaleData();
        Rect clipRect = new Rect(rect);
        Rect scaleRect = scaleData.scaleRect;
        Rect zoomRect = helper.getZoomProviderRect(scaleData.getOffsetRect(new Rect(rect), scaleRect));

        chartData.getScaleData().zoom = helper.getZoom();

        if (direction == AxisDirection.BOTTOM || direction == AxisDirection.TOP) {
            zoomRect.top = rect.top;
            zoomRect.bottom = rect.bottom;
            clipRect.left = rect.left + scaleRect.left;
            clipRect.right = rect.right - scaleRect.right;
        } else {
            zoomRect.left = rect.left;
            zoomRect.right = rect.right;
            clipRect.top = rect.top + scaleRect.top;
            clipRect.bottom = rect.bottom - scaleRect.bottom;
        }
        drawScale(canvas, zoomRect, clipRect, paint, chartData);
    }

    /**
     * 繪製軸線
     * @param canvas 畫布
     * @param rect 繪製範圍
     * @param paint 畫筆
     * @param chartData 圖表數據
     */
    protected void drawAxis(Canvas canvas, Rect rect, Paint paint,  ChartData<? extends BarData> chartData) {
        if(isShowAxisLine) {
            Rect scaleRect = chartData.getScaleData().scaleRect;
            axisStyle.fillPaint(paint);
            int[] r = calculation(rect, scaleRect);
            Path path = new Path();
            path.moveTo(r[0],r[1]);
            path.lineTo(r[2],r[3]);
            canvas.drawPath(path, paint);
        }

    }

    /**
     * 計算軸範圍
     * @param rect 原始範圍
     * @param scaleRect 縮放範圍
     * @return 軸上下左右的大小
     */
    protected abstract int[] calculation(Rect rect, Rect scaleRect);


    /**
     * 提供給子類繪製刻度抽像方法
     * @param canvas 畫布
     * @param rect  原始範圍
     * @param clipRect 裁切軸之後範圍
     * @param paint  畫筆
     * @param chartData 圖表數據
     */
    protected abstract void drawScale(Canvas canvas, Rect rect, Rect clipRect, Paint paint,ChartData<? extends BarData> chartData);

    /**
     * 獲取文字格式化
     * @return 文字格式化
     */
    public IFormat<V> getFormat() {
        return format;
    }

    /**
     * 設置文字格式化
     * @param format 文字格式化
     */
    @Override
    public void setFormat(IFormat<V> format) {
        this.format = format;
    }

    /**
     * 是否顯示軸線
     * @return 是否顯示軸線
     */
    public boolean isShowAxisLine() {
        return isShowAxisLine;
    }
    /**
     * 設置是否顯示軸線
     * @param showAxisLine 是否顯示軸線
     */
    public void setShowAxisLine(boolean showAxisLine) {
        isShowAxisLine = showAxisLine;
    }
    /**
     * 設置是否顯示軸
     * @param isShow 是否顯示軸
     */
    @Override
    public void setDisplay(boolean isShow) {
        this.isDisplay = isShow;
    }
}
