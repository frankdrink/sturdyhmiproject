package com.sturdy.chart.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sturdy.chart.component.base.IEmpty;
import com.sturdy.chart.data.style.FontStyle;

/**
 * 空白提示
 */

public class EmptyView implements IEmpty {
    /**
     * 空白提示字體樣式
     */
    private FontStyle fontStyle = new FontStyle();
    /**
     * 空白文字
     */
    private String emptyTip = "No Data";
    /**
     * 空白範圍
     */
    private Rect rect;

    /**
     * 繪製空白
     * @param canvas 畫布
     * @param paint 畫筆
     */
    @Override
    public void draw(Canvas canvas, Paint paint) {
        draw(canvas,emptyTip,paint);
    }

    /**
     * 獲取空白字體樣式
     * @return 空白字體樣式
     */
    public FontStyle getFontStyle() {
        return fontStyle;
    }
    /**
     * 設置空白字體樣式
     * @param fontStyle 空白字體樣式
     */
    public void setFontStyle(FontStyle fontStyle) {
        this.fontStyle = fontStyle;
    }
    /**
     * 設置空白提示
     * @return   空白提示
     */
    @Override
    public String getEmptyTip() {
        return emptyTip;
    }
    /**
     * 獲取空白提示
     * @param   emptyTip 空白提示
     */
    @Override
    public void setEmptyTip(String emptyTip) {
        this.emptyTip = emptyTip;
    }

    /**
     * 計算空白大小
     * <p>因為空白提示是佔整個圖表不需要計算</>
     * @param chartRect 圖表範圍
     */
    @Override
    public void computeRect(Rect chartRect) {
        rect = chartRect;
    }

    /**
     * 繪製空白
     * @param canvas 畫布
     * @param emptyTip 空白提示
     * @param paint 畫筆
     */
    @Override
    public void draw(Canvas canvas, String emptyTip, Paint paint) {
        fontStyle.fillPaint(paint);
        paint.setTextAlign(Paint.Align.LEFT);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        int textHeight = (int) (fontMetrics.descent - fontMetrics.ascent);
        int textWidth = (int) paint.measureText(emptyTip);
        canvas.drawText(emptyTip,rect.left +(rect.right-rect.left-textWidth)/2,rect.top+(rect.bottom - rect.top-textHeight)/2,paint);
    }
}
