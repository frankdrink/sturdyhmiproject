package com.sturdy.hmi.model;

/**
 * Created by lalit on 9/12/2016.
 */
public class User {

    private int id;
    private String name;
    private int level;
    private String password;
    private String barcode;
    private int order;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserLevel() {
        return level;
    }

    public int getUserOrder() {
        return order;
    }


    public void setUserLevel(int level) {
        this.level = level;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setUserOrder(int order) {
        this.order = order;
    }

}
