package com.sturdy.hmi;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.ModeRadioListAdapter;
import com.sturdy.hmi.adapter.UnitRadioListAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;

import static com.sturdy.hmi.Constants.RecordModeKey;
import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class RecordModeActivity extends BaseToolBarActivity implements ModeRadioListAdapter.UpdateMainClass{
    private Activity mActivity;
    ModeRadioListAdapter adapter1;
    int saveRecordMode;
    List<String> data1;
    List<String> data2;

    List<MainMenuSettingElement> settingElementList;
    Handler handler;

    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_recordmode);
        mActivity = this;
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Mode", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        data1 = new ArrayList<String>();
        data1.add("Standard");
//        data1.add("Detailed only sterilization step");
        data1.add("Detailed all cycle");
        data2 = new ArrayList<String>();
        data2.add("Sterilization phase recorded every minute, up to 2000.");
//        data2.add("Sterilization phase recorded every second, up to 100.\n");
        data2.add("All cycle recorded every second, up to 30.");

        saveRecordMode = Constants.instance().fetchValueInt(RecordModeKey);
        adapter1 = new ModeRadioListAdapter(this, data1,data2, "recordmode", saveRecordMode);
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void updateItemList(final String itemName) {
        if (itemName.equals("mode0")) {
            saveRecordMode = 0;
            Constants.instance().storeValueInt(RecordModeKey,saveRecordMode);
        }else if (itemName.equals("mode1")) {
            saveRecordMode = 1;
            Constants.instance().storeValueInt(RecordModeKey,saveRecordMode);
        } if (itemName.equals("mode2")) {
            saveRecordMode = 2;
            Constants.instance().storeValueInt(RecordModeKey,saveRecordMode);
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {

    }

}
