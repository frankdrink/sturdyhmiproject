package com.sturdy.hmi;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.EditListAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import android.provider.Settings.System;

import static com.sturdy.hmi.Constants.AutoAddWaterKey;
import static com.sturdy.hmi.Constants.BrightnessKey;
import static com.sturdy.hmi.Constants.BuzzerVolumeKey;
import static com.sturdy.hmi.Constants.ExhaustTempKey;
import static com.sturdy.hmi.Constants.HMI_SLEEP_BROADCAST;
import static com.sturdy.hmi.Constants.PipeheaterKey;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.SleepTimeKey;
import static com.sturdy.hmi.Constants.SoundKey;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.savePresFormat;

public class PowerSettingActivity extends BaseToolBarActivity implements EditListAdapter.UpdateMainClass{
    private Activity mActivity;
    EditListAdapter adapter1;
    List<String> data1;
    Handler handler;
    ListView list1;
    String[] title_01 = {
            "Time to sleep",
            "Brightness"
            //"Buzzer volume"
    } ;
    Integer[] imageId_01 = {
            R.drawable.setting_icon_power_saving,
            R.drawable.setting_icon_power_brightness
            //R.drawable.setting_icon_power_siren
    };

    ListView list;
    List<Boolean> listIsChecked;    // 這個用來記錄哪幾個 item 是被打勾的
    String[] web = {
            "Buzzer volume"

    } ;
    Integer[] imageId = {
            R.drawable.setting_icon_power_siren
    };

    private String[] pickerTimeArrayList;
    private ContentResolver cResolver;
    int BrightnessValue;
    int SoundValue;
    String valueStr;


    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_powersetting);
        mActivity = this;
        pickerTimeArrayList = new String[]{
                "10 mins",  "20 mins", "30 mins", "60 mins", "120 mins", "Never"
        };
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Power Management", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        try {
            BrightnessValue = System.getInt(cResolver, System.SCREEN_BRIGHTNESS);
            BrightnessValue= (int)(BrightnessValue /(float)255)*100;
        } catch (Exception e) {
            // TODO: handle exception
            BrightnessValue = Constants.instance().fetchValueInt(BrightnessKey,100);
        }
//        maxSelectTextView = (TextView) findViewById(R.id.max_program_view);
        SoundValue = Constants.instance().fetchValueInt(SoundKey,100);
        int sleepIndex = 5;
        switch(Constants.instance().fetchValueInt(SleepTimeKey,0)){
            case 0:
                sleepIndex = 5;
                break;
            case 10:
                sleepIndex = 0;
                break;
            case 20:
                sleepIndex = 1;
                break;
            case 30:
                sleepIndex = 2;
                break;
            case 60:
                sleepIndex = 3;
                break;
            case 120:
                sleepIndex = 4;
                break;
        }
        data1 = new ArrayList<String>();
        data1.add(pickerTimeArrayList[sleepIndex]);
        valueStr = Integer.toString(BrightnessValue)+" %";
        data1.add(valueStr);
//        valueStr = Integer.toString(SoundValue)+" %";
//        data1.add(valueStr);

        adapter1 = new EditListAdapter(this,title_01, data1, imageId_01);
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();


        final boolean[] listChecked = new boolean[web.length+1];
        listIsChecked = new ArrayList<Boolean>();
        boolean isTrue;

        for(int x=0;x<web.length;x++)
        {
            switch(x) {
                case 0:
                    isTrue = Constants.instance().fetchValueInt(BuzzerVolumeKey) > 0 ? true : false;
                    listIsChecked.add(isTrue);
                    listChecked[x] = isTrue;
                    break;
            }

        }
        final MainSettingListAdapter listAdapter = new
                MainSettingListAdapter(this, web, imageId, listChecked);
        list=(ListView)findViewById(R.id.list2);
        list.setClickable(false);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SwitchButton switchButton=(SwitchButton)view.findViewById(R.id.menu_setting_swich);
                boolean isChecked = !listChecked[position];//switchButton.isChecked();
                switchButton.setChecked(isChecked);
                listIsChecked.set(position,isChecked);
                listChecked[position] = isChecked;
                listAdapter.setChecked(listChecked);

                switch (position){
                    case 0:
                        Constants.instance().storeValueInt(BuzzerVolumeKey,isChecked?1 : 0);
                        break;

                }

            }
        });
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
    }

    public void ShowBrightnessDialog()
    {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final SeekBar seek = new SeekBar(this);
        final int[] seekProgress = new int[1];
        seek.setMax(100);
        seek.setProgress(BrightnessValue);
        popDialog.setIcon(R.drawable.setting_icon_power_brightness);
        popDialog.setTitle("Brightness");
        popDialog.setView(seek);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
//                txtView.setText("Value of : " + progress);
                BrightnessValue = progress;
                float b = (float)progress/(float)100*(float)255;
                saveScreenBrightness((int)b);
                valueStr = Integer.toString(progress) + " %";
                data1.set(1,valueStr);
                adapter1.resetAdapter(data1);
                adapter1.notifyDataSetChanged();
                Constants.instance().storeValueInt(BrightnessKey,BrightnessValue);
            }
            public void onStartTrackingTouch(SeekBar arg0) {
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Brightness")
                .setMessage(" ")
                .setPositiveButton("OK",null)
//                .setNegativeButton("cancel",null)
                .create();
        dialog.setView(seek);

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLUE);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(32);

//        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);


//        popDialog.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        Constants.instance().storeValueInt(BrightnessKey,BrightnessValue);
//                        dialog.dismiss();
//                    }
//                });
//        popDialog.create();
//        popDialog.show();
    }

    /**
     * 設定當前螢幕亮度值  0--255
     */
    private void saveScreenBrightness(int paramInt) {
        try {
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, paramInt);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

//    public void ShowSoundDialog()
//    {
//        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
//        final SeekBar seek = new SeekBar(this);
//        seek.setMax(100);
//        seek.setProgress(SoundValue);
//        popDialog.setIcon(R.drawable.setting_icon_power_siren);
//        popDialog.setTitle("Buzzer");
//        popDialog.setView(seek);
//        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
//                SoundValue = progress;
//                valueStr = Integer.toString(progress) + " %";
//                data1.set(2,valueStr);
//                adapter1.resetAdapter(data1);
//                adapter1.notifyDataSetChanged();
//            }
//            public void onStartTrackingTouch(SeekBar arg0) {
//            }
//            public void onStopTrackingTouch(SeekBar seekBar) {
//            }
//        });
//        popDialog.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        Constants.instance().storeValueInt(SoundKey,SoundValue);
//                        dialog.dismiss();
//                    }
//                });
//        popDialog.create();
//        popDialog.show();
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        char[] f3_data = new char[8];
        f3_data[0] = 0x07;
        f3_data[1] = 0xf3;
        f3_data[2] = Constants.instance().fetchValueInt(AutoAddWaterKey) > 0?(char)1:0;
        f3_data[3] = Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0?(char)1:0;
        f3_data[4] = Constants.instance().fetchValueInt(ExhaustTempKey) > 0?(char)1:0;
        f3_data[5] = Constants.instance().fetchValueInt(VacuumpumpKey) > 0?(char)1:0;
        f3_data[6] = Constants.instance().fetchValueInt(PipeheaterKey) > 0?(char)1:0;
        f3_data[7] = Constants.instance().fetchValueInt(BuzzerVolumeKey) > 0?(char)1:0;

        sendToService(f3_data);
        super.onDestroy();

    }

    @Override
    public void updateItemList(final int itemPostion) {
        if (itemPostion==0) {
            int sleepIndex = 5;
            switch(Constants.instance().fetchValueInt(SleepTimeKey,0)){
                case 0:
                    sleepIndex = 5;
                    break;
                case 10:
                    sleepIndex = 0;
                    break;
                case 20:
                    sleepIndex = 1;
                    break;
                case 30:
                    sleepIndex = 2;
                    break;
                case 60:
                    sleepIndex = 3;
                    break;
                case 120:
                    sleepIndex = 4;
                    break;
            }
            OptionPicker picker = new OptionPicker(PowerSettingActivity.this, pickerTimeArrayList);
            picker.setSelectedIndex(sleepIndex);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
//                        showToast("index=" + index + ", item=" + item);
                    String str = pickerTimeArrayList[index];
                    data1.set(0,str);
                    adapter1.resetAdapter(data1);
                    adapter1.notifyDataSetChanged();
                    str = str.replaceAll("[^\\d]+", "");
                    try{
                        Constants.instance().storeValueInt(SleepTimeKey,Integer.valueOf(str));
                        Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
                        sendBroadcast(broadcast);
                    }catch (Exception e){
                        Constants.instance().storeValueInt(SleepTimeKey,10000);
                        Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
                        sendBroadcast(broadcast);
                    }
                }
            });
            picker.show();
        } else if (itemPostion==1) {
            ShowBrightnessDialog();
        }
//        } else if (itemPostion==2) {
//            ShowSoundDialog();
//        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {

    }


}
