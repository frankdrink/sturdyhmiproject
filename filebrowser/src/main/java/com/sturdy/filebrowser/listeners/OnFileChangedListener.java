package com.sturdy.filebrowser.listeners;

import java.io.File;

/**
 * Created by sturdy on 4/18/2017.
 */
public interface OnFileChangedListener {
    void onFileChanged(File updatedFile);
}
