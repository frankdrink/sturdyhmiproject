package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.hmi.Dialog.LoginNoticeDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.Keyboard.Keyboard;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import java.util.ArrayList;
import java.util.List;

import static android.view.KeyEvent.KEYCODE_0;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginAccount;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;
import static com.sturdy.hmi.Constants.userLoginLevel;
import static com.sturdy.hmi.Constants.userLoginPassword;

public class UserLoginActivity extends BaseToolBarActivity {
    private Activity mActivity;
    private DatabaseHelper mDatabaseHelper;
    private List<User> listUsers;
    ListView list;
    private String loginCheckPoint;
    String menuSelectName;
    int menuSelectId;
    private String keyinString = "";

    String[] web = {
            "Admin"
    } ;
    Integer[] imageId = {
            R.drawable.user_list_icon_top_administrator_normal


    };

    private static final String[] KEY = new String[] {
            "1", "2", "3",
            "4", "5", "6",
            "7", "8", "9",
            "<<", "0", "Done"
    };

    private Keyboard keyboard;
    private String keyInPassword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_login);
        Constants.instance(this.getApplicationContext());
        mActivity = this;
        mDatabaseHelper=new DatabaseHelper(mActivity);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loginCheckPoint = (Constants.instance().fetchValueString(userLoginCheckPoint));
        String navTitle = "";
        if (loginCheckPoint.equals("PreID")) {
            navTitle = "Pre ID control";
        } else if (loginCheckPoint.equals("PostID")) {
            navTitle = "Post ID control";
        } else if (loginCheckPoint.equals("UserManagement")) {
            navTitle = "User Management";
        }
        Button mNavTitleView = mNavTopBar.addLeftTextButton(navTitle, R.id.topbar_sterilization_right_title, Color.BLACK,38);
//        mNavTopBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
////            }
////        });
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        menuSelectName = (Constants.instance().fetchValueString(saveSelectMenuNameKey));
        menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
        listUsers = new ArrayList<>();
        listUsers.addAll(mDatabaseHelper.getAllUser());
        if (listUsers.size()>0){
            web = new String[listUsers.size()];
            imageId = new Integer[listUsers.size()];
            for (int i=0;i<listUsers.size();i++) {
                web[i] = listUsers.get(i).getName();
                switch (listUsers.get(i).getUserLevel()) {
                    case 0:
                        imageId[i] =  R.drawable.user_list_icon_top_administrator_normal;
                        break;
                    case 1:
                        imageId[i] =  R.drawable.user_list_icon_administrator_normal;
                        break;
                    case 2:
                        imageId[i] =  R.drawable.user_list_icon_user_normal;
                        break;
                }
            }
        }
        SteriMenuList listAdapter = new
                SteriMenuList(this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Constants.instance().storeValueString(userLoginAccount,listUsers.get(position).getName());
                Constants.instance().storeValueInt(userLoginLevel,listUsers.get(position).getUserLevel());
                Constants.instance().storeValueString(userLoginPassword,listUsers.get(position).getPassword());
                GlobalClass.getInstance().cLoginUserID = (char)listUsers.get(position).getUserOrder();
                Intent intent = new Intent();
//                switch (menuSelectId){
//                    case 0:
//                        intent.setClass(UserLoginActivity.this,UnwrappedEditActivity.class);
//                        intent.putExtra("name","Unwrapped 121");
//                        intent.putExtra("temp","121");
//                        break;
//                    case 1:
//                        intent.setClass(UserLoginActivity.this,UnwrappedEditActivity.class);
//                        intent.putExtra("name","Unwrapped 134");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 2:
//                        intent.setClass(UserLoginActivity.this,WrappedEditActivity.class);
//                        intent.putExtra("name","Wrapped 121");
//                        intent.putExtra("temp","121");
//                        break;
//                    case 3:
//                        intent.setClass(UserLoginActivity.this,WrappedEditActivity.class);
//                        intent.putExtra("name","Wrapped 134");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 4:
//                        intent.setClass(UserLoginActivity.this,InstrumentEditActivity.class);
//                        intent.putExtra("name","Instrument 121");
//                        intent.putExtra("temp","121");
//                        break;
//                    case 5:
//                        intent.setClass(UserLoginActivity.this,InstrumentEditActivity.class);
//                        intent.putExtra("name","Instrument 134");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 6:
//                        intent.setClass(UserLoginActivity.this,PrionEditActivity.class);
//                        intent.putExtra("name","Prion");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 7:
//                        intent.setClass(UserLoginActivity.this,FlashEditActivity.class);
//                        intent.putExtra("name","Flash");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 8:
//                        intent.setClass(UserLoginActivity.this,DryEditActivity.class);
//                        intent.putExtra("name","Dry");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 9:
//                        intent.setClass(UserLoginActivity.this,CustomizationEditActivity.class);
//                        intent.putExtra("name","Customization");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 10:
//                        intent.setClass(UserLoginActivity.this,UnwrappedEditActivity.class);
//                        intent.putExtra("name","Leakage test");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 11:
//                        intent.setClass(UserLoginActivity.this,UnwrappedEditActivity.class);
//                        intent.putExtra("name","Helix test");
//                        intent.putExtra("temp","134");
//                        break;
//                    case 12:
//                        intent.setClass(UserLoginActivity.this,UnwrappedEditActivity.class);
//                        intent.putExtra("name","B&D test");
//                        intent.putExtra("temp","134");
//                        break;
//                }
                intent.setClass(UserLoginActivity.this,UserLoginPasswordActivity.class);

                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        keyboard = (Keyboard) findViewById(R.id.KeyboardView_pay);
//        keyboard.setVisibility(View.GONE);
        keyboard.setKeyboardKeys(KEY);
        keyboard.setOnClickKeyboardListener(new Keyboard.OnClickKeyboardListener() {
            @Override
            public void onKeyClick(int position, String value) {
                if (position < 11 && position != 9) {
                    keyInPassword += value;
                } else if (position == 9) {
                    keyInPassword = "";
                }else if (position == 11) {
                    if (keyInPassword.length()>0) {
//                        Toast.makeText(getApplication(), keyInPassword, Toast.LENGTH_SHORT).show();
                        boolean bFind=false;
                        for (int i=0;i<listUsers.size();i++) {
                            String barCode = listUsers.get(i).getBarcode();
                            if (keyInPassword.equals(barCode)) {
                                bFind = true;
                                new LoginNoticeDialog(mActivity)
                                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                        .setAnimationEnable(true)
                                        .setTitleText("success")
                                        .setContentText("'"+listUsers.get(i).getName()+"' Recognized", Gravity.CENTER)
                                        .setConfirmListener("Confirm", new LoginNoticeDialog.OnConfirmListener() {
                                            @Override
                                            public void onClick(LoginNoticeDialog dialog) {
                                                quicklyLoing();
                                                dialog.dismiss();
                                            }
                                        }).show();
                                break;
                            }
                        }
                        if (!bFind) {
                            new LoginNoticeDialog(mActivity)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("fail")
                                    .setContentText("Unrecognized", Gravity.CENTER)
                                    .setConfirmListener("Confirm", new LoginNoticeDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(LoginNoticeDialog dialog) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    }
                    keyInPassword = "";
                }
            }
        });
    }

    void quicklyLoing() {
        String programName;
        Intent intent = new Intent();
        if (loginCheckPoint.equals("PreID")) {
            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                switch (menuSelectId) {
                    case 0:
                        intent.setClass(mActivity, Liquid1EditActivity.class);
                        programName = "Liquid 1";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 1:
                        intent.setClass(mActivity, Liquid2EditActivity.class);
                        programName = "Liquid 2";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 2:
                        intent.setClass(mActivity, Solid1EditActivity.class);
                        programName = "Solid 1";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 3:
                        intent.setClass(mActivity, Solid2EditActivity.class);
                        programName = "Solid 2";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 4:
                        intent.setClass(mActivity, AgarEditActivity.class);
                        programName = "Agar";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 5:
                        intent.setClass(mActivity, DissolutionEditActivity.class);
                        programName = "Dissolution";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "60");
                        GlobalClass.getInstance().Steri_Temp = 60;
                        break;
                    case 6:
                        intent.setClass(mActivity, DryOnlyEditActivity.class);
                        programName = "Dry Only";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 7:
                        intent.setClass(mActivity, WasteEditActivity.class);
                        programName = "Waste";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 8:
                        intent.setClass(mActivity, User1EditActivity.class);
                        programName = "User 1";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 9:
                        intent.setClass(mActivity, User2EditActivity.class);
                        programName = "User 2";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 10:
                        intent.setClass(mActivity, LeakageTestEditActivity.class);
                        programName = "Leakage Test";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 13:
                        intent.setClass(mActivity, LatexEditActivity.class);
                        programName = "Latex";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name", programName);
                        intent.putExtra("temp", "121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                }
            }else{
                switch (menuSelectId){
                    case 0:
                        intent.setClass(mActivity,UnwrappedEditActivity.class);
                        programName = "Unwrapped 121";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 1:
                        intent.setClass(mActivity,UnwrappedEditActivity.class);
                        programName = "Unwrapped 134";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 2:
                        intent.setClass(mActivity,WrappedEditActivity.class);
                        programName = "Wrapped 121";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 3:
                        intent.setClass(mActivity,WrappedEditActivity.class);
                        programName = "Wrapped 134";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 4:
                        intent.setClass(mActivity,InstrumentEditActivity.class);
                        programName = "Instrument 121";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","121");
                        GlobalClass.getInstance().Steri_Temp = 121;
                        break;
                    case 5:
                        intent.setClass(mActivity,InstrumentEditActivity.class);
                        programName = "Instrument 134";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 6:
                        intent.setClass(mActivity,PrionEditActivity.class);
                        programName = "Prion";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 7:
                        intent.setClass(mActivity,FlashEditActivity.class);
                        programName = "Flash";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;

                        break;
                    case 8:
                        intent.setClass(mActivity,DryEditActivity.class);
                        programName = "Dry";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;

                        break;
                    case 9:
                        intent.setClass(mActivity,CustomizationEditActivity.class);
                        programName = "Customization";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;

                        break;
                    case 10:
                        intent.setClass(mActivity,LeakageTestEditActivity.class);
                        programName = "Leakage test";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 11:
                        intent.setClass(mActivity,HelixTestEditActivity.class);
                        programName = "Helix test";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                    case 12:
                        intent.setClass(mActivity,BDTestEditActivity.class);
                        programName = "B&D test";
                        Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                        intent.putExtra("name",programName);
                        intent.putExtra("temp","134");
                        GlobalClass.getInstance().Steri_Temp = 134;
                        break;
                }
            }
        } else if (loginCheckPoint.equals("PostID")) {
            char[] data = new char[]{0x01, 0x60};
            sendToService(data);
            String printData = "--------------------------------\n";
            sendToServicePrinter(printData.toCharArray(),printData);
            saveDtlToCache(printData.toCharArray());
            printData = String.format("Post ID:\n%s\n"
                    , Constants.instance().fetchValueString(userLoginAccount));
            sendToServicePrinter(printData.toCharArray(),printData);
            saveDtlToCache(printData.toCharArray());
            intent.setClass(UserLoginActivity.this,DoorOpenActivity.class);
        } else if (loginCheckPoint.equals("UserManagement")) {
            intent.setClass(UserLoginActivity.this,UserManagementSettingActivity.class);
        }  else if (loginCheckPoint.equals("MaintainMenu")) {
            intent.setClass(UserLoginActivity.this,MaintainMenuActivity.class);
        }


        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
         if (allowKeyCode(e.getKeyCode())){
            if(e.getAction()==KeyEvent.ACTION_DOWN){
                char pressedKey = (char) e.getUnicodeChar();
                if (e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
                    keyInPassword += pressedKey;
                }
            }
            if (e.getAction()==KeyEvent.ACTION_DOWN && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                Toast.makeText(getApplicationContext(), keyInPassword, Toast.LENGTH_SHORT).show();
                if (keyInPassword.length()>0) {
                    for (int i=0;i<listUsers.size();i++) {
                        String barCode = listUsers.get(i).getBarcode();
                        if (keyInPassword.equals(barCode)) {
                            quicklyLoing();
                            break;
                        }
                    }
                }
                keyInPassword="";
                return false;
            }

            return super.dispatchKeyEvent(e);
        }
        return false;
    }

    private boolean allowKeyCode(int keyCode) {
        if (keyCode>=KeyEvent.KEYCODE_A && keyCode<= KeyEvent.KEYCODE_Z)
            return true;
        if (keyCode>=KeyEvent.KEYCODE_0 && keyCode<= KeyEvent.KEYCODE_9)
            return true;
        if (keyCode==KeyEvent.KEYCODE_ENTER)
            return true;
        return false;
    }


}
