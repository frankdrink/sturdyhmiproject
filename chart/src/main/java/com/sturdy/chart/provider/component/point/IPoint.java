package com.sturdy.chart.provider.component.point;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Frank on 2019/10/20.
 */

public interface IPoint {

     void drawPoint(Canvas canvas, float x, float y, int position, boolean isShowDefaultColor, Paint paint);


}
