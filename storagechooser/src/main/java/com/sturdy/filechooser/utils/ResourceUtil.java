package com.sturdy.filechooser.utils;


import android.content.Context;


import com.sturdy.filechooser.R;

public class ResourceUtil {

    private Context context;

    public ResourceUtil(Context context) {
        this.context = context;
    }

    public int getColor(int id) {
        return 0;
    }

    public int getAppliedAlpha(int color) {
        return 0;
    }

    public int getPrimaryColorWithAlpha() {
        return getAppliedAlpha(getColor(R.color.colorPrimary));
    }
}
