package com.sturdy.wifilibrary.listener;


public interface OnWifiConnectListener {

    /**
     * WIFI連接信息的回調
     *
     * @param log log
     */
    void onWiFiConnectLog(String log);

    /**
     * WIFI連接成功的回調
     *
     * @param SSID 熱點名
     */
    void onWiFiConnectSuccess(String SSID);

    /**
     * WIFI連接失敗的回調
     *
     * @param SSID 熱點名
     */
    void onWiFiConnectFailure(String SSID);
}
