package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.UnitConvert;
import com.sturdy.hmi.view.DashedLineView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.saveTempFormat;

public class SteriReportActivity extends BaseToolBarActivity {
    ListView list;
    private Activity mActivity;
    private Handler handler = new Handler();
    private String[] pickerTempArrayList;
    private RecyclerView mRecyclerView;
    private CustomAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private static int textSize = 26;
    private TableLayout tableLayout;
    private int rowIndex;
    private String str;

    private Runnable updateTableRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            rowIndex = 0;
            tableLayout.removeAllViews();
            DashedLineView div_v = new DashedLineView(mActivity);
            div_v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1
            ));
//        TableRow division_row= new TableRow(this);
//        TableRow.LayoutParams division_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
//                TableRow.LayoutParams.WRAP_CONTENT);
//        division_row.setLayoutParams(division_lp);
//        TextView division_tv = new TextView(this);
//        division_tv.setGravity(Gravity.LEFT);
//        division_tv.setTextSize(textSize);
//        division_tv.setText("---------------------------------------------------");
//        division_row.addView(division_tv);


            TableRow model_row= new TableRow(mActivity);
            TableRow.LayoutParams model_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            model_lp.span = 4;
            model_lp.weight = 1;
            model_row.setLayoutParams(model_lp);
            TextView model_tv = new TextView(mActivity);
            model_tv.setLayoutParams(model_lp);
            model_tv.setGravity(Gravity.LEFT);
            model_tv.setTextSize(textSize);
            model_tv.setText("Model:"+GlobalClass.getInstance().CB_Model);
            model_row.addView(model_tv);
            tableLayout.addView(model_row);
            rowIndex++;

            TableRow ver_row= new TableRow(mActivity);
            TableRow.LayoutParams ver_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            ver_lp.span = 4;
            ver_row.setLayoutParams(ver_lp);
            TextView ver_tv = new TextView(mActivity);
            ver_tv.setLayoutParams(ver_lp);
            ver_tv.setGravity(Gravity.LEFT);
            ver_tv.setTextSize(textSize);
            ver_tv.setText("Ver.");
            ver_row.addView(ver_tv);
            tableLayout.addView(ver_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            TableRow ver2_row= new TableRow(mActivity);
            TableRow.LayoutParams ver2_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            ver2_lp.span = 4;
            ver_row.setLayoutParams(ver2_lp);
            TextView ver2_tv = new TextView(mActivity);
            ver2_tv.setLayoutParams(ver2_lp);
            ver2_tv.setGravity(Gravity.LEFT);
            ver2_tv.setTextSize(textSize);
            ver2_tv.setText(GlobalClass.getInstance().CB_Firmware);
            ver2_row.addView(ver2_tv);
            tableLayout.addView(ver2_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            TableRow sn_row= new TableRow(mActivity);
            TableRow.LayoutParams sn_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            sn_lp.span = 4;
            ver_row.setLayoutParams(sn_lp);
            TextView sn_tv = new TextView(mActivity);
            sn_tv.setLayoutParams(sn_lp);
            sn_tv.setGravity(Gravity.LEFT);
            sn_tv.setTextSize(textSize);
            sn_tv.setText(GlobalClass.getInstance().CB_SN);
            sn_row.addView(sn_tv);
            tableLayout.addView(sn_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            tableLayout.addView(div_v);
            rowIndex++;

//        TableRow perid_row= new TableRow(this);
//        TableRow.LayoutParams perid_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
//                TableRow.LayoutParams.WRAP_CONTENT);
//        perid_lp.span = 4;
//        perid_row.setLayoutParams(perid_lp);
//        TextView perid_tv = new TextView(this);
//        perid_tv.setLayoutParams(perid_lp);
//        perid_tv.setGravity(Gravity.LEFT);
//        perid_tv.setTextSize(textSize);
//        perid_tv.setText("Per ID:12462174-001");
//        perid_row.addView(perid_tv);
//        tableLayout.addView(perid_row,new TableLayout.LayoutParams(
//                TableLayout.LayoutParams.WRAP_CONTENT,
//                TableLayout.LayoutParams.WRAP_CONTENT));
//        rowIndex++;

            div_v = new DashedLineView(mActivity);
            div_v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1
            ));
            tableLayout.addView(div_v);
            rowIndex++;

            TableRow program_row= new TableRow(mActivity);
            TableRow.LayoutParams program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            TextView perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("Program:");
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText(Constants.instance().fetchValueString(saveSelectMenuNameKey));
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("Ster. Temp:"+UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_SteriTemp));
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("Ster. Time:"+String.format("  %2d h %2d m",GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60));
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("Open Door Temp.:");
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("C.T.:"+UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_CTTemp));
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            program_row= new TableRow(mActivity);
            program_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            program_lp.span = 4;
            program_row.setLayoutParams(program_lp);
            perid_tv = new TextView(mActivity);
            perid_tv.setLayoutParams(program_lp);
            perid_tv.setGravity(Gravity.LEFT);
            perid_tv.setTextSize(textSize);
            perid_tv.setText("F.T.:"+UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_FTTemp));
            program_row.addView(perid_tv);
            tableLayout.addView(program_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            div_v = new DashedLineView(mActivity);
            div_v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1
            ));
            tableLayout.addView(div_v);
            rowIndex++;

            TableRow head_row= new TableRow(mActivity);
            TableRow.LayoutParams head_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            head_row.setLayoutParams(head_lp);
            TextView head_tv1 = new TextView(mActivity);
            head_tv1.setGravity(Gravity.LEFT);
            head_tv1.setWidth(10);
            head_tv1.setTextSize(textSize);
            head_tv1.setText("Step");
            TextView head_tv2 = new TextView(mActivity);
            head_tv2.setGravity(Gravity.RIGHT);
            head_tv2.setTextSize(textSize);
            head_tv2.setText("Time");
//        TextView head_tv3 = new TextView(this);
//        head_tv3.setGravity(Gravity.CENTER);
//        head_tv3.setTextSize(textSize);
//        head_tv3.setText("ts");
            TextView head_tv4 = new TextView(mActivity);
            head_tv4.setGravity(Gravity.RIGHT);
            head_tv4.setTextSize(textSize);
            head_tv4.setText("Temp.");
            TextView head_tv5 = new TextView(mActivity);
            head_tv5.setGravity(Gravity.RIGHT);
            head_tv5.setTextSize(textSize);
            head_tv5.setText("Pres.");
            head_row.addView(head_tv1);
            head_row.addView(head_tv2);
//        head_row.addView(head_tv3);
            head_row.addView(head_tv4);
            head_row.addView(head_tv5);
            tableLayout.addView(head_row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;

            TableRow row1= new TableRow(mActivity);
            TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            row1.setLayoutParams(lp1);
            TextView tv1_1 = new TextView(mActivity);
            tv1_1.setGravity(Gravity.LEFT);
            tv1_1.setTextSize(textSize);
            tv1_1.setWidth(10);
            tv1_1.setText("");
            TextView tv1_2 = new TextView(mActivity);
            tv1_2.setGravity(Gravity.RIGHT);
            tv1_2.setTextSize(textSize);
            tv1_2.setText("mmm:ss");
//        TextView tv1_3 = new TextView(mActivity);
//        tv1_3.setGravity(Gravity.CENTER);
//        tv1_3.setTextSize(textSize);
//        tv1_3.setText("mm:ss");
            TextView tv1_4 = new TextView(mActivity);
            tv1_4.setGravity(Gravity.RIGHT);
            tv1_4.setTextSize(textSize);
            if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                tv1_4.setText("°C");
            else
                tv1_4.setText("°F");

            TextView tv1_5 = new TextView(mActivity);
            tv1_5.setGravity(Gravity.RIGHT);
            tv1_5.setTextSize(textSize);
            if (Constants.instance().fetchValueInt(savePresFormat)==0) {
                str = String.format("bar");
            }else if (Constants.instance().fetchValueInt(savePresFormat)==1) {
                str = String.format("kPa");
            }else if (Constants.instance().fetchValueInt(savePresFormat)==2) {
                str = String.format("Mpa");
            }else if (Constants.instance().fetchValueInt(savePresFormat)==3) {
                str = String.format("psi");
            }else if (Constants.instance().fetchValueInt(savePresFormat)==4) {
                str = String.format("kgf/cm2");
            }
            tv1_5.setText(str);
            row1.addView(tv1_1);
            row1.addView(tv1_2);
//        row1.addView(tv1_3);
            row1.addView(tv1_4);
            row1.addView(tv1_5);
            tableLayout.addView(row1,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rowIndex++;
            String[] stepStrings = {"PV1", "H1", "PV2", "H2", "PV3", "H3", "PV4", "H4", "S00-00", "EX"};
            for (int i=0;i<GlobalClass.getInstance().globalRecordStepList.size();i++) {
                RecordStepClass stepClass = GlobalClass.getInstance().globalRecordStepList.get(i);
                TableRow row= new TableRow(mActivity);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                View l = new View(mActivity);
                l.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        1
                ));
                TextView tv1 = new TextView(mActivity);
                tv1.setWidth(10);
                tv1.setGravity(Gravity.LEFT);
                tv1.setTextSize(textSize);
                tv1.setText(stepClass.step);
                TextView tv2 = new TextView(mActivity);
                tv2.setGravity(Gravity.RIGHT);
                tv2.setTextSize(textSize);
                if (stepClass.step.length()>0)
                    str = String.format("%03d:%02d",stepClass.time/60,stepClass.time%60);
                else str = "";
                tv2.setText(str);
                TextView tv4 = new TextView(mActivity);
                tv4.setGravity(Gravity.RIGHT);
                tv4.setTextSize(textSize);
                str = UnitConvert.sTempConvert(0,stepClass.temp,true);
                tv4.setText(str);
                TextView tv5 = new TextView(mActivity);
                tv5.setGravity(Gravity.RIGHT);
                tv5.setTextSize(textSize);
                //float bar = (float)stepClass.pres / (float)1000;
                if (stepClass.step.length()>0)
                    str = UnitConvert.sPresConvert(0,stepClass.pres);
                else
                    str = "";
                tv5.setText(str);
                row.addView(tv1);
                row.addView(tv2);
//            row.addView(tv3);
                row.addView(tv4);
                row.addView(tv5);
                tableLayout.addView(row,new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT));
            }
            handler.postDelayed(updateTableRunnable, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterireport);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = Constants.instance().fetchValueString(saveSelectMenuNameKey);
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        registerBaseActivityReceiver();
        tableLayout = (TableLayout) findViewById(R.id.tableLayout1);
        handler.postDelayed(updateTableRunnable, 10);

    }

    private String getSterilizationString(int time) {
        if (time < 0) time = 0;
        String timeStr;
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_minute = time/60;
        int i_second = time%60;

        minute = String.format("%03d", i_minute);

        second = String.format("%02d", i_second);
        timeStr = minute+":"+second;
        return timeStr;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(updateTableRunnable);
        super.onDestroy();
    }
}
