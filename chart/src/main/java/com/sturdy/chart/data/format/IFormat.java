package com.sturdy.chart.data.format;


public interface IFormat<T> {

    String format( T t);
}
