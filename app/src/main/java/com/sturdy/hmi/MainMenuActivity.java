package com.sturdy.hmi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.adapter.MainMenuGridViewAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;

import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.RecyclerTouchListener;

import static com.sturdy.hmi.Constants.VacuumpumpKey;

public class MainMenuActivity extends BaseToolBarActivity implements MainMenuGridViewAdapter.OnItemClickListener{

    private RecyclerView listView;
    private RecyclerView gridView;
    private MainMenuGridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main_menu);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        gridView = (RecyclerView) findViewById(R.id.grid);
        setDummyData();

        ImageButton backButton =(ImageButton) findViewById(R.id.back_button);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView menuTitleView = (TextView) findViewById(R.id.menu_title_view);
        menuTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();

                }
                return false;
            }
        });




//        listView.setHasFixedSize(true);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "ListView"
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(horizontalManager);
//        listViewAdapter = new ListViewAdapter(this, corporations);
//        listView.setAdapter(listViewAdapter);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridView.setLayoutManager(layoutManager);
        gridViewAdapter = new MainMenuGridViewAdapter(this, recyclerViewButton, recyclerViewButton_press);
        gridViewAdapter.setOnItemClickListener(this);
        gridView.setAdapter(gridViewAdapter);
        gridView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gridView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTouch(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is touch!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    protected void onResume() {
        super.onResume();
        gridViewAdapter.notifyDataSetChanged();

    }



    @Override
    public void onItemClick(int position) {
        if (position==0){
            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                this.startActivity(new Intent(MainMenuActivity.this, SterilizationMenu30LActivity.class));
            }else{
                this.startActivity(new Intent(MainMenuActivity.this, SterilizationMenuActivity.class));
            }
            this.overridePendingTransition(0, 0);
        }else if (position==1){
            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                    this.startActivity(new Intent(MainMenuActivity.this, FunctionTestMenuActivity.class));
                    this.overridePendingTransition(0, 0);
                }
            }else{
                this.startActivity(new Intent(MainMenuActivity.this, FunctionTestMenuActivity.class));
                this.overridePendingTransition(0, 0);
            }
        }else if (position==2){
            this.startActivity(new Intent(MainMenuActivity.this, RecordActivity.class));
            this.overridePendingTransition(0, 0);
        }else if (position>=3){
            this.startActivity(new Intent(MainMenuActivity.this, SystemSettingActivity.class));
            this.overridePendingTransition(0, 0);
        }


    }

    private void setDummyData() {
        recyclerViewButton = new ArrayList<>();
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_sterilization, "Sterilization"));
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_function_test, "Function Test"));
            }else{
                recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_function_test_gone, "Function Test"));
            }
        }else{
            recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_function_test, "Function Test"));
        }
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_record, "Record"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.menu_setting, "System Setting"));

        recyclerViewButton_press = new ArrayList<>();
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_sterilization_pressed, "Sterilization"));
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_function_test_pressed, "Function Test"));
            }else{
                recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_function_test_gone, "Function Test"));
            }
        }else{
            recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_function_test, "Function Test"));
        }
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_record_pressed, "Record"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.menu_setting_pressed, "System Setting"));

    }

    @Override
    public void onBackPressed()
    {
    }

}
