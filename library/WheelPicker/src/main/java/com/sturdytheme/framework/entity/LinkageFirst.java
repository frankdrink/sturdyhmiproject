package com.sturdytheme.framework.entity;

import java.util.List;


public interface LinkageFirst<Snd> extends LinkageItem {

    List<Snd> getSeconds();

}