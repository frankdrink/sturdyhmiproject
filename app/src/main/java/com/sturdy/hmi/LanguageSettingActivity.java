package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveRealPrint;

public class LanguageSettingActivity extends BaseToolBarActivity {
    private Activity mActivity;
    List<MainMenuSettingElement> settingElementList;
    ListView list;
    List<Boolean> listIsChecked;    // 這個用來記錄哪幾個 item 是被打勾的
    String[] web = {
            "English"

    } ;
    Integer[] imageId = {
            R.drawable.inactive_dot
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_printer_setting);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Language", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });


        final boolean[] listChecked = new boolean[web.length];
        listIsChecked = new ArrayList<Boolean>();
        boolean isRealPrint = Constants.instance().fetchValueInt(saveRealPrint) > 0 ? true : false;

        for(int x=0;x<web.length;x++)
        {
            listIsChecked.add(isRealPrint);
            listChecked[x] = isRealPrint;
        }
        // 取出主畫面的設定資料
//        String json = (Constants.instance().fetchValueString(saveMenuSettingKey));
//        if (json != null)
//        {
//            Gson gson = new Gson();
//            Type type = new TypeToken<List<MainMenuSettingElement>>(){}.getType();
//            settingElementList = new ArrayList<MainMenuSettingElement>();
//            settingElementList = gson.fromJson(json, type);
//            for(int i = 0; i < settingElementList.size(); i++)
//            {
//                listIsChecked.set(settingElementList.get(i).getId(), true);
//                listChecked[settingElementList.get(i).getId()] = true;
////                Log.d(TAG, alterSamples.get(i).getName()+":" + alterSamples.get(i).getX() + "," + alterSamples.get(i).getY());
//            }
//        }
        final MainSettingListAdapter listAdapter = new
                MainSettingListAdapter(this, web, imageId, listChecked);
        list=(ListView)findViewById(R.id.list);
        list.setClickable(false);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SwitchButton switchButton=(SwitchButton)view.findViewById(R.id.menu_setting_swich);
                boolean isChecked = !listChecked[position];//switchButton.isChecked();
                switchButton.setChecked(isChecked);
                listIsChecked.set(position,isChecked);
                listChecked[position] = isChecked;
                listAdapter.setChecked(listChecked);
                Constants.instance().storeValueInt(saveRealPrint,isChecked?1 : 0);

//                listAdapter.notifyDataSetChanged();
//                switch (position){
//                    case 0:
//                        startActivity(new Intent(MainMenuSettingActivity.this, UnwrappedEditActivity.class));
//                        break;
//                    case 1:
//                        startActivity(new Intent(MainMenuSettingActivity.this, WrappedEditActivity.class));
//                        break;
//                    case 2:
//                        startActivity(new Intent(MainMenuSettingActivity.this, InstrumentEditActivity.class));
//                        break;
//                    case 3:
//                        startActivity(new Intent(MainMenuSettingActivity.this, PrionEditActivity.class));
//                        break;
//                    case 4:
//                        startActivity(new Intent(MainMenuSettingActivity.this, LiquidEditActivity.class));
//                        break;
//                    case 5:
////                        startActivity(new Intent(SterilizationMenuActivity.this, InstrumentEditActivity.class));
//                        break;
//                }
//                overridePendingTransition(0, 0);
            }
        });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });


//        for (int i = 0; i < listAdapter.getCount(); i++){
////            MainSettingListAdapter ss = listAdapter.getPosition(i);
//            if(listAdapter.isChecked()) {
////                tv.setText(tv.getText() + " " + listAdapter.modelArrayList.get(i).getAnimal());
//            }
//        }

    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
