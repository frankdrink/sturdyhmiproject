package com.sturdy.chart.provider.component.point;

/**
 * Created by Frank on 2019/12/13.
 */

public interface ILegendPoint extends IPoint{

    float getWidth();
    float getHeight();
}
