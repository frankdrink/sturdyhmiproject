package com.sturdy.hmi;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.DateRadioListAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveDateFormat;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveTimeFormat;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class DateTimeSettingActivity extends BaseToolBarActivity implements DateRadioListAdapter.UpdateMainClass{
    private Activity mActivity;
    DateRadioListAdapter adapter1;
    DateRadioListAdapter adapter2;
    int saveDateFormatBase;
    int saveTimeFormatBase;
    List<String> data1;
    List<String> data2;

    List<MainMenuSettingElement> settingElementList;
//    TextView maxSelectTextView;
    Handler handler;
    ListView list1, list2;

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            refreshTime();
            handler.postDelayed(this, 60000);
        }
    };
    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_datetimesetting);
        mActivity = this;
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Date & Time", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        TextView titleView1 = (TextView) findViewById(R.id.list1_text_view);
        titleView1.setText("Date");
        TextView titleView2 = (TextView) findViewById(R.id.list2_text_view);
        titleView2.setText("Time");

        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        int i_year = calendar.get(Calendar.YEAR);
        data1 = new ArrayList<String>();
        data1.add("Y / M / D");
        data1.add("D / M / Y");
        data1.add("M / D / Y");
        timeStr = String.format("%02d / %02d / %02d", i_year, i_month, i_date);
        data1.add(timeStr);
        saveDateFormatBase = Constants.instance().fetchValueInt(saveDateFormat);
        adapter1 = new DateRadioListAdapter(this, data1, "date", saveDateFormatBase);
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);
        timeStr = String.format("%02d:%02d", i_hour, i_minute);
        data2 = new ArrayList<String>();
        data2.add("12 H");
        data2.add("24 H");
        data2.add(timeStr);
        saveTimeFormatBase = Constants.instance().fetchValueInt(saveTimeFormat);
        adapter2 = new DateRadioListAdapter(this, data2, "time", saveTimeFormatBase);
        ListView lvMain2 = (ListView) findViewById(R.id.list2);
        lvMain2.setAdapter(adapter2);

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        handler = new Handler();
        handler.postAtTime(runnable, 60000);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void updateItemList(final String itemName) {
        calendar = Calendar.getInstance();
        if (itemName.equals("date0")) {
            saveDateFormatBase = 0;
            Constants.instance().storeValueInt(saveDateFormat,0);
            refreshTime();
        }else if (itemName.equals("date1")) {
            saveDateFormatBase = 1;
            Constants.instance().storeValueInt(saveDateFormat,1);
            refreshTime();
        }else if (itemName.equals("date2")) {
            saveDateFormatBase = 2;
            Constants.instance().storeValueInt(saveDateFormat,2);
            refreshTime();
        }else if (itemName.equals("date")) {
            final DatePicker picker = new DatePicker(this);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTextSize(32);
            picker.setTitleTextSize(32);
            picker.setCanceledOnTouchOutside(true);
            picker.setUseWeight(true);
            picker.setTopPadding(ConvertUtils.toPx(this, 10));
            picker.setRangeEnd(2111, 1, 11);
            picker.setRangeStart(1960, 1, 1);
            picker.setSelectedItem(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DAY_OF_MONTH));
            picker.setResetWhileWheel(false);
            picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                @Override
                public void onDatePicked(String year, String month, String day) {
                    setSysDate(Integer.parseInt(year),Integer.parseInt(month),Integer.parseInt(day));
                    refreshTime();
                }
            });
            picker.setOnWheelListener(new DatePicker.OnWheelListener() {
                @Override
                public void onYearWheeled(int index, String year) {
                    picker.setTitleText(year + "-" + picker.getSelectedMonth() + "-" + picker.getSelectedDay());
                }

                @Override
                public void onMonthWheeled(int index, String month) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + month + "-" + picker.getSelectedDay());
                }

                @Override
                public void onDayWheeled(int index, String day) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + picker.getSelectedMonth() + "-" + day);
                }
            });
            picker.show();
        } else if (itemName.equals("time")) {
            if (saveTimeFormatBase==0) {
                TimePicker picker = new TimePicker(this, TimePicker.HOUR_12);
                picker.setUseWeight(false);
                picker.setTopHeight(50);
                picker.setSubmitTextSize(40);
                picker.setTextSize(32);
                picker.setCycleDisable(false);
                picker.setRangeStart(1, 0);//00:00
                picker.setRangeEnd(12, 59);//23:59
                int currentHour = Calendar.getInstance().get(Calendar.HOUR);
                int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
                picker.setSelectedItem(currentHour, currentMinute);
                picker.setTopLineVisible(true);
                picker.setTextPadding(ConvertUtils.toPx(this, 15));
                picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
                    @Override
                    public void onTimePicked(String hour, String minute, String timeAt) {
                        int iHour = Integer.valueOf(hour);
                        if (timeAt=="PM") {
                            if (iHour<12) {
                                iHour+=12;
                            }
                        }else{
                            if (iHour==12) {
                                iHour-=12;
                            }
                        }
                        hour = String.valueOf(iHour);
                        setTime(Integer.parseInt(hour),Integer.parseInt(minute));
                        refreshTime();
                    }
                });
                picker.show();
            }else{
                TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
                picker.setUseWeight(false);
                picker.setTopHeight(50);
                picker.setSubmitTextSize(40);
                picker.setTextSize(32);
                picker.setCycleDisable(false);
                picker.setRangeStart(0, 0);//00:00
                picker.setRangeEnd(23, 59);//23:59
                int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
                picker.setSelectedItem(currentHour, currentMinute);
                picker.setTopLineVisible(true);
                picker.setTextPadding(ConvertUtils.toPx(this, 15));
                picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
                    @Override
                    public void onTimePicked(String hour, String minute, String timeAt) {
                        setTime(Integer.parseInt(hour),Integer.parseInt(minute));
                        refreshTime();
                    }
                });
                picker.show();
            }

        } if (itemName.equals("time0")) {
            saveTimeFormatBase = 0;
            Constants.instance().storeValueInt(saveTimeFormat,0);
            refreshTime();
        }else if (itemName.equals("time1")) {
            saveTimeFormatBase = 1;
            Constants.instance().storeValueInt(saveTimeFormat, 1);
            refreshTime();
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {

    }

    public void refreshTime() {
        calendar = Calendar.getInstance();
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        int i_year = calendar.get(Calendar.YEAR);
        String hourStr = "";
        if (saveDateFormatBase==0) {
            timeStr = String.format("%02d / %02d / %02d", i_year, i_month, i_date);
        }else if (saveDateFormatBase==1) {
            timeStr = String.format("%02d / %02d / %02d", i_date, i_month, i_year);
        }else if (saveDateFormatBase==2) {
            timeStr = String.format("%02d / %02d / %02d", i_month, i_date, i_year);
        }
        data1.set(3,timeStr);
        if (saveTimeFormatBase==0) {
            if (i_hour<12) hourStr = "AM";
            else {
                hourStr = "PM";
                if (i_hour>12)
                i_hour -= 12;
            }
            timeStr = String.format("%02d:%02d %s", i_hour, i_minute, hourStr);
        }else {
            timeStr = String.format("%02d:%02d", i_hour, i_minute);
        }
        data2.set(2,timeStr);
        adapter1.resetAdapter(data1, saveDateFormatBase);
        adapter2.resetAdapter(data2, saveTimeFormatBase);
        adapter1.notifyDataSetChanged();
        adapter2.notifyDataSetChanged();
    }

    private void setSysDate(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month-1);
        c.set(Calendar.DAY_OF_MONTH, day);
        long when = c.getTimeInMillis();
        if(when / 1000 < Integer.MAX_VALUE){
            ((AlarmManager)mActivity.getSystemService(mActivity.ALARM_SERVICE)).setTime(when);
        }
    }

    private void setTime(int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long when = c.getTimeInMillis();
        if (when / 1000 < Integer.MAX_VALUE) {
            ((AlarmManager) mActivity.getSystemService(mActivity.ALARM_SERVICE)).setTime(when);
        }
    }
}
