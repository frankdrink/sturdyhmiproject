package com.sturdy.chart.core;

import android.content.Context;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseRotateChart;
import com.sturdy.chart.data.PieData;
import com.sturdy.chart.matrix.RotateHelper;
import com.sturdy.chart.provider.pie.PieProvider;



public class PieChart  extends BaseRotateChart<PieProvider,PieData> {


    public PieChart(Context context) {
        super(context);
    }

    public PieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PieChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




    @Override
    protected PieProvider initProvider(RotateHelper rotateHelper) {
        PieProvider provider =  new PieProvider();
        provider.setRotateHelper(rotateHelper);
        return provider;
    }



}
