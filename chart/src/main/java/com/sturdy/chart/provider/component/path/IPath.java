package com.sturdy.chart.provider.component.path;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;

/**
 * Created by Frank on 2019/11/30.
 */

public interface IPath {

     void drawPath(Canvas canvas, Rect rect, Path path, int perWidth, Paint paint, float progress);
}
