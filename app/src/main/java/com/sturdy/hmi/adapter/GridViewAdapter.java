package com.sturdy.hmi.adapter;
/**
 * Created by franklin on 2019/8/8.
 */
import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.sturdy.hmi.MainActivity;
import com.sturdy.hmi.R;
import com.sturdy.hmi.RecyclerViewItem;
import com.sturdy.hmi.SterilizationFinishActivity;

public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.ViewHolder> {
    private List<RecyclerViewItem> items,items_press;
    private Activity activity;

    public GridViewAdapter(Activity activity, List<RecyclerViewItem> items, List<RecyclerViewItem> items_press) {
        this.activity = activity;
        this.items = items;
        this.items_press = items_press;
    }

    public void resetAdapter(List<RecyclerViewItem> items, List<RecyclerViewItem> items_press) {
        this.items = items;
        this.items_press = items_press;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_grid, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GridViewAdapter.ViewHolder viewHolder,final int position) {
        viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());
//        viewHolder.imageView.setBackgroundResource(R.drawable.steri_list_icon_wrapped_normal);

        viewHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    viewHolder.imageView.setBackgroundResource(items_press.get(position).getDrawableId());
//                    onItemClickListener.onItemClick(position);

//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_pressed));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());

//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_normal));
                }
                return false;
            }
        });

        viewHolder.imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    viewHolder.imageView.setBackgroundResource(items_press.get(position).getDrawableId());


//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_pressed));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());

//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_normal));
                }
                return false;
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });


        viewHolder.textView.setText(items.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageButton imageView;
        private TextView textView;

        public ViewHolder(View view) {
            super(view);
            textView = (TextView)view.findViewById(R.id.text);
            imageView = (ImageButton) view.findViewById(R.id.image);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
