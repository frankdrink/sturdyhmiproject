package com.sturdy.chart.component.base;

import com.sturdy.chart.data.style.FontStyle;


public interface IChartTitle extends IComponent<String> {

    float getPercent();

    void setPercent(float percent);

    FontStyle getFontStyle();

    int getDirection();

    void setDirection(int direction);

    void setFontStyle(FontStyle fontStyle);
}
