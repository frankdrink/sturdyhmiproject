package com.sturdy.hmi.USBList.data;

public interface DataProvider {
    String getUrl();

    String getDataFilePath();

    boolean isDataAvailable();
}
