package com.sturdy.chart.listener;

import com.sturdy.chart.component.Legend;
import com.sturdy.chart.data.ColumnData;


public interface OnClickLegendListener<C extends ColumnData> {

   void onClickLegend(C c, Legend<C> legend);
}
