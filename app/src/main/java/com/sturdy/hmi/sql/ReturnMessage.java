package com.sturdy.hmi.sql;

/**
 * Created by franklin on 2019/8/14.
 */

public class ReturnMessage {
    public static boolean success = true ;
    public static String message = "" ;

    public ReturnMessage() {
        this.success = true ;
        this.message = "" ;
    }
}
