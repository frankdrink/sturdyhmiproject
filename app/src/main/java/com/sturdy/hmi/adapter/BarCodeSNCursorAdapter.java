package com.sturdy.hmi.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sturdy.hmi.R;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;

/**
 * Created by franklin on 2019/8/14.
 */

public class BarCodeSNCursorAdapter extends CursorAdapter {
    public BarCodeSNCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate( R.layout.barcode_listview_delete, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        BarCode c = (BarCode) DatabaseHelper.getObjectFromCursor(cursor,BarCode.class);
        TextView tvUpdateDeleteTitle =(TextView) view.findViewById(R.id.tvUpdateDeleteTitle);
        ImageButton ibDeleteCustomer =(ImageButton) view.findViewById(R.id.ibDeleteCustomer);
        ibDeleteCustomer.setTag( "delete," + cursor.getPosition());
        tvUpdateDeleteTitle.setText(c.getSerialNumber());
    }
}
