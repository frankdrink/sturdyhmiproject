package com.sturdy.chart.matrix;

import android.view.MotionEvent;

import com.sturdy.chart.core.base.BaseChart;


public interface ITouch {
    /**
     * 用於判斷是否請求不攔截事件
     * 解決手勢衝突問題
     * @param chart
     * @param event
     */
    void onDisallowInterceptEvent(BaseChart chart, MotionEvent event);

    /**
     * 處理touchEvent
     * @param event
     */
    boolean handlerTouchEvent(MotionEvent event);
}
