package com.sturdy.chart.core;

import android.content.Context;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseBarLineChart;
import com.sturdy.chart.data.BarLineData;
import com.sturdy.chart.provider.barLine.BarLineProvider;

/**
 * Created by Frank 2020/01/01
 */

public class BarLineChart extends BaseBarLineChart<BarLineProvider,BarLineData> {

    public BarLineChart(Context context) {
        super(context);
    }

    public BarLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BarLineChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




    @Override
    protected BarLineProvider initProvider() {
        return new BarLineProvider();
    }


}
