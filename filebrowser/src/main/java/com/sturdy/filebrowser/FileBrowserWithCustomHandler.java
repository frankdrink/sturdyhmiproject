package com.sturdy.filebrowser;

/**
 * Created by sturdy on 4/15/2020.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.roughike.bottombar.BottomBar;
import com.simplecityapps.recyclerview_fastscroll.interfaces.OnFastScrollStateChangeListener;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.sturdy.filebrowser.adapters.CustomAdapter;
import com.sturdy.filebrowser.adapters.CustomAdapterItemClickListener;
import com.sturdy.filebrowser.fileoperations.FileIO;
import com.sturdy.filebrowser.fileoperations.Operations;
import com.sturdy.filebrowser.interfaces.IContextSwitcher;
import com.sturdy.filebrowser.interfaces.IFuncPtr;
import com.sturdy.filebrowser.listeners.OnFileChangedListener;
import com.sturdy.filebrowser.listeners.SearchViewListener;
import com.sturdy.filebrowser.listeners.TabChangeListener;
import com.sturdy.filebrowser.models.FileItem;
import com.sturdy.filebrowser.utils.AssortedUtils;
import com.sturdy.filebrowser.utils.Permissions;
import com.sturdy.filebrowser.utils.UIUpdateHelper;
import com.sturdy.filebrowser.utils.UIUtils;
import com.sturdy.filechooser.Content;
import com.sturdy.filechooser.StorageChooser;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class FileBrowserWithCustomHandler extends AppCompatActivity implements OnFileChangedListener, IContextSwitcher {

    private Context mContext;
    ExecutorService executor;
    Handler mUIUpdateHandler;
    UIUpdateHelper mHelper;

    private CustomAdapter mAdapter;
    private FastScrollRecyclerView.LayoutManager mLayoutManager;
    private FastScrollRecyclerView mFilesListView;

    private BottomBar mBottomView;
    private BottomBar mTopStorageView;
    private TabChangeListener mTabChangeListener;

    private SearchView mCurrentPath;
    private NavigationHelper mNavigationHelper;
    private Operations op;
    private FileIO io;

    //Action Mode for filebrowser_toolbar
    private static ActionMode mActionMode;
    private static final int APP_PERMISSION_REQUEST = 0;

    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;
    private SearchViewListener mSearchViewListener;
    private List<FileItem> mFileList = new ArrayList<>();
    private StorageChooser.Builder builder = new StorageChooser.Builder();
    private StorageChooser chooser;
    public static final IntentFilter INTENT_FILTER = createIntentFilter();
    private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();


    public class BaseActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION")){
                finish();
            }
        }
    }

    private static IntentFilter createIntentFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction("FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION");
        return filter;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        registerReceiver(baseActivityReceiver, INTENT_FILTER);

        mContext = this;

        Intent in = new Intent(this, Permissions.class);
        Bundle bundle = new Bundle();
        bundle.putStringArray(Constants.APP_PREMISSION_KEY, Constants.APP_PREMISSIONS);
        in.putExtras(bundle);
        startActivityForResult(in, APP_PERMISSION_REQUEST);

        mNavigationHelper = new NavigationHelper(mContext,mAdapter);
        mNavigationHelper.setmChangeDirectoryListener(this);
        mUIUpdateHandler = new Handler(Looper.getMainLooper());
        io = new FileIO(mNavigationHelper, new Handler(Looper.getMainLooper()), mContext);
        op = Operations.getInstance(mContext);
        mHelper = new UIUpdateHelper(mNavigationHelper, mContext);
        executor  = Executors.newFixedThreadPool(1);

        String filterFilesWithExtension = getIntent().getStringExtra(Constants.ALLOWED_FILE_EXTENSIONS);
        if(filterFilesWithExtension != null && !filterFilesWithExtension.isEmpty()) {
            Set<String> allowedFileExtensions = new HashSet<String>(Arrays.asList(filterFilesWithExtension.split(";")));
            mNavigationHelper.setAllowedFileExtensionFilter(allowedFileExtensions);
        }

        mFileList = mNavigationHelper.getFilesItemsInCurrentDirectory();
    }

    @Override
    public void onBackPressed() {
        if (mAdapter.getChoiceMode() == Constants.CHOICE_MODE.MULTI_CHOICE) {
            switchMode(Constants.CHOICE_MODE.SINGLE_CHOICE);
            return;
        }

        if (!mNavigationHelper.navigateBack()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_PERMISSION_REQUEST) {
            if (resultCode != Activity.RESULT_OK)
                Toast.makeText(mContext, mContext.getString(R.string.error_no_permissions), Toast.LENGTH_LONG).show();
            loadUi();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_default_menu, menu);
        // Get the SearchView and set the searchable configuration
//        mSearchMenuItem = menu.findItem(R.id.action_search);
//        mSearchView = (SearchView)mSearchMenuItem.getActionView();
        // Assumes current activity is the searchable activity
//        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setSubmitButtonEnabled(true);
//        mSearchView.setOnQueryTextListener(mSearchViewListener);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_showfoldersizes) {
            if (AssortedUtils.GetPrefs(Constants.SHOW_FOLDER_SIZE, mContext).equalsIgnoreCase("true"))
                AssortedUtils.SavePrefs(Constants.SHOW_FOLDER_SIZE, "false", mContext);
            else
                AssortedUtils.SavePrefs(Constants.SHOW_FOLDER_SIZE, "true", mContext);
            onFileChanged(mNavigationHelper.getCurrentDirectory());
        }
        else if (item.getItemId() == R.id.action_newfolder) {
            UIUtils.showEditTextDialog(this, getString(R.string.new_folder), "", new IFuncPtr(){
                @Override
                public void execute(final String val) {
                    io.createDirectory(new File(mNavigationHelper.getCurrentDirectory(),val.trim()));
                }
            });
        }
        else if (item.getItemId() == R.id.action_paste) {
            if (op.getOperation() == Operations.FILE_OPERATIONS.NONE) {
                UIUtils.ShowToast(mContext.getString(R.string.no_operation_error), mContext);
            }
            if (op.getSelectedFiles() == null) {
                UIUtils.ShowToast(mContext.getString(R.string.no_files_paste), mContext);
            }
            io.pasteFiles(mNavigationHelper.getCurrentDirectory());
        }

        return false;

    }

    @Override
    public void onFileChanged(File updatedDirectory) {
        if(updatedDirectory!=null && updatedDirectory.exists() && updatedDirectory.isDirectory()) {
            mFileList = mNavigationHelper.getFilesItemsInCurrentDirectory();
//            mCurrentPath.setText(updatedDirectory.getAbsolutePath());
            mAdapter.notifyDataSetChanged();
            mTopStorageView.getTabWithId(R.id.menu_internal_storage).setTitle(FileUtils.byteCountToDisplaySize(Constants.internalStorageRoot.getUsableSpace()) + "/" + FileUtils.byteCountToDisplaySize(Constants.internalStorageRoot.getTotalSpace()));
//            if (Constants.externalStorageRoot != null)
//                mTopStorageView.getTabWithId(R.id.menu_external_storage).setTitle(FileUtils.byteCountToDisplaySize(Constants.externalStorageRoot.getUsableSpace()) + "/" + FileUtils.byteCountToDisplaySize(Constants.externalStorageRoot.getTotalSpace()));
        }
    }

    private void loadUi() {
        setContentView(R.layout.filebrowser_activity_main);
        Content c = new Content();
        c.setCreateLabel("Create");
        c.setInternalStorageText("My Storage");
        c.setCancelLabel("Cancel");
        c.setSelectLabel("Select");
        c.setOverviewHeading("Choose Drive");

        builder.withActivity(this)
                .withFragmentManager(getFragmentManager())
                .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                .withContent(c);
        builder.withMemoryBar(true);

        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        UIAlphaImageButton backButton;
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Search", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        mFilesListView = (FastScrollRecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new CustomAdapter(mFileList,mContext);
        mFilesListView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(mContext);
        mFilesListView.setLayoutManager(mLayoutManager);
        final CustomAdapterItemClickListener onItemClickListener = new CustomAdapterItemClickListener(mContext, mFilesListView, new CustomAdapterItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // TODO Handle item click
//                if (mAdapter.getChoiceMode()== Constants.CHOICE_MODE.SINGLE_CHOICE) {
                File f = mAdapter.getItemAt(position).getFile();
                if (f.isDirectory()) {
                    closeSearchView();
                    mNavigationHelper.changeDirectory(f);
                } else {
//                        Uri selectedFileUri = Uri.fromFile(f);
                    final ProgressDialog dialog = ProgressDialog.show(FileBrowserWithCustomHandler.this,
                            "Loading", "Please Wait...\n",true);
                    new Thread(new Runnable(){
                        @Override
                        public void run() {
                            try{
                                Thread.sleep(100);
                            }
                            catch(Exception e){
                                e.printStackTrace();
                            }
                            finally{
                                dialog.dismiss();
                            }
                        }
                    }).start();
                    Intent i = new Intent(Constants.FILE_SELECTED_BROADCAST);
                    i.putExtra(Constants.BROADCAST_SELECTED_FILE, f.toString());
                    Bundle extras = getIntent().getExtras();
                    if(extras!=null)
                        i.putExtras(extras);
                    sendBroadcast(i);
                }


            }

            @Override
            public void onItemLongClick(View view, int position) {
                switchMode(Constants.CHOICE_MODE.MULTI_CHOICE);
                mAdapter.selectItem(position);
                mFilesListView.scrollToPosition(position);
            }
        });
        mFilesListView.addOnItemTouchListener(onItemClickListener);

        mFilesListView.setOnFastScrollStateChangeListener(new OnFastScrollStateChangeListener() {
            @Override
            public void onFastScrollStart() {
                onItemClickListener.setmFastScrolling(true);
            }

            @Override
            public void onFastScrollStop() {
                onItemClickListener.setmFastScrolling(false);
            }
        });

        mSearchViewListener = new SearchViewListener(mAdapter);
        mCurrentPath = (SearchView) findViewById(R.id.currentPath);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mCurrentPath.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mCurrentPath.setOnQueryTextListener(mSearchViewListener);
        Toolbar toolbar = findViewById(R.id.filebrowser_tool_bar);
        setSupportActionBar(toolbar);

        mBottomView = findViewById(R.id.bottom_navigation);
        mTopStorageView = findViewById(R.id.currPath_Nav);

        mTabChangeListener = new TabChangeListener(this, mNavigationHelper, mAdapter, io,this);

        mBottomView.setOnTabSelectListener(mTabChangeListener);
        mBottomView.setOnTabReselectListener(mTabChangeListener);

        mTopStorageView.setOnTabSelectListener(mTabChangeListener);
        mTopStorageView.setOnTabReselectListener(mTabChangeListener);

        mBottomView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);
        mTopStorageView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);

        onFileChanged(mNavigationHelper.getCurrentDirectory());

        //switch to initial directory if given
        String initialDirectory = getIntent().getStringExtra(Constants.INITIAL_DIRECTORY);
        if (initialDirectory != null && !initialDirectory.isEmpty() ) {
            File initDir = new File(initialDirectory);
            if (initDir.exists())
                mNavigationHelper.changeDirectory(initDir);
        }

        TextView selectAllTV = (TextView) findViewById(R.id.select_all_tv);
        selectAllTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.selectAll();
            }
        });

        TextView unSelectAllTV = (TextView) findViewById(R.id.unselect_tv);
        unSelectAllTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.unSelectAll();
            }
        });

        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    sendBroadcast(new Intent("FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION"));
                    finish();
                }
                return false;
            }
        });



        DrawMeButton mDeleteButton =(DrawMeButton) findViewById(R.id.delete_button);
        mDeleteButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    final List<FileItem> selectedItems = mAdapter.getSelectedItems();
                    if (selectedItems != null && selectedItems.size() > 0) {
                        final ProgressDialog progressDialog = new ProgressDialog(mContext);
                        String title = mContext.getString(R.string.wait);
                        progressDialog.setTitle(title);
                        progressDialog.setTitle(mContext.getString(R.string.deleting,title));

                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("");
                        progressDialog.setProgress(0);
                        progressDialog.show();
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                int i = 0;
                                float TOTAL_ITEMS = selectedItems.size();
                                try {
                                    for (; i < selectedItems.size(); i++) {
                                        mUIUpdateHandler.post(mHelper.progressUpdater(progressDialog, (int) ((i / TOTAL_ITEMS) * 100), "File: " + selectedItems.get(i).getFile().getName()));
                                        String field = selectedItems.get(i).getFile().getName().substring(1,selectedItems.get(i).getFile().getName().length()-4);
                                        String filename = selectedItems.get(i).getFile().getAbsolutePath();
                                        File file = new File(filename);
                                        file.delete();
//                                        deleteFile(filename);
                                        //copyFile(selectedItems.get(i).getFile().getAbsolutePath(), path+"/"+field+"/"+selectedItems.get(i).getFile().getName(),true);
//                                        String dtlFile = selectedItems.get(i).getFile().getAbsolutePath();
//                                        dtlFile = dtlFile.replace("std","dtl");
//                                        dtlFile = dtlFile.replace("/S","/");
//                                        copyFile(dtlFile, path+"/"+field+"/"+field+".std",true);
                                    }
                                    mUIUpdateHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            op.resetOperation();
                                        }
                                    });
                                    mUIUpdateHandler.post(mHelper.toggleProgressBarVisibility(progressDialog));
                                    mUIUpdateHandler.post(mHelper.updateRunner());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    mUIUpdateHandler.post(mHelper.toggleProgressBarVisibility(progressDialog));
                                    mUIUpdateHandler.post(mHelper.errorRunner(mContext.getString(R.string.pasting_error)));
                                }
                            }
                        });
                    } else {
//                                UIUtils.ShowToast(mContext.getString(R.string.no_items_selected), mContext);
                    }

                }
                return false;
            }
        });

        DrawMeButton exportButton =(DrawMeButton) findViewById(R.id.export_button);
        exportButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    chooser = builder.build();
                    chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                        @Override
                        public void onSelect(final String path) {
                            final List<FileItem> selectedItems = mAdapter.getSelectedItems();
                            if (selectedItems != null && selectedItems.size() > 0) {
                                final ProgressDialog progressDialog = new ProgressDialog(mContext);
                                String title = mContext.getString(R.string.wait);
                                progressDialog.setTitle(title);
                                progressDialog.setTitle(mContext.getString(R.string.copying,title));

                                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                                progressDialog.setCancelable(false);
                                progressDialog.setMessage("");
                                progressDialog.setProgress(0);
                                progressDialog.show();
                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        int i = 0;
                                        float TOTAL_ITEMS = selectedItems.size();
                                        try {
                                            for (; i < selectedItems.size(); i++) {
                                                mUIUpdateHandler.post(mHelper.progressUpdater(progressDialog, (int) ((i / TOTAL_ITEMS) * 100), "File: " + selectedItems.get(i).getFile().getName()));
                                                String field = selectedItems.get(i).getFile().getName().substring(1,selectedItems.get(i).getFile().getName().length()-4);
                                                copyFile(selectedItems.get(i).getFile().getAbsolutePath(), path+"/"+field+"/"+selectedItems.get(i).getFile().getName(),true);
                                                String dtlFile = selectedItems.get(i).getFile().getAbsolutePath();
                                                dtlFile = dtlFile.replace("std","dtl");
                                                dtlFile = dtlFile.replace("/S","/");
                                                copyFile(dtlFile, path+"/"+field+"/"+field+".std",true);
                                                dtlFile = selectedItems.get(i).getFile().getAbsolutePath();
                                                dtlFile = dtlFile.replace("std","csv");
                                                copyFile(dtlFile, path+"/"+field+"/"+field+".csv",true);
                                            }
                                            mUIUpdateHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    op.resetOperation();
                                                    Toast toast = Toast.makeText(getApplicationContext(), "Export Complete!!", Toast.LENGTH_SHORT);
                                                    ViewGroup group = (ViewGroup) toast.getView();
                                                    TextView messageTextView = (TextView) group.getChildAt(0);
                                                    messageTextView.setTextSize(42);
                                                    toast.show();
                                                }
                                            });
                                            mUIUpdateHandler.post(mHelper.toggleProgressBarVisibility(progressDialog));
                                            mUIUpdateHandler.post(mHelper.updateRunner());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            mUIUpdateHandler.post(mHelper.toggleProgressBarVisibility(progressDialog));
                                            mUIUpdateHandler.post(mHelper.errorRunner(mContext.getString(R.string.pasting_error)));
                                        }

                                    }
                                });
                            } else {
//                                UIUtils.ShowToast(mContext.getString(R.string.no_items_selected), mContext);
                            }
//                            Toast.makeText(getApplicationContext(), "Export Complete!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                        @Override
                        public void onCancel() {
//                            Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                        }
                    });
                    chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
                        @Override
                        public void onDone(ArrayList<String> selectedFilePaths) {
                            for(String s: selectedFilePaths) {
//                            Log.e(TAG, s);
                            }
                        }
                    });
                    chooser.show();
                }
                return false;
            }
        });


    }

    public void switchMode(Constants.CHOICE_MODE mode) {
        mode = Constants.CHOICE_MODE.MULTI_CHOICE;

        if(mode == Constants.CHOICE_MODE.SINGLE_CHOICE) {
            if(mActionMode != null)
                mActionMode.finish();
        } else {
            if(mActionMode == null) {
                closeSearchView();
                ToolbarActionMode newToolBar = new ToolbarActionMode(this,this, mAdapter, Constants.APP_MODE.FILE_BROWSER, io);
                mActionMode = startSupportActionMode(newToolBar);
                mActionMode.setTitle(mContext.getString(R.string.select_multiple));
            }
        }
    }

    public void changeBottomNavMenu(Constants.CHOICE_MODE multiChoice) {
        if (multiChoice == Constants.CHOICE_MODE.SINGLE_CHOICE) {
            mBottomView.setItems(R.xml.bottom_nav_items);
            mBottomView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);
            mTopStorageView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);
        } else {
            mBottomView.setItems(R.xml.bottom_nav_items_multiselect);
            mBottomView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);
            mTopStorageView.getTabWithId(R.id.menu_none).setVisibility(View.GONE);
        }
    }

    @Override
    public void setNullToActionMode() {
        if (mActionMode != null)
            mActionMode = null;
    }

    @Override
    public void reDrawFileList() {
        mFilesListView.setLayoutManager(null);
        mFilesListView.setAdapter(mAdapter);
        mFilesListView.setLayoutManager(mLayoutManager);
        mTabChangeListener.setmAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public static boolean copyFile(String srcFileName, String destFileName,
                                   boolean overlay) {
        File srcFile = new File(srcFileName);

        // 判斷原始檔是否存在
        if (!srcFile.exists()) {
            return false;
        } else if (!srcFile.isFile()) {
            return false;
        }

        // 判斷目標檔案是否存在
        File destFile = new File(destFileName);
        if (destFile.exists()) {
            // 如果目標檔案存在並允許覆蓋
            if (overlay) {
                // 刪除已經存在的目標檔案,無論目標檔案是目錄還是單個檔案
                new File(destFileName).delete();
            }
        } else {
            // 如果目標檔案所在目錄不存在,則建立目錄
            if (!destFile.getParentFile().exists()) {
                // 目標檔案所在目錄不存在
                if (!destFile.getParentFile().mkdirs()) {
                    // 複製檔案失敗:建立目標檔案所在目錄失敗
                    return false;
                }
            }
        }

        // 複製檔案
        int byteread = 0; // 讀取的位元組數
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(destFile);
            byte[] buffer = new byte[1024];

            while ((byteread = in.read(buffer)) != -1) {
                out.write(buffer, 0, byteread);
            }
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (out != null)
                    out.close();
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void installApk(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.d("installApk", "installApk");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }


    private void closeSearchView() {
//        if (mSearchView.isShown()) {
//            mSearchView.setQuery("", false);
//            mSearchMenuItem.collapseActionView();
//            mSearchView.setIconified(true);
//        }
    }
}
