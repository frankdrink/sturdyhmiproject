package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.sturdy.hmi.Constants.saveMenuSettingKey;

public class MainMenuSettingActivity extends BaseToolBarActivity {
    private Activity mActivity;
    List<MainMenuSettingElement> settingElementList;
    TextView maxSelectTextView;
    ListView list;
    List<Boolean> listIsChecked;    // 這個用來記錄哪幾個 item 是被打勾的
    String[] homeSettingName = {
            "Unwrapped 121",
            "Unwrapped 134",
            "Wrapped 121",
            "Wrapped 134",
            "Instrument 121",
            "Instrument 134",
            "Prion",
            "Flash",
            "Dry",
            "Customization",
            "Leakage test",
            "Helix test",
            "B&D test"

    } ;

    String[] homeSettingName_30L = {
            "Liquid 1",
            "Liquid 2",
            "Solid 1",
            "Solid 2",
            "Agar",
            "Dissolution",
            "Dry Only",
            "Waste",
            "User 1",
            "User 2",
            "Leakage test",
            "Flash",
            "B&D test",
            "Latex"

    };

    Integer[] imageId = {
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_prion_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.steri_list_icon_dry_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.fuction_test_list_icon_leakage_test_normal,
            R.drawable.fuction_test_list_icon_helix_test_normal,
            R.drawable.fuction_test_list_icon_b_d_test_normal
    };

    Integer[] imageId_30L = {
            R.drawable.steri_list_icon_liquid_normal,
            R.drawable.steri_list_icon_liquid_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_agar_normal,
            R.drawable.steri_list_icon_dissollution_normal,
            R.drawable.steri_list_icon_dry_normal,
            R.drawable.steri_list_icon_waste_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.fuction_test_list_icon_leakage_test_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.fuction_test_list_icon_b_d_test_normal,
            R.drawable.steri_list_icon_latex_normal

    };

    Integer[] homeImageId = {
            R.drawable.home_icon_unwrapped_normal,
            R.drawable.home_icon_unwrapped_normal,
            R.drawable.home_icon_wrapped_normal,
            R.drawable.home_icon_wrapped_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_prion_normal,
            R.drawable.home_icon_flash_normal,
            R.drawable.home_icon_dry_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_leakage_test_normal,
            R.drawable.home_icon_helix_test_normal,
            R.drawable.home_icon_b_d_test_normal
    };


    Integer[] homeImageId_30L = {
            R.drawable.home_icon_liquid_normal,
            R.drawable.home_icon_liquid_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_instrument_normal,
            R.drawable.home_icon_agar_normal,
            R.drawable.home_icon_dissollution_normal,
            R.drawable.home_icon_dry_normal,
            R.drawable.home_icon_waste_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_user_normal,
            R.drawable.home_icon_leakage_test_normal,
            R.drawable.home_icon_flash_normal,
            R.drawable.home_icon_b_d_test_normal,
            R.drawable.home_icon_latex_normal

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_setting_menu);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Program Selection", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        maxSelectTextView = (TextView) findViewById(R.id.max_program_view);
        final MainSettingListAdapter listAdapter;
        final boolean[] listChecked;
        listIsChecked = new ArrayList<Boolean>();

        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            listChecked = new boolean[homeSettingName_30L.length+1];
            for(int x=1;x<=homeSettingName_30L.length;x++)
            {
                listIsChecked.add(false);
                listChecked[x] = false;
            }
            // 取出主畫面的設定資料
            String json = (Constants.instance().fetchValueString(saveMenuSettingKey));
            if (json != null)
            {
                Gson gson = new Gson();
                Type type = new TypeToken<List<MainMenuSettingElement>>(){}.getType();
                settingElementList = new ArrayList<MainMenuSettingElement>();
                settingElementList = gson.fromJson(json, type);
                for(int i = 0; i < settingElementList.size(); i++)
                {
                    listIsChecked.set(settingElementList.get(i).getId(), true);
                    listChecked[settingElementList.get(i).getId()] = true;
//                Log.d(TAG, alterSamples.get(i).getName()+":" + alterSamples.get(i).getX() + "," + alterSamples.get(i).getY());
                }
            }else{
                listIsChecked.set(0, true);
                listChecked[0] = true;
                listIsChecked.set(2, true);
                listChecked[2] = true;
                listIsChecked.set(4, true);
                listChecked[4] = true;
                listIsChecked.set(5, true);
                listChecked[5] = true;
                listIsChecked.set(7, true);
                listChecked[7] = true;
                listIsChecked.set(8, true);
                listChecked[8] = true;

            }
            listAdapter = new
                    MainSettingListAdapter(this, homeSettingName_30L, imageId_30L, listChecked);
        }else{
            listChecked = new boolean[homeSettingName.length+1];
            for(int x=1;x<=homeSettingName.length;x++)
            {
                listIsChecked.add(false);
                listChecked[x] = false;
            }
            // 取出主畫面的設定資料
            String json = (Constants.instance().fetchValueString(saveMenuSettingKey));
            if (json != null)
            {
                Gson gson = new Gson();
                Type type = new TypeToken<List<MainMenuSettingElement>>(){}.getType();
                settingElementList = new ArrayList<MainMenuSettingElement>();
                settingElementList = gson.fromJson(json, type);
                for(int i = 0; i < settingElementList.size(); i++)
                {
                    listIsChecked.set(settingElementList.get(i).getId(), true);
                    listChecked[settingElementList.get(i).getId()] = true;
//                Log.d(TAG, alterSamples.get(i).getName()+":" + alterSamples.get(i).getX() + "," + alterSamples.get(i).getY());
                }
            }else{
                listIsChecked.set(2, true);
                listChecked[2] = true;
                listIsChecked.set(3, true);
                listChecked[3] = true;
                listIsChecked.set(6, true);
                listChecked[6] = true;
                listIsChecked.set(7, true);
                listChecked[7] = true;
                listIsChecked.set(10, true);
                listChecked[10] = true;
                listIsChecked.set(11, true);
                listChecked[11] = true;
            }
            listAdapter = new
                    MainSettingListAdapter(this, homeSettingName, imageId, listChecked);
        }
        list=(ListView)findViewById(R.id.list);
        list.setClickable(false);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SwitchButton switchButton=(SwitchButton)view.findViewById(R.id.menu_setting_swich);
                boolean isChecked = !listChecked[position];//switchButton.isChecked();
                int checkedCount = getSelectCheckedCount();
                if (checkedCount>=6 && isChecked) return;
                if (checkedCount<=0 && !isChecked) return;
                switchButton.setChecked(isChecked);
                listIsChecked.set(position,isChecked);
                listChecked[position] = isChecked;
                listAdapter.setChecked(listChecked);
                checkedCount = getSelectCheckedCount();
                maxSelectTextView.setText(checkedCount+"/6");
                if (checkedCount==6){
                    maxSelectTextView.setTextColor(Color.RED);
                }else{
                    maxSelectTextView.setTextColor(Color.BLACK);
                }
//                listAdapter.notifyDataSetChanged();
//                switch (position){
//                    case 0:
//                        startActivity(new Intent(MainMenuSettingActivity.this, UnwrappedEditActivity.class));
//                        break;
//                    case 1:
//                        startActivity(new Intent(MainMenuSettingActivity.this, WrappedEditActivity.class));
//                        break;
//                    case 2:
//                        startActivity(new Intent(MainMenuSettingActivity.this, InstrumentEditActivity.class));
//                        break;
//                    case 3:
//                        startActivity(new Intent(MainMenuSettingActivity.this, PrionEditActivity.class));
//                        break;
//                    case 4:
//                        startActivity(new Intent(MainMenuSettingActivity.this, LiquidEditActivity.class));
//                        break;
//                    case 5:
////                        startActivity(new Intent(SterilizationMenuActivity.this, InstrumentEditActivity.class));
//                        break;
//                }
//                overridePendingTransition(0, 0);
            }
        });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });


//        for (int i = 0; i < listAdapter.getCount(); i++){
////            MainSettingListAdapter ss = listAdapter.getPosition(i);
//            if(listAdapter.isChecked()) {
////                tv.setText(tv.getText() + " " + listAdapter.modelArrayList.get(i).getAnimal());
//            }
//        }

    }

    private int getSelectCheckedCount() {
        int checked = 0;
        for (int i=0;i<listIsChecked.size();i++) {
            if (listIsChecked.get(i).booleanValue()==true){
                checked++;
            }
        }
        return checked;
    }

    private void saveMainMenuSettting() {
        List<MainMenuSettingElement> alterSamples = new ArrayList<MainMenuSettingElement>();
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            for (int i=0;i<listIsChecked.size();i++) {
                if (listIsChecked.get(i).booleanValue()==true){
                    alterSamples.add(new MainMenuSettingElement(i,homeImageId_30L[i],homeSettingName_30L[i]));
                }
            }
        }else{
            for (int i=0;i<listIsChecked.size();i++) {
                if (listIsChecked.get(i).booleanValue()==true){
                    alterSamples.add(new MainMenuSettingElement(i,homeImageId[i],homeSettingName[i]));
                }
            }
        }
        Gson gsonW = new Gson();
        String jsonW = gsonW.toJson(alterSamples);
        Constants.instance().storeValueString(saveMenuSettingKey, jsonW);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int checkedCount = getSelectCheckedCount();
        maxSelectTextView.setText(checkedCount+"/6");
        if (checkedCount==6){
            maxSelectTextView.setTextColor(Color.RED);
        }else{
            maxSelectTextView.setTextColor(Color.BLACK);
        }
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        saveMainMenuSettting();
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
