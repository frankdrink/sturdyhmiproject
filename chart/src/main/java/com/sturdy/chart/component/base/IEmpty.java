package com.sturdy.chart.component.base;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sturdy.chart.data.style.FontStyle;


public interface IEmpty extends IComponent<String> {

     void draw(Canvas canvas,  Paint paint);

     FontStyle getFontStyle();

     void setFontStyle(FontStyle fontStyle) ;

     String getEmptyTip();

     void setEmptyTip(String emptyTip);
}
