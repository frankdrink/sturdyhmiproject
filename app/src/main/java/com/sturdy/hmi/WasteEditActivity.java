package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DoublePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.preScanControl;

public class WasteEditActivity extends BaseToolBarActivity implements NumberDialog.OnNumberDialogDoneListener,SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;
    private TextView mCTTextView;
    private TextView mFTTextView;
    private String[] pickerTypeArrayList;
    private String[] pickerExhaustLevelArrayList;
    private int[] iPickerExhaustLevelArrayList;
    private String[] pickerTempArrayList;
    private int[] iPickerTempArrayList;
    private String[] pickerCTTempArrayList;
    private int[] iPickerCTTempArrayList;
    private String[] pickerFTTempArrayList;
    private int[] iPickerFTTempArrayList;
    private String[] pickerDryTimeArrayList;

    private String[] switchArray = {"ON","OFF"};

    private int highTemp;
    private int lowTemp;
    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String str;

    private boolean isFirstPrescan = true;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x11) { // ID(0~11);Type(Liquid/Solid/Wixed);滅菌溫度(Word,0.1℃); 滅菌時間(Word,秒); 高壓快速冷卻(ON/OFF);
                    GlobalClass.getInstance().CB_WasteType = recv[3];
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    ch2 = new char[2];
                    System.arraycopy(recv, 6, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTime = CharUtils.charArrayToInt(ch2);
                    GlobalClass.getInstance().CB_PressurizedCooling = recv[8]!=0;
                    str = pickerTypeArrayList[GlobalClass.getInstance().CB_WasteType];
                    mItems.set(0, new RecyclerViewClass("Type", str, R.drawable.parameter_icon_prevacuum,true));
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_SteriTemp);
                    mItems.set(1, new RecyclerViewClass("Ster. Temp.", str, R.drawable.parameter_icon_temp,false));
                    str = String.format("%dm %ds",GlobalClass.getInstance().CB_SteriTime/60,GlobalClass.getInstance().CB_SteriTime%60);
                    mItems.set(2, new RecyclerViewClass("Ster. Time", str, R.drawable.parameter_icon_time,false));
                    if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
                        str = GlobalClass.getInstance().CB_PressurizedCooling == true ? "ON" : "OFF";
                        if (GlobalClass.getInstance().CB_WasteType==1) {
                            mItems.set(3, new RecyclerViewClass("v", str, R.drawable.parameter_icon_pressurized_cooling,true));
                        }else{
                            mItems.set(3, new RecyclerViewClass("Pressurized Cooling", str, R.drawable.parameter_icon_pressurized_cooling,true));
                        }
                    }else{
                        mItems.set(3, new RecyclerViewClass("v", "", R.drawable.parameter_icon_pressurized_cooling,true));
                    }

//                    mItems.set(0, new RecyclerViewClass("Ster. Temp.", str, R.drawable.parameter_icon_prevacuum,true));
//                    mItems.set(1, new RecyclerViewClass("Ster. Time", str, R.drawable.parameter_icon_temp,true));
//                    str = Integer.toString(CB_ExhaustLevel);
//                    mItems.set(2, new RecyclerViewClass("Exhaust Level", str, R.drawable.parameter_icon_time,true));
//                    if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
//                        str = CB_PressurizedCooling == true ? "ON" : "OFF";
//                        mItems.set(3, new RecyclerViewClass("Pressurized Cooling", str, R.drawable.parameter_icon_time, true));
//                    }else{
//                        CB_PressurizedCooling = false;
//                    }
                    mAdapter.notifyDataSetChanged();
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_waste_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        GlobalClass.getInstance().isFloatingTemp = false;

        pickerTypeArrayList = new String[]{
                "Liquid",  "Solid",  "Mixed"
        };

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_2_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_3_button, 64);


        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();
        mItems.add(0, new RecyclerViewClass("Type", "", R.drawable.parameter_icon_prevacuum,true));
        mItems.add(1, new RecyclerViewClass("Ster. Temp.", "", R.drawable.parameter_icon_temp,false));
        mItems.add(2, new RecyclerViewClass("Ster. Time", "", R.drawable.parameter_icon_time,false));
        if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
            mItems.add(3, new RecyclerViewClass("Pressurized Cooling", "", R.drawable.parameter_icon_pressurized_cooling,true));
        }else{
            mItems.add(3, new RecyclerViewClass("v", "", R.drawable.parameter_icon_pressurized_cooling,true));
        }

        mAdapter = new SteriEditAdapter(this, mItems, true);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WasteEditActivity.this,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WasteEditActivity.this,WasteInformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });


        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(WasteEditActivity.this, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendCustomizationSetting();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendCustomizationSetting();
                    sendGetSterillzationStep();
                }
            }
        });
        // 取得消毒行程資(Waste)
        char[] data = new char[]{0x02,0x11,GlobalClass.getInstance().cLoginUserID};
        sendToService(data);

    }

    void sendCustomizationSetting() {   // Type(Liquid/Solid/Wixed); 高壓快速冷卻(ON/OFF); Solid:不顯示FloatSensor 其他的都要顯示
        if (GlobalClass.getInstance().CB_WasteType==1) {
            GlobalClass.getInstance().isFloatingTemp = false;
        }else{
            GlobalClass.getInstance().isFloatingTemp = true;
        }
        char[] data = new char[4];
        char[] ch22 = new char[2];
        data[0] = 0x03;
        data[1] = 0x2d;
        data[2] = (char)GlobalClass.getInstance().CB_WasteType;
        data[3] = GlobalClass.getInstance().CB_PressurizedCooling?(char)1:0;
        sendToService(data);
    }



    @Override
    public void updateItemList(final int position) {
        if (position==0) {
            OptionPicker picker = new OptionPicker(WasteEditActivity.this, pickerTypeArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedIndex(GlobalClass.getInstance().CB_WasteType);
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_WasteType = index;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    if (index==1) {
                        mItems.set(3, new RecyclerViewClass("v", mItems.get(3).getMessage2(), R.drawable.parameter_icon_pressurized_cooling, true));
                    }else{
                        if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
                            mItems.set(3, new RecyclerViewClass("Pressurized Cooling", mItems.get(3).getMessage2(), R.drawable.parameter_icon_pressurized_cooling, true));
                        }else{
                            mItems.set(3, new RecyclerViewClass("v", mItems.get(3).getMessage2(), R.drawable.parameter_icon_pressurized_cooling, true));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
//        }else if (position==1) {
//            TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setTitleTextSize(40);
//            picker.setDividerVisible(true);
//            picker.setUseWeight(false);
//            picker.setCycleDisable(false);
//            picker.setRangeStart(0, 0);//00:00
//            picker.setRangeEnd(23, 59);//23:59
//            int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//            int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
//            picker.setSelectedItem(currentHour, currentMinute);
//            picker.setTopLineVisible(true);
//            picker.setCanceledOnTouchOutside(true);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setTextPadding(ConvertUtils.toPx(this, 15));
//            picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
//                @Override
//                public void onTimePicked(String hour, String minute) {
//                    CB_SteriTime = Integer.valueOf(hour)*60*60+Integer.valueOf(minute);
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), hour+" h "+minute+" m", mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
//        }else if (position==2) {
//            OptionPicker picker = new OptionPicker(WasteEditActivity.this, pickerExhaustLevelArrayList);
//            picker.setCanceledOnTouchOutside(false);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setTitleTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setSelectedIndex(1);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
//                @Override
//                public void onOptionPicked(int index, String item) {
//                    CB_ExhaustLevel = index;
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
        }else if (position==3) {
            OptionPicker picker = new OptionPicker(WasteEditActivity.this, switchArray);
            picker.setSelectedIndex(GlobalClass.getInstance().CB_PressurizedCooling?0:1);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_PressurizedCooling = (index==0)?true:false;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else {
//            NumberDialog myDiag =
//                    NumberDialog.newInstance(4, 1000);
//            myDiag.show(getSupportFragmentManager(), "altitude");
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }

    @Override
    public void onDone(int value) {
//        iNextService = value;
//        valueStr = Integer.toString(iNextService);
//        data1.set(1,valueStr);
//        adapter1.resetAdapter(data1);
//        adapter1.notifyDataSetChanged();
    }
}
