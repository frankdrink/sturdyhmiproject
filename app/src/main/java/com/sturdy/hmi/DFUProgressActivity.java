package com.sturdy.hmi;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.view.circleprogressview.CircleProgressView;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.STM32.STM32Firmware;
import com.sturdy.hmi.STM32.STM32Flasher;
import com.sturdy.hmi.STM32.STM32OperationProgressListener;
import com.sturdy.hmi.STM32.STM32UsartInterface;
import com.sturdy.hmi.adapter.CycleListAdapter;
import com.sturdy.hmi.adapter.EditListAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import dma.xch.hmi.api.HmiClient;
import dma.xch.hmi.api.UntiTools;
import dma.xch.hmi.api.callback.UartDataCallBack;

import android.provider.Settings.System;

import static com.sturdy.hmi.Constants.ACTION_TO_SERVICE;
import static com.sturdy.hmi.Constants.BrightnessKey;
import static com.sturdy.hmi.Constants.SoundKey;

public class DFUProgressActivity extends BaseToolBarActivity {
    private Activity mActivity;
    Handler handler;
    private CircleProgressView cpv;
    TextView descTV;
    String str;
    String dfuFileName="";
    STM32Flasher flasher=null;
    JsscSerialIface jsscIface=null;
    STM32Firmware mFw=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.CB_DFU;
        GlobalClass.getInstance().mSendCmdQueue.clear();
        mActivity = this;
        Intent intent = this.getIntent();
        dfuFileName = intent.getStringExtra("path");
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_usbprogress);
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        registerBaseActivityReceiver();
        startShowTitleClock(rightTimeButton);

        mActivity = this;
        cpv = findViewById(R.id.cpv);
        cpv.setShowTick(false);
        cpv.setTurn(false);
        cpv.setProgressColor(0xff0277bd);
//        cpv.showAnimation(100,3000);
        descTV =  (TextView)findViewById(R.id.description_textview);

//
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalClass.getInstance().dfuBufferPos = 0;
        GlobalClass.getInstance().client.setGpioState(17, 1);
        GlobalClass.getInstance().client.setGpioState(18, 0);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GlobalClass.getInstance().client.setGpioState(18, 1);
        new GetCSDNLogoTask().execute( "" );
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class GetCSDNLogoTask extends AsyncTask<String,Integer,Boolean> {
        int totalCount;
        int currentCount;
        @Override
        protected Boolean doInBackground(String... params) {
            runOnUiThread(() -> {
                str = String.format("Load Firmware Bin...");
                descTV.setText(str);
            });
            publishProgress(0);

            try{
                STM32Firmware mFw = new STM32Firmware(dfuFileName);
//                str = String.format("Load Firmware Data");
//                descTV.setText(str);
//            jsscIface = new JsscSerialIface(false);
                flasher = new STM32Flasher(mActivity, false);
//                str = String.format("Init Bootload...");
//                descTV.setText(str);
                flasher.registerProgressListener(new STM32OperationProgressListener() {
                    @Override
                    public void completed(boolean successfull) {
//                        if (!successfull)
//                            System.err.println("failure");
//                        else
//                            System.err.println("done");
                    }

                    @Override
                    public void progress(long current, long total) {
//                        System.out.println("progress: " + current + " of " + total);
                        totalCount = (int)total;
                        currentCount = (int)current;
                        float progress = (float)current/(float)total*(float)100;
                        publishProgress((int)progress);
                    }
                });


                boolean bConn = flasher.connect();
                flasher.flashFirmware(mFw.getBuffer(), STM32Flasher.EraseMode.Full, false);

            } catch (Exception e) {
                e.printStackTrace();
            }
//            int totalCount = 3600*4;
//            for (int i=0;i<totalCount;i++) {
//                appendLog("S00-00 045:43  135.0  2.297");
//                appendLog("S00-00 045:43  135.0  2.297");
//                appendLog("S00-00 045:43  135.0  2.297");
//                appendLog("S00-00 045:43  135.0  2.297");
//                float progress = (float)i/(float)totalCount*(float)100;
//
//                publishProgress((int)progress);
//            }

            publishProgress(100);
            //mImageView.setImageBitmap(result); // 不能在背景處理UI
            return (Boolean.TRUE);
        }

        protected void onProgressUpdate(Integer... progress) {
            try {
                cpv.setProgress((int) progress[0]);
                if (progress[0] == 0) {
                    str = String.format("Update Initial...");
                } else {
//                str = String.format("%d / %d byte\nFirmware Update...",currentCount,totalCount);
                    //str = String.format("%d % byte\nFirmware Update...",(float)currentCount/(float)totalCount*(float)100);
                    str = String.format("%d ％\nFirmware Update...", (int) ((float) currentCount / (float) totalCount * (float) 100));

                }
            }catch (Exception e) {
                str = "";
            }
            descTV.setText(str);

        }

        protected void onPostExecute(Boolean aBoolean) {
            GlobalClass.getInstance().client.setGpioState(17, 0);
            GlobalClass.getInstance().client.setGpioState(18, 0);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            GlobalClass.getInstance().client.setGpioState(18, 1);
            str = "100 %\nDFU Finish";
            descTV.setText(str);
            GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.IDLE;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeAllActivities();
                }
            }, 3000);
        }

        protected void onPreExecute () {//在 doInBackground(Params...) 之前

        }

        protected void onCancelled () {
        }

    }

    public void appendLog(String text)
    {
        File logFile = new File("/storage/udisk3", "P20191118.Detial");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return;
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static class JsscSerialIface extends STM32UsartInterface implements UartDataCallBack {
        HmiClient client=null;
        private HashMap<Integer,Integer> portMap = null;
        private ArrayList<char[]> mRecvCmdQueue = null;
        boolean mDebug = false;
        static int BUFFER_SIZE = 2048;
        private byte[] dfuRecvBuffer = new byte[BUFFER_SIZE];
        private int dfuBufferPos = 0;

        public JsscSerialIface(boolean debug) {
            mDebug = debug;
            try {
                if (client == null) {
                    mRecvCmdQueue = new ArrayList<char[]>();
                    client = HmiClient.getInstance();
                    client.initSDK();
                    int count =0;
                    int openCount = 0;
                    StringBuffer sbHint = new StringBuffer();
                    portMap = new HashMap<>();
                    sbHint.append("串口");
                    boolean firstAppend = true;
                    for(int i=0;i<2;i++){
                        boolean chooice = true;
                        if(chooice){
                            boolean isOpen = client.openUart(count,this,i+1,HmiClient.BAUD_RATE_115200);
                            int port = i+1;
                            portMap.put(count,port);
                            if(isOpen){
                                openCount++;
                                if(firstAppend){
                                    firstAppend = false;
                                    sbHint.append(""+Integer.valueOf(i));
                                }else{
                                    sbHint.append(","+Integer.valueOf(i));
                                }
                            }
                            count++;
                        }
                    }
                }
            }catch (Exception e){

            }
        }

        @Override
        public void UartData(final int i, final char[] chars) {
            if (chars != null && chars.length > 0) {
//                mRecvCmdQueue.add(chars);
                writeDfuBuffer(chars);
                String receviceMsg = UntiTools.CharToString(chars,chars.length);
                appendLog("U:"+receviceMsg);
            }
        }

        private void writeDfuBuffer(char[]recv) {
            for(int i=0;i < recv.length;i++){
                dfuRecvBuffer[dfuBufferPos]=(byte)recv[i];
                dfuBufferPos++;
            }
        }

        private byte[] recvDfuBuffer(int recvCount) {
            if (dfuBufferPos-recvCount<0) return null;
            dfuBufferPos-=recvCount;
            byte[] recvBuffer = new byte[recvCount];
            java.lang.System.arraycopy(dfuRecvBuffer, 0, recvBuffer, 0, recvCount);
            byte[] result = new byte[BUFFER_SIZE];
            java.lang.System.arraycopy(dfuRecvBuffer, recvCount, result, 0, BUFFER_SIZE-recvCount);
            java.lang.System.arraycopy(result, 0, dfuRecvBuffer, 0, BUFFER_SIZE);

            return recvBuffer;
        }

        public static String asHex (byte buf[]) {
            StringBuffer strbuf = new StringBuffer(buf.length * 2);
            int i;
            for (i = 0; i < buf.length; i++) {
                if (((int) buf[i] & 0xff) < 0x10)
                    strbuf.append("0");
                strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
            }
            return strbuf.toString();
        }

        public static String ByteToString(byte[] data, int len) {
            String str = "";

            for(int i = 0; i < len; ++i) {
                if((data[i] >> 4 & 15) < 10) {
                    str = str + (byte)(48 + (data[i] >> 4 & 15));
                } else {
                    str = str + (byte)(65 + ((data[i] >> 4 & 15) - 10));
                }

                if((data[i] & 15) < 10) {
                    str = str + (byte)(48 + (data[i] & 15));
                } else {
                    str = str + (byte)(65 + ((data[i] & 15) - 10));
                }

                str = str + " ";
            }

            return str;
        }

        @Override
        public byte[] read(int count, int timeout) throws IOException, TimeoutException {
            try {
//                byte[] buffer = mSerialPort.readBytes(count, timeout);
//
//                if (mDebug)
//                    System.out.println("read bytes 0x" + Hex.encodeHexString( buffer ));
//
//                return buffer;
                char[] recv;
                byte[] buffer=null;
                boolean bRecv = false;
                for (int i=0;i<100;i++) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    buffer = recvDfuBuffer(count);
                    if (buffer!=null) break;
                }
                if (buffer!=null) {
                    String receviceMsg = asHex(buffer);
                    appendLog(receviceMsg);
                }else{
//                    buffer = new byte[1];
//                    buffer[0] = 0x79;
                    appendLog("null");
                }

                return buffer;
            } catch (Exception e) {
                throw new IOException(e);
            }
        }

        public void appendLog(String text)
        {
            File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "dfu_uart.txt");
            if (!logFile.exists())
            {
                try
                {
                    logFile.createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                return;
            }
            try
            {
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append(text);
                buf.newLine();
                buf.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void write(byte[] bytes) throws IOException {
            int select_port = 1;
            if (select_port==1){                // CB
                char[] convertedChar = new char[bytes.length];
                for(int i=0;i < bytes.length;i++){
                    convertedChar[i]=(char)bytes[i];
                }
                client.sendUartByte(getKeyFromMap(portMap, select_port + 1), convertedChar, convertedChar.length);
                String receviceMsg = asHex(bytes);
                appendLog("S:"+receviceMsg);
            }
        }

        public int getKeyFromMap(HashMap<Integer,Integer> portMap,int port){
            int portNum = -1;
            Set<Integer> keySet = portMap.keySet();
            Iterator<Integer> iterator = keySet.iterator();
            while(iterator.hasNext()){
                int key = iterator.next();
                int portVal = portMap.get(key);
                if(portVal==port){
                    portNum = key;
                }
            }
            return portNum;
        }

        public void setDebug(boolean d) {
            this.mDebug = d;
        }
    }

}
