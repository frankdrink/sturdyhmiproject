package com.sturdy.hmi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.RecyclerViewDisabler;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import javax.microedition.khronos.opengles.GL;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.ExhaustTempKey;
import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class CompleteFailedSterilizationActivity extends BaseToolBarActivity implements StartSterGridViewAdapter1.OnItemClickListener,StartSterGridViewAdapter2.OnItemClickListener{
    Handler handler;
    Runnable run;
    private RecyclerView gridView_1, gridView_2;
    private StartSterGrid2Adapter mAdapter;
    private List<StartSterGridRecycler> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private StartSterGridViewAdapter1 gridViewAdapter1;
    private StartSterGridViewAdapter2 gridViewAdapter2;
    private ImageView fragmentBackgroundImage;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<StartSterInformationItemA> startSterInformationItemA;
    private ArrayList<RecyclerViewItem> recyclerViewButton2;
    TextView progressBarTitleView, programBarTitleView;
    ScrollerViewPager viewPager;
    DottedProgressBar progressBar;
    private int progressBarNumberOfDots;
    private int progressBarIndex = 0;

    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;
    private int iDelay = 5;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x34) { // 行程成功狀態通知 溫度1(Word,0.1℃); 壓力(Word,0.001bar); 溫度2(Word,0.1℃)
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 2, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    GlobalClass.getInstance().CB_Bar = CharUtils.charArrayToInt(ch2);
                    if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                        if (recv.length > 7) {
                            System.arraycopy(recv, 6, ch2, 0, 2);
                            GlobalClass.getInstance().CB_ETTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        if (recv.length>9) {
                            System.arraycopy(recv, 8, ch2, 0, 2);
                            GlobalClass.getInstance().CB_FTTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    refreshInformaton();
                    mAdapter.notifyDataSetChanged();
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void refreshInformaton() {
        if (GlobalClass.getInstance().isFloatingTemp || (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
            if (GlobalClass.getInstance().isFloatingTemp && (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure, false));
                }
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_ETTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_FTTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(3, new StartSterGridRecycler("Floating Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(3, new StartSterGridRecycler("Floating Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }

            }else if (GlobalClass.getInstance().isFloatingTemp) {
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure, false));
                }
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_FTTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(2, new StartSterGridRecycler("Floating Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(2, new StartSterGridRecycler("Floating Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
            }else if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure, false));
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure, false));
                }
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_ETTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    mItems.set(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
                } else {
                    mItems.set(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
                }
            }

        }else{
            String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
            if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp, true));
            } else {
                mItems.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp, true));
            }
            str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
            if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure, false));
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure, false));
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure, false));
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure, false));
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                mItems.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure, false));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_completesterilization);
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        Constants.instance(this.getApplicationContext());
        GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.STERI_FAIL;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        mTopBar.setBackgroundColor( this.getResources().getColor(R.color.colorTitlecCompleteFailedColor));
        fragmentBackgroundImage = (ImageView) findViewById(R.id.fragment_bgview);
        fragmentBackgroundImage.setImageResource(R.drawable.stage_bg_orange);
        programBarTitleView = (TextView) findViewById(R.id.program_title_view);
        programBarTitleView.setText(Constants.instance().fetchValueString(saveSelectMenuNameKey));
        progressBarTitleView = (TextView) findViewById(R.id.progress_title_view);
        progressBarTitleView.setText("Failed");
        setDummyData();
        gridView_1 = (RecyclerView) findViewById(R.id.information_grid_1);
        gridView_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        gridView_1.setHasFixedSize(true);
        GridLayoutManager layoutManager1 = new GridLayoutManager(this, 1);
        gridView_1.setLayoutManager(layoutManager1);
        gridViewAdapter1 = new StartSterGridViewAdapter1(this, startSterInformationItemA,true);
        gridViewAdapter1.setOnItemClickListener(this);
        gridView_1.setAdapter(gridViewAdapter1);


        gridView_2 = (RecyclerView)findViewById(R.id.information_grid_2);
        gridView_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        gridView_2.setLayoutManager(mLayoutManager);
        gridView_2.setItemAnimator(new DefaultItemAnimator());
        gridView_2.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();
        if ((GlobalClass.getInstance().isFloatingTemp) || (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
            if ((GlobalClass.getInstance().isFloatingTemp) && (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp,true));

                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat)==0)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==1)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==2)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==3)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==4)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure,false));

                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_ETTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str, R.drawable.parameter_icon_temp,true));

                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_FTTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(3, new StartSterGridRecycler("Floating Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(3, new StartSterGridRecycler("Floating Temp.", "°F", str, R.drawable.parameter_icon_temp,true));

            }else if (GlobalClass.getInstance().isFloatingTemp){
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp,true));
                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat)==0)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==1)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==2)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==3)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==4)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure,false));
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_FTTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(2, new StartSterGridRecycler("Floating Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(2, new StartSterGridRecycler("Floating Temp.", "°F", str, R.drawable.parameter_icon_temp,true));
            }else if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", str, R.drawable.parameter_icon_temp,true));
                str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
                if (Constants.instance().fetchValueInt(savePresFormat)==0)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==1)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==2)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==3)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure,false));
                else if (Constants.instance().fetchValueInt(savePresFormat)==4)
                    mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure,false));
                str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_ETTemp,true);
                if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                    mItems.add(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
                else
                    mItems.add(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str, R.drawable.parameter_icon_temp,true));
            }

        }else{
            String str = UnitConvert.sTempConvert(0,GlobalClass.getInstance().CB_SteriTemp,true);
            if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                mItems.add(0, new StartSterGridRecycler("Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
            else
                mItems.add(0, new StartSterGridRecycler("Temp.", "°F", str, R.drawable.parameter_icon_temp,true));
            str = UnitConvert.sPresConvert(0,(float)GlobalClass.getInstance().CB_Bar);
            if (Constants.instance().fetchValueInt(savePresFormat)==0)
                mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "bar", str, R.drawable.parameter_icon_pressure,false));
            else if (Constants.instance().fetchValueInt(savePresFormat)==1)
                mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str, R.drawable.parameter_icon_pressure,false));
            else if (Constants.instance().fetchValueInt(savePresFormat)==2)
                mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str, R.drawable.parameter_icon_pressure,false));
            else if (Constants.instance().fetchValueInt(savePresFormat)==3)
                mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "psi", str, R.drawable.parameter_icon_pressure,false));
            else if (Constants.instance().fetchValueInt(savePresFormat)==4)
                mItems.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str, R.drawable.parameter_icon_pressure,false));

        }

        mAdapter = new StartSterGrid2Adapter(this, mItems);
        gridView_2.setAdapter(mAdapter);

//        gridView_2 = (RecyclerView) findViewById(R.id.information_grid_2);
//        gridView_2.setHasFixedSize(true);
//        GridLayoutManager layoutManager2 = new GridLayoutManager(this, 2);
//        gridView_2.setLayoutManager(layoutManager2);
//        gridViewAdapter2 = new StartSterGridViewAdapter2(this, recyclerViewButton2, recyclerViewButton2);
//        gridView_2.setAdapter(gridViewAdapter2);


        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
//        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
        progressBarNumberOfDots = getBgRes().size()-1;
        PagerModelManager manager = new PagerModelManager();
        manager.addCommonFragment(GuideFragment.class, getBgRes(), getTitles());
        ModelPagerAdapter adapter = new ModelPagerAdapter(getSupportFragmentManager(), manager);
        viewPager.setAdapter(adapter);
        viewPager.fixScrollSpeed();

        registerBaseActivityReceiver();
        DrawMeButton recordButton =(DrawMeButton) findViewById(R.id.record_button);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CompleteFailedSterilizationActivity.this, SteriRecordReaderActivity.class));
                overridePendingTransition(0, 0);
            }
        });
//        springIndicator.setViewPager(viewPager);

//        progressBar = (DottedProgressBar) findViewById(R.id.progress);
//        progressBar.setActiveDotIndex(0);
//        progressBar.invalidate();
//        progressBarTitleView.setText(getTitles().get(0));
//        handler = new Handler();
//        handler.postAtTime(runnable, 1000);
    }


    public void stopProgress(View view) {
        progressBar.stopProgress();
    }

    public void startProgress(View view) {
        progressBar.startProgress();
    }

    private List<String> getTitles(){
        return Lists.newArrayList("Failed");
    }

    private List<Integer> getBgRes(){
        return Lists.newArrayList(R.drawable.compile_failed);
    }

    @Override
    public void onItemClick(int position) {
        if ((Constants.instance().fetchValueInt(preIDControl))==1) {
            Constants.instance().storeValueString(userLoginCheckPoint, "PostID");
            this.startActivity(new Intent(CompleteFailedSterilizationActivity.this, UserLoginActivity.class));
        }else {
            this.startActivity(new Intent(CompleteFailedSterilizationActivity.this, DoorOpenActivity.class));
        }
    }

    private void setDummyData() {
        startSterInformationItemA = new ArrayList<>();
        startSterInformationItemA.add(new StartSterInformationItemA(R.drawable.toolbar_information, "Estimated time", "00:00:00"));

//        recyclerViewButton2 = new ArrayList<>();
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_pressure, "Pres.","0.00 bar"));
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_temperature, "Temp","40.0 °C"));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent;
        char[] data;
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if ((Constants.instance().fetchValueInt(postIDControl))==1) {
                    Constants.instance().storeValueString(userLoginCheckPoint, "PostID");
                    this.startActivity(new Intent(CompleteFailedSterilizationActivity.this, UserLoginActivity.class));
                }else {
                    goDoorOpenActvity();
                }
                break;
        }
        return false;
    }

    void goDoorOpenActvity() {
        char[] data = new char[]{0x01, 0x60};
        sendToService(data);
//        this.startActivity(new Intent(mActivity, DoorOpenActivity.class));
//        finish();
    }

    private String getSterilizationTime() {
        String timeStr;
        calendar = Calendar.getInstance();
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        if(i_hour<10){
            hour = "0"+i_hour;
        }else{
            hour = ""+i_hour;
        }
        if(i_minute<10){
            minute = "0"+i_minute;
        }else{
            minute = ""+i_minute;
        }
        if(i_second<10){
            second = "0"+i_second;
        }else{
            second = ""+i_second;
        }
        if(i_month<10){
            month = "0"+i_month;
        }else{
            month = ""+i_month;
        }
        if(i_date<10){
            date = "0"+i_date;
        }else{
            date = ""+i_date;
        }
        hour = "00";
        timeStr = hour+":"+minute+":"+second+"\r\r"+calendar.get(Calendar.YEAR)+"."+month+"."+date;
        calendar.clear();
        calendar = null;
        return timeStr;
    }

    @Override
    public void onPause() {
        super.onPause();
//        handler.removeCallbacks(runnable);
        overridePendingTransition(0, 0);
    }
}
