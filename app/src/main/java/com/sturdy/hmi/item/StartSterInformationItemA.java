package com.sturdy.hmi.item;

public class StartSterInformationItemA {

    private int drawableId;
    private String name;
    private String time;


    public StartSterInformationItemA(int drawableId, String name, String time) {
        this.drawableId = drawableId;
        this.name = name;
        this.time = time;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

}
