package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.filebrowser.FileBrowser;
import com.sturdy.filebrowser.FileBrowserWithCustomHandler;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.RecyclerTouchListener;

import static com.sturdy.filebrowser.Constants.ALLOWED_FILE_EXTENSIONS;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class RecordActivity extends BaseToolBarActivity implements GridViewAdapter.OnItemClickListener{

    private RecyclerView listView;
    private RecyclerView gridView;
    private GridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;
    List<MainMenuSettingElement> settingElementList;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_system_setting);
        mDatabaseHelper=new DatabaseHelper(this);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        gridView = (RecyclerView) findViewById(R.id.grid);

        ImageButton backButton =(ImageButton) findViewById(R.id.back_button);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView menuTitleView = (TextView) findViewById(R.id.menu_title_view);
        menuTitleView.setText("Record");
        menuTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        ImageButton mInfoButton = (ImageButton)findViewById(R.id.main_info_button);
        mInfoButton.setVisibility(View.GONE);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

//        listView.setHasFixedSize(true);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "ListView"
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(horizontalManager);
//        listViewAdapter = new ListViewAdapter(this, corporations);
//        listView.setAdapter(listViewAdapter);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridView.setLayoutManager(layoutManager);
        setSystemMenuElement();
        gridViewAdapter = new GridViewAdapter(this, recyclerViewButton, recyclerViewButton_press);
        gridViewAdapter.setOnItemClickListener(this);
        gridView.setAdapter(gridViewAdapter);
        gridView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gridView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTouch(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is touch!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    public void onItemClick(int position) {

        switch (position) {
            case 0:
                Intent i = new Intent(this, FileBrowserWithCustomHandler.class);
                i.putExtra(ALLOWED_FILE_EXTENSIONS, "std"); // std;std;std
                this.startActivity(i);
                this.overridePendingTransition(0, 0);
                break;

            case 1:
                this.startActivity(new Intent(RecordActivity.this, RecordModeActivity.class));
                this.overridePendingTransition(0, 0);
                break;


        }


    }

    private void setSystemMenuElement() {
        recyclerViewButton = new ArrayList<>();
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.record_card_icon_search_normal, "Search"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.record_card_icon_setting_normal, "Mode"));

        recyclerViewButton_press = new ArrayList<>();
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.record_card_icon_search_pressed, "Search"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.record_card_icon_search_pressed, "Mode"));
    }

    @Override
    public void onBackPressed()
    {
    }

}
