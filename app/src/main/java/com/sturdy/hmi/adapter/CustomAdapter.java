package com.sturdy.hmi.adapter;
/**
 * Created by franklin on 2019/8/8.
 */
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.List;

import com.sturdy.hmi.R;
import com.sturdy.hmi.utils.RecyclerViewClass;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomRecyclerViewHolder>{

    List<RecyclerViewClass> mItems;
    Context mContext;
    boolean onLongPressReceived = false;
    boolean onTextViewEditable = false;

    UpdateMainClass updateMainClass;

    public CustomAdapter(Context context, List<RecyclerViewClass> items){
        mContext = context;
        mItems = items;
        if(context instanceof UpdateMainClass){
            updateMainClass = (UpdateMainClass)context;
        }
    }

    public CustomAdapter(Context context, List<RecyclerViewClass> items, boolean showEditable){
        mContext = context;
        mItems = items;
        if(context instanceof UpdateMainClass){
            updateMainClass = (UpdateMainClass)context;
        }
        onTextViewEditable = showEditable;
    }

    public void setOnLongPressStatus(boolean status){
        onLongPressReceived = status;
        notifyDataSetChanged();
    }

    public void setTextViewEditable(boolean status){
        onTextViewEditable = status;
        notifyDataSetChanged();
    }

    public void setTextViewValue(int index,String newValue){
        notifyDataSetChanged();
    }


    public boolean getLongPressStatus(){
        return onLongPressReceived;
    }

    @Override
    public CustomRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.custom_recyclerview_layout, parent, false);
        return new CustomRecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CustomRecyclerViewHolder holder,final int position) {
        holder.mAvatarView.setImageDrawable(mContext.getResources().getDrawable(mItems.get(position).getmImage()));
        holder.mMsg1.setText(mItems.get(position).getMessage1());
        holder.mMsg2.setText(mItems.get(position).getMessage2());
        holder.mMsg2.setTextColor(Color.BLUE);
        if (mItems.get(position).getmIsTextViewEditable()) {
            if (onTextViewEditable){
                holder.mMsg2.setBackgroundResource(R.drawable.edittext_border);
            }else{
                holder.mMsg2.setBackgroundResource(R.color.trans);
            }
        }

//        if(onLongPressReceived) {
//            holder.checkboxHolder.setVisibility(View.VISIBLE);
//            holder.mCheckBox.setChecked(mItems.get(position).getmIsChecked());
//            holder.mDeleteRow.setVisibility(View.VISIBLE);
//            if(mItems.get(holder.getAdapterPosition()).getmIsChecked()){
//                holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.list_selected));
//            }else {
//                holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.list_long_pressed));
//            }
//        }
            //Checking whether a particular view is clicked or not
//        else{
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBackground));
//            holder.mDeleteRow.setVisibility(View.INVISIBLE);
//            if (mItems.get(position).getmIsChecked()) {
//                holder.checkboxHolder.setVisibility(View.VISIBLE);
//                holder.mCheckBox.setChecked(true);
//            } else {
//                holder.checkboxHolder.setVisibility(View.GONE);
//                holder.mCheckBox.setChecked(false);
//            }
//        }
        //Calls the interface method in Activity to respond to CheckBox changes
//        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                updateMainClass.updateListBackground(holder.getAdapterPosition(), b);
//            }
//        });
        /**
         * <p>Responds to long press made on any row</p>
         * <p>Checks the item on which long press is made
         * sets the onLongPressReceived status to true and notify the adapter to refresh the list.</p>
         */
//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                onLongPressReceived = true;
//                mItems.get(holder.getAdapterPosition()).setmIsChecked(true);
//                notifyDataSetChanged();
//                return true;
//            }
//        });
        //Calls the interface in Activity to remove the item from the List.
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItems.get(position).getmIsTextViewEditable()) {
                    if (onTextViewEditable) {
                        updateMainClass.updateItemList(holder.getAdapterPosition());
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class CustomRecyclerViewHolder extends RecyclerView.ViewHolder{

        private TextView mMsg1, mMsg2;
        private ImageView mAvatarView;
//        private CheckBox mCheckBox;
//        private LinearLayout checkboxHolder;
//        private ImageView mDeleteRow;
        private CardView cardView;


        /**
         * Initializes all the views of a ViewHolder
         * @param itemView parent view in which all the List Item views are present
         */
        public CustomRecyclerViewHolder(View itemView) {
            super(itemView);
            mMsg1 = (TextView)itemView.findViewById(R.id.text_view1);
            mMsg2 = (TextView)itemView.findViewById(R.id.text_view2);
            mAvatarView = (ImageView)itemView.findViewById(R.id.avatar_holder);
//            mCheckBox = (CheckBox)itemView.findViewById(R.id.checkbox);
//            checkboxHolder = (LinearLayout)itemView.findViewById(R.id.checkbox_holder);
//            mDeleteRow = (ImageView)itemView.findViewById(R.id.delete_row);
            cardView = (CardView)itemView.findViewById(R.id.card_holder);
            cardView.setMaxCardElevation(0);
            cardView.setCardElevation(0);
        }
    }
    public interface UpdateMainClass{
        void updateItemList(int position);
        void updateListBackground(int position, boolean isChecked);
    }
}
