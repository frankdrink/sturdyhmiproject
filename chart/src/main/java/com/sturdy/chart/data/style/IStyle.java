package com.sturdy.chart.data.style;

import android.graphics.Paint;


public interface IStyle {

    void fillPaint(Paint paint);
}
