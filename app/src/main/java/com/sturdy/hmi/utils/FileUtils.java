package com.sturdy.hmi.utils;

import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Created by franklin on 2019/12/11.
 */

public class FileUtils {
    public static boolean fileCopy(String oldFilePath,String newFilePath) throws IOException {
        if(fileExists(oldFilePath) == false){
            return false;
        }
        FileInputStream inputStream = new FileInputStream(new File(oldFilePath));
        byte[] data = new byte[1024];
        FileOutputStream outputStream =new FileOutputStream(new File(newFilePath));
        while (inputStream.read(data) != -1) {
            outputStream.write(data);
        }
        inputStream.close();
        outputStream.close();
        return true;
    }

    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }
//  no checksum
//    public static boolean saveStdRecordFile(String saveFileName,byte[] fileDataBytes) {
//        // Encryption Record Data Array
////        byte[] fileDataBytes = recordFileData.getBytes();
//        int index = 0;
//        for (byte b : fileDataBytes) {
//            fileDataBytes[index] = (byte)(((int)b) ^ ((int)'"'));
//            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
//            index++;
//        }
////        writeByteToFile(saveFileName,fileDataBytes);
//        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveFileName);
//        try {
//            FileOutputStream fos = new FileOutputStream(logFile);
//            fos.write(fileDataBytes);
//            fos.close();
//        }catch (Exception e) {
//
//        }
//
//        return true;
//    }
//
//    public static String loadStdRecordFile(String saveFileName) {
//        // Decrypt Record Data Array
//        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveFileName);
//        int size = (int) file.length();
//        byte[] fileDataBytes = new byte[size];
//        try {
//            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
//            buf.read(fileDataBytes, 0, fileDataBytes.length);
//            buf.close();
//        } catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        // Decrypt
//        int index = 0;
//        for (int i=0;i<size;i++) {
//            fileDataBytes[index] = (byte)(((int)fileDataBytes[i]) ^ ((int)'"'));
//            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
//            index++;
//        }
//        String str = new String(fileDataBytes);
////        writeByteToFile(saveFileName,fileDataBytes);
//        return str;
//    }

    // last not 128
    public static boolean saveStdRecordFile(String saveFileName,byte[] fileDataBytes) {
        // Encryption Record Data Array
        String newSave = saveFileName.replace("std", "txt");
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), newSave);
        try {
            FileOutputStream fos = new FileOutputStream(logFile);
            fos.write(fileDataBytes);
            fos.close();
        }catch (Exception e) {

        }
        int appendByte = fileDataBytes.length / 128;
        byte[] saveFileDataBytes = new byte[fileDataBytes.length+appendByte+1];
        int index = 0;
        for (byte b : fileDataBytes) {
            byte xorByte = (byte)Integer.parseInt("10101010", 2);
            fileDataBytes[index] = (byte)(0xff & ((int)b) ^ ((int)xorByte));
            //fileDataBytes[index] = (byte)(((char)b) ^ (0x10101010));
            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
            index++;
        }
        index = 0;
        int nowIndex=0;
        byte[] everyDst_128 = new byte[128];//Arrays.copyOf(fileDataBytes, 128);
        for (int i=0;i<appendByte;i++) {
            for (int j=0;j<128;j++) {
                if (nowIndex>=fileDataBytes.length)
                    break;
                saveFileDataBytes[index] = fileDataBytes[nowIndex];
                everyDst_128[j] = fileDataBytes[nowIndex];
                index++;
                nowIndex++;
            }
            // Checksum every 128 bytes and add to the 129th position
            int iCheckSum = CharUtils.checkSum(everyDst_128);
            saveFileDataBytes[index] = (byte)iCheckSum;
            index++;
        }
        Arrays.fill( everyDst_128, (byte) 0x0 );
        int lastBlockBytes = fileDataBytes.length-nowIndex;
        if (lastBlockBytes>0) {
            appendByte = fileDataBytes.length / 128;
            for (int i=0;i<lastBlockBytes;i++) {
                //nowIndex = appendByte*128+i;
                if (nowIndex>=fileDataBytes.length)
                    break;
                everyDst_128[i] = fileDataBytes[nowIndex];
                saveFileDataBytes[index] = fileDataBytes[nowIndex];
                index++;
                nowIndex++;
            }
            int iCheckSum = CharUtils.checkSum(everyDst_128);
            saveFileDataBytes[index] = (byte)iCheckSum;
        }
        logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveFileName);
        try {
            FileOutputStream fos = new FileOutputStream(logFile);
            fos.write(saveFileDataBytes);
            fos.close();
        }catch (Exception e) {

        }
        return true;
    }


    public static String loadStdRecordFile(String saveFileName) {
        // Decrypt Record Data Array
        File file = new File(saveFileName);
        if (!file.exists()) return "";
        int size = (int) file.length();
        byte[] fileDataBytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(fileDataBytes, 0, fileDataBytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        byte[] saveFileDataBytes = new byte[fileDataBytes.length];
        int save_index = 0;
        int count=0;
        for (int i=0;i<fileDataBytes.length;i++) {
            count++;
            if (count<=128) {
                saveFileDataBytes[save_index] = fileDataBytes[i];
                save_index++;
            }else{
                byte b = fileDataBytes[i];
                count=0;
                b = 0;
            }
        }
        fileDataBytes = new byte[save_index-1];
        // Decrypt
        int index = 0;
        for (int i=0;i<(save_index-1);i++) {
            if (saveFileDataBytes[i]==0) break;
            byte xorByte = (byte)Integer.parseInt("10101010", 2);
            fileDataBytes[index] = (byte)(0xff & ((int)saveFileDataBytes[i]) ^ ((int)xorByte));
            //fileDataBytes[index] = (byte)(((int)saveFileDataBytes[i]) ^ ((int)'"'));
            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
            index++;
        }
        String str = new String(fileDataBytes);
//        writeByteToFile(saveFileName,fileDataBytes);
        return str;
    }

    public static boolean saveCSVRecordFile(String saveFileName,byte[] fileDataBytes) {
        // Encryption Record Data Array
        int appendByte = fileDataBytes.length / 128;
        byte[] saveFileDataBytes = new byte[fileDataBytes.length+appendByte+1];
        int index = 0;
        for (byte b : fileDataBytes) {
            byte xorByte = (byte)Integer.parseInt("10101010", 2);
            fileDataBytes[index] = (byte)(0xff & ((int)b) ^ ((int)xorByte));
            //fileDataBytes[index] = (byte)(((char)b) ^ (0x10101010));
            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
            index++;
        }
        index = 0;
        int nowIndex=0;
        byte[] everyDst_128 = new byte[128];//Arrays.copyOf(fileDataBytes, 128);
        for (int i=0;i<appendByte;i++) {
            for (int j=0;j<128;j++) {
                if (nowIndex>=fileDataBytes.length)
                    break;
                saveFileDataBytes[index] = fileDataBytes[nowIndex];
                everyDst_128[j] = fileDataBytes[nowIndex];
                index++;
                nowIndex++;
            }
            // Checksum every 128 bytes and add to the 129th position
            int iCheckSum = CharUtils.checkSum(everyDst_128);
            saveFileDataBytes[index] = (byte)iCheckSum;
            index++;
        }
        Arrays.fill( everyDst_128, (byte) 0x0 );
        int lastBlockBytes = fileDataBytes.length-nowIndex;
        if (lastBlockBytes>0) {
            appendByte = fileDataBytes.length / 128;
            for (int i=0;i<lastBlockBytes;i++) {
                //nowIndex = appendByte*128+i;
                if (nowIndex>=fileDataBytes.length)
                    break;
                everyDst_128[i] = fileDataBytes[nowIndex];
                saveFileDataBytes[index] = fileDataBytes[nowIndex];
                index++;
                nowIndex++;
            }
            int iCheckSum = CharUtils.checkSum(everyDst_128);
            saveFileDataBytes[index] = (byte)iCheckSum;
        }
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveFileName);
        try {
            FileOutputStream fos = new FileOutputStream(logFile);
            fos.write(saveFileDataBytes);
            fos.close();
        }catch (Exception e) {

        }
        return true;
    }


//    public static boolean saveStdRecordFile(String saveFileName,byte[] fileDataBytes) {
//        // Encryption Record Data Array
//        int appendByte = fileDataBytes.length / 128;
//        String newSave = saveFileName.replace("std", "txt");
//        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), newSave);
//        try {
//            FileOutputStream fos = new FileOutputStream(logFile);
//            fos.write(fileDataBytes);
//            fos.close();
//        }catch (Exception e) {
//
//        }
//        byte[] saveFileDataBytes = new byte[fileDataBytes.length+appendByte+128+1];
//        int index = 0;
//        for (byte b : fileDataBytes) {
//            fileDataBytes[index] = (byte)(((int)b) ^ ((int)'"'));
//            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
//            index++;
//        }
//        index = 0;
//        byte[] everyDst_128 = new byte[128];//Arrays.copyOf(fileDataBytes, 128);
//        for (int i=0;i<appendByte;i++) {
//            for (int j=0;j<128;j++) {
//                int nowIndex = i*128+j;
//                if (nowIndex>=fileDataBytes.length) break;
//                saveFileDataBytes[index] = fileDataBytes[nowIndex];
//                everyDst_128[j] = fileDataBytes[nowIndex];
//                index++;
//            }
//            // Checksum every 128 bytes and add to the 129th position
//            int iCheckSum = CharUtils.checkSum(everyDst_128);
//            saveFileDataBytes[index] = (byte)iCheckSum;
//            index++;
//        }
//        Arrays.fill( everyDst_128, (byte) 0x0 );
//        int lastBlockBytes = fileDataBytes.length%128;
//        if (lastBlockBytes>0) {
//            appendByte = fileDataBytes.length / 128;
//            for (int i=0;i<128;i++) {
//                int nowIndex = appendByte*128+i;
//                if (nowIndex<fileDataBytes.length) {
//                    everyDst_128[i] = fileDataBytes[nowIndex];
//                    saveFileDataBytes[index] = fileDataBytes[nowIndex];
//                }
//                index++;
//            }
//            int iCheckSum = CharUtils.checkSum(everyDst_128);
//            saveFileDataBytes[index] = (byte)iCheckSum;
//        }
//        logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), saveFileName);
//        try {
//            FileOutputStream fos = new FileOutputStream(logFile);
//            fos.write(saveFileDataBytes);
//            fos.close();
//        }catch (Exception e) {
//
//        }
//        return true;
//    }
//
//    public static String loadStdRecordFile(String saveFileName) {
//        // Decrypt Record Data Array
//        File file = new File(saveFileName);
//        if (!file.exists()) return "";
//        int size = (int) file.length();
//        byte[] fileDataBytes = new byte[size];
//        try {
//            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
//            buf.read(fileDataBytes, 0, fileDataBytes.length);
//            buf.close();
//        } catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        byte[] saveFileDataBytes = new byte[fileDataBytes.length];
//        int save_index = 0;
//        int count=0;
//        for (int i=0;i<fileDataBytes.length;i++) {
//            count++;
//            if (count<=128) {
//                saveFileDataBytes[save_index] = fileDataBytes[i];
//                save_index++;
//            }else{
//                byte b = fileDataBytes[i];
//                count=0;
//                b = 0;
//            }
//        }
//        fileDataBytes = new byte[save_index];
//        // Decrypt
//        int index = 0;
//        for (int i=0;i<save_index;i++) {
//            fileDataBytes[index] = (byte)(((int)saveFileDataBytes[i]) ^ ((int)'"'));
//            fileDataBytes[index] = (byte) CharUtils.swapNibbles((int)fileDataBytes[index]);
//            index++;
//        }
//        String str = new String(fileDataBytes);
////        writeByteToFile(saveFileName,fileDataBytes);
//        return str;
//    }




    public static void writeByteToFile(String newFileName,byte[] data)
    {
        if (newFileName==null) return;
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), newFileName);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        char[] writeData = new char[data.length];
        for (int i=0;i<data.length;i++) {
            writeData[i] = (char)data[i];
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.write(writeData);
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 删除文件，可以是文件或文件夹
     *
     * @param fileName
     *            要删除的文件名
     * @return 删除成功返回true，否则返回false
     */
    public static boolean delete(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            System.out.println("删除文件失败:" + fileName + "不存在！");
            return false;
        } else {
            if (file.isFile())
                return deleteFile(fileName);
            else
                return deleteDirectory(fileName);
        }
    }

    /**
     * 删除单个文件
     *
     * @param fileName
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

    /**
     * 删除目录及目录下的文件
     *
     * @param dir
     *            要删除的目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String dir) {
        // 如果dir不以文件分隔符结尾，自动添加文件分隔符
        if (!dir.endsWith(File.separator))
            dir = dir + File.separator;
        File dirFile = new File(dir);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if ((!dirFile.exists()) || (!dirFile.isDirectory())) {
            System.out.println("删除目录失败：" + dir + "不存在！");
            return false;
        }
        boolean flag = true;
        // 删除文件夹中的所有文件包括子目录
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            // 删除子文件
            if (files[i].isFile()) {
                flag = FileUtils.deleteFile(files[i].getAbsolutePath());
                if (!flag)
                    break;
            }
            // 删除子目录
            else if (files[i].isDirectory()) {
                flag = FileUtils.deleteDirectory(files[i]
                        .getAbsolutePath());
                if (!flag)
                    break;
            }
        }
        if (!flag) {
            System.out.println("删除目录失败！");
            return false;
        }
        // 删除当前目录
//        if (dirFile.delete()) {
//            System.out.println("删除目录" + dir + "成功！");
//            return true;
//        } else {
//            return false;
//        }
        return true;
    }
}
