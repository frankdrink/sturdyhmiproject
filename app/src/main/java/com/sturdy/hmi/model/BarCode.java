package com.sturdy.hmi.model;

/**
 * Created by franklin on 2019/8/13.
 */

public class BarCode {
    private int id = -1 ;
    private String serialNumber ="" ;
    private String lastName ="" ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
