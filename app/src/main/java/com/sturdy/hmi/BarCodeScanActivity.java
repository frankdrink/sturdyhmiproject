package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.BarCodeSNCursorAdapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.sql.ReturnMessage;
import com.sturdy.hmi.utils.KyEditText;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import java.util.Calendar;

import static android.view.KeyEvent.KEYCODE_ENTER;

public class BarCodeScanActivity extends BaseToolBarActivity {
    private Activity mActivity;
    private ListView list;
    private ImageView barCodeKeyboardView;
    private KyEditText barCodeSNEdit;
    private DatabaseHelper helper;
    private int bStartSteri;
    private Handler mHandler = new Handler();

    BarCodeSNCursorAdapter adapter;
    String[] web = {
            "Unwrapped",
            "Wrapped",
            "Instrument",
            "Prion",
            "Liquid",
            "Manual",
            "Flash",
            "Dry"
    } ;
    Integer[] imageId = {
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_prion_normal,
            R.drawable.steri_list_icon_prion_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.steri_list_icon_dry_normal


    };
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            barCodeSNEdit.requestFocus();
        }
    };
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_barcodescan);
        mActivity = this;
        Intent intent = this.getIntent();
        bStartSteri = intent.getIntExtra("StartSteri",0);

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitleBackColor));
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        Button mNavTitleView = mNavTopBar.addLeftTextButton("Scan", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });


        helper = new DatabaseHelper(this);
        Cursor cursor = helper.getCustomerDao().getCustomers(new BarCode());
        adapter = new BarCodeSNCursorAdapter(this, cursor, true);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        barCodeSNEdit = (KyEditText)findViewById(R.id.barcode_sn_edit);
        barCodeSNEdit.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        final boolean[] bKeyboard = {false};
        barCodeSNEdit.setOnEditorActionListener(
                new KyEditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                                ) {

                            if (barCodeSNEdit.getText().length()>5){
                                calendar = Calendar.getInstance();

                                String time = String.format("%02d:%02d",calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));

                                BarCode barCode = new BarCode();
                                barCode.setId(-1);
                                barCode.setSerialNumber(barCodeSNEdit.getText().toString());
                                barCode.setLastName(time);
                                ReturnMessage rm = helper.getCustomerDao().addCustomer(barCode);
                                if (rm.success) {
                                    adapter.changeCursor(helper.getCustomerDao().getCustomers(new BarCode()));

                                } else {
                                }
                                barCodeSNEdit.setText("");
                                mHandler.postDelayed(runnable, 1000);
//                            adapter.notifyDataSetChanged();
                                return false;
                            }
                            bKeyboard[0] = !bKeyboard[0];
                            if (bKeyboard[0])
                                return false;
                            else
                            {
                                showKeyboard(barCodeSNEdit);
                                return true;
                            }

                        }
                        return false;
                    }
                });
        barCodeSNEdit.setShowSoftInputOnFocus(false);
        barCodeKeyboardView = (ImageView) findViewById(R.id.barcode_keyboard);
        barCodeKeyboardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    barCodeSNEdit.onEditorAction(EditorInfo.IME_ACTION_DONE);
                }
                return false;
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        hideKeyboard(this);

        DrawMeButton mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bStartSteri==1) {
//                    startActivity(new Intent(BarCodeScanActivity.this, StartSterilizationActivity.class));
//                    overridePendingTransition(0, 0);
//                }
                finish();
            }
        });

//        CustomList listAdapter = new
//                CustomList(this, web, imageId);
//        list=(ListView)findViewById(R.id.list);
//        list.setAdapter(listAdapter);
//        list.setBackgroundColor(Color.WHITE);
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                switch (position){
//                    case 0:
//                        startActivity(new Intent(BarCodeScanActivity.this, UnwrappedEditActivity.class));
//                        break;
//                    case 1:
//                        startActivity(new Intent(BarCodeScanActivity.this, WrappedEditActivity.class));
//                        break;
//                    case 2:
//                        startActivity(new Intent(BarCodeScanActivity.this, InstrumentEditActivity.class));
//                        break;
//                    case 3:
//                        startActivity(new Intent(BarCodeScanActivity.this, PrionEditActivity.class));
//                        break;
//                    case 4:
//                        startActivity(new Intent(BarCodeScanActivity.this, LiquidEditActivity.class));
//                        break;
//                    case 5:
////                        startActivity(new Intent(SterilizationMenuActivity.this, InstrumentEditActivity.class));
//                        break;
//                }
//                overridePendingTransition(0, 0);
//            }
//        });
        startShowTitleClock(rightTimeButton);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(KyEditText edtView) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtView, InputMethodManager.SHOW_IMPLICIT);
    }


    public void onClickOfUpdateDelete(View view) {

        ImageButton ib = (ImageButton) view;
        String[] temp = ib.getTag().toString().split(",");
        int position = Integer.parseInt(temp[1]);
        if (temp[0].equals("update")) {
            Cursor c = (Cursor) adapter.getItem(position);
            BarCode customer = (BarCode) DatabaseHelper.getObjectFromCursor(c, BarCode.class);
            Toast.makeText(this, customer.getSerialNumber() + " " + customer.getLastName(), Toast.LENGTH_SHORT).show();
//            etFirstName.setText(customer.getFirstName());
//            etLastName.setText(customer.getLastName());
//            lvCustomer.setItemChecked(position, true);
        } else if (temp[0].equals("delete")) {
            Cursor c = (Cursor) adapter.getItem(position);
            BarCode customer = (BarCode) DatabaseHelper.getObjectFromCursor(c, BarCode.class);
            ReturnMessage rm = helper.getCustomerDao().deleteCustomer(customer.getId());
//            tvMessage.setText(rm.message);
            if (rm.success) {
//                tvMessage.setTextColor(Color.BLUE);
                Cursor cursor = helper.getCustomerDao().getCustomers(new BarCode());
                adapter.changeCursor(cursor);
            } else {
//                tvMessage.setTextColor(Color.RED);
            }

        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
