package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.chart.component.axis.BaseAxis;
import com.sturdy.chart.component.axis.VerticalAxis;
import com.sturdy.chart.component.base.IAxis;
import com.sturdy.chart.component.base.IComponent;
import com.sturdy.chart.core.LineChart;
import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.LineData;
import com.sturdy.chart.data.style.FontStyle;
import com.sturdy.chart.data.style.LineStyle;
import com.sturdy.chart.data.style.PointStyle;
import com.sturdy.chart.listener.OnClickColumnListener;
import com.sturdy.chart.provider.component.cross.VerticalCross;
import com.sturdy.chart.provider.component.point.LegendPoint;
import com.sturdy.chart.provider.component.point.Point;
import com.sturdy.chart.provider.component.tip.MultiLineBubbleTip;
import com.sturdy.chart.utils.DensityUtils;
import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.filechooser.utils.FileUtil;
import com.sturdy.hmi.Dialog.Notice2Dialog;
import com.sturdy.hmi.Dialog.NotificationDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.FileUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.ExhaustTempKey;
import static com.sturdy.hmi.Constants.MACHINE_STATE.STERI_RUN;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveDateFormat;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.saveTimeFormat;
import static com.sturdy.hmi.Constants.userLoginAccount;

public class StartSterilizationActivity extends BaseToolBarActivity implements StartSterGridViewAdapter1.OnItemClickListener,StartSterGridViewAdapter2.OnItemClickListener{
    Handler handler;
    Runnable run;
    private RecyclerView gridView_1, gridView_2;
    private StartSterGrid2Adapter gridViewAdapter2;
    private List<StartSterGridRecycler> startSterInformationItemB;
    private RecyclerView.LayoutManager mLayoutManager;
    private StartSterGridViewAdapter1 gridViewAdapter1;
    private List<Integer> totalRes;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<StartSterInformationItemA> startSterInformationItemA;
    private ArrayList<RecyclerViewItem> recyclerViewButton2;
    TextView progressBarTitleView, programBarTitleView;
    ScrollerViewPager viewPager;
    DottedProgressBar progressBar;
    private int progressBarNumberOfDots;
    private int progressBarIndex = 0;
    private int totaSterilTime;
    private int printSummaryCount = 0;

    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;
    private int iDelay = 5;
    private char CB_NowStage;
    private String CB_StageName;
    private int CB_TotalTime;
    private int   CB_Time;
    private int CB_Temp;
    private int CB_FTTemp;
    private int CB_ETTemp;
    private int CB_Bar;
    private String printData,CSVData;
    private String str;
    private Double temp_average = new Double(0);
    private Double temp2_average = new Double(0);
    private Double tempET_average = new Double(0);
    private Double pres_average = new Double(0);
    private int total_TempValue;
    private int total_TempCount;
    private int total_TempFTValue;
    private int total_TempFTCount;
    private int total_TempETValue;
    private int total_TempETCount;
    private int total_PresValue;
    private int total_PresCount;
    private boolean bComplete = false;
    private double itemp_average;
    private double ipres_average ;
    private String str1,str2,str3,str4;
    private double F0;
    private int F0_count=0;

    private LineChart lineChart;
    //    private BaseCheckDialog<ChartStyle> chartDialog;
    List<String> chartYDataList;
    ArrayList<Double> tempList1,tempFTList,tempETList;
    ArrayList<Double> presList;
    ChartData<LineData> chartData2;
    List<LineData> ColumnDatas;
    private int addCount = 0;
    String presStr="";

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                str1 = str2 = str3 = str4 = "";
                if (recv[1] == 0x30) {  // 行程中轉換階段通知 "目前階段(Byte); 階段名稱(11 Bytes,ASCII code);時間(Double Word,秒);溫度1(Word,0.1℃); 壓力(Word,0.001bar); E.T.溫度(Word,0.1℃); 溫度2(Word,0.1℃)"
                    char[] ch11 = new char[11];
                    CB_NowStage = recv[2];
                    System.arraycopy(recv, 3, ch11, 0, 11);
                    ch11[10] = ' ';
                    CB_StageName= CharUtils.charToString(ch11);
                    char[] ch4 = new char[4];
                    System.arraycopy(recv, 14, ch4, 0, 4);
                    CB_TotalTime = CharUtils.charArrayToInt(ch4);
                    ch4 = new char[4];
                    System.arraycopy(recv, 18, ch4, 0, 4);
                    CB_Time = CharUtils.charArrayToInt(ch4);
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 22, ch2, 0, 2);
                    CB_Temp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 24, ch2, 0, 2);
                    CB_Bar = CharUtils.charArrayToInt(ch2);
                    if (recv.length>=29) {
                        System.arraycopy(recv, 28, ch2, 0, 2);
                        CB_FTTemp = CharUtils.charArrayToInt(ch2);
                    }
                    viewPager.setCurrentItem(CB_NowStage);
                    int hour = CB_TotalTime/3600;
                    int min = (CB_TotalTime % 3600) / 60;
                    int sec = CB_TotalTime%60;
                    String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
                    StartSterInformationItemA item = new StartSterInformationItemA(startSterInformationItemA.get(0).getDrawableId(),startSterInformationItemA.get(0).getName(),timeStr);
                    startSterInformationItemA.set(0,item);
                    gridViewAdapter1.notifyDataSetChanged();
                    float bar = (float)CB_Bar / (float)1000;
                    str1 = UnitConvert.sTempConvert(0,CB_Temp,true);
                    //refreshInformaton();
                    printData = String.format("%-11s%03d:%02d%6s  %6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,CB_Bar));
                    CSVData = String.format("%-11s,%03d:%02d,%6s,%6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,CB_Bar));
                    saveDtlToCache(CSVData.toCharArray());
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        printData = String.format("%23s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                        CSVData = String.format(",,,%23s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                        saveDtlToCache(CSVData.toCharArray());
                    }
                    //gridViewAdapter2.notifyDataSetChanged();
//                    lineChart.getLegend().setCurrentValue(str1,str2,str3);
                    int stage = (int)GlobalClass.getInstance().globalStageList[CB_NowStage];
                    progressBarTitleView.setText(getStageToString(stage));
                    programEndTime=CB_TotalTime;
//                    new Thread(new Runnable(){
//                        @Override
//                        public void run() {
//                            HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//
//                            try {
//                                List<NameValuePair> params = new ArrayList<NameValuePair>();
//                                params.add(new BasicNameValuePair("programName",Constants.instance().fetchValueString(saveSelectMenuNameKey)));
//                                params.add(new BasicNameValuePair("programStep",getStageToString(stage)));
//                                params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
//                                params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
//                                params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                                params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
//
//
//                                myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                                HttpResponse response = new DefaultHttpClient().execute(myPost);
//                                String er = response.toString();
//                            } catch (Exception e) {
//                                String ee = e.toString();
//                                e.printStackTrace();
//                            }
//                        }
//                    }).start();
                }else if (recv[1] == 0x31) {  // 行程紀錄通知 "目前階段(Byte); 階段名稱(7 Bytes,ASCII code);時間(Double Word,秒); 溫度1(Word,0.1℃); 壓力(Word,0.001bar); 溫度2(Word,0.1℃)"
                    char[] ch11 = new char[11];
                    int cb_NowStage = recv[2];
                    System.arraycopy(recv, 3, ch11, 0, 11);
                    ch11[10] = ' ';
                    CB_StageName= CharUtils.charToString(ch11);
                    char[] ch4 = new char[4];
                    System.arraycopy(recv, 14, ch4, 0, 4);
                    CB_Time = CharUtils.charArrayToInt(ch4);
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 18, ch2, 0, 2);
                    CB_Temp = CharUtils.charArrayToInt(ch2);
                    total_TempValue += CB_Temp;
                    total_TempCount++;
                    float temp = (float)(CB_Temp) / (float)10;
                    F0_count++;
                    if (cb_NowStage>=4) {
                        if (F0_count>=60) {
                            float tempc = temp-GlobalClass.getInstance().Steri_Temp;
                            double f0 = Math.pow(10,(tempc)/10);
                            F0+=f0;
                        }
                    }

                    System.arraycopy(recv, 20, ch2, 0, 2);
                    CB_Bar = CharUtils.charArrayToInt(ch2);
                    total_PresValue += CB_Bar;
                    total_PresCount++;
                    if (recv.length>=23) {
                        System.arraycopy(recv, 22, ch2, 0, 2);
                        CB_ETTemp = CharUtils.charArrayToInt(ch2);
                        total_TempETValue += CB_ETTemp;
                        total_TempETCount++;
                    }
                    if (recv.length>=25) {
                        System.arraycopy(recv, 24, ch2, 0, 2);
                        CB_FTTemp = CharUtils.charArrayToInt(ch2);
                        total_TempFTValue += CB_FTTemp;
                        total_TempFTCount++;
                    }
                    //float bar = (float)CB_Bar / (float)1000;
                    itemp_average = CB_Temp;
                    ipres_average = CB_Bar;
                    int hour = CB_TotalTime/3600;
                    int min = (CB_TotalTime % 3600) / 60;
                    int sec = CB_TotalTime%60;
                    String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
                    StartSterInformationItemA item = new StartSterInformationItemA(startSterInformationItemA.get(0).getDrawableId(),startSterInformationItemA.get(0).getName(),timeStr);
                    startSterInformationItemA.set(0,item);
                    gridViewAdapter1.notifyDataSetChanged();
                    str1 = UnitConvert.sTempConvert(0,CB_Temp,true);
                    str2 = UnitConvert.sPresConvert(0, CB_Bar);
                    str3 = UnitConvert.sTempConvert(0,CB_ETTemp,true);
                    str4 = UnitConvert.sTempConvert(0,CB_FTTemp,true);

                    refreshInformaton();

                    str1 = UnitConvert.sTempConvert(0,CB_Temp,true);
                    str2 = UnitConvert.sPresConvert(0, CB_Bar);
                    str3 = UnitConvert.sTempConvert(0,CB_ETTemp,true);
                    str4 = UnitConvert.sTempConvert(0,CB_FTTemp,true);
                    str1 = "C.T.:"+str1;
                    str2 = "Pres.:"+str2;
                    if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                        str3 = "E.T.:"+str3;
                    }else {
                        str3 = "";
                    }
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        str4 = "F.T.:"+str4;
                    }else{
                        str4 = "";
                    }

                    lineChart.getLegend().setCurrentValue(str1,str2,str3,str4);
                    lineChart.setChartData(chartData2);

                    gridViewAdapter2.notifyDataSetChanged();
                    printData = String.format("%-11s%03d:%02d%6s  %6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,CB_Bar));
                    CSVData = String.format("%-11s,%03d:%02d,%6s,%6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,CB_Bar));
                    saveDtlToCache(CSVData.toCharArray());
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        printData = String.format("%23s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                        CSVData = String.format(",,,%23s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                        saveDtlToCache(CSVData.toCharArray());
                    }
                    int stage = (int)GlobalClass.getInstance().globalStageList[CB_NowStage];

                    programEndTime=CB_TotalTime;
                    if ((programEndTime>600)&&(programEndTime%10==0)) {
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                //HttpClient client = new DefaultHttpClient();
//                                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//
//                                try {
//                                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                                    params.add(new BasicNameValuePair("programName", Constants.instance().fetchValueString(saveSelectMenuNameKey)));
//                                    params.add(new BasicNameValuePair("programStep", getStageToString(stage)));
//                                    params.add(new BasicNameValuePair("programStarttime", String.valueOf(GlobalClass.getInstance().programStartTime)));
//                                    params.add(new BasicNameValuePair("programEndtime", String.valueOf(programEndTime)));
//                                    params.add(new BasicNameValuePair("sn", GlobalClass.getInstance().CB_SN));
//                                    params.add(new BasicNameValuePair("steriSN", GlobalClass.getInstance().steriSN));
//
//
//                                    myPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
//                                    HttpResponse response = new DefaultHttpClient().execute(myPost);
//                                    String er = response.toString();
//                                } catch (Exception e) {
//                                    String ee = e.toString();
//                                    e.printStackTrace();
//                                }
//                            }
//                        }).start();
                    }
                }else if (recv[1] == 0x32) { // 行程成功狀態通知 溫度1(Word,0.1℃); 壓力(Word,0.001bar); 溫度2(Word,0.1℃)
                    if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                        if (Constants.instance().fetchValueInt(saveSelectMenuIdKey)==10)
                            return;
                    }
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 2, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    GlobalClass.getInstance().CB_Bar = CharUtils.charArrayToInt(ch2);
                    if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                        if (recv.length > 7) {
                            System.arraycopy(recv, 6, ch2, 0, 2);
                            GlobalClass.getInstance().CB_ETTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        if (recv.length>9) {
                            System.arraycopy(recv, 8, ch2, 0, 2);
                            GlobalClass.getInstance().CB_FTTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    if (!bComplete) {
                        programEndTime=0;
//                        new Thread(new Runnable(){
//                            @Override
//                            public void run() {
//                                //HttpClient client = new DefaultHttpClient();
//                                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//
//                                try {
//                                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                                    params.add(new BasicNameValuePair("programName","Complete"));
//                                    params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
//                                    params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
//                                    params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                                    myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                                    HttpResponse response = new DefaultHttpClient().execute(myPost);
//                                    String er = response.toString();
//                                } catch (Exception e) {
//                                    String ee = e.toString();
//                                    e.printStackTrace();
//                                }
//                            }
//                        }).start();

                        bComplete = true;
                        printData =  "Program complete\r\n";
                        sendToServicePrinter(printData.toCharArray(),printData);
                        saveDtlToCache(printData.toCharArray());
                        startActivity(new Intent(StartSterilizationActivity.this, CompleteSterilizationActivity.class));
                        overridePendingTransition(0, 0);
                        finish();
                    }

                }else if (recv[1] == 0x34) { // 行程失敗狀態通知 溫度1(Word,0.1℃); 壓力(Word,0.001bar); 溫度2(Word,0.1℃)
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 2, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    GlobalClass.getInstance().CB_Bar = CharUtils.charArrayToInt(ch2);

                    if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                        if (recv.length > 7) {
                            System.arraycopy(recv, 6, ch2, 0, 2);
                            GlobalClass.getInstance().CB_ETTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    if (GlobalClass.getInstance().isFloatingTemp) {
                        if (recv.length>9) {
                            System.arraycopy(recv, 8, ch2, 0, 2);
                            GlobalClass.getInstance().CB_FTTemp = CharUtils.charArrayToInt(ch2);
                        }
                    }
                    int Error=1;
                    if (recv.length>10) {
                        System.arraycopy(recv, 10, ch2, 0, 2);
                         Error = CharUtils.charArrayToInt(ch2);
                    }

                    if (!bComplete) {
                        programEndTime=0;
//                        new Thread(new Runnable(){
//                            @Override
//                            public void run() {
//                                //HttpClient client = new DefaultHttpClient();
//                                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//
//                                try {
//                                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                                    params.add(new BasicNameValuePair("programName","Program Fail"));
//                                    params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
//                                    params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
//                                    params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                                    myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                                    HttpResponse response = new DefaultHttpClient().execute(myPost);
//                                    String er = response.toString();
//                                } catch (Exception e) {
//                                    String ee = e.toString();
//                                    e.printStackTrace();
//                                }
//                            }
//                        }).start();
                        bComplete = true;
                        printData =  "Program Fail\r\n";
                        sendToServicePrinter(printData.toCharArray(),printData);
                        printData =  "Error Code:"+Error+"\r\n";
                        sendToServicePrinter(printData.toCharArray(),printData);
                        saveDtlToCache(printData.toCharArray());
                        startActivity(new Intent(StartSterilizationActivity.this, CompleteFailedSterilizationActivity.class));
                        overridePendingTransition(0, 0);
                        finish();
                    }

                }else if (recv[1] == 0x37) {    // Leakage test Complete
                    char[] ch4 = new char[4];
                    System.arraycopy(recv, 2, ch4, 0, 4);
                    GlobalClass.getInstance().CB_LeakageRate = CharUtils.charArrayToInt(ch4);
                    GlobalClass.getInstance().CB_SteriSuccess = true;
                    System.arraycopy(recv, 6, ch4, 0, 4);
                    int TotalTime = CharUtils.charArrayToInt(ch4);
                    String totalTimeStr = String.format("%dh %02dm %02ds",TotalTime/3600,(TotalTime%3600)/60,TotalTime%60);

                    printData =  "--------------------------------\r\n"+
                            "Leakage Rate : "+UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageRate)+"\r\n"+
                            "Leakage Test : Pass\r\n"+
                            "Total time : "+totalTimeStr+"\r\n"+//+GlobalClass.getInstance().CB_LeakageTime[3]/60+"m "+GlobalClass.getInstance().CB_LeakageTime[3]%60+"s\r\n"+
                            "--------------------------------\r\n"+
                            "Program complete\r\n"+
                            "--------------------------------\r\n";

                    CSVData =  "--------------------------------\r\n"+
                            "Leakage Rate : "+UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageRate)+"\r\n"+
                            "Leakage Test : Pass\r\n"+
                            "Total time : "+totalTimeStr+"\r\n"+//+GlobalClass.getInstance().CB_LeakageTime[3]/60+"m "+GlobalClass.getInstance().CB_LeakageTime[3]%60+"s\r\n"+
                            "--------------------------------\r\n"+
                            "Program complete\r\n"+
                            "--------------------------------\r\n";
                    sendToServicePrinter(printData.toCharArray(),CSVData);
                    startActivity(new Intent(StartSterilizationActivity.this, LeakageTestCompleteSterilizationActivity.class));
                    overridePendingTransition(0, 0);
                    finish();
                }else if (recv[1] == 0x38) {    // Leakage test Fail
                    char[] ch4 = new char[4];
                    System.arraycopy(recv, 2, ch4, 0, 4);
                    GlobalClass.getInstance().CB_LeakageRate = CharUtils.charArrayToInt(ch4);
                    GlobalClass.getInstance().CB_SteriSuccess = false;
                    printData =  "--------------------------------\r\n"+
                            "Leakage Rate :"+UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageRate)+"\r\n"+
                            "Leakage Test : Fail\r\n"+
                            "Total time : "+GlobalClass.getInstance().CB_LeakageTime[3]/60+"m "+GlobalClass.getInstance().CB_LeakageTime[3]%60+"s\r\n"+
                            "--------------------------------\r\n"+
                            "Program error\r\n"+
                            "--------------------------------\r\n";

                    CSVData =  "--------------------------------\r\n"+
                            "Leakage Rate :"+UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageRate)+"\r\n"+
                            "Leakage Test : Fail\r\n"+
                            "Total time : "+GlobalClass.getInstance().CB_LeakageTime[3]/60+"m "+GlobalClass.getInstance().CB_LeakageTime[3]%60+"s\r\n"+
                            "--------------------------------\r\n"+
                            "Program error\r\n"+
                            "--------------------------------\r\n";
                    sendToServicePrinter(printData.toCharArray(),CSVData);
                    startActivity(new Intent(StartSterilizationActivity.this, LeakageTestCompleteFailActivity.class));
                    overridePendingTransition(0, 0);
                    finish();
                }else if (recv[1] == 0x35) { // Print Summary 最低溫度(Word,0.1℃); 最高溫度(Word,0.1℃); 最低壓力(Word,0.001bar); 最高壓力(Word,0.001bar)
                    int minTemp,maxTemp,minPres,maxPres,SterTime,TotalTime;
                    if (printSummaryCount<2) {
                        char[] ch2 = new char[2];
                        System.arraycopy(recv, 2, ch2, 0, 2);
                        minTemp = CharUtils.charArrayToInt(ch2);
                        System.arraycopy(recv, 4, ch2, 0, 2);
                        maxTemp = CharUtils.charArrayToInt(ch2);
                        System.arraycopy(recv, 6, ch2, 0, 2);
                        minPres = CharUtils.charArrayToInt(ch2);
                        System.arraycopy(recv, 8, ch2, 0, 2);
                        maxPres = CharUtils.charArrayToInt(ch2);
                        char[] ch4 = new char[4];
                        System.arraycopy(recv, 10, ch4, 0, 4);
                        SterTime = CharUtils.charArrayToInt(ch4);
                        String sterTimeStr;
                        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                            sterTimeStr = String.format("%dh %02dm",SterTime/3600,(SterTime%3600)/60);
                        }else{
                            sterTimeStr = String.format("%02dm %02ds", SterTime / 60, SterTime % 60);
                        }
                        System.arraycopy(recv, 14, ch4, 0, 4);
                        TotalTime = CharUtils.charArrayToInt(ch4);
                        String totalTimeStr = String.format("%dh %02dm %02ds",TotalTime/3600,(TotalTime%3600)/60,TotalTime%60);
                        String F0Str = String.format("%.2f (Min)" , F0/(double) 60);
                        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                            int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
                            switch (menuSelectId) {
                                case 0:
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 1:
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 2: // Solid 1
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 3:
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 4: // Agar
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 5: // Dissolution
                                    printData =     "--------------------------------\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 6: // Dry Only
                                    printData =     "--------------------------------\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 7: // Waste
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 8: // User 1
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";

                                    break;
                                case 9:
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 10: // Leakage test"
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
                                            "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 11: // Flash
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
//                                    "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                                case 12: // B&D
                                    printData =     "--------------------------------\r\n" +
                                            "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                            "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                            "Ster. Time:  "+sterTimeStr+"\r\n" +
                                            "Total Time:  "+totalTimeStr+"\r\n" +
//                                    "F0        :  "+F0Str+"\r\n" +
                                            "--------------------------------\r\n";
                                    break;
                             }
                        }else{
                            printData =     "--------------------------------\r\n" +
                                    "Ster. Temp: "+ UnitConvert.sTempConvert(0,minTemp,true)+" - "+ UnitConvert.sTempConvert(2,maxTemp,true)+" \r\n" +
                                    "Ster. Pres: "+String.format("%.3f",(float)minPres/(float)1000)+" - "+String.format("%.3f",(float)maxPres/(float)1000)+presStr+"\r\n" +
                                    "Ster. Time:  "+sterTimeStr+"\r\n" +
                                    "Total Time:  "+totalTimeStr+"\r\n" +
//                                    "F0        :  "+F0Str+"\r\n" +
                                    "--------------------------------\r\n";
                        }
                        sendToServicePrinter(printData.toCharArray(),printData);
                        saveDtlToCache(printData.toCharArray());
                    }
                    printSummaryCount++;
                }else if (recv[1] == 0x36) {  // Print 目前階段(Byte); 階段名稱(11 Bytes,ASCII code); 時間(Double Word,秒); 溫度1(Word,0.1℃); 壓力(Word,0.001bar); E.T.溫度(Word,0.1℃); 溫度2(Word,0.1℃)
                    //溫度1(Word,0.1℃); 壓力(Word,0.001bar); 溫度2(Word,0.1℃)"
                    if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                        int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
                        if (menuSelectId==10) { // Leakage Test
                            char[] ch11 = new char[11];
                            CB_NowStage = recv[2];
                            System.arraycopy(recv, 3, ch11, 0, 11);
                            ch11[10] = ' ';
                            CB_StageName= CharUtils.charToString(ch11);
                            char[] ch4 = new char[4];
                            System.arraycopy(recv, 14, ch4, 0, 4);
                            CB_Time = CharUtils.charArrayToInt(ch4);
                            char[] ch2 = new char[2];
                            System.arraycopy(recv, 18, ch2, 0, 2);
                            CB_Temp = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 20, ch2, 0, 2);
                            CB_Bar = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 22, ch2, 0, 2);
                            CB_ETTemp = CharUtils.charArrayToInt(ch2);
                            if (ch11[0]=='0') {
                                GlobalClass.getInstance().CB_LeakageBar[0] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[0] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P0:",UnitConvert.sPresConvert(1,CB_Bar),"t0:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P0:",UnitConvert.sPresConvert(1,CB_Bar),"t0:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='1') {
                                GlobalClass.getInstance().CB_LeakageBar[1] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[1] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P1:",UnitConvert.sPresConvert(1,CB_Bar),"t1:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P1:",UnitConvert.sPresConvert(1,CB_Bar),"t1:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='2') {
                                GlobalClass.getInstance().CB_LeakageBar[2] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[2] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P2:",UnitConvert.sPresConvert(1,CB_Bar),"t2:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P2:",UnitConvert.sPresConvert(1,CB_Bar),"t2:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='3') {
                                GlobalClass.getInstance().CB_LeakageBar[3] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[3] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P3:",UnitConvert.sPresConvert(1,CB_Bar),"t3:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P3:",UnitConvert.sPresConvert(1,CB_Bar),"t3:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }

                            int hour = CB_TotalTime/3600;
                            int min = (CB_TotalTime % 3600) / 60;
                            int sec = CB_TotalTime%60;
                            String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
                            float bar = (float)CB_Bar;
                            RecordStepClass step = new RecordStepClass();
                            step.step = CB_StageName;
                            step.time = CB_Time;
                            step.temp = CB_Temp;
                            step.pres = CB_Bar;
                            GlobalClass.getInstance().globalRecordStepList.add(step);
                        }else{
                            char[] ch11 = new char[11];
                            CB_NowStage = recv[2];
                            System.arraycopy(recv, 3, ch11, 0, 11);
                            ch11[10] = ' ';
                            CB_StageName= CharUtils.charToString(ch11);
                            char[] ch4 = new char[4];
                            System.arraycopy(recv, 14, ch4, 0, 4);
                            CB_Time = CharUtils.charArrayToInt(ch4);
                            char[] ch2 = new char[2];
                            System.arraycopy(recv, 18, ch2, 0, 2);
                            CB_Temp = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 20, ch2, 0, 2);
                            CB_Bar = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 22, ch2, 0, 2);
                            CB_ETTemp = CharUtils.charArrayToInt(ch2);

                            int hour = CB_TotalTime/3600;
                            int min = (CB_TotalTime % 3600) / 60;
                            int sec = CB_TotalTime%60;
                            String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
                            float bar = (float)CB_Bar;
                            String printData = String.format("%-11s%03d:%02d%6s  %6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,bar));
                            CSVData = String.format("%-11s,%03d:%02d,%6s,%6s\r\n",CB_StageName,CB_Time/60,CB_Time%60,UnitConvert.sTempConvertP(false,CB_Temp),UnitConvert.sPresConvert(0,bar));
                            sendToServicePrinter(printData.toCharArray(),CSVData);
                            RecordStepClass step = new RecordStepClass();
                            step.step = CB_StageName;
                            step.time = CB_Time;
                            step.temp = CB_Temp;
                            step.pres = CB_Bar;
                            GlobalClass.getInstance().globalRecordStepList.add(step);
                            if (GlobalClass.getInstance().isFloatingTemp) {
                                if (recv.length>=25) {
                                    System.arraycopy(recv, 24, ch2, 0, 2);
                                    CB_FTTemp = CharUtils.charArrayToInt(ch2);
                                }
                                printData = String.format("%23s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                                CSVData = String.format(",,%s\r\n",UnitConvert.sTempConvertP(false,CB_FTTemp));
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                                RecordStepClass FT_step = new RecordStepClass();
                                FT_step.step = "";
                                FT_step.time = 0;
                                FT_step.temp = CB_FTTemp;
                                FT_step.pres = 0;
                                GlobalClass.getInstance().globalRecordStepList.add(FT_step);
                            }
                        }
                    }else{
                        int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
                        if (menuSelectId==10) { // Leakage Test
                            char[] ch11 = new char[11];
                            CB_NowStage = recv[2];
                            System.arraycopy(recv, 3, ch11, 0, 11);
                            ch11[10] = ' ';
                            CB_StageName= CharUtils.charToString(ch11);
                            char[] ch4 = new char[4];
                            System.arraycopy(recv, 14, ch4, 0, 4);
                            CB_Time = CharUtils.charArrayToInt(ch4);
                            char[] ch2 = new char[2];
                            System.arraycopy(recv, 18, ch2, 0, 2);
                            CB_Temp = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 20, ch2, 0, 2);
                            CB_Bar = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 22, ch2, 0, 2);
                            CB_ETTemp = CharUtils.charArrayToInt(ch2);
                            if (ch11[0]=='0') {
                                GlobalClass.getInstance().CB_LeakageBar[0] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[0] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P0:",UnitConvert.sPresConvert(1,CB_Bar),"t0:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P0:",UnitConvert.sPresConvert(1,CB_Bar),"t0:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='1') {
                                GlobalClass.getInstance().CB_LeakageBar[1] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[1] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P1:",UnitConvert.sPresConvert(1,CB_Bar),"t1:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P1:",UnitConvert.sPresConvert(1,CB_Bar),"t1:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='2') {
                                GlobalClass.getInstance().CB_LeakageBar[2] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[2] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P2:",UnitConvert.sPresConvert(1,CB_Bar),"t2:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P2:",UnitConvert.sPresConvert(1,CB_Bar),"t2:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }else if (ch11[0]=='3') {
                                GlobalClass.getInstance().CB_LeakageBar[3] = CB_Bar;
                                GlobalClass.getInstance().CB_LeakageTime[3] = CB_Time;
                                String printData = String.format("%-6s%10s   %-6s%5dS\r\n","P3:",UnitConvert.sPresConvert(1,CB_Bar),"t3:",CB_Time);
                                CSVData = String.format("%-6s,%10s,%-6s,%5dS\r\n","P3:",UnitConvert.sPresConvert(1,CB_Bar),"t3:",CB_Time);
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                            }

                            int hour = CB_TotalTime/3600;
                            int min = (CB_TotalTime % 3600) / 60;
                            int sec = CB_TotalTime%60;
                            String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
                            float bar = (float)CB_Bar;
                            RecordStepClass step = new RecordStepClass();
                            step.step = CB_StageName;
                            step.time = CB_Time;
                            step.temp = CB_Temp;
                            step.pres = CB_Bar;
                            GlobalClass.getInstance().globalRecordStepList.add(step);
                        }else {
                            char[] ch11 = new char[11];
                            CB_NowStage = recv[2];
                            System.arraycopy(recv, 3, ch11, 0, 11);
                            ch11[10] = ' ';
                            CB_StageName = CharUtils.charToString(ch11);
                            char[] ch4 = new char[4];
                            System.arraycopy(recv, 14, ch4, 0, 4);
                            CB_Time = CharUtils.charArrayToInt(ch4);
                            char[] ch2 = new char[2];
                            System.arraycopy(recv, 18, ch2, 0, 2);
                            CB_Temp = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 20, ch2, 0, 2);
                            CB_Bar = CharUtils.charArrayToInt(ch2);
                            System.arraycopy(recv, 22, ch2, 0, 2);
                            CB_ETTemp = CharUtils.charArrayToInt(ch2);

                            int hour = CB_TotalTime / 3600;
                            int min = (CB_TotalTime % 3600) / 60;
                            int sec = CB_TotalTime % 60;
                            String timeStr = String.format("%02d:%02d:%02d", hour, min, sec);
                            float bar = (float) CB_Bar;
                            String printData = String.format("%-11s%03d:%02d%6s  %6s\r\n", CB_StageName, CB_Time / 60, CB_Time % 60, UnitConvert.sTempConvertP(false, CB_Temp), UnitConvert.sPresConvert(0, bar));
                            CSVData = String.format("%-11s,%03d:%02d,%6s,%6s\r\n", CB_StageName, CB_Time / 60, CB_Time % 60, UnitConvert.sTempConvertP(false, CB_Temp), UnitConvert.sPresConvert(0, bar));
                            sendToServicePrinter(printData.toCharArray(),CSVData);
                            RecordStepClass step = new RecordStepClass();
                            step.step = CB_StageName;
                            step.time = CB_Time;
                            step.temp = CB_Temp;
                            step.pres = CB_Bar;
                            GlobalClass.getInstance().globalRecordStepList.add(step);
                            if (GlobalClass.getInstance().isFloatingTemp) {
                                if (recv.length>=25) {
                                    System.arraycopy(recv, 24, ch2, 0, 2);
                                    CB_FTTemp = CharUtils.charArrayToInt(ch2);
                                }
                                printData = String.format("%23s\r\n", UnitConvert.sTempConvertP(false, CB_FTTemp));
                                CSVData = String.format("%23s\r\n", UnitConvert.sTempConvertP(false, CB_FTTemp));
                                sendToServicePrinter(printData.toCharArray(),CSVData);
                                RecordStepClass FT_step = new RecordStepClass();
                                FT_step.step = "";
                                FT_step.time = 0;
                                FT_step.temp = CB_FTTemp;
                                FT_step.pres = 0;
                                GlobalClass.getInstance().globalRecordStepList.add(FT_step);
                            }
                        }
                    }
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            if (CB_TotalTime>0)
                CB_TotalTime--;
//            iDelay--;
//            totaSterilTime--;
//            if (iDelay==0){
//                iDelay = 5;
//                progressBarIndex = progressBarIndex + 1;
//                if (progressBarIndex>4){
//                    if (Constants.instance().fetchValueInt(saveSelectMenuIdKey)==10) {
//                        startActivity(new Intent(StartSterilizationActivity.this, LeakageTestCompleteSterilizationActivity.class));
//                    }
//                    else {
//                        startActivity(new Intent(StartSterilizationActivity.this, CompleteSterilizationActivity.class));
//                    }
//
//                    overridePendingTransition(0, 0);
//                    finish();
//                    return;
//                }
////                progressBar.setActiveDotIndex(progressBarIndex);
////                progressBar.invalidate();
//                viewPager.setCurrentItem(progressBarIndex);
//                progressBarTitleView.setText(getTitles().get(progressBarIndex));
//            }
//
//
//            String timeStr = getSterilizationTime();
//            StartSterInformationItemA item = new StartSterInformationItemA(startSterInformationItemA.get(0).getDrawableId(),startSterInformationItemA.get(0).getName(),timeStr);
//            startSterInformationItemA.set(0,item);
//            gridViewAdapter1.notifyDataSetChanged();
            handler.postDelayed(this, 1000);
        }
    };

    private void refreshInformaton() {
        if (GlobalClass.getInstance().isFloatingTemp || (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
            if (GlobalClass.getInstance().isFloatingTemp && (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°C";
                } else {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°F";
                }
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "bar";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kPa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "Mpa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "psi";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kgf/cm";
                }
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str3, R.drawable.parameter_icon_temp, true));
                    str3 += "°C";
                } else {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str3, R.drawable.parameter_icon_temp, true));
                    str3 += "°F";
                }
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(3, new StartSterGridRecycler("Floating Temp.", "°C", str4, R.drawable.parameter_icon_temp, true));
                    str3 += "°C";
                } else {
                    startSterInformationItemB.set(3, new StartSterGridRecycler("Floating Temp.", "°F", str4, R.drawable.parameter_icon_temp, true));
                    str3 += "°F";
                }

            }else if (GlobalClass.getInstance().isFloatingTemp) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°C";
                } else {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°F";
                }
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "bar";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kPa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "Mpa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "psi";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kgf/cm";
                }
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Floating Temp.", "°C", str4, R.drawable.parameter_icon_temp, true));
                    str4 += "°C";
                } else {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Floating Temp.", "°F", str4, R.drawable.parameter_icon_temp, true));
                    str4 += "°F";
                }
            }else if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°C";
                } else {
                    startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str1, R.drawable.parameter_icon_temp, true));
                    str1 += "°F";
                }
                if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "bar";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kPa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "Mpa";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "psi";
                } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                    startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str2, R.drawable.parameter_icon_pressure, false));
                    str2 += "kgf/cm";
                }
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Exhaust Temp.", "°C", str3, R.drawable.parameter_icon_temp, true));
                    str3 += "°C";
                } else {
                    startSterInformationItemB.set(2, new StartSterGridRecycler("Exhaust Temp.", "°F", str3, R.drawable.parameter_icon_temp, true));
                    str3 += "°F";
                }
            }

        }else{
            if (Constants.instance().fetchValueInt(saveTempFormat) == 0) {
                startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°C", str1, R.drawable.parameter_icon_temp, true));
                str1 += "°C";
                str3 += "°C";
            } else {
                startSterInformationItemB.set(0, new StartSterGridRecycler("Chamber Temp.", "°F", str1, R.drawable.parameter_icon_temp, true));
                str1 += "°F";
                str3 += "°F";
            }
            if (Constants.instance().fetchValueInt(savePresFormat) == 0) {
                startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "bar", str2, R.drawable.parameter_icon_pressure, false));
                str2 += "bar";
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 1) {
                startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kPa", str2, R.drawable.parameter_icon_pressure, false));
                str2 += "kPa";
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 2) {
                startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "Mpa", str2, R.drawable.parameter_icon_pressure, false));
                str2 += "Mpa";
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 3) {
                startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "psi", str2, R.drawable.parameter_icon_pressure, false));
                str2 += "psi";
            } else if (Constants.instance().fetchValueInt(savePresFormat) == 4) {
                startSterInformationItemB.set(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", str2, R.drawable.parameter_icon_pressure, false));
                str2 += "kgf/cm";
            }
        }
    }

    private Runnable mTestGraphChartRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            double i1,i2,i3,i4;
            addCount++;
            chartYDataList.add(Integer.toString(addCount));
            i1 =(Math.random()*100);
            str1 = String.format("%.1f",i1);
            tempList1.add(i1);
            i2 =(Math.random()*100);
            str2 = String.format("%.1f",i2);
            tempFTList.add(i2);
            i3 =(Math.random()*1000)/1000;
            str3 = String.format("%.1f",i3);;
            presList.add(i3);
            i4 =(Math.random()*100);
            str4 = String.format("%.1f",i4);
            tempETList.add(i4);
//            chartData2 = new ChartData<>("Line chart", chartYDataList, ColumnDatas);
            lineChart.setChartData(chartData2);
            str1 = "C.T.:"+str1;
            str2 = "Pres.:"+str2;
            str3 = "E.T.:"+str3;
            str4 = "F.T.:"+str4;

            lineChart.getLegend().setCurrentValue(str1,str2,str3,str4);
            if (addCount>13)
                lineChart.setScrollNext(30,0);
//            lineChart.notifyAll();
            handler.postDelayed(mTestGraphChartRunnable, 60000);
        }
    };


    private Runnable mGraphChartRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            addCount++;
            chartYDataList.add(Integer.toString(addCount));
            if (total_TempValue>0) {
                temp_average = Double.valueOf((float)UnitConvert.fTempConvert(total_TempValue/total_TempCount));
            }
            total_TempValue = total_TempCount = 0;
            tempList1.add(temp_average);
            if (GlobalClass.getInstance().isFloatingTemp) {
                if (total_TempFTValue>0) {
                    temp2_average = Double.valueOf((float)UnitConvert.fTempConvert(total_TempFTValue/total_TempFTCount));
                }
                total_TempFTValue = total_TempFTCount = 0;
                tempFTList.add(temp2_average);
            }else{
                tempFTList.add(new Double(0));
            }
            if (Constants.instance().fetchValueInt(ExhaustTempKey) > 0) {
                if (total_TempETValue>0) {
                    tempET_average = Double.valueOf((float)UnitConvert.fTempConvert(total_TempETValue/total_TempETCount));
                }
                total_TempETValue = total_TempETCount = 0;
                tempETList.add(tempET_average);
            }else{
                tempETList.add(new Double(0));
            }
            if (total_PresValue>0) {
                pres_average = Double.valueOf((float)UnitConvert.fPresConvert(total_PresValue/total_PresCount));
            }
            total_PresValue = total_PresCount = 0;
            presList.add(pres_average);
//            chartData2 = new ChartData<>("Line chart", chartYDataList, ColumnDatas);
            lineChart.setChartData(chartData2);
            if (addCount>13)
                lineChart.setScrollNext(30,0);
//            lineChart.notifyAll();
            handler.postDelayed(mGraphChartRunnable, 100000);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_startsterilization);
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        registerBaseActivityReceiver();
        GlobalClass.getInstance().MachineState = STERI_RUN;
//        Date date = new Date();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss") ;
//        GlobalClass.getInstance().steriSN = dateFormat.format(date);
        GlobalClass.getInstance().saveStdRecordFileName = ("S"+ GlobalClass.getInstance().steriSN + ".std");
        GlobalClass.getInstance().saveDtlRecordFileName = (GlobalClass.getInstance().steriSN + ".dtl");
        GlobalClass.getInstance().saveStdCSVRecordFileName = ("S"+ GlobalClass.getInstance().steriSN + ".csv");

        if (Constants.instance().fetchValueInt(savePresFormat)==0) {
            presStr = " bar";
        }else if (Constants.instance().fetchValueInt(savePresFormat)==1) {
            presStr = " kPa";
        }else if (Constants.instance().fetchValueInt(savePresFormat)==2) {
            presStr = " Mpa";
        }else if (Constants.instance().fetchValueInt(savePresFormat)==3) {
            presStr = " psi";
        }else if (Constants.instance().fetchValueInt(savePresFormat)==4) {
            presStr = " kgf/cm2";
        }

        String str = String.valueOf(GlobalClass.getInstance().programStartTime);

        GlobalClass.getInstance().isPrintSignature = true;
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        GlobalClass.getInstance().globalRecordStepList.clear();
        GlobalClass.getInstance().globalCloudRecordSaveString = "";
        GlobalClass.getInstance().globalStdRecordSaveString = "";
        GlobalClass.getInstance().globalStdCSVRecordSaveString = "";
        GlobalClass.getInstance().globalDtlRecordSaveString = "";

        programBarTitleView = (TextView) findViewById(R.id.program_title_view);
        programBarTitleView.setText(Constants.instance().fetchValueString(saveSelectMenuNameKey));
        progressBarTitleView = (TextView) findViewById(R.id.progress_title_view);
        progressBarTitleView.setText("Check");
        setDummyData();
        CB_TotalTime = 3600;
        gridView_1 = (RecyclerView) findViewById(R.id.information_grid_1);
        gridView_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        gridView_1.setHasFixedSize(true);
        GridLayoutManager layoutManager1 = new GridLayoutManager(this, 1);
        gridView_1.setLayoutManager(layoutManager1);
        gridViewAdapter1 = new StartSterGridViewAdapter1(this, startSterInformationItemA);
        gridViewAdapter1.setOnItemClickListener(this);
        gridView_1.setAdapter(gridViewAdapter1);


        gridView_2 = (RecyclerView)findViewById(R.id.information_grid_2);
        gridView_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        gridView_2.setLayoutManager(mLayoutManager);
        gridView_2.setItemAnimator(new DefaultItemAnimator());
        gridView_2.addItemDecoration(new RecyclerViewDecorator(this));
        startSterInformationItemB = new ArrayList<>();
        if (GlobalClass.getInstance().isFloatingTemp || (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
            if (GlobalClass.getInstance().isFloatingTemp && (Constants.instance().fetchValueInt(ExhaustTempKey)>0)) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));

                startSterInformationItemB.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", "-0.000", R.drawable.parameter_icon_pressure, false));

                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Exhaust Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Exhaust Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));

                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(3, new StartSterGridRecycler("Floating Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(3, new StartSterGridRecycler("Floating Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));
            }else if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));

                startSterInformationItemB.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", "-0.000", R.drawable.parameter_icon_pressure, false));

                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Exhaust Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Exhaust Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));
            }else if (GlobalClass.getInstance().isFloatingTemp) {
                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));

                startSterInformationItemB.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", "-0.000", R.drawable.parameter_icon_pressure, false));

                if (Constants.instance().fetchValueInt(saveTempFormat) == 0)
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Floating Temp.", "°C", "000.0", R.drawable.parameter_icon_temp, true));
                else
                    startSterInformationItemB.add(2, new StartSterGridRecycler("Floating Temp.", "°F", "000.0", R.drawable.parameter_icon_temp, true));

            }
        }else{
            if (Constants.instance().fetchValueInt(saveTempFormat)==0)
                startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°C", "000.0", R.drawable.parameter_icon_temp,true));
            else
                startSterInformationItemB.add(0, new StartSterGridRecycler("Chamber Temp.", "°F", "000.0", R.drawable.parameter_icon_temp,true));
            startSterInformationItemB.add(1, new StartSterGridRecycler("Chamber Pres.", "kgf/cm", "-0.000", R.drawable.parameter_icon_pressure,false));
        }

        gridViewAdapter2 = new StartSterGrid2Adapter(this, startSterInformationItemB);
        gridView_2.setAdapter(gridViewAdapter2);

        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
//        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
        progressBarNumberOfDots = getBgRes().size()-1;
        PagerModelManager manager = new PagerModelManager();
        manager.addCommonFragment(GuideFragment.class, getBgRes(), getTitles());
        ModelPagerAdapter adapter = new ModelPagerAdapter(getSupportFragmentManager(), manager);
        viewPager.setAdapter(adapter);
        viewPager.fixScrollSpeed();
//        springIndicator.setViewPager(viewPager);

//        progressBar = (DottedProgressBar) findViewById(R.id.progress);
//        progressBar.setActiveDotIndex(0);
//        progressBar.invalidate();
//        progressBarTitleView.setText(getTitles().get(0));

        DrawMeButton mCancelButton = (DrawMeButton)findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NotificationDialog(mActivity)
                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                        .setAnimationEnable(true)
                        .setTitleText("Notification")
                        .setContentText("Confirm to stop program?")
                        .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                dialog.dismiss();
                                char[] data = new char[]{0x01, 0x50};
                                sendToService(data);
                                closeAllActivities();
                                Intent intent = new Intent(StartSterilizationActivity.this, CompleteFailedSterilizationActivity.class);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        }).show();



            }
        });
        totaSterilTime = 25;
        handler = new Handler();
        handler.postAtTime(runnable, 1000);
//        if (GlobalClass.getInstance().isETemp) {
//            handler.postDelayed(mTestGraphChartRunnable, 1000);
//        }else{
//            handler.postDelayed(mGraphChartRunnable, 10000);
//        }
        handler.postDelayed(mGraphChartRunnable, 10000);

        int CycleCounter = Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0);
        CycleCounter++;
        Settings.System.putInt(this.getContentResolver(),CycleCounterKey, CycleCounter);
        printData = String.format("--------------------------------\r\nModel:%s\r\nVer.\r\n%s_%s\r\nSN:%s\r\n--------------------------------\r\n"
                ,GlobalClass.getInstance().CB_Model
                ,GlobalClass.getInstance().CB_Model
                ,GlobalClass.getInstance().CB_Firmware
                ,GlobalClass.getInstance().CB_SN
        );
        CSVData = String.format("--------------------------------,,,\r\nModel:%s,,,\r\nVer.,,,\r\n%s_%s,,,\r\nSN:%s,,,\r\n--------------------------------\r\n"
                ,GlobalClass.getInstance().CB_Model
                ,GlobalClass.getInstance().CB_Model
                ,GlobalClass.getInstance().CB_Firmware
                ,GlobalClass.getInstance().CB_SN
        );
        sendToServicePrinter(printData.toCharArray(),CSVData);
        saveDtlToCache(CSVData.toCharArray());
        if ((Constants.instance().fetchValueInt(preIDControl))==1) {
            printData = String.format("Pre ID:\r\n%s\r\n--------------------------------\r\n"
                    , Constants.instance().fetchValueString(userLoginAccount));
            CSVData = String.format("Pre ID:,,,\r\n%s,,,\r\n--------------------------------,,,\r\n"
                    , Constants.instance().fetchValueString(userLoginAccount));
            sendToServicePrinter(printData.toCharArray(),CSVData);
            saveDtlToCache(CSVData.toCharArray());
        }
        DatabaseHelper helper = new DatabaseHelper(this);
        List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
        if (allBarCodeList.size()>0){
            printData = String.format("%-17shh:mm\r\n","Item Number");
            CSVData = String.format("%-17shh:mm,,,\r\n","Item Number");
            sendToServicePrinter(printData.toCharArray(),CSVData);
            saveDtlToCache(CSVData.toCharArray());

            for (int i=0;i<allBarCodeList.size();i++) {
                String bardCode = allBarCodeList.get(i).getSerialNumber();
                String time = allBarCodeList.get(i).getLastName();
                printData = String.format("%-17s%s\r\n",bardCode,time);
                CSVData = String.format("%-17s%s,,,\r\n",bardCode,time);
                sendToServicePrinter(printData.toCharArray(),CSVData);
                saveDtlToCache(CSVData.toCharArray());
            }
            helper.getCustomerDao().deleteAllBarCode();
            printData = String.format("--------------------------------\r\n");
            sendToServicePrinter(printData.toCharArray(),printData);
            saveDtlToCache(printData.toCharArray());
        }
        String unitTemp;
        if (Constants.instance().fetchValueInt(saveTempFormat)==0)
            unitTemp = "C";
        else
            unitTemp = "F";
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
            switch (menuSelectId) {
                case 0:
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nExhaust Level: %d\r\nPressurized Cooling:%s\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_ExhaustLevel
                            ,GlobalClass.getInstance().CB_PressurizedCooling?"ON":"OFF"
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));
                    break;
                case 1:
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nExhaust Level: %d\r\nPressurized Cooling:%s\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_ExhaustLevel
                            ,GlobalClass.getInstance().CB_PressurizedCooling?"ON":"OFF"
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));
                    break;
                case 2: // Solid 1
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nDry   Time:  %2d m\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_DryTime/60);
                    break;
                case 3:
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nDry   Time:  %2d m\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_DryTime/60);
                    break;
                case 4: // Agar
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nExhaust Level: %d\r\nInsulation Temp.: %s\r\nInsulation Time: %2d h\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_ExhaustLevel
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_InsulationTemp)
                            ,GlobalClass.getInstance().CB_InsulationTime/3600
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));
                    break;
                case 5: // Dissolution
                    printData = String.format("Program:\r\n%s\r\nDissol. Temp: %s\r\nDissol. Time: %2d m\r\nInsulation Temp.: %s\r\nInsulation Time: %2d h\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_DissolTemp)
                            ,GlobalClass.getInstance().CB_DissolTime/60
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_InsulationTemp)
                            ,GlobalClass.getInstance().CB_InsulationTime/3600
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));
                    break;
                case 6: // Dry Only
                    printData = String.format("Program:\r\n%s\r\nDry Time:%2d m\r\nVacuum Pump: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,GlobalClass.getInstance().CB_DryTime/60
                            ,(Constants.instance().fetchValueInt(VacuumpumpKey) > 0)?"ON":"OFF");
                    break;
                case 7: // Waste
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nPressurized Cooling:%s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_PressurizedCooling?"ON":"OFF");

                    break;
                case 8: // User 1
                    printData = String.format("Program:\r\n%s\r\nReservation: %d h\r\nPre-Vacuum: %s\r\nDissol. Temp: %s\r\nDissol. Time: %2d h %2d m\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nFloating Sensor:%s\r\nFan Cooling:%s\r\nPressurized Cooling:%s\r\nExhaust Level: %d\r\nDry Time:  %2d m %2d s\r\nInsulation Temp.: %s\r\nInsulation Time: %2d h\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,GlobalClass.getInstance().CB_Reservation
                            ,GlobalClass.getInstance().CB_PreVacuum?"ON":"OFF"
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_DissolTemp)
                            ,GlobalClass.getInstance().CB_DissolTime/3600,(GlobalClass.getInstance().CB_DissolTime%3600)/60
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().isFloatingTemp?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_FanCooling?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_PressurizedCooling?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_ExhaustLevel
                            ,GlobalClass.getInstance().CB_DryTime/60,GlobalClass.getInstance().CB_DryTime%60
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_InsulationTemp)
                            ,GlobalClass.getInstance().CB_InsulationTime/3600
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));

                    break;
                case 9:
                    printData = String.format("Program:\r\n%s\r\nReservation: %d h\r\nPre-Vacuum: %s\r\nDissol. Temp: %s\r\nDissol. Time: %2d h %2d m\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nFloating Sensor:%s\r\nFan Cooling:%s\r\nPressurized Cooling:%s\r\nExhaust Level: %d\r\nDry Time:  %2d m %2d s\r\nInsulation Temp.: %s\r\nInsulation Time: %2d h\r\nOpen Door Temp.:\r\nC.T.: %s\r\nF.T.: %s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,GlobalClass.getInstance().CB_Reservation
                            ,GlobalClass.getInstance().CB_PreVacuum?"ON":"OFF"
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_DissolTemp)
                            ,GlobalClass.getInstance().CB_DissolTime/3600,(GlobalClass.getInstance().CB_DissolTime%3600)/60
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().isFloatingTemp?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_FanCooling?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_PressurizedCooling?"ON":"OFF"
                            ,GlobalClass.getInstance().CB_ExhaustLevel
                            ,GlobalClass.getInstance().CB_DryTime/60,GlobalClass.getInstance().CB_DryTime%60
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_InsulationTemp)
                            ,GlobalClass.getInstance().CB_InsulationTime/3600
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_CTTemp)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_FTTemp));
                    break;
                case 10: // Leakage test"
                    printData = String.format("Program:\r\n%s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey));

                    break;
                case 11: // Flash
                    printData = String.format("Program:\r\n%s\r\nPre-Vacuum: %d\r\nSter. Temp: %d '%s\r\nSter. Time:  %2d m %2d s\r\nDry   Time:  %2d m %2d s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,GlobalClass.getInstance().CB_PreVacuumCount
                            ,GlobalClass.getInstance().Steri_Temp
                            ,unitTemp
                            ,GlobalClass.getInstance().CB_SteriTime/60,GlobalClass.getInstance().CB_SteriTime%60
                            ,GlobalClass.getInstance().CB_DryTime/60,GlobalClass.getInstance().CB_DryTime%60);
                    break;
                case 12:
                    printData = String.format("Program:\r\n%s\r\nPre-Vacuum: %d\r\nSter. Temp: %d '%s\r\nSter. Time:  %2d m %2d s\r\nDry   Time:  %2d m %2d s\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,GlobalClass.getInstance().CB_PreVacuumCount
                            ,GlobalClass.getInstance().Steri_Temp
                            ,unitTemp
                            ,GlobalClass.getInstance().CB_SteriTime/60,GlobalClass.getInstance().CB_SteriTime%60
                            ,GlobalClass.getInstance().CB_DryTime/60,GlobalClass.getInstance().CB_DryTime%60);
                    break;
                case 13: // Latex
                    printData = String.format("Program:\r\n%s\r\nSter. Temp: %s\r\nSter. Time:  %2d h %2d m\r\nDry   Time:  %2d m\r\n--------------------------------\r\n"
                            ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                            ,UnitConvert.sTempConvert(2,GlobalClass.getInstance().CB_SteriTemp)
                            ,GlobalClass.getInstance().CB_SteriTime/3600,(GlobalClass.getInstance().CB_SteriTime%3600)/60
                            ,GlobalClass.getInstance().CB_DryTime/60);
                    break;
            }

        }else{
            printData = String.format("Program:\r\n%s\r\nPre-Vacuum: %d\r\nSter. Temp: %d '%s\r\nSter. Time:  %2d m %2d s\r\nDry   Time:  %2d m %2d s\r\n--------------------------------\r\n"
                    ,Constants.instance().fetchValueString(saveSelectMenuNameKey)
                    ,GlobalClass.getInstance().CB_PreVacuumCount
                    ,GlobalClass.getInstance().Steri_Temp
                    ,unitTemp
                    ,GlobalClass.getInstance().CB_SteriTime/60,GlobalClass.getInstance().CB_SteriTime%60
                    ,GlobalClass.getInstance().CB_DryTime/60,GlobalClass.getInstance().CB_DryTime%60);
        }

        sendToServicePrinter(printData.toCharArray(),printData);
        saveDtlToCache(printData.toCharArray());
        String months[] = {
                "Jan", "Feb", "Mar", "Apr",
                "May", "Jun", "Jul", "Aug",
                "Sep", "Oct", "Nov", "Dec"};
        calendar = Calendar.getInstance();



        // 0:Y/M/D 1:D/M/Y 2:M/D/Y
        String printDate="";
        if ((Constants.instance().fetchValueInt(saveDateFormat))==0) {
            printDate = String.format("%d.%s.%02d", calendar.get(Calendar.YEAR), months[calendar.get(Calendar.MONTH)], calendar.get(Calendar.DAY_OF_MONTH));
        }else if ((Constants.instance().fetchValueInt(saveDateFormat))==1) {
            printDate = String.format("%02d.%s.%d",calendar.get(Calendar.DAY_OF_MONTH), months[calendar.get(Calendar.MONTH)], calendar.get(Calendar.YEAR));
        }else if ((Constants.instance().fetchValueInt(saveDateFormat))==2) {
            printDate = String.format("%s.%02d.%d",months[calendar.get(Calendar.MONTH)], calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.YEAR));
        }

        String hourStr = "";
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        String topTime = "";
        String sleepTime = "";
        if ((Constants.instance().fetchValueInt(saveTimeFormat))==0) {
            if (i_hour<12) hourStr = "AM";
            else {
                hourStr = "PM";
                if (i_hour>12)
                    i_hour -= 12;
            }
        }

        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            if (Constants.instance().fetchValueInt(saveSelectMenuIdKey)==10) {
                printData = String.format("Date:%s\r\nTime:%s %02d:%02d:%02d\r\nCycle Counter:%07d\r\n--------------------------------\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter);
                CSVData = String.format("Date:,%s\r\nTime:,%s %02d:%02d:%02d\r\nCycle Counter:,%07d\r\n--------------------------------\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter);
                sendToServicePrinter(printData.toCharArray(),CSVData);
                saveDtlToCache(CSVData.toCharArray());
            }else{
                printData = String.format("Date:%s\r\nTime:%s %02d:%02d:%02d\r\nCycle Counter:%07d\r\n--------------------------------\r\nStep       Time    Temp.  Pres.\r\n           mmm:ss  '%s    "+presStr+"\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter,unitTemp);
                CSVData = String.format("Date:,%s\r\nTime:,%s %02d:%02d:%02d\r\nCycle Counter:,%07d\r\n--------------------------------\r\nStep,Time,Temp.,Pres.\r\n,mmm:ss,'%s,"+presStr+"\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter,unitTemp);
                sendToServicePrinter(printData.toCharArray(),CSVData);
                saveDtlToCache(CSVData.toCharArray());
            }
        }else{
            if (Constants.instance().fetchValueInt(saveSelectMenuIdKey)==10) {
                printData = String.format("Date:%s\r\nTime:%s %02d:%02d:%02d\r\nCycle Counter:%07d\r\n--------------------------------\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter);
                CSVData = String.format("Date:,%s\r\nTime:,%s %02d:%02d:%02d\r\nCycle Counter:,%07d\r\n--------------------------------\r\n",
                        printDate,hourStr,i_hour,calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND),CycleCounter);
                sendToServicePrinter(printData.toCharArray(),CSVData);
                saveDtlToCache(CSVData.toCharArray());
            }else {
                printData = String.format("Date:%s\r\nTime:%s %02d:%02d:%02d\r\nCycle Counter:%07d\r\n--------------------------------\r\nStep       Time    Temp.  Pres.\r\n           mmm:ss  '%s    "+presStr+"\r\n",
                        printDate,hourStr,i_hour, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), CycleCounter, unitTemp);
                CSVData = String.format("Date:,%s\r\nTime:,%s %02d:%02d:%02d\r\nCycle Counter:,%07d\r\n--------------------------------\r\nStep,Time,Temp.,Pres.\r\n,mmm:ss,'%s,"+presStr+"\r\n",
                        printDate,hourStr,i_hour, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), CycleCounter, unitTemp);
                sendToServicePrinter(printData.toCharArray(),CSVData);
                saveDtlToCache(CSVData.toCharArray());
            }
        }
        sendStartSterillzation();

        lineChart = (LineChart) findViewById(R.id.lineChart);
        lineChart.setVisibility(View.GONE);
        Resources res = getResources();
        FontStyle.setDefaultTextSpSize(this, 22);

        chartYDataList = new ArrayList<>();
        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        addCount++;
//        chartYDataList.add(Integer.toString(addCount));
//        chartYDataList.add("Hong Kong");
//        chartYDataList.add("Singapore");
//        chartYDataList.add("Tokyo");
//        chartYDataList.add("Paris");
//        chartYDataList.add("Hong Kong");
//        chartYDataList.add("Singapore");
        ColumnDatas = new ArrayList<>();
        tempList1 = new ArrayList<>();
        tempList1.add(35d);
//        tempList1.add(100d);
//        tempList1.add(30d);
//        tempList1.add(65d);
//        tempList1.add(60d);
//        tempList1.add(50d);
//        tempList1.add(30d);
//        tempList1.add(65d);
//        tempList1.add(-40d);
//        tempList1.add(10d);
//        tempList1.add(26d);
//        tempList1.add(-35d);
//        tempList1.add(-40d);
//        tempList1.add(10d);
        final LineData columnData1 = new LineData("Temperature", "℃",  getResources().getColor(R.color.arc3), tempList1);
        presList = new ArrayList<>();
        presList.add(0d);
////        presList.add(1.08);
////        presList.add(-0.02);
////        presList.add(1.08);
////        presList.add(-0.02);
////        presList.add(1.08);
////        presList.add(-0.02);
////        presList.add(1.08);
        LineData columnData2 = new LineData("Pres.", "RH%", IAxis.AxisDirection.RIGHT,getResources().getColor(R.color.arc2), presList);
        ColumnDatas.add(columnData1);
        ColumnDatas.add(columnData2);
        //if (GlobalClass.getInstance().isFloatingTemp) {
            tempETList = new ArrayList<>();
            tempETList.add(0d);
            final LineData columnData3 = new LineData("E.T.", "℃",  getResources().getColor(R.color.arc24), tempETList);
            ColumnDatas.add(columnData3);
        //}
        //if (Constants.instance().fetchValueInt(ExhaustTempKey)>0) {
            tempFTList = new ArrayList<>();
            tempFTList.add(0d);
            final LineData columnData4 = new LineData("F.T.", "℃",  getResources().getColor(R.color.arc25), tempFTList);
            ColumnDatas.add(columnData4);
        //}

        chartData2 = new ChartData<>("Line chart", chartYDataList, ColumnDatas);

        lineChart.setLineModel(LineChart.CURVE_MODEL);
        BaseAxis verticalAxis = lineChart.getLeftVerticalAxis();
        BaseAxis horizontalAxis = lineChart.getHorizontalAxis();

        VerticalAxis leftAxis = lineChart.getLeftVerticalAxis();
        leftAxis.setStartZero(false);
        float max = 160;
        float min = 0;
        if (Constants.instance().fetchValueInt(saveTempFormat) == 1) {
            max = (float)9/(float)5 * (float)max + (float)32;
            min = (float)9/(float)5 * (float)min + (float)32;
        }
        leftAxis.setMaxValue(max);
        leftAxis.setMinValue(min);
        leftAxis.df = new java.text.DecimalFormat("0");
        leftAxis.unitLabel = "T";

        VerticalAxis rightAxis = lineChart.getRightVerticalAxis();
        rightAxis.setStartZero(false);
        if (Constants.instance().fetchValueInt(savePresFormat)==0) {
            rightAxis.setMaxValue(3.000);
            rightAxis.setMinValue(-1.000);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==1) {
            rightAxis.setMaxValue(300);
            rightAxis.setMinValue(-100);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==2) {
            rightAxis.setMaxValue(43);
            rightAxis.setMinValue(-14);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==3) {
            rightAxis.setMaxValue(3);
            rightAxis.setMinValue(-1);
        }else if (Constants.instance().fetchValueInt(savePresFormat)==4) {
            rightAxis.setMaxValue(0.3);
            rightAxis.setMinValue(-0.1);
        }

        rightAxis.unitLabel = "P";

        //設置豎軸方向
        verticalAxis.setAxisDirection(IAxis.AxisDirection.LEFT);
        //設置網格
        verticalAxis.setDrawGrid(true);
        //設置橫軸方向
        horizontalAxis.setAxisDirection(IAxis.AxisDirection.BOTTOM);
        horizontalAxis.setDrawGrid(true);
        //設置線條樣式
        verticalAxis.getAxisStyle().setWidth(this, 1);
//        DashPathEffect effects = new DashPathEffect(new float[]{1, 2, 4, 8}, 1);
//        verticalAxis.getGridStyle().setWidth(this, 1).setColor(res.getColor(R.color.arc_text)).setEffect(effects);
//        horizontalAxis.getGridStyle().setWidth(this, 1).setColor(res.getColor(R.color.arc_text)).setEffect(effects);
        horizontalAxis.getGridStyle().setColor(R.color.cal_safe_color);
        VerticalCross cross = new VerticalCross();
        LineStyle crossStyle = cross.getCrossStyle();
        crossStyle.setWidth(this, 1);
        crossStyle.setColor(res.getColor(R.color.arc21));
        lineChart.getProvider().setCross(cross);
        lineChart.setZoom(true);
        //開啟十字架
        lineChart.getProvider().setOpenCross(false);
        //開啟MarkView
        lineChart.getProvider().setOpenMark(false);
        //設置MarkView
//        lineChart.getProvider().setMarkView(new CustomMarkView(this));
        //設置顯示點
        Point point = new Point();
        point.getPointStyle().setShape(PointStyle.CIRCLE);
        //設置顯示點的樣式
//        lineChart.getProvider().setPoint(point);

        lineChart.getHorizontalAxis().setDrawGrid(false);
        Paint paint = new Paint();
        paint.setAntiAlias(false);
        paint.setTextSize(DensityUtils.sp2px(this, 30));
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        MultiLineBubbleTip tip = new MultiLineBubbleTip<LineData>(this,
                R.mipmap.round_rect, R.mipmap.triangle, paint) {
            @Override
            public boolean isShowTip(LineData lineData, int position) {
                return position == 2;
            }

            @Override
            public String[] format(LineData lineData, int position) {
                String title = lineData.getName();
                String value = lineData.getChartYDataList().get(position) + lineData.getUnit();
                return new String[]{title, value};
            }
        };
        tip.setColorFilter(Color.parseColor("#FA8072"));
        tip.setAlpha(0.8f);
//        lineChart.getProvider().setTip(tip);

        //設置顯示標題
        lineChart.setShowChartName(false);
        lineChart.getMatrixHelper().setWidthMultiple(3);
        //設置標題方向
        lineChart.getChartTitle().setDirection(IComponent.TOP);
        //設置標題比例
        lineChart.getChartTitle().setPercent(0.2f);
        //設置標題樣式
        FontStyle fontStyle = lineChart.getChartTitle().getFontStyle();
        fontStyle.setTextColor(res.getColor(R.color.arc_temp));
        fontStyle.setTextSpSize(this, 30);

//        LevelLine levelLine = new LevelLine(20);
//        DashPathEffect effects2 = new DashPathEffect(new float[]{1, 2, 2, 4}, 1);
//        levelLine.getLineStyle().setWidth(this, 1).setColor(res.getColor(R.color.arc23)).setEffect(effects);
//        levelLine.getLineStyle().setEffect(effects2);
//        lineChart.getProvider().addLevelLine(levelLine);
        lineChart.getLegend().setDirection(IComponent.BOTTOM);
        LegendPoint legendPoint = (LegendPoint) lineChart.getLegend().getPoint();
        PointStyle style = legendPoint.getPointStyle();
        style.setShape(PointStyle.RECT);
        lineChart.getLegend().setPercent(0.2f);
        lineChart.getHorizontalAxis().setRotateAngle(0);
        lineChart.setFirstAnim(false);
        lineChart.setChartData(chartData2);
        lineChart.startChartAnim(10);
        lineChart.setOnClickColumnListener(new OnClickColumnListener<LineData>() {
            @Override
            public void onClickColumn(LineData lineData, int position) {
                //  Toast.makeText(LineChartActivity.this,lineData.getChartYDataList().get(position)+lineData.getUnit(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void stopProgress(View view) {
        progressBar.stopProgress();
    }

    public void startProgress(View view) {
        progressBar.startProgress();
    }

    private List<String> getTitles(){
        return Lists.newArrayList("Pre-vacuum", "Heating", "Sterilization", "Exhaust", "Dry");
    }

    private List<Integer> getBgRes(){
        totalRes = new ArrayList<Integer>();
        for (int i=0;i<20;i++) {
            int stage = (int)GlobalClass.getInstance().globalStageList[i];
            if (stage>0) {
                totalRes.add(getStageToRes(stage));
            }
        }

        return totalRes;
    }

    private Integer getStageToRes(int stage) {
        Integer res = 0;
        switch (stage) {
            case 1:
                res = R.drawable.stage_icon_check;
                break;
            case 2:
                res = R.drawable.stage_icon_preheat;
                break;
            case 3:
                res = R.drawable.stage_icon_prevacuum;
                break;
            case 4:
                res = R.drawable.stage_icon_heating;
                break;
            case 5:
                res = R.drawable.stage_icon_sterilization;
                break;
            case 6:
                res = R.drawable.stage_icon_exhaust;
                break;
            case 7:
                res = R.drawable.stage_icon_dry;
                break;
            case 8:
                res = R.drawable.stage_icon_vrelease;
                break;
            case 9:
                res = R.drawable.stage_icon_prevacuum;
                break;
            case 10:
                res = R.drawable.stage_icon_p_1;
                break;
            case 11:
                res = R.drawable.stage_icon_p_2;
                break;
            case 12:
                res = R.drawable.stage_icon_p_3;
                break;
            case 13:
                res = R.drawable.statge_icon_equilibrium;
                break;
            case 14:
                res = R.drawable.stage_icon_cooling;
                break;
            case 15:
                res = R.drawable.stage_icon_dissolution;
                break;
            case 16:
                res = R.drawable.stage_icon_insulation;
                break;
        }
        return res;
    }

    private String getStageToString(int stage) {
        String res="Stage";
        switch (stage) {
            case 1:
                res = "Check";
                break;
            case 2:
                res = "Pre-Heat";
                break;
            case 3:
                res = "Pre-Vacuum";
                break;
            case 4:
                res = "Heating";
                break;
            case 5:
                res = "Sterilization";
                break;
            case 6:
                res = "Exhaust";
                break;
            case 7:
                res = "Dry";
                break;
            case 8:
                res = "Vrelease";
                break;
            case 9:
                res = "Pre-Vacuum";
                break;
            case 10:
                res = "P1";
                break;
            case 11:
                res = "P2";
                break;
            case 12:
                res = "P3";
                break;
            case 13:
                res = "Equilibrium";
                break;
            case 14:
                res = "Cooling";
                break;
            case 15:
                res = "Dissolution";
                break;
            case 16:
                res = "Insulation";
                break;
        }
        return res;
    }

    @Override
    public void onItemClick(int position) {
        if (position==0) {
            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
//                if (Constants.instance().fetchValueInt(saveSelectMenuIdKey) == 10) {
                    this.startActivity(new Intent(StartSterilizationActivity.this, SteriRecordReaderActivity.class));
                    this.overridePendingTransition(0, 0);
//                }else{
//                    this.startActivity(new Intent(StartSterilizationActivity.this, SteriReportActivity.class));
//                    this.overridePendingTransition(0, 0);
//                }
            }else{
                this.startActivity(new Intent(StartSterilizationActivity.this, SteriRecordReaderActivity.class));
                this.overridePendingTransition(0, 0);
            }
        }else if (position==1) {
            lineChart.setVisibility(View.VISIBLE);
        }else if (position==2) {
            lineChart.setVisibility(View.GONE);
        }
    }

    private void setDummyData() {
        startSterInformationItemA = new ArrayList<>();
        startSterInformationItemA.add(new StartSterInformationItemA(R.drawable.toolbar_information, "Estimated time", "01:00:00"));

//        recyclerViewButton2 = new ArrayList<>();
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_pressure, "Pres.","0.00 bar"));
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_temperature, "Temp","40.0 °C"));
    }

    private String getSterilizationTime() {
        if (totaSterilTime < 0) totaSterilTime = 0;
        String timeStr;
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_hour = totaSterilTime/60/60;
        int i_minute = (totaSterilTime % 3600) / 60;
        int i_second = totaSterilTime%60;

        if(i_hour<10){
            hour = "0"+i_hour;
        }else{
            hour = ""+i_hour;
        }
        if(i_minute<10){
            minute = "0"+i_minute;
        }else{
            minute = ""+i_minute;
        }
        if(i_second<10){
            second = "0"+i_second;
        }else{
            second = ""+i_second;
        }

        timeStr = hour+":"+minute+":"+second;

        return timeStr;
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
//        new Thread(new Runnable(){
//            @Override
//            public void run() {
//                GlobalClass.getInstance().programEndTime = System.currentTimeMillis()/1000;
//                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_ProductReport");
//                String printData = "<br/><br/><br/>" +
//                        "Signature:______________________<br/><br/>";
//                GlobalClass.getInstance().globalCloudRecordSaveString+=printData;
//                try {
//                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                    params.add(new BasicNameValuePair("programName",Constants.instance().fetchValueString(saveSelectMenuNameKey)));
//                    params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
//                    params.add(new BasicNameValuePair("endDate",String.valueOf(GlobalClass.getInstance().programEndTime)));
//                    params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                    params.add(new BasicNameValuePair("report",GlobalClass.getInstance().globalCloudRecordSaveString));
//                    int CycleCounter = Settings.System.getInt(mActivity.getContentResolver(),CycleCounterKey,0);
//                    params.add(new BasicNameValuePair("cycle",String.valueOf(CycleCounter)));
//
//                    myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                    HttpResponse response = new DefaultHttpClient().execute(myPost);
//                    String er = response.toString();
//                } catch (Exception e) {
//                    String ee = e.toString();
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        FileUtils.saveStdRecordFile(GlobalClass.getInstance().saveStdRecordFileName,GlobalClass.getInstance().globalStdRecordSaveString.getBytes());
        FileUtils.saveStdRecordFile(GlobalClass.getInstance().saveDtlRecordFileName,GlobalClass.getInstance().globalDtlRecordSaveString.getBytes());

        FileUtils.saveCSVRecordFile(GlobalClass.getInstance().saveStdCSVRecordFileName,GlobalClass.getInstance().globalStdCSVRecordSaveString.getBytes());

        handler.removeCallbacks(mGraphChartRunnable);
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}
