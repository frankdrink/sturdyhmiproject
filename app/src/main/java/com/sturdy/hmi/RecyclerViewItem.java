package com.sturdy.hmi;

public class RecyclerViewItem {

    private int drawableId;
    private String name;
    private String value;

    public RecyclerViewItem(int drawableId, String name) {
        this.drawableId = drawableId;
        this.name = name;
    }

    public RecyclerViewItem(int drawableId, String name, String value) {
        this.drawableId = drawableId;
        this.name = name;
        this.value = value;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

}
