package com.sturdy.hmi.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.sturdy.switchicon.SwitchIconView;
import com.sturdy.hmi.R;
import com.sturdy.hmi.RecyclerViewItem;
import com.sturdy.hmi.item.StartSterInformationItemA;

public class StartSterGridViewAdapter1 extends RecyclerView.Adapter<StartSterGridViewAdapter1.ViewHolder> {
    private List<StartSterInformationItemA> items;
    private Activity activity;
    private boolean mComplete;
    int buttonState = 0;

    public StartSterGridViewAdapter1(Activity activity, List<StartSterInformationItemA> items) {
        this.activity = activity;
        this.items = items;
    }

    public StartSterGridViewAdapter1(Activity activity, List<StartSterInformationItemA> items,boolean bComplete) {
        this.activity = activity;
        this.items = items;
        this.mComplete = bComplete;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.startsterilization_grid1, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StartSterGridViewAdapter1.ViewHolder viewHolder,final int position) {
        if (this.mComplete) {
            viewHolder.switchChartButton.setVisibility(View.GONE);
            viewHolder.imageView.setVisibility(View.GONE);
        }
        viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());
        viewHolder.switchChartButton.setBackgroundResource(R.drawable.ic_chartswitch);

        viewHolder.imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    onItemClickListener.onItemClick(0);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                }
                return false;
            }
        });

        viewHolder.switchChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonState==0) {
                    onItemClickListener.onItemClick(1);
                    buttonState = 1;
                }else{
                    onItemClickListener.onItemClick(2);
                    buttonState = 0;
                }
//                viewHolder.switchChartButton.switchState();
//                if (viewHolder.switchChartButton.isIconEnabled())
//                    onItemClickListener.onItemClick(1);
//                else
//                    onItemClickListener.onItemClick(2);

            }
        });

        viewHolder.name_textView.setText(items.get(position).getName());
        viewHolder.time_textView.setText(items.get(position).getTime());
        viewHolder.time_textView.setTextColor(Color.BLUE);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageButton imageView;
        private ImageButton switchChartButton;
        private TextView name_textView;
        private TextView time_textView;


        public ViewHolder(View view) {
            super(view);
            name_textView = (TextView)view.findViewById(R.id.text);
            switchChartButton = (ImageButton) view.findViewById(R.id.switch_chart_button);
            time_textView = (TextView)view.findViewById(R.id.time_text);
            imageView = (ImageButton) view.findViewById(R.id.image);
            switchChartButton.setVisibility(View.VISIBLE);
//            switchChartButton.switchState();
            imageView.setVisibility(View.VISIBLE);

        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
