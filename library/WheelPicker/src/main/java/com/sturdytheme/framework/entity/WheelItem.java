package com.sturdytheme.framework.entity;


public interface WheelItem extends java.io.Serializable {

    String getName();

}
