package com.sturdy.chart.provider.component.line;

import android.graphics.Path;

import java.util.List;

/**
 * Created by Frank on 2019/10/15.
 */

public interface ILineModel {

     Path getLinePath(List<Float> pointX, List<Float> pointY);
}
