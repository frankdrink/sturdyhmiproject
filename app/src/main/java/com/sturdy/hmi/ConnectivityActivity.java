package com.sturdy.hmi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;

import java.util.ArrayList;
import java.util.List;

import br.com.marcellogalhardo.simplesettings.SimpleSettingsHelper;
import br.com.marcellogalhardo.simplesettings.entity.SimpleSettings;

public class ConnectivityActivity extends BaseToolBarActivity implements GridViewAdapter.OnItemClickListener{

    private RecyclerView listView;
    private RecyclerView gridView;
    private GridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;
    List<MainMenuSettingElement> settingElementList;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_user_management_setting);
        mDatabaseHelper=new DatabaseHelper(this);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        gridView = (RecyclerView) findViewById(R.id.grid);

        ImageButton backButton =(ImageButton) findViewById(R.id.back_button);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView menuTitleView = (TextView) findViewById(R.id.menu_title_view);
        menuTitleView.setText("Connectivity");
        menuTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

//        listView.setHasFixedSize(true);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "ListView"
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(horizontalManager);
//        listViewAdapter = new ListViewAdapter(this, corporations);
//        listView.setAdapter(listViewAdapter);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridView.setLayoutManager(layoutManager);
        setSystemMenuElement();
        gridViewAdapter = new GridViewAdapter(this, recyclerViewButton, recyclerViewButton_press);
        gridViewAdapter.setOnItemClickListener(this);
        gridView.setAdapter(gridViewAdapter);
        gridView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), gridView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTouch(View view, int position) {
//                Movie movie = movieList.get(position);
//                Toast.makeText(getApplicationContext(), position + " is touch!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    public void onItemClick(int position) {

        switch (position) {
            case 0:
                if (GlobalClass.getInstance().HM_WiFiStatus==1) {
                    this.startActivity(new Intent(ConnectivityActivity.this, WiFiManagerActivity.class));
                    this.overridePendingTransition(0, 0);
                }
                break;

            case 1:
                this.startActivity(new Intent(ConnectivityActivity.this, EthernetConfigActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 2:
                if (GlobalClass.getInstance().HM_WiFiStatus==1) {
                    this.startActivity(new Intent(ConnectivityActivity.this, BluetoothActivity.class));
                    this.overridePendingTransition(0, 0);
                }
                break;

            case 3:
                this.startActivity(new Intent(ConnectivityActivity.this, USBManagerActivity.class));
                this.overridePendingTransition(0, 0);
                break;

            case 4:
                SimpleSettings settings = new SimpleSettings.Builder()
                        .baseUrl("http://iot.opage.idv.tw/")
                        .timeout(20)
                        .build();
                SimpleSettingsHelper.putDefaultSettings(ConnectivityActivity.this, settings);
                SimpleSettingsHelper.startSettingsActivity(ConnectivityActivity.this);

                break;



        }
    }

    private void setSystemMenuElement() {
        recyclerViewButton = new ArrayList<>();
        if (GlobalClass.getInstance().HM_WiFiStatus==0) {
            recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_wi_fi_none, "Wi-Fi"));
        }else{
            recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_wi_fi_normal, "Wi-Fi"));
        }
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_internet_normal, "Internet"));
        if (GlobalClass.getInstance().HM_WiFiStatus==0) {
            recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_bluetooth_none, "Bluetooth"));
        }else{
            recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_bluetooth_normal, "Bluetooth"));
        }
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_us_bdreive_normal, "USB Device"));
        recyclerViewButton.add(new RecyclerViewItem(R.drawable.setting_card_icon_language, "Cloud"));

        recyclerViewButton_press = new ArrayList<>();
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_wi_fi_pressed, "Wi-Fi"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_internet_pressed, "Internet"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_bluetooth_pressed, "Bluetooth"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_us_bdreive_pressed, "USB Device"));
        recyclerViewButton_press.add(new RecyclerViewItem(R.drawable.setting_card_icon_language, "Cloud"));

    }

    @Override
    public void onBackPressed()
    {
    }

}
