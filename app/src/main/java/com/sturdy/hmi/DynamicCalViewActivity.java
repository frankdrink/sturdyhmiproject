package com.sturdy.hmi;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.UnitRadioListAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.UnitConvert;
import com.sturdy.hmi.view.QuantityView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.microedition.khronos.opengles.GL;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class DynamicCalViewActivity extends BaseToolBarActivity {
    private Activity mActivity;
    UnitRadioListAdapter adapter1;
    UnitRadioListAdapter adapter2;
    int saveTempFormatBase;
    int savePresFormatBase;
    List<String> data1;
    List<String> data2;
    TextView ChamberCurrentTempTV;
    TextView FloatingCurrentTempTV;
    TextView BHCurrentTempTV;
    TextView SGCurrentTempTV;
    TextView ChambeCurrentPresTV;
    TextView AmbientCurrentPresTV;
    Button mNavTitleView;
    TextView ChamberCurrentTempQV;
    TextView FloatingCurrentTempQV;
    TextView BHCurrentTempQV;
    TextView SGCurrentTempQV;
    TextView ChambeCurrentPresQV;
    TextView AmbientCurrentPresQV;
    TextView mConfirmTextView;
    int CTTemp,FTTemp,PT3Temp,SGTemp,BHTemp,ETTemp,K4Temp,ChamberPres,AmbientPres;
    int CTTempOffset,FTTempOffset,PT3TempOffset,SGTempOffset,BHTempOffset,ETTempOffset,K4TempOffset,ChamberPresOffset,AmbientPresOffset;

    List<MainMenuSettingElement> settingElementList;
    Handler handler;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x63) {
                    if (recv[2]==0x02) {
                        GlobalClass.getInstance().CalState=2;
                        intent = new Intent();
                        intent.setClass(DynamicCalViewActivity.this,DynamicCalEditActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();
                    }else if (recv[2]==0x06) {
                        GlobalClass.getInstance().CalState=2;
                        intent = new Intent();
                        intent.setClass(DynamicCalViewActivity.this,DynamicCalEditActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();
                    }else if (recv[2]==0x04) {
                        GlobalClass.getInstance().CalState=4;
                        mNavTitleView.setText("Complete");
                        mConfirmTextView.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable(){
                            public void run() {
                                finish();
                            }
                        }, 5000);
                    }
                }else if (recv[1] == 0x64) {
                    //資料型態(0x01);CT Temp (Word,0.1C);FT Temp(Word,0.1C);PT3 Temp(Word,0.1C);SG Temp(Word,0.1C);BH Temp(Word,0.1C);ET Temp(Word,0.1C);K4 Temp(Word,0.1C);Chamber Pres(Word,0.001 bar);AIR Pres(Word,0.001 bar)"
                    //資料型態(0x02);CT offset(-200~+200,Word,0.1C);FT offset(-200~+200,Word,0.1C);PT3 offset(-200~+200,Word,0.1C);SG offset(-200~+200,Word,0.1C);BH offset(-200~+200,Word,0.1C);ET offset(-200~+200,Word,0.1C);K4 offset(-200~+200,Word,0.1C);Chamber Pres offset(-500~+500,Word,0.001 bar);AIR Pres offset(-500~+500,Word,0.001 bar)"
                    //資料型態(0x03);CT Temp (Word,0.1C);FT Temp(Word,0.1C);PT3 Temp(Word,0.1C);SG Temp(Word,0.1C);BH Temp(Word,0.1C);ET Temp(Word,0.1C);K4 Temp(Word,0.1C);Chamber Pres(Word,0.001 bar);AIR Pres(Word,0.001 bar);CT offset(-200~+200,Word,0.1C);FT offset(-200~+200,Word,0.1C);PT3 offset(-200~+200,Word,0.1C);SG offset(-200~+200,Word,0.1C);BH offset(-200~+200,Word,0.1C);ET offset(-200~+200,Word,0.1C);K4 offset(-200~+200,Word,0.1C);Chamber Pres offset(-500~+500,Word,0.001 bar);AIR Pres offset(-500~+500,Word,0.001 bar)"

                    char calibrationMode = recv[2];
                    char calDataStyle = recv[3];
                    if (calDataStyle==0x01) {
                        char[] ch2 = new char[2];
                        System.arraycopy(recv, 4, ch2, 0, 2);
                        CTTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 6, ch2, 0, 2);
                        FTTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 8, ch2, 0, 2);
                        PT3Temp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 10, ch2, 0, 2);
                        SGTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 12, ch2, 0, 2);
                        BHTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 14, ch2, 0, 2);
                        ETTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 16, ch2, 0, 2);
                        K4Temp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 18, ch2, 0, 2);
                        ChamberPres = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 20, ch2, 0, 2);
                        AmbientPres = CharUtils.charArrayToInt(ch2);

                        ChamberCurrentTempTV.setText(UnitConvert.sTempConvert(0,CTTemp,1));
                        FloatingCurrentTempTV.setText(UnitConvert.sTempConvert(0,FTTemp,1));
                        BHCurrentTempTV.setText(UnitConvert.sTempConvert(0,BHTemp,1));
                        SGCurrentTempTV.setText(UnitConvert.sTempConvert(0,SGTemp,1));
                        ChambeCurrentPresTV.setText(UnitConvert.sPresConvert(0,ChamberPres));
                        AmbientCurrentPresTV.setText(UnitConvert.sPresConvert(0,AmbientPres));

                    }else if (calDataStyle==0x02) {

                    }else if (calDataStyle==0x03) {
                        char[] ch2 = new char[2];
                        System.arraycopy(recv, 4, ch2, 0, 2);
                        CTTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 6, ch2, 0, 2);
                        FTTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 8, ch2, 0, 2);
                        PT3Temp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 10, ch2, 0, 2);
                        SGTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 12, ch2, 0, 2);
                        BHTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 14, ch2, 0, 2);
                        ETTemp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 16, ch2, 0, 2);
                        K4Temp = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 18, ch2, 0, 2);
                        ChamberPres = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 20, ch2, 0, 2);
                        AmbientPres = CharUtils.charArrayToInt(ch2);

                        ch2 = new char[2];
                        System.arraycopy(recv, 22, ch2, 0, 2);
                        CTTempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 24, ch2, 0, 2);
                        FTTempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 26, ch2, 0, 2);
                        PT3TempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 28, ch2, 0, 2);
                        SGTempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 30, ch2, 0, 2);
                        BHTempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 32, ch2, 0, 2);
                        ETTempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 34, ch2, 0, 2);
                        K4TempOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 36, ch2, 0, 2);
                        ChamberPresOffset = CharUtils.charArrayToInt(ch2);
                        ch2 = new char[2];
                        System.arraycopy(recv, 38, ch2, 0, 2);
                        AmbientPresOffset = CharUtils.charArrayToInt(ch2);

                        ChamberCurrentTempTV.setText(UnitConvert.sTempConvert(0,CTTemp,1));
                        FloatingCurrentTempTV.setText(UnitConvert.sTempConvert(0,FTTemp,1));
                        BHCurrentTempTV.setText(UnitConvert.sTempConvert(0,BHTemp,1));
                        SGCurrentTempTV.setText(UnitConvert.sTempConvert(0,SGTemp,1));
                        ChambeCurrentPresTV.setText(UnitConvert.sPresConvert(0,ChamberPres));
                        AmbientCurrentPresTV.setText(UnitConvert.sPresConvert(0,AmbientPres));

                        ChamberCurrentTempQV.setText(UnitConvert.sTempConvert(0,CTTempOffset,1));
                        FloatingCurrentTempQV.setText(UnitConvert.sTempConvert(0,FTTempOffset,1));
                        BHCurrentTempQV.setText(UnitConvert.sTempConvert(0,BHTempOffset,1));
                        SGCurrentTempQV.setText(UnitConvert.sTempConvert(0,SGTempOffset,1));
                        ChambeCurrentPresQV.setText(UnitConvert.sTempConvert(0,ChamberPresOffset,1));
                        AmbientCurrentPresQV.setText(UnitConvert.sTempConvert(0,AmbientPresOffset,1));

                    }

                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    Runnable updateDataRunnable=new Runnable() {
        @Override
        public void run() {
            char[] data = new char[]{0x03,0x64,0x01,0x03};
            if (GlobalClass.getInstance().CalSelectTemp==121) {
                data = new char[]{0x03,0x64,0x02,0x03};
            }else if (GlobalClass.getInstance().CalSelectTemp==134) {
                data = new char[]{0x03,0x64,0x03,0x03};
            }
            sendToService(data);
            handler.postDelayed(this, 1000);
        }
    };

    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dynamiccalview);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
//        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
//        mNavTopBar.setBackgroundDividerEnabled(true);
        String titleStr="";
        if (GlobalClass.getInstance().CalSelectTemp==121) {
            if (GlobalClass.getInstance().CalState==1)
                titleStr = "Heated to 121";
            else if (GlobalClass.getInstance().CalState==3)
                titleStr = "121 Exhaust";
        }
        else if (GlobalClass.getInstance().CalSelectTemp==134) {
            if (GlobalClass.getInstance().CalState==1)
                titleStr = "Heated to 134";
            else if (GlobalClass.getInstance().CalState==3)
                titleStr = "134 Exhaust";
        }
        GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.DYNAMIC_CAL;

        mNavTitleView = mNavTopBar.addLeftTextButton(titleStr, R.id.topbar_sterilization_right_title, Color.BLACK,38);

        ChamberCurrentTempTV = (TextView) findViewById(R.id.chamber_current_temp);
        FloatingCurrentTempTV = (TextView) findViewById(R.id.floating_current_temp);
        BHCurrentTempTV = (TextView) findViewById(R.id.bh_current_temp);
        SGCurrentTempTV = (TextView) findViewById(R.id.sg_current_temp);
        ChambeCurrentPresTV = (TextView) findViewById(R.id.chamber_current_pres);
        AmbientCurrentPresTV = (TextView) findViewById(R.id.ambient_current_pres);

        ChamberCurrentTempQV = (TextView) findViewById(R.id.chamber_temp_quantityView);
        FloatingCurrentTempQV = (TextView) findViewById(R.id.floating_temp_quantityView);
        BHCurrentTempQV = (TextView) findViewById(R.id.bh_temp_quantityView);
        SGCurrentTempQV = (TextView) findViewById(R.id.sg_temp_quantityView);
        ChambeCurrentPresQV = (TextView) findViewById(R.id.chamber_pres_quantityView);
        AmbientCurrentPresQV = (TextView) findViewById(R.id.ambient_pres_quantityView);

        mConfirmTextView = (TextView)findViewById(R.id.confirm_button);
        mConfirmTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();

        // 0x01-134動態校正啟動訊號
        if (GlobalClass.getInstance().CalSelectTemp==134) {
            if (GlobalClass.getInstance().CalState==1) {
                char[] data = new char[]{0x02,0x63,0x01};
                sendToService(data);
            }else if (GlobalClass.getInstance().CalState==3) {
                char[] data = new char[]{0x02,0x63,0x03};
                sendToService(data);
            }

        }else if (GlobalClass.getInstance().CalSelectTemp==121) {
            if (GlobalClass.getInstance().CalState==1) {
                char[] data = new char[]{0x02,0x63,0x05};
                sendToService(data);
            }else if (GlobalClass.getInstance().CalState==3) {
                char[] data = new char[]{0x02,0x63,0x03};
                sendToService(data);
            }
        }

        //  // 取得校正中溫度壓力資訊 校正模式(0x01~0x03);資料型態(0x01~0x03);
        char[] data = new char[]{0x03,0x64,0x01,0x03};
        if (GlobalClass.getInstance().CalSelectTemp==121) {
            data = new char[]{0x03,0x64,0x02,0x03};
        }else if (GlobalClass.getInstance().CalSelectTemp==134) {
            data = new char[]{0x03,0x64,0x03,0x03};
        }
        sendToService(data);
        handler = new Handler();
        handler.postAtTime(updateDataRunnable, 1000);

    }

    // "校正模式(0x01~0x03); CT offset(-200~+200,Word,0.1C);
    // FT offset(-200~+200,Word,0.1C);PT3 offset(-200~+200,Word,0.1C);
    // SG offset(-200~+200,Word,0.1C);BH offset(-200~+200,Word,0.1C);
    // ET offset(-200~+200,Word,0.1C);K4 offset(-200~+200,Word,0.1C);
    // Chamber Pres offset(-500~+500,Word,0.001 bar);
    // AIR Pres offset(-500~+500,Word,0.001 bar)"
    private void sendCalibration() {
//        char[] data = new char[21];
//        char[] ch22 = new char[2];
//        data[0] = 0x14;
//        data[1] = 0x62;
//        data[2] = 0x01;
//        float f = ChamberCurrentTempQV.getQuantity();
//        f = (float)(Math.round(f*100))/100;
//        CTTempOffset = (int)(f*(float)10.f);
//        ch22 = CharUtils.fromShort((short)CTTempOffset);
//        data[3] = ch22[0];
//        data[4] = ch22[1];
//        f = FloatingCurrentTempQV.getQuantity();
//        f = (float)(Math.round(f*100))/100;
//        FTTempOffset = (int)(f*(float)10.f);
//        ch22 = CharUtils.fromShort((short)FTTempOffset);
//        data[5] = ch22[0];
//        data[6] = ch22[1];
//        ch22 = CharUtils.fromShort((short)PT3TempOffset);
//        data[7] = ch22[0];
//        data[8] = ch22[1];
//        f = SGCurrentTempQV.getQuantity();
//        f = (float)(Math.round(f*100))/100;
//        SGTempOffset = (int)(f*(float)10.f);
//        ch22 = CharUtils.fromShort((short)SGTempOffset);
//        data[9] = ch22[0];
//        data[10] = ch22[1];
//        f = BHCurrentTempQV.getQuantity();
//        f = (float)(Math.round(f*100))/100;
//        BHTempOffset = (int)(f*(float)10.f);
//        ch22 = CharUtils.fromShort((short)BHTempOffset);
//        data[11] = ch22[0];
//        data[12] = ch22[1];
//        ch22 = CharUtils.fromShort((short)ETTempOffset);
//        data[13] = ch22[0];
//        data[14] = ch22[1];
//        ch22 = CharUtils.fromShort((short)K4TempOffset);
//        data[15] = ch22[0];
//        data[16] = ch22[1];
//        f = ChambeCurrentPresQV.getQuantity();
//        //f = (float)(Math.round(f*100))/100;
//        ChamberPresOffset = (int)(f*(float)1000.f);
//        ch22 = CharUtils.fromShort((short)ChamberPresOffset);
//        data[17] = ch22[0];
//        data[18] = ch22[1];
//        f = AmbientCurrentPresQV.getQuantity();
//        //f = (float)(Math.round(f*100))/100;
//        AmbientPresOffset = (int)(f*(float)1000.f);
//        ch22 = CharUtils.fromShort((short)AmbientPresOffset);
//        data[19] = ch22[0];
//        data[20] = ch22[1];
//        sendToService(data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.IDLE;
        handler.removeCallbacks(updateDataRunnable);

    }




}
