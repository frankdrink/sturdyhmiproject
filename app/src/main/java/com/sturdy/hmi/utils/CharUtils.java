package com.sturdy.hmi.utils;

import java.util.Arrays;

/**
 * Created by franklin on 2019/10/23.
 */

public class CharUtils {

    public static String charToString(char value[]) {
//        for (int i=0;i<value.length;i++) {
//            if (value[i]==0) {
//                for (int j=i+1;j<value.length;j++){
//                    value[j] = 32;
//                }
//                break;
//            }
//        }
        return new String(value);
    }

    public static char[] fromShort(short n) {
        return new char[] {
                (char) (n & 0xff), (char) ((n >> 8) & 0xff)
        };
    }

    public static char[] fromInt(int n) {
        char[] bytes = new char[4];

        for (int i = 0; i < 4; i++) {
            bytes[i] = (char) (n >> (i * 8));
        }

        return bytes;
    }

    public static int charArrayToInt(char[] b) {
        int value = 0;
        if (b.length == 4) {
            value = b[3] << 24 | (b[2] & 0xff) << 16 | (b[1] & 0xff) << 8
                    | (b[0] & 0xff);
        }else if (b.length == 2) {
            short sh = (short) ((b[1] & 0xff) << 8 | (b[0] & 0xff));
            value = (int)sh;
        }else if (b.length == 1) {
            value = (int)b[0];
        }
        return value;
    }

    public static int swapNibbles(int x)
    {
        return ((x & 0x0F) << 4 | (x & 0xF0) >> 4);
    }

    public static int checkSum(byte[] input) {
        int checkSum = 0;
        for(byte b : input) {
            checkSum += b & 0xFF;
        }
        return checkSum;
    }
}
