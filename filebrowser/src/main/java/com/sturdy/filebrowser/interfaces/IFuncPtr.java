package com.sturdy.filebrowser.interfaces;

/**
 * Created by sturdy on 4/15/2017.
 */
public interface IFuncPtr {
    void execute(String val);
}
