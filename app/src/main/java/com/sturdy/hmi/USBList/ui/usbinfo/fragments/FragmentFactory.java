package com.sturdy.hmi.USBList.ui.usbinfo.fragments;

import android.support.v4.app.Fragment;

import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbDevice;

public final class FragmentFactory {

    public static Fragment getFragment(String usbKey) {
        return InfoFragmentAndroid.create(usbKey);
    }

    public static Fragment getFragment(SysBusUsbDevice usbDevice) {
        return InfoFragmentLinux.create(usbDevice);
    }
}
