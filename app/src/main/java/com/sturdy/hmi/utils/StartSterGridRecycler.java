package com.sturdy.hmi.utils;

import android.graphics.drawable.Drawable;

public class StartSterGridRecycler {
    private String mMsg1, mMsg2, mValue;
    private int mImage;
    private boolean mIsTextViewEditable;

    public StartSterGridRecycler(String mMsg1, String mMsg2, String mValue, int mImage){
        this(mMsg1, mMsg2, mValue, mImage, false);
    }


    public StartSterGridRecycler(String mMsg1, String mMsg2, String mValue, int mImage, boolean mIsTextViewEditable){
        this.mMsg1 = mMsg1;
        this.mMsg2 = mMsg2;
        this.mValue = mValue;

        this.mImage = mImage;
        this.mIsTextViewEditable = mIsTextViewEditable;
    }

    //setters

    /**
     * Sets the main string to be displayed in the row
     * @param mMsg1 String which is displayed as main message
     */
    public void setMessage1(String mMsg1){
        this.mMsg1 = mMsg1;
    }

    /**
     * Sets the supporting string to be displayed in the row
     * @param mMsg2 String which is displayed as supporting message
     */
    public void setMessage2(String mMsg2){
        this.mMsg2 = mMsg2;
    }

    public void setValue(String mValue){
        this.mValue = mValue;
    }

    /**
     * Sets the data of the image to be displayed as avatar
     * @param mImage_url image drawable object
     */
    public void setmImage(Drawable mImage_url){
        this.mImage = mImage;
    }

    /**
     * Sets the checkbox status of the row
     * @param mIsChecked status of the checkbox
     */
    public void setmIsChecked(boolean mIsTextViewEditable){
        this.mIsTextViewEditable = mIsTextViewEditable;
    }

    //getters

    /**
     * @return String containing Main message of the row
     */
    public String getMessage1(){
        return mMsg1;
    }

    /**
     * @return String containing Supporting message of the row
     */
    public String getMessage2(){
        return mMsg2;
    }

    public String getValue(){
        return mValue;
    }


    /**
     * @return Drawable of avatar if the row
     */
    public int getmImage(){
        return mImage;
    }

    /**
     * @return current status of the checkbox
     */
    public boolean getmIsTextViewEditable(){
        return mIsTextViewEditable;
    }

}
