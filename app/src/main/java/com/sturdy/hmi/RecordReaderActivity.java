package com.sturdy.hmi;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.filebrowser.FileBrowserWithCustomHandler;
import com.sturdy.filebrowser.models.FileItem;
import com.sturdy.filebrowser.utils.UIUtils;
import com.sturdy.filechooser.Content;
import com.sturdy.filechooser.StorageChooser;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.FileUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.view.DashedLineView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import dma.xch.hmi.api.UntiTools;

import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class RecordReaderActivity extends BaseToolBarActivity {
    private Handler handler = new Handler();
    private StorageChooser.Builder builder = new StorageChooser.Builder();
    private StorageChooser chooser;
    public ArrayList<char[]> mSendCmdQueue = null;
    TableLayout tableLayout;
    String mRecordData[];

    private Runnable mPrintSendCmdRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (GlobalClass.getInstance().MachineState != Constants.MACHINE_STATE.CB_DFU) {
                try {
                    if (mSendCmdQueue != null && mSendCmdQueue.size() > 0) {
                        char[] sendData = mSendCmdQueue.get(0);
                        sendToServiceRecordPrinter(sendData);
                        mSendCmdQueue.remove(0);
                        handler.postDelayed(mPrintSendCmdRunnable, 300);
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_recordreader);
        mActivity = this;
        mSendCmdQueue = new ArrayList<char[]>();
        Content c = new Content();
        c.setCreateLabel("Create");
        c.setInternalStorageText("My Storage");
        c.setCancelLabel("Cancel");
        c.setSelectLabel("Select");
        c.setOverviewHeading("Choose Drive");

        builder.withActivity(this)
                .withFragmentManager(getFragmentManager())
                .setMemoryBarHeight(1.5f)
//                .disableMultiSelect()
                .withContent(c);
        builder.withMemoryBar(true);
//        mRecordData = new ArrayList<String>();
        Intent intent = this.getIntent();
        String fileName = intent.getStringExtra("fileName");
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        final File file = new File(fileName);

        Button mNavTitleView = mNavTopBar.addLeftTextButton(file.getName(), R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        DrawMeButton mPrintButton = (DrawMeButton)findViewById(R.id.print_button);
        mPrintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int count=0;
                        for (int i=0;i<mRecordData.length;i++) {
                            String print = mRecordData[i]+"\n";
                            mSendCmdQueue.add(print.toCharArray());
                        }
                        handler.postDelayed(mPrintSendCmdRunnable, 1);
                    }
                });

            }
        });



        DrawMeButton mExportButton = (DrawMeButton)findViewById(R.id.export_button);
        mExportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooser = builder.build();
                chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
                    @Override
                    public void onSelect(final String path) {
                        copyFile(file.getAbsolutePath(), path+"/Download/"+file.getName(),true);

                        Toast toast = Toast.makeText(getApplicationContext(), "Export Complete!!", Toast.LENGTH_SHORT);
                        ViewGroup group = (ViewGroup) toast.getView();
                        TextView messageTextView = (TextView) group.getChildAt(0);
                        messageTextView.setTextSize(42);
                        toast.show();
                    }
                });
                chooser.setOnCancelListener(new StorageChooser.OnCancelListener() {
                    @Override
                    public void onCancel() {
//                            Toast.makeText(getApplicationContext(), "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show();
                    }
                });
                chooser.setOnMultipleSelectListener(new StorageChooser.OnMultipleSelectListener() {
                    @Override
                    public void onDone(ArrayList<String> selectedFilePaths) {
                        for(String s: selectedFilePaths) {
//                            Log.e(TAG, s);
                        }
                    }
                });
                chooser.show();

            }
        });
        final ProgressDialog dialog = ProgressDialog.show(RecordReaderActivity.this,
                "Loading", "Please Wait...",true);
        new Thread(new Runnable(){
            @Override
            public void run() {
                try{
                    Thread.sleep(100);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally{
                    dialog.dismiss();
                }
            }
        }).start();
        tableLayout = (TableLayout) findViewById(R.id.tableLayout1);
//        BufferedReader reader;
        try{
            String fileString = FileUtils.loadStdRecordFile(fileName);
            if (fileString!="") {
                mRecordData = fileString.split("\\n");
                for(String line: mRecordData) {
                    TableRow row= new TableRow(this);
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lp);
                    TextView tv1 = new TextView(this);
                    tv1.setGravity(Gravity.LEFT);
                    tv1.setTextSize(28);
                    tv1.setTypeface(Typeface.MONOSPACE,Typeface.ITALIC);
                    tv1.setText(line);
                    tv1.setTextColor(Color.BLACK);
                    row.addView(tv1);
                    tableLayout.addView(row,new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableLayout.LayoutParams.WRAP_CONTENT));
                }
//                InputStreamReader isr = new InputStreamReader(new FileInputStream(file));
//                reader = new BufferedReader(isr);
//                String line = reader.readLine();
//                while(line != null){
//                    line = reader.readLine();
//                    mRecordData.add(line);
//                    TableRow row= new TableRow(this);
//                    TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
//                            TableRow.LayoutParams.WRAP_CONTENT);
//                    row.setLayoutParams(lp);
//                    TextView tv1 = new TextView(this);
//                    tv1.setGravity(Gravity.LEFT);
//                    tv1.setTextSize(28);
//                    tv1.setText(line);
//                    tv1.setTextColor(Color.BLACK);
//                    row.addView(tv1);
//                    tableLayout.addView(row,new TableLayout.LayoutParams(
//                            TableLayout.LayoutParams.WRAP_CONTENT,
//                            TableLayout.LayoutParams.WRAP_CONTENT));
//                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        dialog.dismiss();
    }

    public static boolean copyFile(String srcFileName, String destFileName,
                                   boolean overlay) {
        File srcFile = new File(srcFileName);

        // 判斷原始檔是否存在
        if (!srcFile.exists()) {
            return false;
        } else if (!srcFile.isFile()) {
            return false;
        }

        // 判斷目標檔案是否存在
        File destFile = new File(destFileName);
        if (destFile.exists()) {
            // 如果目標檔案存在並允許覆蓋
            if (overlay) {
                // 刪除已經存在的目標檔案,無論目標檔案是目錄還是單個檔案
                new File(destFileName).delete();
            }
        } else {
            // 如果目標檔案所在目錄不存在,則建立目錄
            if (!destFile.getParentFile().exists()) {
                // 目標檔案所在目錄不存在
                if (!destFile.getParentFile().mkdirs()) {
                    // 複製檔案失敗:建立目標檔案所在目錄失敗
                    return false;
                }
            }
        }

        // 複製檔案
        int byteread = 0; // 讀取的位元組數
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(destFile);
            byte[] buffer = new byte[1024];

            while ((byteread = in.read(buffer)) != -1) {
                out.write(buffer, 0, byteread);
            }
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (out != null)
                    out.close();
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
