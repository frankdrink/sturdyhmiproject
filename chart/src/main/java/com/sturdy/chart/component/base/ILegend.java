package com.sturdy.chart.component.base;

import android.graphics.PointF;

import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.ColumnData;
import com.sturdy.chart.data.style.FontStyle;
import com.sturdy.chart.provider.component.point.ILegendPoint;
import com.sturdy.chart.provider.component.point.IPoint;
import com.sturdy.chart.listener.OnClickLegendListener;


public interface ILegend<C extends ColumnData> extends IComponent<ChartData<C>> {

    float getPercent();

    void setPercent(float percent);

    FontStyle getFontStyle();

    int getDirection();

    void setDirection(int direction);

    void setFontStyle(FontStyle fontStyle);

    void onClickLegend(PointF pointF);

    void setOnClickLegendListener(OnClickLegendListener<C> onClickLegendListener);


    int getPadding();

    void setPadding(int padding);

    IPoint getPoint();

    void setPoint(ILegendPoint point);

    void setSelectColumn(boolean selectColumn);

    void setDisplay(boolean isDisplay);

    public void setCurrentValue(String temp,String pres,String temp2,String tempET);

}