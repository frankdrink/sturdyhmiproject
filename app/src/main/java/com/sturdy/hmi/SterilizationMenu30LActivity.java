package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeImageButton;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class SterilizationMenu30LActivity extends BaseToolBarActivity {
    private Activity mActivity;
    ListView list;
    String[] web = {
            "Liquid 1",
            "Liquid 2",
            "Solid 1",
            "Solid 2",
            "Agar",
            "Dissolution",
            "Dry Only",
            "Waste",
            "User 1",
            "User 2",
            "Flash",
            "Latex"

    } ;
    Integer[] imageId = {
            R.drawable.steri_list_icon_liquid_normal,
            R.drawable.steri_list_icon_liquid_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_agar_normal,
            R.drawable.steri_list_icon_dissollution_normal,
            R.drawable.steri_list_icon_dry_normal,
            R.drawable.steri_list_icon_waste_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.steri_list_icon_user_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.steri_list_icon_latex_normal


    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterilization_menu);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Sterilization", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        SteriMenuList listAdapter = new
                SteriMenuList(this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position<10) {  // position=10 LeakageTest 2021/04/19 30L/60L新增Flash程序position=11
                    Constants.instance().storeValueInt(saveSelectMenuIdKey, position);
                }else{
                    switch (position) {
                        case 10:
                            Constants.instance().storeValueInt(saveSelectMenuIdKey, 11);
                            break;
                        case 11:
                            Constants.instance().storeValueInt(saveSelectMenuIdKey, 13);
                            break;
                    }

                }
                if ((Constants.instance().fetchValueInt(preIDControl))==1) {
                    Constants.instance().storeValueString(userLoginCheckPoint, "PreID");
                    startActivity(new Intent(SterilizationMenu30LActivity.this, UserLoginActivity.class));
                }else {
                    Intent intent = new Intent();
                    String programName;
                    switch (position) {
                        case 0:
                            intent.setClass(SterilizationMenu30LActivity.this, Liquid1EditActivity.class);
                            programName = "Liquid 1";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 1:
                            intent.setClass(SterilizationMenu30LActivity.this, Liquid2EditActivity.class);
                            programName = "Liquid 2";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 2:
                            intent.setClass(SterilizationMenu30LActivity.this, Solid1EditActivity.class);
                            programName = "Solid 1";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 3:
                            intent.setClass(SterilizationMenu30LActivity.this, Solid2EditActivity.class);
                            programName = "Solid 2";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 4:
                            intent.setClass(SterilizationMenu30LActivity.this, AgarEditActivity.class);
                            programName = "Agar";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 5:
                            intent.setClass(SterilizationMenu30LActivity.this, DissolutionEditActivity.class);
                            programName = "Dissolution";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 6:
                            intent.setClass(SterilizationMenu30LActivity.this, DryOnlyEditActivity.class);
                            programName = "Dry Only";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 7:
                            intent.setClass(SterilizationMenu30LActivity.this, WasteEditActivity.class);
                            programName = "Waste";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 8:
                            intent.setClass(SterilizationMenu30LActivity.this, User1EditActivity.class);
                            programName = "User 1";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 9:
                            intent.setClass(SterilizationMenu30LActivity.this, User2EditActivity.class);
                            programName = "User 2";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 10:
                            intent.setClass(SterilizationMenu30LActivity.this, FlashEditActivity.class);
                            programName = "Flash";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 11:
                            intent.setClass(SterilizationMenu30LActivity.this, LatexEditActivity.class);
                            programName = "Latex";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                    }
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            }
        });
        startShowTitleClock(rightTimeButton);
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
    }


}
