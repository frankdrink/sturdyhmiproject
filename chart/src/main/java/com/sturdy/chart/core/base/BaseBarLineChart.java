package com.sturdy.chart.core.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;

import com.sturdy.chart.component.axis.HorizontalAxis;
import com.sturdy.chart.component.base.IAxis;
import com.sturdy.chart.component.axis.VerticalAxis;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.provider.IProvider;


/**
 * 線性和柱狀圖
 */

public abstract class BaseBarLineChart<P extends IProvider<C>,C extends BarData> extends BaseChart<P,C> {


    protected HorizontalAxis horizontalAxis; //橫軸
    protected VerticalAxis leftVerticalAxis;//豎軸
    protected VerticalAxis rightVerticalAxis;//豎軸


    public BaseBarLineChart(Context context) {
        super(context);

    }

    public BaseBarLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public BaseBarLineChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected  void init(){
        horizontalAxis = new HorizontalAxis();
        leftVerticalAxis = new VerticalAxis(IAxis.AxisDirection.LEFT);
        rightVerticalAxis = new VerticalAxis(IAxis.AxisDirection.RIGHT);
        super.init();

    }

    protected  void drawContent(Canvas canvas){
        horizontalAxis.computeScale(chartData, chartRect, paint);
        if (chartData.getScaleData().isLeftHasValue)
            leftVerticalAxis.computeScale(chartData, chartRect, paint);
        if (chartData.getScaleData().isRightHasValue)
            rightVerticalAxis.computeScale(chartData, chartRect, paint);
        if (chartData.getScaleData().isLeftHasValue)
            leftVerticalAxis.draw(canvas, chartRect, matrixHelper, paint, chartData);
        if (chartData.getScaleData().isRightHasValue)
            rightVerticalAxis.draw(canvas, chartRect, matrixHelper, paint, chartData);
        horizontalAxis.draw(canvas, chartRect, matrixHelper, paint, chartData);
        computeScaleRect();
        provider.drawProvider(canvas, chartRect, matrixHelper, paint);
    }

    /**
     * 計算繪製刻度之後的大小
     */
    private void computeScaleRect(){

        Rect rect = chartData.getScaleData().scaleRect;
        computeChartRect(rect);
    }

    public void setHeightPercent(){

    }
    public HorizontalAxis getHorizontalAxis() {
        return horizontalAxis;
    }

    public VerticalAxis getLeftVerticalAxis() {
        return leftVerticalAxis;
    }

    public VerticalAxis getRightVerticalAxis() {
        return rightVerticalAxis;
    }
}
