package com.sturdy.chart.core;

import android.content.Context;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseBarLineChart;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.provider.barLine.BarProvider;

/**
 * Created by Frank 2020/01/01
 */

public class BarChart extends BaseBarLineChart<BarProvider<BarData>,BarData> {

    public BarChart(Context context) {
        super(context);
    }

    public BarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BarChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




    @Override
    protected BarProvider initProvider() {
        return new BarProvider();
    }


}
