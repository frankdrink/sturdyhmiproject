package com.sturdy.hmi;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class SterilizationMenuActivity extends BaseToolBarActivity {
    private Activity mActivity;
    ListView list;
    String[] web = {
            "Unwrapped 121",
            "Unwrapped 134",
            "Wrapped 121",
            "Wrapped 134",
            "Instrument 121",
            "Instrument 134",
            "Prion",
            "Flash",
            "Dry",
            "Customization",
    } ;
    Integer[] imageId = {
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_unwrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_wrapped_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_instrument_normal,
            R.drawable.steri_list_icon_prion_normal,
            R.drawable.steri_list_icon_flash_normal,
            R.drawable.steri_list_icon_dry_normal,
            R.drawable.steri_list_icon_user_normal

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sterilization_menu);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Sterilization", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        SteriMenuList listAdapter = new
                SteriMenuList(this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                helper.getCustomerDao().deleteAllBarCode();
                Constants.instance().storeValueInt(saveSelectMenuIdKey, position);
                if ((Constants.instance().fetchValueInt(preIDControl))==1) {
                    Constants.instance().storeValueString(userLoginCheckPoint, "PreID");
                    startActivity(new Intent(SterilizationMenuActivity.this, UserLoginActivity.class));
                }else {
                    Intent intent = new Intent();
                    String programName;
                    switch (position) {
                        case 0:
                            intent.setClass(SterilizationMenuActivity.this, UnwrappedEditActivity.class);
                            programName = "Unwrapped 121";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 1:
                            intent.setClass(SterilizationMenuActivity.this, UnwrappedEditActivity.class);
                            programName = "Unwrapped 134";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 2:
                            intent.setClass(SterilizationMenuActivity.this, WrappedEditActivity.class);
                            programName = "Wrapped 121";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 3:
                            intent.setClass(SterilizationMenuActivity.this, WrappedEditActivity.class);
                            programName = "Wrapped 134";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 4:
                            intent.setClass(SterilizationMenuActivity.this, InstrumentEditActivity.class);
                            programName = "Instrument 121";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 5:
                            intent.setClass(SterilizationMenuActivity.this, InstrumentEditActivity.class);
                            programName = "Instrument 134";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "134");
                            GlobalClass.getInstance().Steri_Temp = 134;
                            break;
                        case 6:
                            intent.setClass(SterilizationMenuActivity.this, PrionEditActivity.class);
                            programName = "Prion";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 7:
                            intent.setClass(SterilizationMenuActivity.this, FlashEditActivity.class);
                            programName = "Flash";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 8:
                            intent.setClass(SterilizationMenuActivity.this, DryEditActivity.class);
                            programName = "Dry";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                        case 9:
                            intent.setClass(SterilizationMenuActivity.this, CustomizationEditActivity.class);
                            programName = "Customization";
                            Constants.instance().storeValueString(saveSelectMenuNameKey, programName);
                            intent.putExtra("name", programName);
                            intent.putExtra("temp", "121");
                            GlobalClass.getInstance().Steri_Temp = 121;
                            break;
                    }
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            }
        });
        startShowTitleClock(rightTimeButton);
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
    }


}
