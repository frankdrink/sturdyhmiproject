package com.sturdy.hmi.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import java.util.Date;
import java.util.Random;

import dma.xch.hmi.api.HmiClient;

/**
 * Created by franklin on 2019/9/26.
 */

public class GPIOService extends Service {
    private Handler handler = new Handler();
    HmiClient client;
    private boolean high = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        client=HmiClient.getInstance();
        client.initSDK();

        handler.postDelayed(showTime, 100);
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(showTime);
        super.onDestroy();
    }

    private Runnable showTime = new Runnable() {
        public void run() {
//            Log.i("time:", new Date().toString());

            handler.postDelayed(this, 100);
        }
    };



}
