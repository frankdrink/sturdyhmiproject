package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.adapter.SterFinishGridViewAdapter1;
import com.sturdy.hmi.adapter.SterFinishGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class SterilizationFinishActivity extends BaseToolBarActivity implements SterFinishGridViewAdapter1.OnItemClickListener,SterFinishGridViewAdapter2.OnItemClickListener{
    Handler handler;
    Runnable run;
    CustomTitleBar mTopBar;
    private RecyclerView gridView_1, gridView_2;
    private SterFinishGridViewAdapter1 gridViewAdapter1;
    private SterFinishGridViewAdapter2 gridViewAdapter2;

    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<StartSterInformationItemA> startSterInformationItemA;
    private ArrayList<RecyclerViewItem> recyclerViewButton2;
    ImageView fragmentBackgroundImage;
    TextView progressBarTitleView;
    ScrollerViewPager viewPager;
    DottedProgressBar progressBar;
    private int progressBarNumberOfDots;
    private int progressBarIndex = 0;

    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
//            progressBarIndex = (progressBarIndex + 1) % progressBarNumberOfDots;
//            progressBar.setActiveDotIndex(progressBarIndex);
//            progressBar.invalidate();
//            viewPager.setCurrentItem(progressBarIndex);
//            progressBarTitleView.setText(getTitles().get(progressBarIndex));
//            String timeStr = getSterilizationTime();
//            StartSterInformationItemA item = new StartSterInformationItemA(startSterInformationItemA.get(0).getDrawableId(),startSterInformationItemA.get(0).getName(),timeStr);
//            startSterInformationItemA.set(0,item);
//            gridViewAdapter1.notifyDataSetChanged();
//            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_sterilizationfinish);
        mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        fragmentBackgroundImage = (ImageView) findViewById(R.id.fragment_bgview);
        progressBarTitleView = (TextView) findViewById(R.id.progress_title_view);
        progressBarTitleView.setText("Complete");
        setDummyData();
        gridView_1 = (RecyclerView) findViewById(R.id.information_grid_1);
        gridView_1.setHasFixedSize(true);
        GridLayoutManager layoutManager1 = new GridLayoutManager(this, 1);
        gridView_1.setLayoutManager(layoutManager1);
        gridViewAdapter1 = new SterFinishGridViewAdapter1(this, startSterInformationItemA);
        gridViewAdapter1.setOnItemClickListener(this);
        gridView_1.setAdapter(gridViewAdapter1);


        gridView_2 = (RecyclerView) findViewById(R.id.information_grid_2);
        gridView_2.setHasFixedSize(true);
        GridLayoutManager layoutManager2 = new GridLayoutManager(this, 2);
        gridView_2.setLayoutManager(layoutManager2);
        gridViewAdapter2 = new SterFinishGridViewAdapter2(this, recyclerViewButton2, recyclerViewButton2);
        gridView_2.setAdapter(gridViewAdapter2);

        mTopBar.setBackgroundColor( this.getResources().getColor(R.color.colorTitlecCompleteColor));
        fragmentBackgroundImage.setImageResource(R.drawable.stage_bg_green);
//        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
//        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
//        progressBarNumberOfDots = getBgRes().size();
//        PagerModelManager manager = new PagerModelManager();
//        manager.addCommonFragment(GuideFragment.class, getBgRes(), getTitles());
//        ModelPagerAdapter adapter = new ModelPagerAdapter(getSupportFragmentManager(), manager);
//        viewPager.setAdapter(adapter);
//        viewPager.fixScrollSpeed();
//        springIndicator.setViewPager(viewPager);
//        handler = new Handler();

//        progressBar = (DottedProgressBar) findViewById(R.id.progress);
//        handler.postAtTime(runnable, 1000);

        DrawMeButton mMenuButton = (DrawMeButton)findViewById(R.id.menu_button);
        mMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SterilizationFinishActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        DrawMeButton mRecordButton = (DrawMeButton)findViewById(R.id.record_button);
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SterilizationFinishActivity.this, StartSterInformationActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
    }

    public void stopProgress(View view) {
        progressBar.stopProgress();
    }

    public void startProgress(View view) {
        progressBar.startProgress();
    }

    private List<String> getTitles(){
        return Lists.newArrayList("Pre-vacuum", "Heating", "Sterilization", "Exhaust", "Dry");
    }

    private List<Integer> getBgRes(){
        return Lists.newArrayList(R.drawable.stage_icon_prevacuum, R.drawable.stage_icon_heating, R.drawable.stage_icon_sterilization
                , R.drawable.stage_icon_exhaust, R.drawable.stage_icon_dry);
    }

    @Override
    public void onItemClick(int position) {
        this.startActivity(new Intent(SterilizationFinishActivity.this, StartSterInformationActivity.class));
        this.overridePendingTransition(0, 0);

    }

    private void setDummyData() {
        startSterInformationItemA = new ArrayList<>();
        startSterInformationItemA.add(new StartSterInformationItemA(R.drawable.toolbar_information, Constants.instance().fetchValueString(saveSelectMenuNameKey), "01:00:00"));

        recyclerViewButton2 = new ArrayList<>();
        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_pressure, "Pres.","0.00 bar"));
        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_temperature, "Temp","40.0 °C"));
    }

    private String getSterilizationTime() {
        String timeStr;
        calendar = Calendar.getInstance();
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        if(i_hour<10){
            hour = "0"+i_hour;
        }else{
            hour = ""+i_hour;
        }
        if(i_minute<10){
            minute = "0"+i_minute;
        }else{
            minute = ""+i_minute;
        }
        if(i_second<10){
            second = "0"+i_second;
        }else{
            second = ""+i_second;
        }
        if(i_month<10){
            month = "0"+i_month;
        }else{
            month = ""+i_month;
        }
        if(i_date<10){
            date = "0"+i_date;
        }else{
            date = ""+i_date;
        }
        //Log.i(TAG,"time changes："+date+" "+minute+"--"+second+"--"+new Date().getSeconds());
        hour = "00";
        timeStr = hour+":"+minute+":"+second+"\r\r"+calendar.get(Calendar.YEAR)+"."+month+"."+date;
        calendar.clear();
        calendar = null;
        return timeStr;
    }




}
