package com.sturdy.hmi.Dialog;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

import com.sturdy.hmi.R;

import java.util.ArrayList;
import java.util.List;



public class PayPasswordView extends View {
    private List<String> result;//保存輸入數據
    private int count;//密碼位數
    private int size;//默認每格大小
    private Paint mBorderPaint;//邊界畫筆
    private Paint mDotPaint;//小黑點畫筆
    private int mBorderColor;//邊界顏色
    private int mDotColor;//小黑點顏色
    private RectF mRoundRect;//圓角背景
    private int mRoundRadius;//圓角半徑

    private PayKeyboardView mPayKeyboardView;//數字鍵盤
    private InputCallBack mInputCallBack;//完全輸入的回調
    private Context context;
    public interface InputCallBack {
        void onInputFinsh(String result);
    }

    public PayPasswordView(Context context) {
        super(context);
        init(null,context);
    }

    public PayPasswordView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs,context);
    }

    public PayPasswordView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs,context);
    }

    private void init(AttributeSet attrs,Context context) {
        float dp = getResources().getDisplayMetrics().density;
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        context = context;
        result = new ArrayList<>();
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PayPasswordView);
            mBorderColor = typedArray.getColor(R.styleable.PayPasswordView_border_color, Color.LTGRAY);
            mDotColor = typedArray.getColor(R.styleable.PayPasswordView_dot_color, Color.BLACK);
            count = typedArray.getInt(R.styleable.PayPasswordView_count, 6);
            typedArray.recycle();
        } else {
            mBorderColor = Color.LTGRAY;
            mDotColor = Color.GRAY;
            count = 6;
        }
        size = (int) (dp * 30);//默認每格30dp
        mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBorderPaint.setStrokeWidth(3);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setColor(mBorderColor);

        mDotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDotPaint.setStrokeWidth(3);
        mDotPaint.setStyle(Paint.Style.FILL);
        mDotPaint.setColor(mDotColor);
        mRoundRect = new RectF();
        mRoundRadius = (int) (5 * dp);
        hideKeyboardFrom(context,this);
    }

    public static void hideKeyboardFrom(Context context, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = measureWidth(widthMeasureSpec);
        int h = measureHeight(heightMeasureSpec);
        int wSize = MeasureSpec.getSize(widthMeasureSpec);
        int hSize = MeasureSpec.getSize(heightMeasureSpec);
        if (w == -1) {
            if (h != -1) {
                w = h * count;
            } else {
                w = size * count;
                h = size;
            }
        } else {
            if (h == -1) {
                h = w / count;
                size = h;
            }
        }
        setMeasuredDimension(Math.min(w, wSize), Math.min(h, hSize));
    }

    private int measureWidth(int withMeasureSpec) {
        //寬度
        int wMode = MeasureSpec.getMode(withMeasureSpec);
        int wSize = MeasureSpec.getSize(withMeasureSpec);
        if (wMode == MeasureSpec.AT_MOST) {//wrap_content
            return -1;
        }
        return wSize;
    }

    private int measureHeight(int heightMeasureSpec) {
        //高度
        int hMode = MeasureSpec.getMode(heightMeasureSpec);
        int hSize = MeasureSpec.getSize(heightMeasureSpec);
        if (hMode == MeasureSpec.AT_MOST) {
            return -1;
        }
        return hSize;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            requestFocus();
            mPayKeyboardView.setVisibility(VISIBLE);
            return true;
        }
        return true;
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            mPayKeyboardView.setVisibility(VISIBLE);
        } else {
            mPayKeyboardView.setVisibility(GONE);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        final int width = getWidth() - 1;
        final int height = getHeight() - 1;
        mRoundRect.set(0, 0, width, height);
        canvas.drawRoundRect(mRoundRect, 0, 0, mBorderPaint);
        //分割線
        for (int i = 1; i < count; i++) {
            final int x = i * size;
            canvas.drawLine(x, 0, x, height, mBorderPaint);
        }
        //小黑點
        int dotRadius = size / 8;
        for (int i = 0; i < result.size(); i++) {
            final float x = (float) (size * (i + 0.5));
            final float y = size / 2;
            canvas.drawCircle(x, y, dotRadius, mDotPaint);
        }
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        outAttrs.inputType = InputType.TYPE_CLASS_NUMBER;//輸入類型為數字
        outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE;
        return new MInputConnection(this, false);
    }

    public void setInputCallBack(InputCallBack inputCallBack) {
        this.mInputCallBack = inputCallBack;
    }

    public void clearResult() {
        result.clear();
        invalidate();
    }

    private class MInputConnection extends BaseInputConnection {

        public MInputConnection(View targetView, boolean fullEditor) {
            super(targetView, fullEditor);
        }

        @Override
        public boolean commitText(CharSequence text, int newCursorPosition) {
            //這裡是接受輸入法的文本的，我們只處理數字，所以什麼操作都不做
            return super.commitText(text, newCursorPosition);
        }

        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            //軟鍵盤刪除鍵
            if (beforeLength == 1 && afterLength == 0) {
                return super.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
                        && super.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));
            }
            return super.deleteSurroundingText(beforeLength, afterLength);
        }
    }

    public void setInputMethodView(PayKeyboardView inputMethodView) {
        this.mPayKeyboardView = inputMethodView;
        this.mPayKeyboardView.setKeyboardReceiver(new PayKeyboardView.KeyboardReceiver() {
            @Override
            public void onReveive(String number) {
                if (number.equals("-1")) {
                    if (!result.isEmpty()) {
                        result.remove(result.size() - 1);
                        invalidate();
                    }
                } else {
                    if (result.size() < count) {
                        result.add(number);
                        invalidate();
                        ensureFinishInput();
                    }
                }
            }
        });
    }

    /**
     * 判斷是否輸入完成
     */
    public void ensureFinishInput() {
        if (result.size() == count && mInputCallBack != null) {
            StringBuilder sb = new StringBuilder();
            for (String i : result) {
                sb.append(i);
            }
            mInputCallBack.onInputFinsh(sb.toString());
        }
    }

    /**
     * 獲取輸入文字
     */
    public String getInputText() {
        if (result.size() == count) {
            StringBuilder sb = new StringBuilder();
            for (String i : result) {
                sb.append(i);
            }
            return sb.toString();
        }
        return null;
    }
}
