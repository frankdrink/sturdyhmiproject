package com.sturdy.chart.core;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseBarLineChart;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.provider.barLine.Bar3DProvider;

/**
 * 3DBar圖表
 */

public class Bar3DChart extends BaseBarLineChart<Bar3DProvider,BarData> {

    public Bar3DChart(Context context) {
        super(context);
    }

    public Bar3DChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Bar3DChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }





    @Override
    protected Bar3DProvider initProvider() {
        return  new Bar3DProvider();
    }



}
