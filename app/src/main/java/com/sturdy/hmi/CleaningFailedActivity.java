package com.sturdy.hmi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.sturdy.hmi.adapter.CleaningFailedGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class CleaningFailedActivity extends BaseToolBarActivity implements StartSterGridViewAdapter1.OnItemClickListener,StartSterGridViewAdapter2.OnItemClickListener{
    Handler handler;
    Runnable run;
    private RecyclerView gridView_1, gridView_2;
    private StartSterGrid2Adapter mAdapter;
    private List<StartSterGridRecycler> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private CleaningFailedGridViewAdapter1 gridViewAdapter1;
    private StartSterGridViewAdapter2 gridViewAdapter2;
    private ImageView fragmentBackgroundImage;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<StartSterInformationItemA> startSterInformationItemA;
    private ArrayList<RecyclerViewItem> recyclerViewButton2;
    TextView progressBarTitleView, programBarTitleView, stepTitleView;
    ScrollerViewPager viewPager;
    DottedProgressBar progressBar;
    private int progressBarNumberOfDots;
    private int progressBarIndex = 0;

    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;
    private int iDelay = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_cleaningfailed);
        Constants.instance(this.getApplicationContext());
        GlobalClass.getInstance().MachineState = Constants.MACHINE_STATE.STERI_FAIL;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        mTopBar.setBackgroundColor( this.getResources().getColor(R.color.colorTitlecCompleteFailedColor));
        fragmentBackgroundImage = (ImageView) findViewById(R.id.fragment_bgview);
        fragmentBackgroundImage.setImageResource(R.drawable.stage_bg_orange);
        stepTitleView = (TextView) findViewById(R.id.step_title_view);
        programBarTitleView = (TextView) findViewById(R.id.program_title_view);
        programBarTitleView.setText("Stop operation");
        progressBarTitleView = (TextView) findViewById(R.id.progress_title_view);
        progressBarTitleView.setText("Wait...");
        setDummyData();
        gridView_1 = (RecyclerView) findViewById(R.id.information_grid_1);
        gridView_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        gridView_1.setHasFixedSize(true);
        GridLayoutManager layoutManager1 = new GridLayoutManager(this, 1);
        gridView_1.setLayoutManager(layoutManager1);
        gridViewAdapter1 = new CleaningFailedGridViewAdapter1(this, startSterInformationItemA);
        gridView_1.setAdapter(gridViewAdapter1);


        gridView_2 = (RecyclerView)findViewById(R.id.information_grid_2);
        gridView_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        gridView_2.setLayoutManager(mLayoutManager);
        gridView_2.setItemAnimator(new DefaultItemAnimator());
        gridView_2.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();

        mItems.add(0, new StartSterGridRecycler("Temp.", "°C", "000.0", R.drawable.parameter_icon_temp,true));
        mItems.add(1, new StartSterGridRecycler("Pres.", "kgf/cm", "-0.000", R.drawable.parameter_icon_pressure,false));

        mAdapter = new StartSterGrid2Adapter(this, mItems);
        gridView_2.setAdapter(mAdapter);

//        gridView_2 = (RecyclerView) findViewById(R.id.information_grid_2);
//        gridView_2.setHasFixedSize(true);
//        GridLayoutManager layoutManager2 = new GridLayoutManager(this, 2);
//        gridView_2.setLayoutManager(layoutManager2);
//        gridViewAdapter2 = new StartSterGridViewAdapter2(this, recyclerViewButton2, recyclerViewButton2);
//        gridView_2.setAdapter(gridViewAdapter2);


        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
//        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
        progressBarNumberOfDots = getBgRes().size()-1;
        PagerModelManager manager = new PagerModelManager();
        manager.addCommonFragment(GuideFragment.class, getBgRes(), getTitles());
        ModelPagerAdapter adapter = new ModelPagerAdapter(getSupportFragmentManager(), manager);
        viewPager.setAdapter(adapter);
        viewPager.fixScrollSpeed();

        registerBaseActivityReceiver();
    }


    public void stopProgress(View view) {
        progressBar.stopProgress();
    }

    public void startProgress(View view) {
        progressBar.startProgress();
    }

    private List<String> getTitles(){
        return Lists.newArrayList("Failed");
    }

    private List<Integer> getBgRes(){
        return Lists.newArrayList(R.drawable.compile_failed);
    }

    @Override
    public void onItemClick(int position) {
        if ((Constants.instance().fetchValueInt(preIDControl))==1) {
            Constants.instance().storeValueString(userLoginCheckPoint, "PostID");
            this.startActivity(new Intent(CleaningFailedActivity.this, UserLoginActivity.class));
        }else {
            this.startActivity(new Intent(CleaningFailedActivity.this, DoorOpenActivity.class));
        }
    }

    private void setDummyData() {
        startSterInformationItemA = new ArrayList<>();
        startSterInformationItemA.add(new StartSterInformationItemA(R.drawable.toolbar_information, "Estimated time", "00:00:00"));

//        recyclerViewButton2 = new ArrayList<>();
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_pressure, "Pres.","0.00 bar"));
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_temperature, "Temp","40.0 °C"));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent;
        char[] data;
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if ((Constants.instance().fetchValueInt(postIDControl))==1) {
                    Constants.instance().storeValueString(userLoginCheckPoint, "PostID");
                    this.startActivity(new Intent(CleaningFailedActivity.this, UserLoginActivity.class));
                }else {
                    goDoorOpenActvity();
                }
                break;
        }
        return false;
    }

    void goDoorOpenActvity() {
        char[] data = new char[]{0x01, 0x60};
        sendToService(data);
//        this.startActivity(new Intent(mActivity, DoorOpenActivity.class));
//        finish();
    }

    private String getSterilizationTime() {
        String timeStr;
        calendar = Calendar.getInstance();
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        if(i_hour<10){
            hour = "0"+i_hour;
        }else{
            hour = ""+i_hour;
        }
        if(i_minute<10){
            minute = "0"+i_minute;
        }else{
            minute = ""+i_minute;
        }
        if(i_second<10){
            second = "0"+i_second;
        }else{
            second = ""+i_second;
        }
        if(i_month<10){
            month = "0"+i_month;
        }else{
            month = ""+i_month;
        }
        if(i_date<10){
            date = "0"+i_date;
        }else{
            date = ""+i_date;
        }
        hour = "00";
        timeStr = hour+":"+minute+":"+second+"\r\r"+calendar.get(Calendar.YEAR)+"."+month+"."+date;
        calendar.clear();
        calendar = null;
        return timeStr;
    }

    @Override
    public void onPause() {
        super.onPause();
//        handler.removeCallbacks(runnable);
        overridePendingTransition(0, 0);
    }
}
