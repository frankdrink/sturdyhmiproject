package com.sturdy.chart.component.axis;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.Gravity;

import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.data.ScaleData;
import com.sturdy.chart.exception.ChartException;

import java.util.List;

/**
 * 圖表橫軸
 */

public class HorizontalAxis extends BaseAxis<String> {

    /**
     * 橫軸默認構造方法
     * 默認方位在底部
     */
    public HorizontalAxis() {
        direction = AxisDirection.BOTTOM;
    }

    /**
     * 文字旋轉角度
     */
    private int rotateAngle= 0;
    /**
     * 文字是否旋轉
     */
    private boolean isRotateAngle;
    /**
     * 文字寬度
     */
    private int textWidth;
    /**
     * 文字高度
     */
    private int textHeight;
    /**
     * 旋轉文字高度
     */
    private int rotateTextHeight;
    /**
     * 是否需要偏移來完整顯示左右兩邊文字
     */
    private boolean isShowFullValue=true; //是否顯示全文字
    /**
     * 刻度數據
     */
    private ScaleData scaleData;

    /**
     * 計算刻度大小
     * <p>通過計算刻度的寬高得到軸的大小，然後保存到scaleData對像中，以便後面的計算</p>
     * @param chartData 圖表數據
     * @param rect 圖表原始範圍
     * @param paint 畫筆
     */
    @Override
    public void computeScale(ChartData<? extends BarData> chartData, Rect rect, Paint paint) {
        if(isDisplay()) {
            scaleData = chartData.getScaleData();
            scaleStyle.fillPaint(paint);
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            textHeight = (int) (fontMetrics.descent - fontMetrics.ascent);
            int maxLength = 0;
            String maxLengthXData = "1";
            for (String xData : chartData.getCharXDataList()) {
                String formatData = formatData(xData);
                if (maxLength < formatData.length()) {
                    maxLengthXData = formatData;
                    maxLength = formatData.length();
                }
            }
            textWidth = (int) paint.measureText(maxLengthXData);
            //計算旋轉的高寬
            int dis = textHeight;
            if (isRotateAngle) {
                int tempHeight = (int) Math.abs(textWidth * Math.sin(rotateAngle * Math.PI / 180)
                        + textHeight * Math.cos(rotateAngle * Math.PI / 180));
                int tempWidth = (int) Math.abs(textWidth * Math.cos(rotateAngle * Math.PI / 180)
                        + textHeight * Math.sin(rotateAngle * Math.PI / 180));
                rotateTextHeight = tempHeight;
                dis += rotateTextHeight;
                textWidth = tempWidth;
            }
            dis += (int) (scaleStyle.getPadding() * 2 + axisStyle.getWidth());
            if (direction == AxisDirection.BOTTOM) {
                scaleData.scaleRect.bottom = dis;
            } else {
                scaleData.scaleRect.top = dis;
            }
        }
    }

    /**
     * 繪製刻度
     * <p>通過zoomRect計算出每個刻度的寬度，迭代繪製刻度</p>
     * @param canvas 畫布
     * @param zoomRect 縮放之後範圍
     * @param rect  原始範圍
     * @param paint  畫筆
     * @param chartData 圖表數據
     */
    protected void drawScale(Canvas canvas, Rect zoomRect, Rect rect, Paint paint,  ChartData<? extends BarData> chartData) {

        ScaleData scaleData = chartData.getScaleData();
        List<String> groupDataList = chartData.getCharXDataList();
        int rowSize = scaleData.rowSize;
        int groupSize = groupDataList.size();
        if (groupSize != rowSize) {
            throw new ChartException("Horizontal Vertical axis data inconsistency");
        }
        float startY;
        if (direction == AxisDirection.BOTTOM) {
            startY = zoomRect.bottom -scaleData.scaleRect.bottom/2;
        } else {
            startY = zoomRect.top + scaleData.scaleRect.top/2;
        }
        int left = zoomRect.left ;
        int width = zoomRect.right - left;
        double perWidth = 30;//((double) width) / (isLine? groupSize -1 : groupSize);
        int filterMultiple = (int) (textWidth / perWidth +1);
        for (int i = 0; i < groupSize; i++) {
            String content = groupDataList.get(i);
            int startX = getGravityStartX(left, i, perWidth);
            //留1px緩衝
            if (startX >= rect.left-1 && startX<= rect.right+1) {
                if( i % filterMultiple == 0) {
                    drawText(canvas, content,startX, startY-15,i, paint);
                    drawGrid(canvas, startX, rect, scaleData.scaleRect, paint);
                }
            }
        }
        drawText(canvas, "(10min)",rect.right+35, startY-10,groupSize, paint);

    }

    /**
     * 獲取刻度起始X的位置
     * <p>根據gravity來判斷偏移的值</p>
     * @param left 左邊
     * @param position 位置
     * @param perWidth 每個刻度的寬度
     * @return 刻度起始X的位置
     */
    private int getGravityStartX(int left, int position, double perWidth) {
        int startX = (int) (left + position * perWidth);
        if (gravity == Gravity.CENTER) {
            startX += perWidth / 2;
        } else if (gravity == Gravity.RIGHT) {
            startX += perWidth;
        }
        return startX;
    }

    /**
     * 繪製文字
     * <p>完成文字偏移，文字旋轉，繪製</p>
     * @param canvas 畫布
     * @param contentStr 文字內容
     * @param startX 文字繪製起始X位置
     * @param startY 文字繪製起始Y位置
     * @param position 刻度序號
     * @param paint 畫筆
     */
    private void drawText(Canvas canvas, String contentStr, int startX, float startY, int position, Paint paint) {
        String content = formatData(contentStr);
        scaleStyle.fillPaint(paint);
        paint.setTextAlign(Paint.Align.CENTER);
        if (isShowFullValue && position == 0) {
            int width = (int) paint.measureText(content);
            startX+= width/2;
        } else if (isShowFullValue && position == scaleData.rowSize - 1) {
            int width = (int) paint.measureText(content);
            startX-= width/2;
        }
        if (isRotateAngle) {
            canvas.save();
            canvas.rotate(rotateAngle, startX, startY);
            canvas.drawText(content, startX, startY + textHeight / 2, paint);
            canvas.restore();
        } else {
            canvas.drawText(content, startX, startY + textHeight / 2, paint);
        }
    }

    /**
     * 格式化文字
     * @param data 文字
     * @return 格式化完成之後的文字
     */
    private String formatData(String data){
        return  getFormat()!= null ? getFormat().format(data) :data;
    }

    /**
     * 繪製豎向網格
     * @param canvas 畫布
     * @param startX 網格起始X位置
     * @param rect 原始範圍
     * @param scaleRect 縮放範圍
     * @param paint 畫布
     */
    public void drawGrid(Canvas canvas, float startX, Rect rect, Rect scaleRect, Paint paint) {
        if (gridStyle != null && isDrawGrid) {
            gridStyle.fillPaint(paint);
            Path path = new Path();
            path.moveTo(startX, rect.top + scaleRect.top);
            path.lineTo(startX, rect.bottom - scaleRect.bottom);
            canvas.drawPath(path, paint);
        }
    }


    /**
     * 計算出裁切軸之後的範圍
     * @param rect 原始範圍
     * @param scaleRect 縮放範圍
     * @return 上下左右的大小
     */
    protected int[] calculation(Rect rect, Rect scaleRect) {
        int startX = rect.left + scaleRect.left;
        int endX = rect.right - scaleRect.right;
        int startY, endY;
        if (direction == AxisDirection.BOTTOM) {
            startY = rect.bottom - scaleRect.bottom;
        } else {
            startY = rect.top + scaleRect.top;
        }
        endY = startY;
        return new int[]{startX, startY, endX, endY};
    }

    /**
     * 設置軸方位
     * <p>橫軸只能設置上下方位</p>
     * @param axisDirection 軸方位
     *
     */
    @Override
    public void setAxisDirection(int axisDirection) {
        if (axisDirection == AxisDirection.BOTTOM || axisDirection == AxisDirection.TOP) {
            this.direction = axisDirection;
        } else throw new ChartException("Can only set BOTTOM, TOP direction");
    }


    /**
     * 設置文字旋轉角度
     * @param rotateAngle
     */
    public void setRotateAngle(int rotateAngle) {
        isRotateAngle = true;
        this.rotateAngle = rotateAngle;
    }

    /**
     * 是否需要偏移來完整顯示左右兩邊文字
     *
     * @return 是否需要偏移
     */
    public boolean isShowFullValue() {
        return isShowFullValue;
    }
    /**
     * 設置是否需要偏移來完整顯示左右兩邊文字
     *
     * @param  showFullValue 設置是否需要偏移
     */
    public void setShowFullValue(boolean showFullValue) {
        isShowFullValue = showFullValue;
    }

}
