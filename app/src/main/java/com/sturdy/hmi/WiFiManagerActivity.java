package com.sturdy.hmi;
import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kongqw.permissionslibrary.PermissionsManager;
import com.sturdy.widget.SwitchButton;
import com.sturdy.wifilibrary.WiFiManager;
import com.sturdy.wifilibrary.listener.OnWifiConnectListener;
import com.sturdy.wifilibrary.listener.OnWifiEnabledListener;
import com.sturdy.wifilibrary.listener.OnWifiScanResultsListener;

import java.util.List;

import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.adapter.WifiListAdapter;
import com.sturdy.hmi.view.ConnectWifiDialog;
import com.stx.xhb.commontitlebar.CustomTitleBar;

/**
 * Created by franklin on 2019/10/31.
 */


public class WiFiManagerActivity extends BaseToolBarActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, OnWifiScanResultsListener, OnWifiConnectListener, OnWifiEnabledListener {

    private static final String TAG = "WiFi";

    private ListView mWifiList;
    private SwipeRefreshLayout mSwipeLayout;
    private PermissionsManager mPermissionsManager;
    boolean switchFlag = false;
    // 所需的全部權限
    static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private final int GET_WIFI_LIST_REQUEST_CODE = 0;
    private WifiListAdapter mWifiListAdapter;
    private SwitchButton switchCompat;
    private FrameLayout frameLayout;
    String[] web = {
            "Admin"
    } ;
    Integer[] imageId = {
            R.drawable.setting_icon_wi_fi_lock_4
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_wifimanager);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Wi-Fi", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        // 加載View
        initView();
        // 添加WIFI開關的監聽
//        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    GlobalClass.getInstance().mWiFiManager.openWiFi();
//                } else {
//                    GlobalClass.getInstance().mWiFiManager.closeWiFi();
//                }
//            }
//        });
        // 添加下拉刷新的監聽
        mSwipeLayout.setOnRefreshListener(this);
        // 初始化WIFI列表
        mWifiList.setEmptyView(findViewById(R.id.empty_view));

        SteriMenuList listAdapter = new
                SteriMenuList(this, web, imageId);
        mWifiListAdapter = new WifiListAdapter(getApplicationContext());
        mWifiList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mWifiList.setAdapter(mWifiListAdapter);
        mWifiList.setOnItemClickListener(this);
        mWifiList.setOnItemLongClickListener(this);
        // WIFI管理器
        GlobalClass.getInstance().mWiFiManager = WiFiManager.getInstance(getApplicationContext());
        // 動態權限管理器
        mPermissionsManager = new PermissionsManager(this) {
            @Override
            public void authorized(int requestCode) {
                // 6.0 以上系統授權通過
                if (GET_WIFI_LIST_REQUEST_CODE == requestCode) {
                    // 獲取WIFI列表
                    List<ScanResult> scanResults =  GlobalClass.getInstance().mWiFiManager.getScanResults();
                    refreshData(scanResults);
                }
            }

            @Override
            public void noAuthorization(int requestCode, String[] lacksPermissions) {
                // 6.0 以上系統授權失敗
            }

            @Override
            public void ignore() {
                // 6.0 以下系統 獲取WIFI列表
                List<ScanResult> scanResults =  GlobalClass.getInstance().mWiFiManager.getScanResults();
                refreshData(scanResults);
            }
        };
        // 請求WIFI列表
        mPermissionsManager.checkPermissions(GET_WIFI_LIST_REQUEST_CODE, PERMISSIONS);
    }

    /**
     * 初始化界面
     */
    private void initView() {
        // WIFI 開關
        switchCompat = (SwitchButton) findViewById(R.id.switch_wifi);
        // 顯示WIFI信息的佈局
        frameLayout = (FrameLayout) findViewById(R.id.fl_wifi);
        // 下拉刷新
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        // WIFI列表
        mWifiList = (ListView) findViewById(R.id.wifi_list);


        RelativeLayout relativeclic1 =(RelativeLayout)findViewById(R.id.wifiswich_rl);
        relativeclic1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                switchFlag = !switchFlag;
                switchCompat.setChecked(switchFlag);
                if (switchFlag) {
                     GlobalClass.getInstance().mWiFiManager.openWiFi();
                }else{
                    GlobalClass.getInstance().mWiFiManager.closeWiFi();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 添加監聽
        GlobalClass.getInstance().mWiFiManager.setOnWifiEnabledListener(this);
        GlobalClass.getInstance().mWiFiManager.setOnWifiScanResultsListener(this);
        GlobalClass.getInstance().mWiFiManager.setOnWifiConnectListener(this);
        // 更新WIFI開關狀態
        switchCompat.setChecked( GlobalClass.getInstance().mWiFiManager.isWifiEnabled());
    }

    @Override
    protected void onPause() {
        super.onPause();
        // 移除監聽
        GlobalClass.getInstance().mWiFiManager.removeOnWifiEnabledListener();
        GlobalClass.getInstance().mWiFiManager.removeOnWifiScanResultsListener();
        GlobalClass.getInstance().mWiFiManager.removeOnWifiConnectListener();
    }

    /**
     * 刷新頁面
     *
     * @param scanResults WIFI數據
     */
    public void refreshData(List<ScanResult> scanResults) {
        mSwipeLayout.setRefreshing(false);
        // 刷新界面
        mWifiListAdapter.refreshData(scanResults);

//        Snackbar.make(mWifiList, "WIFI列表刷新成功", Snackbar.LENGTH_SHORT).show();
    }

    /**
     * Android 6.0 權限校驗
     *
     * @param requestCode  requestCode
     * @param permissions  permissions
     * @param grantResults grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // 複查權限
        mPermissionsManager.recheckPermissions(requestCode, permissions, grantResults);
    }

    /**
     * WIFI列表單擊
     *
     * @param parent   parent
     * @param view     view
     * @param position position
     * @param id       id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final ScanResult scanResult = (ScanResult) mWifiListAdapter.getItem(position);
        switch ( GlobalClass.getInstance().mWiFiManager.getSecurityMode(scanResult)) {
            case WPA:
            case WPA2:
                new ConnectWifiDialog(this) {

                    @Override
                    public void connect(String password) {
                        GlobalClass.getInstance().mWiFiManager.connectWPA2Network(scanResult.SSID, password);
                    }
                }.setSsid(scanResult.SSID).show();
                break;
            case WEP:
                new ConnectWifiDialog(this) {

                    @Override
                    public void connect(String password) {
                        GlobalClass.getInstance().mWiFiManager.connectWEPNetwork(scanResult.SSID, password);
                    }
                }.setSsid(scanResult.SSID).show();
                break;
            case OPEN: // 開放網絡
                GlobalClass.getInstance().mWiFiManager.connectOpenNetwork(scanResult.SSID);
                break;
        }
    }

    /**
     * WIFI列表長按
     *
     * @param parent   parent
     * @param view     view
     * @param position position
     * @param id       id
     * @return 是否攔截長按事件
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ScanResult scanResult = (ScanResult) mWifiListAdapter.getItem(position);
        final String ssid = scanResult.SSID;
        new AlertDialog.Builder(this)
                .setTitle(ssid)
                .setItems(new String[]{"斷開連接", "刪除網絡配置"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // 斷開連接
                                WifiInfo connectionInfo =  GlobalClass.getInstance().mWiFiManager.getConnectionInfo();
                                Log.i(TAG, "onClick: connectionInfo :" + connectionInfo.getSSID());
                                if ( GlobalClass.getInstance().mWiFiManager.addDoubleQuotation(ssid).equals(connectionInfo.getSSID())) {
                                    GlobalClass.getInstance().mWiFiManager.disconnectWifi(connectionInfo.getNetworkId());
                                } else {
                                    Toast.makeText(getApplicationContext(), "當前沒有連接 [ " + ssid + " ]", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1: // 刪除網絡配置
                                WifiConfiguration wifiConfiguration =  GlobalClass.getInstance().mWiFiManager.getConfigFromConfiguredNetworksBySsid(ssid);
                                if (null != wifiConfiguration) {
                                    boolean isDelete =  GlobalClass.getInstance().mWiFiManager.deleteConfig(wifiConfiguration.networkId);
                                    Toast.makeText(getApplicationContext(), isDelete ? "刪除成功！" : "其他應用配置的網絡沒有ROOT權限不能刪除！", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "沒有保存該網絡！", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();
        return true;
    }


    /**
     * 下拉刷新的回調
     */
    @Override
    public void onRefresh() {
        // 下拉刷新
        GlobalClass.getInstance().mWiFiManager.startScan();
    }

    /**
     * WIFI列表刷新後的回調
     *
     * @param scanResults 掃瞄結果
     */
    @Override
    public void onScanResults(List<ScanResult> scanResults) {

        refreshData(scanResults);
    }

    /**
     * WIFI連接的Log得回調
     *
     * @param log log
     */
    @Override
    public void onWiFiConnectLog(String log) {
        Log.i(TAG, "onWiFiConnectLog: " + log);
        Snackbar.make(mWifiList, "WIFI正在連接 : " + log, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * WIFI連接成功的回調
     *
     * @param SSID 熱點名
     */
    @Override
    public void onWiFiConnectSuccess(String SSID) {
        Log.i(TAG, "onWiFiConnectSuccess:  [ " + SSID + " ] 連接成功");
        Toast.makeText(getApplicationContext(), SSID + "  連接成功", Toast.LENGTH_SHORT).show();
    }

    /**
     * WIFI連接失敗的回調
     *
     * @param SSID 熱點名
     */
    @Override
    public void onWiFiConnectFailure(String SSID) {
        Log.i(TAG, "onWiFiConnectFailure:  [ " + SSID + " ] 連接失敗");
        Toast.makeText(getApplicationContext(), SSID + "  連接失敗！請重新連接！", Toast.LENGTH_SHORT).show();
    }

    /**
     * WIFI開關狀態的回調
     *
     * @param enabled true 可用 false 不可用
     */
    @Override
    public void onWifiEnabled(boolean enabled) {
        switchCompat.setChecked(enabled);
        frameLayout.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }
}
