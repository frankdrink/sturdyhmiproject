package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.RecyclerViewDisabler;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.sturdy.hmi.view.DashedLineView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
//import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;

public class LeakageTestCompleteSterilizationActivity extends BaseToolBarActivity implements StartSterGridViewAdapter2.OnItemClickListener{
    Handler handler;
    Runnable run;
    private RecyclerView gridView_1, gridView_2;
    private StartSterGrid2Adapter mAdapter;
    private List<StartSterGridRecycler> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
//    private StartSterGridViewAdapter1 gridViewAdapter1;
    private StartSterGridViewAdapter2 gridViewAdapter2;
    private ImageView fragmentBackgroundImage;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<StartSterInformationItemA> startSterInformationItemA;
    private ArrayList<RecyclerViewItem> recyclerViewButton2;
    private TableLayout tableLayout;
    TextView progressBarTitleView, programBarTitleView;
    ScrollerViewPager viewPager;
    DottedProgressBar progressBar;
    private int progressBarNumberOfDots;
    private int progressBarIndex = 0;

    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;
    private int iDelay = 5;
    private String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_leakagetestcompletesterilization);
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        startShowTitleClock(rightTimeButton);
        mActivity = this;
        mTopBar.setBackgroundColor( this.getResources().getColor(R.color.colorTitlecCompleteColor));
        fragmentBackgroundImage = (ImageView) findViewById(R.id.fragment_bgview);
        fragmentBackgroundImage.setImageResource(R.drawable.stage_bg_green);
        programBarTitleView = (TextView) findViewById(R.id.program_title_view);
        programBarTitleView.setText(Constants.instance().fetchValueString(saveSelectMenuNameKey));
        progressBarTitleView = (TextView) findViewById(R.id.progress_title_view);
        if (GlobalClass.getInstance().CB_SteriSuccess) {
            progressBarTitleView.setText("Complete");
        }else{
            progressBarTitleView.setText("Failed");
        }
        setDummyData();
//        gridView_1 = (RecyclerView) findViewById(R.id.information_grid_1);
//        gridView_1.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return true;
//            }
//        });
//        gridView_1.setHasFixedSize(true);
//        GridLayoutManager layoutManager1 = new GridLayoutManager(this, 1);
//        gridView_1.setLayoutManager(layoutManager1);
//        gridViewAdapter1 = new StartSterGridViewAdapter1(this, startSterInformationItemA);
//        gridViewAdapter1.setOnItemClickListener(this);
//        gridView_1.setAdapter(gridViewAdapter1);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout1);
        int textSize = 34;

        DashedLineView div_v = new DashedLineView(this);
        div_v.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                1
        ));

        TableRow row1= new TableRow(this);
        TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        row1.setLayoutParams(lp1);
        TextView tv1_1 = new TextView(this);
        tv1_1.setGravity(Gravity.LEFT);
        tv1_1.setTextSize(textSize);
        tv1_1.setText("P0:");
        TextView tv1_2 = new TextView(this);
        tv1_2.setGravity(Gravity.RIGHT);
        tv1_2.setTextSize(textSize);
        tv1_2.setText(UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageBar[0]));
        TextView tv1_3 = new TextView(this);
        tv1_3.setGravity(Gravity.LEFT);
        tv1_3.setTextSize(textSize);
        tv1_3.setText("    t0:");
        TextView tv1_4 = new TextView(this);
        tv1_4.setGravity(Gravity.RIGHT);
        tv1_4.setTextSize(textSize);
        tv1_4.setText(GlobalClass.getInstance().CB_LeakageTime[0]+"s");
        row1.addView(tv1_1);
        row1.addView(tv1_2);
        row1.addView(tv1_3);
        row1.addView(tv1_4);
        tableLayout.addView(row1,new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT));

        TableRow row2= new TableRow(this);
        TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        row1.setLayoutParams(lp2);
        TextView tv2_1 = new TextView(this);
        tv2_1.setGravity(Gravity.LEFT);
        tv2_1.setTextSize(textSize);
        tv2_1.setText("P1:");
        TextView tv2_2 = new TextView(this);
        tv2_2.setGravity(Gravity.RIGHT);
        tv2_2.setTextSize(textSize);
        tv2_2.setText(UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageBar[1]));
        TextView tv2_3 = new TextView(this);
        tv2_3.setGravity(Gravity.LEFT);
        tv2_3.setTextSize(textSize);
        tv2_3.setText("    t1:");
        TextView tv2_4 = new TextView(this);
        tv2_4.setGravity(Gravity.RIGHT);
        tv2_4.setTextSize(textSize);
        tv2_4.setText(GlobalClass.getInstance().CB_LeakageTime[1]+"s");
        row2.addView(tv2_1);
        row2.addView(tv2_2);
        row2.addView(tv2_3);
        row2.addView(tv2_4);
        tableLayout.addView(row2,new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT));

        TableRow row3= new TableRow(this);
        TableRow.LayoutParams lp3 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        row3.setLayoutParams(lp3);
        TextView tv3_1 = new TextView(this);
        tv3_1.setGravity(Gravity.LEFT);
        tv3_1.setTextSize(textSize);
        tv3_1.setText("P2:");
        TextView tv3_2 = new TextView(this);
        tv3_2.setGravity(Gravity.RIGHT);
        tv3_2.setTextSize(textSize);
        tv3_2.setText(UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageBar[2]));
        TextView tv3_3 = new TextView(this);
        tv3_3.setGravity(Gravity.LEFT);
        tv3_3.setTextSize(textSize);
        tv3_3.setText("    t2:");
        TextView tv3_4 = new TextView(this);
        tv3_4.setGravity(Gravity.RIGHT);
        tv3_4.setTextSize(textSize);
        tv3_4.setText(GlobalClass.getInstance().CB_LeakageTime[2]+"s");
        row3.addView(tv3_1);
        row3.addView(tv3_2);
        row3.addView(tv3_3);
        row3.addView(tv3_4);
        tableLayout.addView(row3,new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT));

        TableRow row4= new TableRow(this);
        TableRow.LayoutParams lp4 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        row1.setLayoutParams(lp4);
        TextView tv4_1 = new TextView(this);
        tv4_1.setGravity(Gravity.LEFT);
        tv4_1.setTextSize(textSize);
        tv4_1.setText("P3:");
        TextView tv4_2 = new TextView(this);
        tv4_2.setGravity(Gravity.RIGHT);
        tv4_2.setTextSize(textSize);
        tv4_2.setText(UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageBar[3]));
        TextView tv4_3 = new TextView(this);
        tv4_3.setGravity(Gravity.LEFT);
        tv4_3.setTextSize(textSize);
        tv4_3.setText("    t3:");
        TextView tv4_4 = new TextView(this);
        tv4_4.setGravity(Gravity.RIGHT);
        tv4_4.setTextSize(textSize);
        tv4_4.setText(GlobalClass.getInstance().CB_LeakageTime[3]+"s");
        row4.addView(tv4_1);
        row4.addView(tv4_2);
        row4.addView(tv4_3);
        row4.addView(tv4_4);
        tableLayout.addView(row4,new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT));

        tableLayout.addView(div_v);

        TableRow rate_row= new TableRow(this);
        TableRow.LayoutParams rate_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        rate_lp.span = 4;
        rate_lp.weight = 1;
        rate_row.setLayoutParams(rate_lp);
        TextView rate_tv = new TextView(this);
        rate_tv.setLayoutParams(rate_lp);
        rate_tv.setGravity(Gravity.LEFT);
        rate_tv.setTextSize(textSize);
        str = String.format("Leakage Rate:%s/min",UnitConvert.sPresConvert(1,GlobalClass.getInstance().CB_LeakageRate));
        rate_tv.setText(str);
        rate_row.addView(rate_tv);
        tableLayout.addView(rate_row);

        TableRow pass_row= new TableRow(this);
        TableRow.LayoutParams pass_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        pass_lp.span = 4;
        pass_lp.weight = 1;
        pass_row.setLayoutParams(pass_lp);
        TextView pass_tv = new TextView(this);
        pass_tv.setLayoutParams(pass_lp);
        pass_tv.setGravity(Gravity.LEFT);
        pass_tv.setTextSize(textSize);
        if (GlobalClass.getInstance().CB_SteriSuccess) {
            str = "Leakage test:Pass";
        }else{
            str = "Leakage test:Fail";
        }
        pass_tv.setText(str);
        pass_row.addView(pass_tv);
        tableLayout.addView(pass_row);


        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
//        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
        progressBarNumberOfDots = getBgRes().size()-1;
        PagerModelManager manager = new PagerModelManager();
        manager.addCommonFragment(GuideFragment.class, getBgRes(), getTitles());
        ModelPagerAdapter adapter = new ModelPagerAdapter(getSupportFragmentManager(), manager);
        viewPager.setAdapter(adapter);
        viewPager.fixScrollSpeed();

        registerBaseActivityReceiver();
        DrawMeButton recordButton =(DrawMeButton) findViewById(R.id.record_button);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LeakageTestCompleteSterilizationActivity.this, SteriRecordReaderActivity.class));
                overridePendingTransition(0, 0);
            }
        });
//        springIndicator.setViewPager(viewPager);

//        progressBar = (DottedProgressBar) findViewById(R.id.progress);
//        progressBar.setActiveDotIndex(0);
//        progressBar.invalidate();
//        progressBarTitleView.setText(getTitles().get(0));
//        handler = new Handler();
//        handler.postAtTime(runnable, 1000);
    }

    public void stopProgress(View view) {
        progressBar.stopProgress();
    }

    public void startProgress(View view) {
        progressBar.startProgress();
    }

    private List<String> getTitles(){
        return Lists.newArrayList("Complete");
    }

    private List<Integer> getBgRes(){
        return Lists.newArrayList(R.drawable.stage_icon_finish);
    }

    @Override
    public void onItemClick(int position) {
        this.startActivity(new Intent(LeakageTestCompleteSterilizationActivity.this, StartSterInformationActivity.class));
        this.overridePendingTransition(0, 0);

    }

    private void setDummyData() {
        startSterInformationItemA = new ArrayList<>();
        startSterInformationItemA.add(new StartSterInformationItemA(R.drawable.toolbar_information, "Estimated time", "00:00:00"));

//        recyclerViewButton2 = new ArrayList<>();
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_pressure, "Pres.","0.00 bar"));
//        recyclerViewButton2.add(new RecyclerViewItem(R.drawable.sign_icon_temperature, "Temp","40.0 °C"));
    }

    private String getSterilizationTime() {
        String timeStr;
        calendar = Calendar.getInstance();
        String hour = "";
        String minute = "";
        String second = "";
        String month = "";
        String date = "";
        int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int i_minute = calendar.get(Calendar.MINUTE);
        int i_second = calendar.get(Calendar.SECOND);
        int i_month = calendar.get(Calendar.MONTH)+1;
        int i_date = calendar.get(Calendar.DAY_OF_MONTH);
        if(i_hour<10){
            hour = "0"+i_hour;
        }else{
            hour = ""+i_hour;
        }
        if(i_minute<10){
            minute = "0"+i_minute;
        }else{
            minute = ""+i_minute;
        }
        if(i_second<10){
            second = "0"+i_second;
        }else{
            second = ""+i_second;
        }
        if(i_month<10){
            month = "0"+i_month;
        }else{
            month = ""+i_month;
        }
        if(i_date<10){
            date = "0"+i_date;
        }else{
            date = ""+i_date;
        }
        hour = "00";
        timeStr = hour+":"+minute+":"+second+"\r\r"+calendar.get(Calendar.YEAR)+"."+month+"."+date;
        calendar.clear();
        calendar = null;
        return timeStr;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent;
        char[] data;
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if ((Constants.instance().fetchValueInt(postIDControl))==1) {
                    Constants.instance().storeValueString(userLoginCheckPoint, "PostID");
                    this.startActivity(new Intent(LeakageTestCompleteSterilizationActivity.this, UserLoginActivity.class));
                }else {
                    goDoorOpenActvity();
                }
                break;
        }
        return false;
    }

    void goDoorOpenActvity() {
        char[] data = new char[]{0x01, 0x60};
        sendToService(data);
//        this.startActivity(new Intent(mActivity, DoorOpenActivity.class));
//        finish();
    }


    @Override
    public void onPause() {
        super.onPause();
//        handler.removeCallbacks(runnable);
        overridePendingTransition(0, 0);
    }
}
