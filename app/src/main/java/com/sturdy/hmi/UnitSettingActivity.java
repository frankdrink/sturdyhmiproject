package com.sturdy.hmi;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.MainMenuSettingElement;
import com.sturdy.hmi.adapter.MainSettingListAdapter;
import com.sturdy.hmi.adapter.UnitRadioListAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.widget.SwitchButton;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DatePicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;

import static com.sturdy.hmi.Constants.postIDControl;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preIDControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;
import static com.sturdy.hmi.Constants.saveMenuSettingKey;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.savePresFormat;
import static com.sturdy.hmi.Constants.userLoginLevel;

public class UnitSettingActivity extends BaseToolBarActivity implements UnitRadioListAdapter.UpdateMainClass{
    private Activity mActivity;
    UnitRadioListAdapter adapter1;
    UnitRadioListAdapter adapter2;
    int saveTempFormatBase;
    int savePresFormatBase;
    List<String> data1;
    List<String> data2;

    List<MainMenuSettingElement> settingElementList;
    Handler handler;

    private String timeStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_datetimesetting);
        mActivity = this;
        calendar = Calendar.getInstance();
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Unit", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        TextView titleView1 = (TextView) findViewById(R.id.list1_text_view);
        titleView1.setText("Temp.");
        TextView titleView2 = (TextView) findViewById(R.id.list2_text_view);
        titleView2.setText("Pres.");

        data1 = new ArrayList<String>();
        data1.add("°C");
        data1.add("°F");
        saveTempFormatBase = Constants.instance().fetchValueInt(saveTempFormat);
        adapter1 = new UnitRadioListAdapter(this, data1, "temp", saveTempFormatBase);
        ListView lvMain1 = (ListView) findViewById(R.id.list1);
        lvMain1.setAdapter(adapter1);
        data2 = new ArrayList<String>();
        data2.add("bar");
        data2.add("kPa");
        data2.add("Mpa");
        data2.add("psi");
        data2.add("kgf/cm²");
        savePresFormatBase = Constants.instance().fetchValueInt(savePresFormat);
        adapter2 = new UnitRadioListAdapter(this, data2, "pres", savePresFormatBase);
        ListView lvMain2 = (ListView) findViewById(R.id.list2);
        lvMain2.setAdapter(adapter2);

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void updateItemList(final String itemName) {
        calendar = Calendar.getInstance();
        if (itemName.equals("temp0")) {
            saveTempFormatBase = 0;
            Constants.instance().storeValueInt(saveTempFormat,saveTempFormatBase);
        }else if (itemName.equals("temp1")) {
            saveTempFormatBase = 1;
            Constants.instance().storeValueInt(saveTempFormat,saveTempFormatBase);
        } if (itemName.equals("pres0")) {
            savePresFormatBase = 0;
            Constants.instance().storeValueInt(savePresFormat,savePresFormatBase);
        }else if (itemName.equals("pres1")) {
            savePresFormatBase = 1;
            Constants.instance().storeValueInt(savePresFormat,savePresFormatBase);
        }else if (itemName.equals("pres2")) {
            savePresFormatBase = 2;
            Constants.instance().storeValueInt(savePresFormat,savePresFormatBase);
        }else if (itemName.equals("pres3")) {
            savePresFormatBase = 3;
            Constants.instance().storeValueInt(savePresFormat,savePresFormatBase);
        }else if (itemName.equals("pres4")) {
            savePresFormatBase = 4;
            Constants.instance().storeValueInt(savePresFormat,savePresFormatBase);
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {

    }

}
