package com.sturdy.wifilibrary.listener;

import android.net.wifi.ScanResult;

import java.util.List;


public interface OnWifiScanResultsListener {

    /**
     * 掃瞄結果的回調
     *
     * @param scanResults 掃瞄結果
     */
    void onScanResults(List<ScanResult> scanResults);
}
