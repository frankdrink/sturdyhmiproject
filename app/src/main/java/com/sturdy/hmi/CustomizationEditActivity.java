package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.sturdytheme.framework.picker.DoublePicker;
import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.preScanControl;

public class CustomizationEditActivity extends BaseToolBarActivity implements SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;

    private String[] pickerVacuumCountArrayList;
    private String[] pickerTempArrayList;
    private String[] pickerDryTimeArrayList;

    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String str;
    private int CB_VacuumCount;
    private int CB_Temp;
    private boolean isFirstPrescan = true;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x07) {
                    char ch = recv[3];
                    CB_VacuumCount = ch;
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    CB_Temp = CharUtils.charArrayToInt(ch2);
                    ch2 = new char[2];
                    System.arraycopy(recv, 6, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTime = CharUtils.charArrayToInt(ch2);
                    ch2 = new char[2];
                    System.arraycopy(recv, 8, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DryTime = CharUtils.charArrayToInt(ch2);
                    str = Integer.toString(CB_VacuumCount);
                    mItems.set(0, new RecyclerViewClass("Pre-Vacuum", str, R.drawable.parameter_icon_prevacuum,true));
                    str = UnitConvert.sTempConvert(1,CB_Temp);
                    mItems.set(1, new RecyclerViewClass("Ster.Temp", str, R.drawable.parameter_icon_temp,true));
                    int currentMinute = (GlobalClass.getInstance().CB_SteriTime%3600)/60;
                    int currentSec = GlobalClass.getInstance().CB_SteriTime%60;
                    str = String.format("%dm %ds",currentMinute,currentSec);
                    mItems.set(2, new RecyclerViewClass("Ster.Time", str, R.drawable.parameter_icon_time,true));
                    str = String.format("%d mins",(GlobalClass.getInstance().CB_DryTime%3600)/60);
                    mItems.set(3, new RecyclerViewClass("Dry Time", str, R.drawable.parameter_icon_time,true));
                    mAdapter.notifyDataSetChanged();
                    GlobalClass.getInstance().Steri_Temp = CB_Temp / 10;
                    GlobalClass.getInstance().CB_SteriTemp = CB_Temp;
                    GlobalClass.getInstance().CB_PreVacuumCount = CB_VacuumCount;
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_customization_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        pickerVacuumCountArrayList = new String[]{
                "0",  "1",  "2",  "3",  "4",  "5"
        };
        pickerTempArrayList = new String[]{
                "121°C",  "134°C"
        };
        pickerDryTimeArrayList = new String[]{
                "0 mins", "5 mins",  "10 mins", "15 mins",  "20 mins","25 mins",  "30 mins", "35 mins",  "40 mins","45 mins",  "50 mins", "55 mins",  "60 mins"
        };
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_2_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_3_button, 64);


        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();
        mItems.add(0, new RecyclerViewClass("Pre-Vacuum", "", R.drawable.parameter_icon_prevacuum,true));
        mItems.add(1, new RecyclerViewClass("Ster.Temp", "", R.drawable.parameter_icon_temp,true));
        mItems.add(2, new RecyclerViewClass("Ster.Time", "", R.drawable.parameter_icon_time,true));
        mItems.add(3, new RecyclerViewClass("Dry Time", "", R.drawable.parameter_icon_time,true));

        mAdapter = new SteriEditAdapter(this, mItems, true);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(CustomizationEditActivity.this,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(CustomizationEditActivity.this,CustomizationInformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bFieldmodified){
//                    bFieldmodified = false;
//                    mAdapter.setTextViewEditable(bFieldmodified);
//                    mStartSterButton.setText("Start");
//                }else{
//                    startActivity(new Intent(CustomizationEditActivity.this, StartSterilizationActivity.class));
//                    overridePendingTransition(0, 0);
//                    finish();
//                }
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(CustomizationEditActivity.this, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendCustomizationSetting();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendCustomizationSetting();
                    sendGetSterillzationStep();
                }
            }
        });
        // 取得消毒行程資(Customization)
        char[] data = new char[]{0x02,0x07,GlobalClass.getInstance().cLoginUserID};
        sendToService(data);

    }

    void sendCustomizationSetting() {
        char[] data = new char[9];
        char[] ch22 = new char[2];
        data[0] = 0x08;
        data[1] = 0x21;
        data[2] = (char)CB_VacuumCount;
        ch22 = CharUtils.fromShort((short)CB_Temp);
        data[3] = ch22[0];
        data[4] = ch22[1];
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_SteriTime);
        data[5] = ch22[0];
        data[6] = ch22[1];
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_DryTime);
        data[7] = ch22[0];
        data[8] = ch22[1];
        sendToService(data);
    }



    @Override
    public void updateItemList(final int position) {
        if (position==0) {
            OptionPicker picker = new OptionPicker(CustomizationEditActivity.this, pickerVacuumCountArrayList);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(false);
            picker.setTextSize(32);
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    CB_VacuumCount = index;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==1) {
            OptionPicker picker = new OptionPicker(CustomizationEditActivity.this, pickerTempArrayList);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
//                        showToast("index=" + index + ", item=" + item);
//                    String str = pickerTempArrayList[index];
                    switch(index) {
                        case 0:
                            CB_Temp = 121*10;
                            GlobalClass.getInstance().Steri_Temp = 121;
                            GlobalClass.getInstance().CB_SteriTemp = 121*10;
                            break;
                        case 1:
                            CB_Temp = 134*10;
                            GlobalClass.getInstance().Steri_Temp = 134;
                            GlobalClass.getInstance().CB_SteriTemp = 134*10;
                            break;
                    }
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==2) {
//            TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
//            picker.setUseWeight(false);
//            picker.setCycleDisable(false);
//            picker.setTitleTextSize(40);
//            picker.setRangeStart(0, 0);//00:00
//            picker.setRangeEnd(23, 59);//23:59
//            int currentMinute = (GlobalClass.getInstance().CB_SteriTime%3600)/60;
//            int currentSec = GlobalClass.getInstance().CB_SteriTime%60;
//            //picker.setSelectedItem(currentMinute,currentSec);
//            picker.setTopLineVisible(true);
//
//            picker.setTextPadding(ConvertUtils.toPx(this, 15));
//            picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
//                @Override
//                public void onTimePicked(String hour, String minute, String timeAt) {
//                    GlobalClass.getInstance().CB_SteriTime = Integer.valueOf(hour)*60+Integer.valueOf(minute);
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), hour+" m "+minute+" s", mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();



            final ArrayList<String> firstData = new ArrayList<>();
            for (int i=0;i<60;i++)  {
                firstData.add(i+" m");
            }

            final ArrayList<String> secondData = new ArrayList<>();
            for (int i=0;i<60;i++)  {
                secondData.add(i+" s");
            }

            final DoublePicker picker = new DoublePicker(this, firstData, secondData);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerVisible(true);
            picker.setCycleDisable(false);
            int currentMinute = (GlobalClass.getInstance().CB_SteriTime%3600)/60;
            int currentSec = GlobalClass.getInstance().CB_SteriTime%60;
            picker.setSelectedIndex(currentMinute, currentSec);
            picker.setFirstLabel("", null);
            picker.setSecondLabel("", "");
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(false);
            picker.setTextSize(32);
            picker.setContentPadding(15, 10);
            picker.setOnPickListener(new DoublePicker.OnPickListener() {
                @Override
                public void onPicked(int selectedFirstIndex, int selectedSecondIndex) {
                    GlobalClass.getInstance().CB_SteriTime = (selectedFirstIndex) * 60 + selectedSecondIndex;
                    String str = firstData.get(selectedFirstIndex) + " " + secondData.get(selectedSecondIndex);
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), str, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==3) {
            OptionPicker picker = new OptionPicker(CustomizationEditActivity.this, pickerDryTimeArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);

            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(false);
            picker.setTextSize(32);
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
//                        showToast("index=" + index + ", item=" + item);
//                    switch(index) {
//                        case 0:
//                            GlobalClass.getInstance().CB_DryTime = 5*60;
//                            break;
//                        case 1:
//                            GlobalClass.getInstance().CB_DryTime = 10*60;
//                            break;
//                        case 2:
//                            GlobalClass.getInstance().CB_DryTime = 15*60;
//                            break;
//                        case 3:
//                            GlobalClass.getInstance().CB_DryTime = 20*60;
//                            break;
//                    }
                    GlobalClass.getInstance().CB_DryTime = index*5*60;
                    String str = pickerDryTimeArrayList[index];
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }


}
