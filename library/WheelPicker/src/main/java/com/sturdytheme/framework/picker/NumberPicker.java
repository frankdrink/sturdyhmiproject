package com.sturdytheme.framework.picker;

import android.app.Activity;

/**
 * 數字選擇器
 */
public class NumberPicker extends SinglePicker<Number> {

    public NumberPicker(Activity activity) {
        super(activity, new Number[]{});
    }

    /**
     * 設置數字範圍，遞增量為1
     */
    public void setRange(int startNumber, int endNumber) {
        setRange(startNumber, endNumber, 1);
    }

    /**
     * 設置數字範圍及遞增量
     */
    public void setRange(int startNumber, int endNumber, int step) {
        for (int i = startNumber; i <= endNumber; i = i + step) {
            addItem(i);
        }
    }

    /**
     * 設置數字範圍及遞增量
     */
    public void setRange(double startNumber, double endNumber, double step) {
        for (double i = startNumber; i <= endNumber; i = i + step) {
            addItem(i);
        }
    }

    /**
     * 設置默認選中的數字
     */
    public void setSelectedItem(int number) {
        super.setSelectedItem(number);
    }

    /**
     * 設置默認選中的數字
     */
    public void setSelectedItem(double number) {
        super.setSelectedItem(number);
    }

    public void setOnNumberPickListener(OnNumberPickListener listener) {
        super.setOnItemPickListener(listener);
    }

    /**
     * @deprecated use {@link #setOnNumberPickListener(OnNumberPickListener)} instead
     */
    @Deprecated
    public void setOnOptionPickListener(final OptionPicker.OnOptionPickListener listener) {
        setOnNumberPickListener(new OnNumberPickListener() {
            @Override
            public void onNumberPicked(int index, Number item) {
                listener.onOptionPicked(index, item.toString());
            }
        });
    }

    public void setOnWheelListener(OnWheelListener onWheelListener) {
        super.setOnWheelListener(onWheelListener);
    }

    public interface OnWheelListener extends SinglePicker.OnWheelListener<Number> {

    }

    public static abstract class OnNumberPickListener implements OnItemPickListener<Number> {

        public abstract void onNumberPicked(int index, Number item);

        @Override
        public final void onItemPicked(int index, Number item) {
            onNumberPicked(index, item);
        }

    }

}

