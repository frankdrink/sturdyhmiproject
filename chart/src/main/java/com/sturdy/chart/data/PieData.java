package com.sturdy.chart.data;

import com.sturdy.chart.component.base.IAxis;

/**
 * Created by Frank 2020/01/01
 */

public class PieData extends ColumnData<Double> {


    public PieData(String name, String unit, int color, Double chartYDataList) {

        super(name, unit, color, chartYDataList);
    }

    public PieData(String name, String unit, IAxis.AxisDirection direction, int color, Double datas) {
        super(name, unit, color, datas);

    }


}
