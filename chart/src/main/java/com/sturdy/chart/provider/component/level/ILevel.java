package com.sturdy.chart.provider.component.level;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Frank on 2019/10/19.
 */

public interface ILevel {

    int getAxisDirection();

    void drawLevel(Canvas canvas, Rect rect, float y, Paint paint);

    double getValue();
}
