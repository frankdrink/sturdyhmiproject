package com.sturdytheme.framework.entity;

import java.util.List;


public interface LinkageSecond<Trd> extends LinkageItem {

    List<Trd> getThirds();

}