package com.sturdy.hmi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.stx.xhb.commontitlebar.CustomTitleBar;

public class CalibrationMenuActivity extends BaseToolBarActivity {
    private Activity mActivity;
    ListView list;
    String[] web = {
            "121 Dynamic calibration",
            "134 Dynamic calibration"

    } ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_calibration_menu);
        mActivity = this;
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Dynamic Calibration", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        SteriMenuList listAdapter = new
                SteriMenuList(this, web,null);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(listAdapter);
        list.setBackgroundColor(Color.WHITE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent();
                String programName;
                switch (position){
                    case 0:
                        GlobalClass.getInstance().CalSelectTemp = 121;
                        GlobalClass.getInstance().CalState = 1;
                        intent.setClass(CalibrationMenuActivity.this,DynamicCalViewActivity.class);
                        break;
                    case 1:
                        GlobalClass.getInstance().CalSelectTemp = 134;
                        GlobalClass.getInstance().CalState = 1;
                        intent.setClass(CalibrationMenuActivity.this,DynamicCalViewActivity.class);
                        break;
                }
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });
        registerBaseActivityReceiver();
    }


}
