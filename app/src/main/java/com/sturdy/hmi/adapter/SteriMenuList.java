package com.sturdy.hmi.adapter;
/**
 * Created by franklin on 2019/8/8.
 */
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sturdy.hmi.R;



public class SteriMenuList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    public SteriMenuList(Activity context,
                      String[] web, Integer[] imageId) {
        super(context, R.layout.steri_menu_list, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.steri_menu_list, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        if(imageId!=null) {
            imageView.setImageResource(imageId[position]);
        }else{
            imageView.setVisibility(View.GONE);
        }
        txtTitle.setText(web[position]);

        return rowView;
    }
}
