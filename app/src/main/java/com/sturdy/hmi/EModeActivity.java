package com.sturdy.hmi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.Notice2Dialog;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.NotificationDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.FileUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.RecyclerViewDisabler;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.MACHINE_STATE.IDLE;
import static com.sturdy.hmi.Constants.NextAirFilterKey;
import static com.sturdy.hmi.Constants.NextExhaustFilterKey;
import static com.sturdy.hmi.Constants.NextGasketServiceKey;
import static com.sturdy.hmi.Constants.NextServiceCycleKey;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class EModeActivity extends BaseToolBarActivity{
    Handler handler = new Handler();
    Runnable run;
    private int cleaningState=0;
    private ImageView mPromptimageview;
    private TextView mProgramTextView;
    private TextView mNextTextView;
    private DrawMeImageButton HomeButton;
    private DrawMeButton mStartSterButton;
    private boolean buttonClicked[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_emode);
        mActivity = this;
        buttonClicked = new boolean[8];
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Mode", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();

        HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });


        TextView cleanCount = (TextView)findViewById(R.id.cleancount_button);
        cleanCount.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new NotificationDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Notification")
                            .setContentText("Confirm to Reset?")
                            .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                                @Override
                                public void onClick(NotificationDialog dialog) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                                @Override
                                public void onClick(NotificationDialog dialog) {
                                    dialog.dismiss();
                                }
                            })
                            .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NotificationDialog dialog) {
                                    char[] data = new char[]{0x04,0xf9,0x01,0x01};
                                    sendToService(data);
                                    Settings.System.putInt(mActivity.getContentResolver(),CycleCounterKey, 0);
                                        Settings.System.putInt(mActivity.getContentResolver(),NextExhaustFilterKey, 250);
                                        Settings.System.putInt(mActivity.getContentResolver(),NextAirFilterKey, 500);
                                        Settings.System.putInt(mActivity.getContentResolver(),NextGasketServiceKey, 2000);
                                        Settings.System.putInt(mActivity.getContentResolver(),NextServiceCycleKey, 3000);
                                    String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString();
                                    FileUtils.deleteDirectory(dir);
                                    dialog.dismiss();
                                    closeAllActivities();
                                }
                            }).show();
                }

                return false;
            }
        });


        TextView cleanelectro = (TextView)findViewById(R.id.cleanelectro_button);
        cleanelectro.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (buttonClicked[1]==false) {
                        char[] data = new char[]{0x04,0xf9,0x02,0x01};
                        sendToService(data);
                        cleanelectro.setBackgroundResource(R.drawable.confirm_redbutton_border);
                        buttonClicked[1] = true;
                        Log.i("EMode", "f9 0x01");

                    }else if (buttonClicked[1]==true) {
                        char[] data = new char[]{0x04,0xf9,0x02,0x00};
                        sendToService(data);
                        cleanelectro.setBackgroundResource(R.drawable.confirm_button_border);
                        buttonClicked[1] = false;
                        Log.i("EMode", "f9 0x00");

                    }
                }

                return false;
            }
        });

        TextView cleanelectro3 = (TextView)findViewById(R.id.cleanelectro_button3);
        cleanelectro3.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (buttonClicked[2]==false) {
                        cleanelectro3.setBackgroundResource(R.drawable.confirm_redbutton_border);
                        char[] data = new char[]{0x04,0xf9,0x03,0x01};
                        sendToService(data);
                        buttonClicked[2] = true;
                    }else if (buttonClicked[2]==true){
                        cleanelectro3.setBackgroundResource(R.drawable.confirm_button_border);
                        char[] data = new char[]{0x04,0xf9,0x03,0x00};
                        sendToService(data);
                        buttonClicked[2] = false;
                    }

                }

                return false;
            }
        });

        TextView cleanelectro4 = (TextView)findViewById(R.id.cleanelectro_button4);
        cleanelectro4.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (buttonClicked[3]==false) {
                        cleanelectro4.setBackgroundResource(R.drawable.confirm_redbutton_border);
                        char[] data = new char[]{0x04,0xf9,0x04,0x01};
                        sendToService(data);
                        buttonClicked[3] = true;
                    }else if (buttonClicked[3]==true){
                        cleanelectro4.setBackgroundResource(R.drawable.confirm_button_border);
                        char[] data = new char[]{0x04,0xf9,0x04,0x00};
                        sendToService(data);
                        buttonClicked[3] = false;
                    }

                }

                return false;
            }
        });

        TextView cleanelectro5 = (TextView)findViewById(R.id.cleanelectro_button5);
        cleanelectro5.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (buttonClicked[4]==false) {
                        cleanelectro5.setBackgroundResource(R.drawable.confirm_redbutton_border);
                        char[] data = new char[]{0x04,0xf9,0x05,0x01};
                        sendToService(data);
                        buttonClicked[4] = true;
                    }else if (buttonClicked[4]==true){
                        cleanelectro5.setBackgroundResource(R.drawable.confirm_button_border);
                        char[] data = new char[]{0x04,0xf9,0x05,0x00};
                        sendToService(data);
                        buttonClicked[4] = false;
                    }

                }

                return false;
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
