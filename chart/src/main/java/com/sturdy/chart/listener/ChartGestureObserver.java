package com.sturdy.chart.listener;


public interface ChartGestureObserver {

    void onClick(float x,float y);

}
