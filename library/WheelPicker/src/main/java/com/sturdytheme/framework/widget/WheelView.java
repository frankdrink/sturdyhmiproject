package com.sturdytheme.framework.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.sturdytheme.framework.entity.WheelItem;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.util.LogUtils;


public class WheelView extends View {
    public static final float LINE_SPACE_MULTIPLIER = 2.0F;
    public static final int TEXT_PADDING = -1;
    public static final int TEXT_SIZE = 26;//單位為sp
    public static final int TEXT_COLOR_FOCUS = 0XFF0288CE;
    public static final int TEXT_COLOR_NORMAL = 0XFFBBBBBB;
    public static final int DIVIDER_COLOR = 0XFF83CDE6;
    public static final int DIVIDER_ALPHA = 220;
    public static final float DIVIDER_THICK = 2f;//單位為px
    public static final int ITEM_OFF_SET = 3;
    private static final float ITEM_PADDING = 13f;//單位為px,480X800的手機邊距不能太大
    private static final int ACTION_CLICK = 1;//點擊
    private static final int ACTION_FLING = 2;//滑翔
    private static final int ACTION_DRAG = 3;//拖拽
    private static final int VELOCITY_FLING = 5;//修改這個值可以改變滑行速度
    private static final float SCALE_CONTENT = 0.8F;//非中間文字用此控制高度，壓扁形成3D錯覺

    private MessageHandler handler;
    private GestureDetector gestureDetector;
    private OnItemSelectListener onItemSelectListener;
    private OnWheelListener onWheelListener;
    private boolean onlyShowCenterLabel = true;//附加單位是否僅僅只顯示在選中項後面
    private ScheduledFuture<?> mFuture;
    private Paint paintOuterText;//未選項畫筆
    private Paint paintCenterText;//選中項畫筆
    private Paint paintIndicator;//分割線畫筆
    private Paint paintShadow;//陰影畫筆
    private List<WheelItem> items = new ArrayList<>();//所有選項
    private String label;//附加單位
    private int maxTextWidth;//最大的文字寬
    private int maxTextHeight;//最大的文字高
    private int textSkewXOffset = 0;//文字傾斜度
    private int textSize = TEXT_SIZE;//文字大小，單位為sp
    private float itemHeight;//每行高度
    private Typeface typeface = Typeface.DEFAULT;//字體樣式
    private int textColorOuter = TEXT_COLOR_NORMAL;//未選項文字顏色
    private int textColorCenter = TEXT_COLOR_FOCUS;//選中項文字顏色
    private DividerConfig dividerConfig = new DividerConfig();
    private float lineSpaceMultiplier = LINE_SPACE_MULTIPLIER;//條目間距倍數，可用來設置上下間距
    private int textPadding = TEXT_PADDING;//文字的左右邊距,單位為px
    private boolean isLoop = true;//循環滾動
    private float firstLineY;//第一條線Y坐標值
    private float secondLineY;//第二條線Y坐標
    private float totalScrollY = 0;//滾動總高度y值
    private int initPosition = -1;//初始化默認選中項
    private int selectedIndex;//選中項的索引
    private int preCurrentIndex;
    private int visibleItemCount = ITEM_OFF_SET * 2 + 1;//繪製幾個條目
    private int measuredHeight;//控件高度
    private int measuredWidth;//控件寬度
    private int radius;//半徑
    private int offset = 0;
    private float previousY = 0;
    private long startTime = 0;
    private int widthMeasureSpec;
    private int gravity = Gravity.CENTER;
    private int drawCenterContentStart = 0;//中間選中文字開始繪製位置
    private int drawOutContentStart = 0;//非中間文字開始繪製位置
    private float centerContentOffset;//偏移量
    private boolean useWeight = false;//使用比重還是包裹內容？
    private boolean textSizeAutoFit = true;//條目內容過長時是否自動減少字號來適配

    public WheelView(Context context) {
        this(context, null);
    }

    public WheelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //密度：0.75、1.0、1.5、2.0、3.0，根據密度不同進行適配
        float density = getResources().getDisplayMetrics().density;
        if (density < 1) {
            centerContentOffset = 2.4F;
        } else if (1 <= density && density < 2) {
            centerContentOffset = 3.6F;
        } else if (1 <= density && density < 2) {
            centerContentOffset = 4.5F;
        } else if (2 <= density && density < 3) {
            centerContentOffset = 6.0F;
        } else if (density >= 3) {
            centerContentOffset = density * 2.5F;
        }
        judgeLineSpace();
        initView(context);
    }

    /**
     * 設置顯示的選項個數，必須是奇數
     */
    public final void setVisibleItemCount(int count) {
        if (count % 2 == 0) {
            throw new IllegalArgumentException("must be odd");
        }
        if (count != visibleItemCount) {
            visibleItemCount = count;
        }
    }

    /**
     * 設置是否禁用循環滾動
     */
    public final void setCycleDisable(boolean cycleDisable) {
        isLoop = !cycleDisable;
    }

    /**
     * 設置滾輪個數偏移量
     */
    public final void setOffset(@IntRange(from = 1, to = 5) int offset) {
        if (offset < 1 || offset > 5) {
            throw new IllegalArgumentException("must between 1 and 5");
        }
        int count = offset * 2 + 1;
        if (offset % 2 == 0) {
            count += offset;
        } else {
            count += offset - 1;
        }
        setVisibleItemCount(count);
    }

    public final int getSelectedIndex() {
        return selectedIndex;
    }

    public final void setSelectedIndex(int index) {
        if (items == null || items.isEmpty()) {
            return;
        }
        int size = items.size();
        if (index == 0 || (index > 0 && index < size && index != selectedIndex)) {
            initPosition = index;
            totalScrollY = 0;//回歸頂部，不然重設索引的話位置會偏移，就會顯示出不對位置的數據
            offset = 0;
            invalidate();
        }
    }

    public final void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        this.onItemSelectListener = onItemSelectListener;
    }

    /**
     * @deprecated use {@link #setOnItemSelectListener(OnItemSelectListener)} instead
     */
    @Deprecated
    public final void setOnWheelListener(OnWheelListener listener) {
        onWheelListener = listener;
    }


    public final void setItems(List<?> items) {
        this.items.clear();
        for (Object item : items) {
            if (item instanceof WheelItem) {
                this.items.add((WheelItem) item);
            } else if (item instanceof CharSequence || item instanceof Number) {
                this.items.add(new StringItem(item.toString()));
            } else {
                throw new IllegalArgumentException("please implements " + WheelItem.class.getName());
            }
        }
        remeasure();
        invalidate();
    }

    public final void setItems(List<?> items, int index) {
        setItems(items);
        setSelectedIndex(index);
    }

    public final void setItems(String[] list) {
        setItems(Arrays.asList(list));
    }

    public final void setItems(List<String> list, String item) {
        int index = list.indexOf(item);
        if (index == -1) {
            index = 0;
        }
        setItems(list, index);
    }

    public final void setItems(String[] list, int index) {
        setItems(Arrays.asList(list), index);
    }

    public final void setItems(String[] items, String item) {
        setItems(Arrays.asList(items), item);
    }

    /**
     * 附加在右邊的單位字符串
     */
    public final void setLabel(String label, boolean onlyShowCenterLabel) {
        this.label = label;
        this.onlyShowCenterLabel = onlyShowCenterLabel;
    }

    public final void setLabel(String label) {
        setLabel(label, true);
    }

    public final void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public void setTextColor(@ColorInt int colorNormal, @ColorInt int colorFocus) {
        this.textColorOuter = colorNormal;
        this.textColorCenter = colorFocus;
        paintOuterText.setColor(colorNormal);
        paintCenterText.setColor(colorFocus);
    }

    public void setTextColor(@ColorInt int color) {
        this.textColorOuter = color;
        this.textColorCenter = color;
        paintOuterText.setColor(color);
        paintCenterText.setColor(color);
    }

    public final void setTypeface(Typeface font) {
        typeface = font;
        paintOuterText.setTypeface(typeface);
        paintCenterText.setTypeface(typeface);
    }

    public final void setTextSize(float size) {
        if (size > 0.0F) {
            textSize = (int) (getContext().getResources().getDisplayMetrics().density * size);
            paintOuterText.setTextSize(textSize);
            paintCenterText.setTextSize(textSize);
        }
    }

    public void setTextSkewXOffset(int textSkewXOffset) {
        this.textSkewXOffset = textSkewXOffset;
        if (textSkewXOffset != 0) {
            paintCenterText.setTextScaleX(1.0F);
        }
    }

    public void setDividerColor(@ColorInt int color) {
        dividerConfig.setColor(color);
        paintIndicator.setColor(color);
    }

    /**
     * @deprecated use {{@link #setDividerConfig(DividerConfig)} instead
     */
    @Deprecated
    public void setLineConfig(DividerConfig config) {
        setDividerConfig(config);
    }

    public void setDividerConfig(DividerConfig config) {
        if (null == config) {
            dividerConfig.setVisible(false);
            dividerConfig.setShadowVisible(false);
            return;
        }
        this.dividerConfig = config;
        paintIndicator.setColor(config.color);
        paintIndicator.setStrokeWidth(config.thick);
        paintIndicator.setAlpha(config.alpha);
        paintShadow.setColor(config.shadowColor);
        paintShadow.setAlpha(config.shadowAlpha);
    }

    public final void setLineSpaceMultiplier(@FloatRange(from = 2, to = 4) float multiplier) {
        lineSpaceMultiplier = multiplier;
        judgeLineSpace();
    }

    /**
     * Use {@link #setTextPadding(int)} instead
     */
    @Deprecated
    public void setPadding(int padding) {
        setTextPadding(padding);
    }

    public void setTextPadding(int textPadding) {
        this.textPadding = ConvertUtils.toPx(getContext(), textPadding);
    }

    public void setUseWeight(boolean useWeight) {
        this.useWeight = useWeight;
    }

    public void setTextSizeAutoFit(boolean textSizeAutoFit) {
        this.textSizeAutoFit = textSizeAutoFit;
    }

    /**
     * 判斷間距是否在有效範圍內
     */
    private void judgeLineSpace() {
        if (lineSpaceMultiplier < 1.5f) {
            lineSpaceMultiplier = 1.5f;
        } else if (lineSpaceMultiplier > 4.0f) {
            lineSpaceMultiplier = 4.0f;
        }
    }

    private void initView(Context context) {
        handler = new MessageHandler(this);
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public final boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                scrollBy(velocityY);
                return true;
            }
        });
        gestureDetector.setIsLongpressEnabled(false);
        initPaints();
        initDataForIDE();
    }

    private void initPaints() {
        paintOuterText = new Paint();
        paintOuterText.setAntiAlias(true);
        paintOuterText.setColor(textColorOuter);
        paintOuterText.setTypeface(typeface);
        paintOuterText.setTextSize(textSize);
        paintCenterText = new Paint();
        paintCenterText.setAntiAlias(true);
        paintCenterText.setColor(textColorCenter);
        paintCenterText.setTextScaleX(1.0F);
        paintCenterText.setTypeface(typeface);
        paintCenterText.setTextSize(textSize);
        paintIndicator = new Paint();
        paintIndicator.setAntiAlias(true);
        paintIndicator.setColor(dividerConfig.color);
        paintIndicator.setStrokeWidth(dividerConfig.thick);
        paintIndicator.setAlpha(dividerConfig.alpha);
        paintShadow = new Paint();
        paintShadow.setAntiAlias(true);
        paintShadow.setColor(dividerConfig.shadowColor);
        paintShadow.setAlpha(dividerConfig.shadowAlpha);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    private void initDataForIDE() {
        if (isInEditMode()) {
            setItems(new String[]{"李玉江", "男", "貴州", "穿青人"});
        }
    }

    /**
     * 重新測量
     */
    private void remeasure() {
        if (items == null) {
            return;
        }
        measureTextWidthHeight();
        //半圓的周長
        int halfCircumference = (int) (itemHeight * (visibleItemCount - 1));
        //整個圓的周長除以PI得到直徑，這個直徑用作控件的總高度
        measuredHeight = (int) ((halfCircumference * 2) / Math.PI);
        //求出半徑
        radius = (int) (halfCircumference / Math.PI);
        ViewGroup.LayoutParams params = getLayoutParams();
        //控件寬度
        if (useWeight) {
            measuredWidth = MeasureSpec.getSize(widthMeasureSpec);
        } else if (params != null && params.width > 0) {
            measuredWidth = params.width;
        } else {
            measuredWidth = maxTextWidth;
            if (textPadding < 0) {
                textPadding = ConvertUtils.toPx(getContext(), ITEM_PADDING);
            }
            measuredWidth += textPadding * 2;
            if (!TextUtils.isEmpty(label)) {
                measuredWidth += obtainTextWidth(paintCenterText, label);
            }
        }
        LogUtils.debug("measuredWidth=" + measuredWidth + ",measuredHeight=" + measuredHeight);
        //計算兩條橫線 和 選中項畫筆的基線Y位置
        firstLineY = (measuredHeight - itemHeight) / 2.0F;
        secondLineY = (measuredHeight + itemHeight) / 2.0F;
        //初始化顯示的item的position
        if (initPosition == -1) {
            if (isLoop) {
                initPosition = (items.size() + 1) / 2;
            } else {
                initPosition = 0;
            }
        }
        preCurrentIndex = initPosition;
    }

    /**
     * 計算最大length的Text的寬高度
     */
    private void measureTextWidthHeight() {
        Rect rect = new Rect();
        for (int i = 0; i < items.size(); i++) {
            String s1 = obtainContentText(items.get(i));
            paintCenterText.getTextBounds(s1, 0, s1.length(), rect);
            int textWidth = rect.width();
            if (textWidth > maxTextWidth) {
                maxTextWidth = textWidth;
            }
            paintCenterText.getTextBounds("測試", 0, 2, rect);
            maxTextHeight = rect.height() + 2;
        }
        itemHeight = lineSpaceMultiplier * maxTextHeight;
    }

    /**
     * 平滑滾動的實現
     */
    private void smoothScroll(int actionType) {
        cancelFuture();
        if (actionType == ACTION_FLING || actionType == ACTION_DRAG) {
            offset = (int) ((totalScrollY % itemHeight + itemHeight) % itemHeight);
            if ((float) offset > itemHeight / 2.0F) {//如果超過Item高度的一半，滾動到下一個Item去
                offset = (int) (itemHeight - (float) offset);
            } else {
                offset = -offset;
            }
        }
        //停止的時候，位置有偏移，不是全部都能正確停止到中間位置的，這裡把文字位置挪回中間去
        SmoothScrollTimerTask command = new SmoothScrollTimerTask(this, offset);
        mFuture = Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(command, 0, 10, TimeUnit.MILLISECONDS);
    }

    /**
     * 滾動慣性的實現
     */
    private void scrollBy(float velocityY) {
        cancelFuture();
        InertiaTimerTask command = new InertiaTimerTask(this, velocityY);
        mFuture = Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(command, 0, VELOCITY_FLING, TimeUnit.MILLISECONDS);
    }

    private void cancelFuture() {
        if (mFuture != null && !mFuture.isCancelled()) {
            mFuture.cancel(true);
            mFuture = null;
        }
    }

    private void itemSelectedCallback() {
        if (onItemSelectListener == null && onWheelListener == null) {
            return;
        }
        postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onItemSelectListener != null) {
                    onItemSelectListener.onSelected(selectedIndex);
                }
                if (onWheelListener != null) {
                    onWheelListener.onSelected(true, selectedIndex, items.get(selectedIndex).getName());
                }
            }
        }, 200L);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (items == null || items.size() == 0) {
            return;
        }
        //可見項的數組
        @SuppressLint("DrawAllocation")
        String[] visibleItemStrings = new String[visibleItemCount];
        //滾動的Y值高度除去每行的高度，得到滾動了多少個項，即change數
        int change = (int) (totalScrollY / itemHeight);
        //滾動中實際的預選中的item(即經過了中間位置的item) ＝ 滑動前的位置 ＋ 滑動相對位置
        preCurrentIndex = initPosition + change % items.size();
        if (!isLoop) {//不循環的情況
            if (preCurrentIndex < 0) {
                preCurrentIndex = 0;
            }
            if (preCurrentIndex > items.size() - 1) {
                preCurrentIndex = items.size() - 1;
            }
        } else {//循環
            if (preCurrentIndex < 0) {//舉個例子：如果總數是5，preCurrentIndex ＝ －1，那麼preCurrentIndex按循環來說，其實是0的上面，也就是4的位置
                preCurrentIndex = items.size() + preCurrentIndex;
            }
            if (preCurrentIndex > items.size() - 1) {//同理上面,自己腦補一下
                preCurrentIndex = preCurrentIndex - items.size();
            }
        }
        //跟滾動流暢度有關，總滑動距離與每個item高度取余，即並不是一格格的滾動，每個item不一定滾到對應Rect裡的，這個item對應格子的偏移值
        float itemHeightOffset = (totalScrollY % itemHeight);
        // 設置數組中每個元素的值
        int counter = 0;
        while (counter < visibleItemCount) {
            int index = preCurrentIndex - (visibleItemCount / 2 - counter);//索引值，即當前在控件中間的item看作數據源的中間，計算出相對源數據源的index值
            //判斷是否循環，如果是循環數據源也使用相對循環的position獲取對應的item值，如果不是循環則超出數據源範圍使用""空白字符串填充，在界面上形成空白無數據的item項
            if (isLoop) {
                index = getLoopMappingIndex(index);
                visibleItemStrings[counter] = items.get(index).getName();
            } else if (index < 0) {
                visibleItemStrings[counter] = "";
            } else if (index > items.size() - 1) {
                visibleItemStrings[counter] = "";
            } else {
                visibleItemStrings[counter] = items.get(index).getName();
            }
            counter++;
        }
        //繪製中間兩條橫線
        if (dividerConfig.visible) {
            float ratio = dividerConfig.ratio;
            canvas.drawLine(measuredWidth * ratio, firstLineY, measuredWidth * (1 - ratio), firstLineY, paintIndicator);
            canvas.drawLine(measuredWidth * ratio, secondLineY, measuredWidth * (1 - ratio), secondLineY, paintIndicator);
        }
        if (dividerConfig.shadowVisible) {
            paintShadow.setColor(dividerConfig.shadowColor);
            paintShadow.setAlpha(dividerConfig.shadowAlpha);
            canvas.drawRect(0.0F, firstLineY, measuredWidth, secondLineY, paintShadow);
        }
        counter = 0;
        while (counter < visibleItemCount) {
            canvas.save();
            // 弧長 L = itemHeight * counter - itemHeightOffset
            // 求弧度 α = L / r  (弧長/半徑) [0,π]
            double radian = ((itemHeight * counter - itemHeightOffset)) / radius;
            // 弧度轉換成角度(把半圓以Y軸為軸心向右轉90度，使其處於第一象限及第四象限
            // angle [-90°,90°]
            float angle = (float) (90D - (radian / Math.PI) * 180D);//item第一項,從90度開始，逐漸遞減到 -90度
            // 計算取值可能有細微偏差，保證負90°到90°以外的不繪製
            if (angle >= 90F || angle <= -90F) {
                canvas.restore();
            } else {
                //獲取內容文字
                String contentText;
                //如果是label每項都顯示的模式，並且item內容不為空、label也不為空
                String tempStr = obtainContentText(visibleItemStrings[counter]);
                if (!onlyShowCenterLabel && !TextUtils.isEmpty(label) && !TextUtils.isEmpty(tempStr)) {
                    contentText = tempStr + label;
                } else {
                    contentText = tempStr;
                }
                if (textSizeAutoFit) {
                    remeasureTextSize(contentText);
                    gravity = Gravity.CENTER;
                } else {
                    gravity = Gravity.START;
                }
                //計算開始繪製的位置
                measuredCenterContentStart(contentText);
                measuredOutContentStart(contentText);
                float translateY = (float) (radius - Math.cos(radian) * radius - (Math.sin(radian) * maxTextHeight) / 2D);
                canvas.translate(0.0F, translateY);
                if (translateY <= firstLineY && maxTextHeight + translateY >= firstLineY) {
                    // 條目經過第一條線
                    canvas.save();
                    canvas.clipRect(0, 0, measuredWidth, firstLineY - translateY);
                    canvas.scale(1.0F, (float) Math.sin(radian) * SCALE_CONTENT);
                    canvas.drawText(contentText, drawOutContentStart, maxTextHeight, paintOuterText);
                    canvas.restore();
                    canvas.save();
                    canvas.clipRect(0, firstLineY - translateY, measuredWidth, (int) (itemHeight));
                    canvas.scale(1.0F, (float) Math.sin(radian) * 1.0F);
                    canvas.drawText(contentText, drawCenterContentStart, maxTextHeight - centerContentOffset, paintCenterText);
                    canvas.restore();
                } else if (translateY <= secondLineY && maxTextHeight + translateY >= secondLineY) {
                    // 條目經過第二條線
                    canvas.save();
                    canvas.clipRect(0, 0, measuredWidth, secondLineY - translateY);
                    canvas.scale(1.0F, (float) Math.sin(radian) * 1.0F);
                    canvas.drawText(contentText, drawCenterContentStart, maxTextHeight - centerContentOffset, paintCenterText);
                    canvas.restore();
                    canvas.save();
                    canvas.clipRect(0, secondLineY - translateY, measuredWidth, (int) (itemHeight));
                    canvas.scale(1.0F, (float) Math.sin(radian) * SCALE_CONTENT);
                    canvas.drawText(contentText, drawOutContentStart, maxTextHeight, paintOuterText);
                    canvas.restore();
                } else if (translateY >= firstLineY && maxTextHeight + translateY <= secondLineY) {
                    // 中間條目
                    canvas.clipRect(0, 0, measuredWidth, maxTextHeight);
                    //讓文字居中
                    float y = maxTextHeight - centerContentOffset;//因為圓弧角換算的向下取值，導致角度稍微有點偏差，加上畫筆的基線會偏上，因此需要偏移量修正一下
                    int i = 0;
                    for (WheelItem item : items) {
                        if (item.getName().equals(tempStr)) {
                            selectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    if (onlyShowCenterLabel && !TextUtils.isEmpty(label)) {
                        contentText += label;
                    }
                    canvas.drawText(contentText, drawCenterContentStart, y, paintCenterText);
                } else {
                    // 其他條目
                    canvas.save();
                    canvas.clipRect(0, 0, measuredWidth, itemHeight);
                    canvas.scale(1.0F, (float) Math.sin(radian) * SCALE_CONTENT);
                    // 根據當前角度計算出偏差係數，用以在繪製時控制文字的 水平移動 透明度 傾斜程度
                    float offsetCoefficient = (float) Math.pow(Math.abs(angle) / 90f, 2.2);
                    if (textSkewXOffset != 0) {
                        //控制文字傾斜度
                        paintOuterText.setTextSkewX((textSkewXOffset > 0 ? 1 : -1) * (angle > 0 ? -1 : 1) * 0.5F * offsetCoefficient);
                        // 控制透明度
                        paintOuterText.setAlpha((int) ((1 - offsetCoefficient) * 255));
                    }
                    // 控制文字水平偏移距離
                    canvas.drawText(contentText, drawOutContentStart + textSkewXOffset * offsetCoefficient, maxTextHeight, paintOuterText);
                    canvas.restore();
                }
                canvas.restore();
                paintCenterText.setTextSize(textSize);
            }
            counter++;
        }
    }

    /**
     * 根據文字的長度 重新設置文字的大小 讓其能完全顯示
     */
    private void remeasureTextSize(String contentText) {
        Rect rect = new Rect();
        paintCenterText.getTextBounds(contentText, 0, contentText.length(), rect);
        int width = rect.width();
        int size = textSize;
        while (width > measuredWidth) {
            size--;
            //設置2條橫線中間的文字大小
            paintCenterText.setTextSize(size);
            paintCenterText.getTextBounds(contentText, 0, contentText.length(), rect);
            width = rect.width();
        }
        //設置2條橫線外面的文字大小
        paintOuterText.setTextSize(size);
    }


    /**
     * 遞歸計算出對應的索引
     */
    private int getLoopMappingIndex(int index) {
        if (index < 0) {
            index = index + items.size();
            index = getLoopMappingIndex(index);
        } else if (index > items.size() - 1) {
            index = index - items.size();
            index = getLoopMappingIndex(index);
        }
        return index;
    }

    /**
     * 根據傳進來的對象來獲取需要顯示的值
     *
     * @param item 數據源的項
     * @return 對應顯示的字符串
     */
    private String obtainContentText(Object item) {
        if (item == null) {
            return "";
        } else if (item instanceof WheelItem) {
            return ((WheelItem) item).getName();
        } else if (item instanceof Integer) {
            //如果為整形則最少保留兩位數.
            return String.format(Locale.getDefault(), "%02d", (int) item);
        }
        return item.toString();
    }

    private void measuredCenterContentStart(String content) {
        Rect rect = new Rect();
        paintCenterText.getTextBounds(content, 0, content.length(), rect);
        switch (gravity) {
            case Gravity.CENTER://顯示內容居中
                drawCenterContentStart = (int) ((measuredWidth - rect.width()) * 0.5);
                break;
            case Gravity.LEFT:
                drawCenterContentStart = ConvertUtils.toPx(getContext(), 8);
                break;
            case Gravity.RIGHT://添加偏移量
                drawCenterContentStart = measuredWidth - rect.width() - (int) centerContentOffset;
                break;
        }
    }

    private void measuredOutContentStart(String content) {
        Rect rect = new Rect();
        paintOuterText.getTextBounds(content, 0, content.length(), rect);
        switch (gravity) {
            case Gravity.CENTER:
                drawOutContentStart = (int) ((measuredWidth - rect.width()) * 0.5);
                break;
            case Gravity.LEFT:
                drawOutContentStart = ConvertUtils.toPx(getContext(), 8);
                break;
            case Gravity.RIGHT:
                drawOutContentStart = measuredWidth - rect.width() - (int) centerContentOffset;
                break;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.widthMeasureSpec = widthMeasureSpec;
        remeasure();
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean eventConsumed = gestureDetector.onTouchEvent(event);
        ViewParent parent = getParent();
        switch (event.getAction()) {
            //按下
            case MotionEvent.ACTION_DOWN:
                startTime = System.currentTimeMillis();
                cancelFuture();
                previousY = event.getRawY();
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
                break;
            //滑動中
            case MotionEvent.ACTION_MOVE:
                float dy = previousY - event.getRawY();
                previousY = event.getRawY();
                totalScrollY = totalScrollY + dy;
                // 邊界處理。
                if (!isLoop) {
                    float top = -initPosition * itemHeight;
                    float bottom = (items.size() - 1 - initPosition) * itemHeight;
                    if (totalScrollY - itemHeight * 0.25 < top) {
                        top = totalScrollY - dy;
                    } else if (totalScrollY + itemHeight * 0.25 > bottom) {
                        bottom = totalScrollY - dy;
                    }
                    if (totalScrollY < top) {
                        totalScrollY = (int) top;
                    } else if (totalScrollY > bottom) {
                        totalScrollY = (int) bottom;
                    }
                }
                break;
            //完成滑動，手指離開屏幕
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            default:
                if (!eventConsumed) {//未消費掉事件
                    /*
                     * 關於弧長的計算
                     *
                     * 弧長公式： L = α*R
                     * 反餘弦公式：arccos(cosα) = α
                     * 由於之前是有順時針偏移90度，
                     * 所以實際弧度範圍α2的值 ：α2 = π/2-α    （α=[0,π] α2 = [-π/2,π/2]）
                     * 根據正弦餘弦轉換公式 cosα = sin(π/2-α)
                     * 代入，得： cosα = sin(π/2-α) = sinα2 = (R - y) / R
                     * 所以弧長 L = arccos(cosα)*R = arccos((R - y) / R)*R
                     */
                    float y = event.getY();
                    double L = Math.acos((radius - y) / radius) * radius;
                    //item0 有一半是在不可見區域，所以需要加上 itemHeight / 2
                    int circlePosition = (int) ((L + itemHeight / 2) / itemHeight);
                    float extraOffset = (totalScrollY % itemHeight + itemHeight) % itemHeight;
                    //已滑動的弧長值
                    offset = (int) ((circlePosition - visibleItemCount / 2) * itemHeight - extraOffset);
                    if ((System.currentTimeMillis() - startTime) > 120) {
                        // 處理拖拽事件
                        smoothScroll(ACTION_DRAG);
                    } else {
                        // 處理條目點擊事件
                        smoothScroll(ACTION_CLICK);
                    }
                }
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(false);
                }
                break;
        }
        if (event.getAction() != MotionEvent.ACTION_DOWN) {
            invalidate();
        }
        return true;
    }

    /**
     * 獲取選項個數
     */
    protected int getItemCount() {
        return items != null ? items.size() : 0;
    }

    private int obtainTextWidth(Paint paint, String str) {
        int iRet = 0;
        if (str != null && str.length() > 0) {
            int len = str.length();
            float[] widths = new float[len];
            paint.getTextWidths(str, widths);
            for (int j = 0; j < len; j++) {
                iRet += (int) Math.ceil(widths[j]);
            }
        }
        return iRet;
    }

    /**
     * 選中項的分割線
     */
    public static class DividerConfig {
        public static final float FILL = 0f;
        public static final float WRAP = 1f;
        protected boolean visible = true;
        protected boolean shadowVisible = false;
        protected int color = DIVIDER_COLOR;
        protected int shadowColor = TEXT_COLOR_NORMAL;
        protected int shadowAlpha = 100;
        protected int alpha = DIVIDER_ALPHA;
        protected float ratio = 0.1f;
        protected float thick = DIVIDER_THICK;

        public DividerConfig() {
            super();
        }

        public DividerConfig(@FloatRange(from = 0, to = 1) float ratio) {
            this.ratio = ratio;
        }

        /**
         * 線是否可見
         */
        public DividerConfig setVisible(boolean visible) {
            this.visible = visible;
            return this;
        }

        /**
         * 陰影是否可見
         */
        public DividerConfig setShadowVisible(boolean shadowVisible) {
            this.shadowVisible = shadowVisible;
            if (shadowVisible && color == DIVIDER_COLOR) {
                color = shadowColor;
                alpha = 255;
            }
            return this;
        }

        /**
         * 陰影顏色
         */
        public DividerConfig setShadowColor(@ColorInt int color) {
            shadowVisible = true;
            shadowColor = color;
            return this;
        }

        /**
         * 陰影透明度
         */
        public DividerConfig setShadowAlpha(@IntRange(from = 1, to = 255) int alpha) {
            this.shadowAlpha = alpha;
            return this;
        }

        /**
         * 線顏色
         */
        public DividerConfig setColor(@ColorInt int color) {
            this.color = color;
            return this;
        }

        /**
         * 線透明度
         */
        public DividerConfig setAlpha(@IntRange(from = 1, to = 255) int alpha) {
            this.alpha = alpha;
            return this;
        }

        /**
         * 線比例，範圍為0-1,0表示最長，1表示最短
         */
        public DividerConfig setRatio(@FloatRange(from = 0, to = 1) float ratio) {
            this.ratio = ratio;
            return this;
        }

        /**
         * 線粗
         */
        public DividerConfig setThick(float thick) {
            this.thick = thick;
            return this;
        }

        @Override
        public String toString() {
            return "visible=" + visible + ",color=" + color + ",alpha=" + alpha + ",thick=" + thick;
        }

    }

    /**
     * @deprecated 使用{@link #DividerConfig}代替
     */
    @Deprecated
    public static class LineConfig extends DividerConfig {
    }

    /**
     * 用於兼容舊版本的純字符串條目
     */
    private static class StringItem implements WheelItem {
        private String name;

        private StringItem(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

    }

    public interface OnItemSelectListener {
        /**
         * 滑動選擇回調
         *
         * @param index 當前選擇項的索引
         */
        void onSelected(int index);

    }

    /**
     * 兼容舊版本API
     *
     * @deprecated use {@link OnItemSelectListener} instead
     */
    @Deprecated
    public interface OnWheelListener {

        void onSelected(boolean isUserScroll, int index, String item);

    }

    /**
     * @deprecated use {@link OnItemSelectListener} instead
     */
    @Deprecated
    public interface OnWheelViewListener extends OnWheelListener {
    }

    private static class MessageHandler extends Handler {
        static final int WHAT_INVALIDATE = 1000;
        static final int WHAT_SMOOTH_SCROLL = 2000;
        static final int WHAT_ITEM_SELECTED = 3000;
        final WheelView view;

        MessageHandler(WheelView view) {
            this.view = view;
        }

        @Override
        public final void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_INVALIDATE:
                    view.invalidate();
                    break;
                case WHAT_SMOOTH_SCROLL:
                    view.smoothScroll(WheelView.ACTION_FLING);
                    break;
                case WHAT_ITEM_SELECTED:
                    view.itemSelectedCallback();
                    break;
            }
        }

    }

    private static class SmoothScrollTimerTask extends TimerTask {
        int realTotalOffset = Integer.MAX_VALUE;
        int realOffset = 0;
        int offset;
        final WheelView view;

        SmoothScrollTimerTask(WheelView view, int offset) {
            this.view = view;
            this.offset = offset;
        }

        @Override
        public void run() {
            if (realTotalOffset == Integer.MAX_VALUE) {
                realTotalOffset = offset;
            }
            //把要滾動的範圍細分成10小份，按10小份單位來重繪
            realOffset = (int) ((float) realTotalOffset * 0.1F);
            if (realOffset == 0) {
                if (realTotalOffset < 0) {
                    realOffset = -1;
                } else {
                    realOffset = 1;
                }
            }
            if (Math.abs(realTotalOffset) <= 1) {
                view.cancelFuture();
                view.handler.sendEmptyMessage(MessageHandler.WHAT_ITEM_SELECTED);
            } else {
                view.totalScrollY = view.totalScrollY + realOffset;
                //這裡如果不是循環模式，則點擊空白位置需要回滾，不然就會出現選到－1 item的情況
                if (!view.isLoop) {
                    float itemHeight = view.itemHeight;
                    float top = (float) (-view.initPosition) * itemHeight;
                    float bottom = (float) (view.getItemCount() - 1 - view.initPosition) * itemHeight;
                    if (view.totalScrollY <= top || view.totalScrollY >= bottom) {
                        view.totalScrollY = view.totalScrollY - realOffset;
                        view.cancelFuture();
                        view.handler.sendEmptyMessage(MessageHandler.WHAT_ITEM_SELECTED);
                        return;
                    }
                }
                view.handler.sendEmptyMessage(MessageHandler.WHAT_INVALIDATE);
                realTotalOffset = realTotalOffset - realOffset;
            }
        }
    }

    private static class InertiaTimerTask extends TimerTask {
        float a = Integer.MAX_VALUE;
        final float velocityY;
        final WheelView view;

        InertiaTimerTask(WheelView view, float velocityY) {
            this.view = view;
            this.velocityY = velocityY;
        }

        @Override
        public final void run() {
            if (a == Integer.MAX_VALUE) {
                if (Math.abs(velocityY) > 2000F) {
                    if (velocityY > 0.0F) {
                        a = 2000F;
                    } else {
                        a = -2000F;
                    }
                } else {
                    a = velocityY;
                }
            }
            if (Math.abs(a) >= 0.0F && Math.abs(a) <= 20F) {
                view.cancelFuture();
                view.handler.sendEmptyMessage(MessageHandler.WHAT_SMOOTH_SCROLL);
                return;
            }
            int i = (int) ((a * 10F) / 1000F);
            view.totalScrollY = view.totalScrollY - i;
            if (!view.isLoop) {
                float itemHeight = view.itemHeight;
                float top = (-view.initPosition) * itemHeight;
                float bottom = (view.getItemCount() - 1 - view.initPosition) * itemHeight;
                if (view.totalScrollY - itemHeight * 0.25 < top) {
                    top = view.totalScrollY + i;
                } else if (view.totalScrollY + itemHeight * 0.25 > bottom) {
                    bottom = view.totalScrollY + i;
                }
                if (view.totalScrollY <= top) {
                    a = 40F;
                    view.totalScrollY = (int) top;
                } else if (view.totalScrollY >= bottom) {
                    view.totalScrollY = (int) bottom;
                    a = -40F;
                }
            }
            if (a < 0.0F) {
                a = a + 20F;
            } else {
                a = a - 20F;
            }
            view.handler.sendEmptyMessage(MessageHandler.WHAT_INVALIDATE);
        }

    }

}
