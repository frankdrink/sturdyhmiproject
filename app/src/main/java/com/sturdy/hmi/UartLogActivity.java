package com.sturdy.hmi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.adapter.MainMenuGridViewAdapter;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.ReturnMessage;
import com.sturdy.hmi.utils.KyEditText;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;

import com.sturdy.hmi.adapter.GridViewAdapter;
import com.sturdy.hmi.adapter.RecyclerTouchListener;

import dma.xch.hmi.api.UntiTools;

import static com.sturdy.hmi.Constants.ACTION_TO_SERVICE;
import static com.sturdy.hmi.Constants.saveDateFormat;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveTimeFormat;

public class UartLogActivity extends BaseToolBarActivity{
    private Handler handler = new Handler();
    private RecyclerView listView;
    private RecyclerView gridView;
    private MainMenuGridViewAdapter gridViewAdapter;
    private ArrayList<RecyclerViewItem> corporations;
    private ArrayList<RecyclerViewItem> recyclerViewButton,recyclerViewButton_press;
    ScrollView svMsg;
    TextView tvMsg;
    EditText etMsg;

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            String receviceMsg = "";
            calendar = Calendar.getInstance();
            String topTime = "";
            topTime = String.format("%02d:%02d:%02d:",calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));

            for (int i=0;i<GlobalClass.getInstance().globalArrayList.size();i++){
                char[] chars = GlobalClass.getInstance().globalArrayList.get(i);
                if (chars.length>0) {
                    char[] buf = new char[chars.length-1];
                    System.arraycopy(chars, 1, buf, 0, chars.length-1);
                    receviceMsg = UntiTools.CharToString(buf,buf.length);
                    if (chars[0]==0xff) {
                        topTime = "HMI->CB:";
                        tvMsg.append(topTime+receviceMsg+"\n");
                    } else {
                        tvMsg.append(topTime+receviceMsg+"\n");
                    }
                }
            }
            GlobalClass.getInstance().globalArrayList.clear();
            svMsg.scrollTo(0,svMsg.getScrollY()+svMsg.getMeasuredHeight());

            handler.postDelayed(this, 1000);
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_uart_log);
        registerBaseActivityReceiver();
        svMsg = (ScrollView)findViewById(R.id.svMsg);
        tvMsg = (TextView) findViewById(R.id.tvMsg);
        etMsg = (EditText) findViewById(R.id.etMsg);
        etMsg.setOnEditorActionListener(
                new KyEditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                                ) {

                            if (etMsg.getText().length()>0){
                                sendUartMsg(etMsg.getText().toString());
                                etMsg.setText("");
                                return false;
                            }

                        }
                        return false;
                    }
                });
        handler.postAtTime(runnable, 1000);

        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

    }

    public boolean judgeHex(String str){
        if(str.contains(" ")){
            str = str.replaceAll(" ","").trim();
        }
        int len = str.length();
        if(len%2!=0){
            Toast.makeText(this,"請輸入Hex字串!",Toast.LENGTH_SHORT).show();
            return  false;
        }
        if(str.toUpperCase().contains("G") || str.toUpperCase().contains("H") || str.toUpperCase().contains("I")
                || str.toUpperCase().contains("J") || str.toUpperCase().contains("K") || str.toUpperCase().contains("L")
                || str.toUpperCase().contains("M") || str.toUpperCase().contains("N") || str.toUpperCase().contains("O")
                || str.toUpperCase().contains("P") || str.toUpperCase().contains("Q") || str.toUpperCase().contains("R")
                || str.toUpperCase().contains("S") || str.toUpperCase().contains("T") || str.toUpperCase().contains("U")
                || str.toUpperCase().contains("V") || str.toUpperCase().contains("W") || str.toUpperCase().contains("X")
                || str.toUpperCase().contains("Y") || str.toUpperCase().contains("Z")){
            Toast.makeText(this,"請輸入Hex字串!",Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;
    }

    public void sendUartMsg(String sendMsg){
        if(judgeHex(sendMsg)){
            StringBuffer sbSend = new StringBuffer();
            byte[] sendMsgBytes = sendMsg.getBytes();
            for(int j=0;j<sendMsgBytes.length;j++){
                byte b = sendMsgBytes[j];
                if(j==0){
                    sbSend.append(b+"");
                }else{
                    sbSend.append(" "+b);
                }
            }
            sendMsg = sendMsg.replaceAll(" ","");
            char[] dataChars = new char[sendMsg.length()/2];
            UntiTools.StringToChar(sendMsg,dataChars,sendMsg.length()/2);
            sendToService(dataChars);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed()
    {
    }



}
