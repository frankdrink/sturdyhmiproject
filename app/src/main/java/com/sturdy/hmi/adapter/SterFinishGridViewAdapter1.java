package com.sturdy.hmi.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.sturdy.hmi.R;
import com.sturdy.hmi.RecyclerViewItem;
import com.sturdy.hmi.item.StartSterInformationItemA;

public class SterFinishGridViewAdapter1 extends RecyclerView.Adapter<SterFinishGridViewAdapter1.ViewHolder> {
    private List<StartSterInformationItemA> items;
    private Activity activity;

    public SterFinishGridViewAdapter1(Activity activity, List<StartSterInformationItemA> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.sterilizationfinish_grid1, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SterFinishGridViewAdapter1.ViewHolder viewHolder,final int position) {
        viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());

        viewHolder.imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    viewHolder.imageView.setBackgroundResource(items_press.get(position).getDrawableId());
                    onItemClickListener.onItemClick(position);

//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_pressed));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    viewHolder.imageView.setBackgroundResource(items.get(position).getDrawableId());

//                    viewHolder.imageView.setTextColor(getResources().getColor(R.color.text_normal));
                }
                return false;
            }
        });

        viewHolder.name_textView.setText(items.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageButton imageView;
        private TextView name_textView;


        public ViewHolder(View view) {
            super(view);
            name_textView = (TextView)view.findViewById(R.id.text);
            imageView = (ImageButton) view.findViewById(R.id.image);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
