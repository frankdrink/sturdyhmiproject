package com.sturdy.hmi.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.widget.EditText;

import com.sturdy.hmi.R;

/**
 * Created by franklin on 2019/8/14.
 */
public class KyEditText extends android.support.v7.widget.AppCompatEditText {
    private int border;
    private int borderColor;
    private int radius;

    public KyEditText(Context context) {
        super(context);
    }
    public KyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFilters(new InputFilter[]{new EmojiExcludeFilter()});
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.KyEditText);
        border = (int)a.getDimension(R.styleable.KyEditText_border, 0);

        borderColor = (int)a.getColor(R.styleable.KyEditText_borderColor, 0);
        if(borderColor == 0)
            borderColor = a.getResourceId(R.styleable.KyEditText_borderColor, 0);

        radius = (int)a.getDimension(R.styleable.KyEditText_radius, 0);
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画边框
        drawBorder(canvas);
    }
    /**
     * 画边框
     * @param border
     */
    private void drawBorder(Canvas canvas){
        if(this.borderColor == 0)
            return;
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(borderColor);
        paint.setStrokeWidth(border);
        RectF rectF = new RectF();
        int halfBorder = this.border/2;
        halfBorder = halfBorder == 0? 1 : halfBorder;

        rectF.left = halfBorder + this.getScrollX();
        rectF.top = halfBorder + this.getScrollY();
        rectF.right = this.getWidth()-halfBorder-1 + this.getScrollX();
        rectF.bottom = this.getHeight()-halfBorder-1 + this.getScrollY();
        canvas.drawRoundRect(rectF, radius, radius, paint);
    }

    private class EmojiExcludeFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                int type = Character.getType(source.charAt(i));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        }
    }
}
