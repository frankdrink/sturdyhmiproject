package com.sturdy.hmi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.List;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;

public class User1EditActivity extends BaseToolBarActivity implements NumberDialog.OnNumberDialogDoneListener,SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;
    private TextView mCTTextView;
    private TextView mFTTextView;
    private String[] pickerReservationArrayList;
    private String[] pickerDissolTempArrayList;
    private String[] pickerExhaustLevelArrayList;
    private String[] pickerInsulationTempArrayList;
    private String[] pickerInsulationTimeArrayList;
    private String[] pickerTempArrayList;

    private String[] pickerCTTempArrayList;
    private int[] iPickerCTTempArrayList;
    private String[] pickerFTTempArrayList;
    private int[] iPickerFTTempArrayList;
    private String[] pickerDryTimeArrayList;

    private String[] switchArray = {"ON","OFF"};

    private int highTemp;
    private int lowTemp;
    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String str;

    private boolean isFirstPrescan = true;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x12) { // ID(0~11); 預約時間(0~99); 預抽真空(ON/OFF); 溶解溫度(600~1000,Word,0.1℃); 溶解時間(60~18000‬‬,Word,秒);
                    // 滅菌溫度(1050~1350,Word,0.1℃); 滅菌時間(0~25140,Word,秒);Floating sensor(ON/OFF);風扇冷卻(ON/OFF);
                    // 高壓快速冷卻(ON/OFF);排壓等級(0~10);乾燥時間 (0~3600,Word,秒);保溫溫度(450~600,Word,0.1℃);
                    // 保溫時間(0~24);開門溫度CT(600~800,Word,0.1C);開門溫度FT(600~950,Word,0.1C);
                    GlobalClass.getInstance().CB_Reservation = recv[3];
                    GlobalClass.getInstance().CB_PreVacuum = recv[4]!=0;
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 5, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DissolTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 7, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DissolTime = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 9, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 11, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTime = CharUtils.charArrayToInt(ch2);
                    GlobalClass.getInstance().isFloatingTemp = recv[13]!=0;
                    GlobalClass.getInstance().CB_FanCooling = recv[14]!=0;
                    GlobalClass.getInstance().CB_PressurizedCooling = recv[15]!=0;
                    GlobalClass.getInstance().CB_ExhaustLevel = recv[16];
                    System.arraycopy(recv, 17, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DryTime = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 19, ch2, 0, 2);
                    GlobalClass.getInstance().CB_InsulationTemp = CharUtils.charArrayToInt(ch2);
                    GlobalClass.getInstance().CB_InsulationTime = recv[21]*60*60;
                    System.arraycopy(recv, 22, ch2, 0, 2);
                    GlobalClass.getInstance().CB_CTTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 24, ch2, 0, 2);
                    GlobalClass.getInstance().CB_FTTemp = CharUtils.charArrayToInt(ch2);
                    str = Integer.toString(GlobalClass.getInstance().CB_Reservation);
                    mItems.set(0, new RecyclerViewClass("Reservation", str, R.drawable.parameter_icon_time,true));
                    if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                        str = GlobalClass.getInstance().CB_PreVacuum == true ? "ON" : "OFF";
                        mItems.set(1, new RecyclerViewClass("Pre-Vacuum", str, R.drawable.parameter_icon_prevacuum, true));
                    }else{
                        mItems.set(1, new RecyclerViewClass("", "", R.drawable.parameter_icon_prevacuum,true));
                    }
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_DissolTemp);
                    mItems.set(2, new RecyclerViewClass("Dissol. Temp.", str, R.drawable.parameter_icon_temp,true));
                    str = UnitConvert.formatTimeHM(GlobalClass.getInstance().CB_DissolTime);
                    mItems.set(3, new RecyclerViewClass("Dissol. Time.", str, R.drawable.parameter_icon_time,true));
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_SteriTemp);
                    mItems.set(4, new RecyclerViewClass("Ster. Temp.", str, R.drawable.parameter_icon_temp,true));
                    str = UnitConvert.formatTimeHM(GlobalClass.getInstance().CB_SteriTime);
                    mItems.set(5, new RecyclerViewClass("Ster. Time", str, R.drawable.parameter_icon_time,true));
                    str = GlobalClass.getInstance().isFloatingTemp == true ? "ON" : "OFF";
                    mItems.set(6, new RecyclerViewClass("Floating Sensor", str, R.drawable.parameter_icon_floating_sensor,true));
                    str = GlobalClass.getInstance().CB_FanCooling == true ? "ON" : "OFF";
                    mItems.set(7, new RecyclerViewClass("Fan Cooling", str, R.drawable.parameter_icon_cooling_fan,true));
                    if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
                        str = GlobalClass.getInstance().CB_PressurizedCooling == true ? "ON" : "OFF";
                        mItems.set(8, new RecyclerViewClass("Pressurized Cooling", str, R.drawable.parameter_icon_pressurized_cooling, true));
                    }else{
                        mItems.set(8, new RecyclerViewClass("", "", R.drawable.parameter_icon_pressurized_cooling, true));
                    }
                    str = Integer.toString(GlobalClass.getInstance().CB_ExhaustLevel);
                    mItems.set(9, new RecyclerViewClass("Exhaust Level", str, R.drawable.parameter_icon_exhaust_level,true));
                    str = UnitConvert.formatTimeM(GlobalClass.getInstance().CB_DryTime);
                    mItems.set(10, new RecyclerViewClass("Dry Time", str, R.drawable.parameter_icon_time,true));
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_InsulationTemp);
                    mItems.set(11, new RecyclerViewClass("Insulation Temp", str, R.drawable.parameter_icon_temp,true));
                    str = UnitConvert.formatTimeH(GlobalClass.getInstance().CB_InsulationTime);
                    mItems.set(12, new RecyclerViewClass("Insulation Time", str, R.drawable.parameter_icon_time,true));

                    mAdapter.notifyDataSetChanged();
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_CTTemp);
                    mCTTextView.setText(str);
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_FTTemp);
                    mFTTextView.setText(str);
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user1_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        pickerReservationArrayList = new String[100];
        for (int i=0;i<=99;i++) {
            pickerReservationArrayList[i] = Integer.toString(i)+" h";
        }
        pickerExhaustLevelArrayList = new String[]{
                "0",  "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9",  "10"
        };

        highTemp = UnitConvert.TempConvert(100);
        lowTemp = UnitConvert.TempConvert(60);
        pickerDissolTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerDissolTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }
        highTemp = UnitConvert.TempConvert(135);
        lowTemp = UnitConvert.TempConvert(105);
        pickerTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }

        pickerDryTimeArrayList = new String[61];
        for (int i=0;i<=60;i++) {
            pickerDryTimeArrayList[i] = Integer.toString(i)+" m";
        }
        highTemp = UnitConvert.TempConvert(60);
        lowTemp = UnitConvert.TempConvert(45);
        pickerInsulationTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerInsulationTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }
        pickerInsulationTimeArrayList = new String[25];
        for (int i=0;i<=24;i++) {
            pickerInsulationTimeArrayList[i] = Integer.toString(i)+" h";
        }
//        highTemp = UnitConvert.TempConvert(80);
//        lowTemp = UnitConvert.TempConvert(60);
//        pickerCTTempArrayList = new String[highTemp-lowTemp+1];
//        iPickerCTTempArrayList = new int[highTemp-lowTemp+1];

        highTemp = UnitConvert.TempConvert(95);
        lowTemp = UnitConvert.TempConvert(60);
        pickerCTTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerCTTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }
        highTemp = UnitConvert.TempConvert(95);
        lowTemp = UnitConvert.TempConvert(60);
        pickerFTTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerFTTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_2_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_3_button, 64);


        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();
        mItems.add(0, new RecyclerViewClass("Reservation", "", R.drawable.parameter_icon_time,true));
        if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
            mItems.add(1, new RecyclerViewClass("Pre-Vacuum", "", R.drawable.parameter_icon_prevacuum, true));
        }else{
            mItems.add(1, new RecyclerViewClass("", "", R.drawable.parameter_icon_prevacuum,true));
        }
        mItems.add(2, new RecyclerViewClass("Dissol. Temp.", "", R.drawable.parameter_icon_temp,true));
        mItems.add(3, new RecyclerViewClass("Dissol. Time.", "", R.drawable.parameter_icon_time,true));
        mItems.add(4, new RecyclerViewClass("Ster. Temp.", "", R.drawable.parameter_icon_temp,true));
        mItems.add(5, new RecyclerViewClass("Ster. Time", "", R.drawable.parameter_icon_time,true));
        mItems.add(6, new RecyclerViewClass("Floating Sensor", "", R.drawable.parameter_icon_floating_sensor,true));
        mItems.add(7, new RecyclerViewClass("Fan Cooling", "", R.drawable.parameter_icon_cooling_fan,true));
        if (Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0) {
            mItems.add(8, new RecyclerViewClass("Pressurized Cooling", "", R.drawable.parameter_icon_pressurized_cooling, true));
        }else{
            mItems.add(8, new RecyclerViewClass("", "", R.drawable.parameter_icon_pressurized_cooling, true));
        }
        mItems.add(9, new RecyclerViewClass("Exhaust Level", "", R.drawable.parameter_icon_exhaust_level,true));
        mItems.add(10, new RecyclerViewClass("Dry Time", "", R.drawable.parameter_icon_time,true));
        mItems.add(11, new RecyclerViewClass("Insulation Temp", "", R.drawable.parameter_icon_temp,true));
        mItems.add(12, new RecyclerViewClass("Insulation Time", "", R.drawable.parameter_icon_time,true));

        mAdapter = new SteriEditAdapter(this, mItems, true);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mActivity,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mActivity,User1InformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        mCTTextView = (TextView) findViewById(R.id.door_open_edit_ct_textview);
        mCTTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionPicker picker = new OptionPicker(mActivity, pickerCTTempArrayList);
                picker.setCanceledOnTouchOutside(false);
                picker.setTitleText("C.T.");
                picker.setSelectedItem(String.valueOf(mCTTextView.getText()));
                picker.setTopHeight(50);
                picker.setSubmitTextSize(40);
                picker.setTitleTextSize(40);
                picker.setDividerRatio(WheelView.DividerConfig.FILL);
                picker.setShadowColor(Color.WHITE, 40);
                picker.setCycleDisable(true);
                picker.setTextSize(32);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
                        str = item;
                        str = str.replaceAll("[^\\d]+", "");
                        GlobalClass.getInstance().CB_CTTemp= UnitConvert.TempConvertF(Integer.valueOf(str)) *10;
                        mCTTextView.setText(item);
                    }
                });
                picker.show();
            }
        });

        mFTTextView = (TextView) findViewById(R.id.door_open_edit_ft_textview);
        mFTTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionPicker picker = new OptionPicker(mActivity, pickerFTTempArrayList);
                picker.setCanceledOnTouchOutside(false);
                picker.setTitleText("F.T.");
                picker.setSelectedItem(String.valueOf(mFTTextView.getText()));
                picker.setTopHeight(50);
                picker.setSubmitTextSize(40);
                picker.setTitleTextSize(40);
                picker.setDividerRatio(WheelView.DividerConfig.FILL);
                picker.setShadowColor(Color.WHITE, 40);
                picker.setCycleDisable(true);
                picker.setTextSize(32);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
                        str = item;
                        str = str.replaceAll("[^\\d]+", "");
                        GlobalClass.getInstance().CB_FTTemp = UnitConvert.TempConvertF(Integer.valueOf(str)) * 10;
                        mFTTextView.setText(item);
                    }
                });
                picker.show();
            }
        });

        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bFieldmodified){
//                    bFieldmodified = false;
//                    mAdapter.setTextViewEditable(bFieldmodified);
//                    mStartSterButton.setText("Start");
//                }else{
//                    startActivity(new Intent(CustomizationEditActivity.this, StartSterilizationActivity.class));
//                    overridePendingTransition(0, 0);
//                    finish();
//                }
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(mActivity, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendCustomizationSetting();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendCustomizationSetting();
                    sendGetSterillzationStep();
                }
            }
        });
        // 取得消毒行程資(User 1)
        char[] data = new char[]{0x02,0x12,GlobalClass.getInstance().cLoginUserID};
        sendToService(data);

    }

    void sendCustomizationSetting() { // 預約時間(0~99); 預抽真空(ON/OFF); 溶解溫度(600~1000,Word,0.1℃);
        // 溶解時間(60~18000‬‬,Word,秒); 滅菌溫度(1050~1350,Word,0.1℃); 滅菌時間(0~25140,Word,秒);
        // Floating sensor(ON/OFF);風扇冷卻(ON/OFF);高壓快速冷卻(ON/OFF);排壓等級(0~10);乾燥時間 (0~3600,Word,秒);
        // 保溫溫度(450~600,Word,0.1℃); 保溫時間(0~24);開門溫度CT(600~800,Word,0.1C);開門溫度FT(600~950,Word,0.1C);
        char[] data = new char[25];
        char[] ch22 = new char[2];
        data[0] = 0x18;
        data[1] = 0x2e;
        data[2] = (char)GlobalClass.getInstance().CB_Reservation;
        data[3] = GlobalClass.getInstance().CB_PreVacuum ? (char)1:(char)0;
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_DissolTemp);
        data[4] = ch22[0];
        data[5] = ch22[1];
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_DissolTime);
        data[6] = ch22[0];
        data[7] = ch22[1];
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_SteriTemp));
        data[8] = ch22[0];
        data[9] = ch22[1];
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_SteriTime));
        data[10] = ch22[0];
        data[11] = ch22[1];
        data[12] = GlobalClass.getInstance().isFloatingTemp ? (char)1:(char)0;
        data[13] = GlobalClass.getInstance().CB_FanCooling ? (char)1:(char)0;
        data[14] = GlobalClass.getInstance().CB_PressurizedCooling ? (char)1:(char)0;
        data[15] = (char)GlobalClass.getInstance().CB_ExhaustLevel;
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_DryTime));
        data[16] = ch22[0];
        data[17] = ch22[1];
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_InsulationTemp));
        data[18] = ch22[0];
        data[19] = ch22[1];
        data[20] = (char)(GlobalClass.getInstance().CB_InsulationTime/60/60);
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_CTTemp));
        data[21] = ch22[0];
        data[22] = ch22[1];
        ch22 = CharUtils.fromShort((short)(GlobalClass.getInstance().CB_FTTemp));
        data[23] = ch22[0];
        data[24] = ch22[1];
        sendToService(data);
    }



    @Override
    public void updateItemList(final int position) {
        if (position==0) {
            OptionPicker picker = new OptionPicker(mActivity, pickerReservationArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_Reservation = Integer.valueOf(str);
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==1) {
            OptionPicker picker = new OptionPicker(mActivity, switchArray);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_PreVacuum = index==0?true:false;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==2) {
            OptionPicker picker = new OptionPicker(mActivity, pickerDissolTempArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_DissolTemp = UnitConvert.TempConvertF(Integer.valueOf(str))*10;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==3) {
            TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerVisible(true);
            picker.setUseWeight(false);
            picker.setCycleDisable(false);
            picker.setRangeStart(0, 0);//00:00
            picker.setRangeEnd(6, 59);//23:59
            int currentHour = GlobalClass.getInstance().CB_DissolTime/3600;
            int currentMinute = (GlobalClass.getInstance().CB_DissolTime%3600)/60;
            picker.setSelectedItem(currentHour, currentMinute);
            picker.setTopLineVisible(true);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setTextPadding(ConvertUtils.toPx(this, 15));
            picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
                @Override
                public void onTimePicked(String hour, String minute, String timeAt) {
                    GlobalClass.getInstance().CB_DissolTime = Integer.valueOf(hour)*60*60+Integer.valueOf(minute)*60;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), hour+" h "+minute+" m", mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==4) {
            OptionPicker picker = new OptionPicker(mActivity, pickerTempArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_SteriTemp = UnitConvert.TempConvertF(Integer.valueOf(str))*10;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==5) {
            TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerVisible(true);
            picker.setUseWeight(false);
            picker.setCycleDisable(false);
            picker.setRangeStart(0, 0);//00:00
            picker.setRangeEnd(6, 59);//23:59
            int currentHour = GlobalClass.getInstance().CB_SteriTime/3600;
            int currentMinute = (GlobalClass.getInstance().CB_SteriTime%3600)/60;
            picker.setSelectedItem(currentHour, currentMinute);
            picker.setTopLineVisible(true);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setTextPadding(ConvertUtils.toPx(this, 15));
            picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
                @Override
                public void onTimePicked(String hour, String minute, String timeAt) {
                    GlobalClass.getInstance().CB_SteriTime = Integer.valueOf(hour)*60*60+Integer.valueOf(minute)*60;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), hour+" h "+minute+" m", mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==6) {    // Floating Sensor
            OptionPicker picker = new OptionPicker(mActivity, switchArray);
            picker.setCanceledOnTouchOutside(false);
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().isFloatingTemp = index==0?true:false;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==7) {    // Fan Cooling
            OptionPicker picker = new OptionPicker(mActivity, switchArray);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_FanCooling = index==0?true:false;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==8) {    // Pressurized Cooling
            OptionPicker picker = new OptionPicker(mActivity, switchArray);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_PressurizedCooling = index==0?true:false;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==9) {
            OptionPicker picker = new OptionPicker(mActivity, pickerExhaustLevelArrayList);
            picker.setCanceledOnTouchOutside(false);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    GlobalClass.getInstance().CB_ExhaustLevel = index;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==10) {
            OptionPicker picker = new OptionPicker(mActivity, pickerDryTimeArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_DryTime = Integer.valueOf(str)*60;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==11) {
            OptionPicker picker = new OptionPicker(mActivity, pickerInsulationTempArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_InsulationTemp = UnitConvert.TempConvertF(Integer.valueOf(str))*10;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else if (position==12) {
            OptionPicker picker = new OptionPicker(mActivity, pickerInsulationTimeArrayList);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setSelectedItem(mItems.get(position).getMessage2());
            picker.setTitleTextSize(40);
            picker.setCanceledOnTouchOutside(true);
            picker.setTopHeight(50);
            picker.setSubmitTextSize(40);
            picker.setTitleText(mItems.get(position).getMessage1());
            picker.setTitleTextSize(40);
            picker.setDividerRatio(WheelView.DividerConfig.FILL);
            picker.setShadowColor(Color.WHITE, 40);
            picker.setCycleDisable(true);
            picker.setTextSize(32);
            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                @Override
                public void onOptionPicked(int index, String item) {
                    str = item;
                    str = str.replaceAll("[^\\d]+", "");
                    GlobalClass.getInstance().CB_InsulationTime = Integer.valueOf(str)*60*60;
                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
                    mItems.set(position,recycler);
                    mAdapter.notifyDataSetChanged();
                }
            });
            picker.show();
        }else {
            NumberDialog myDiag =
                    NumberDialog.newInstance(4, 1000);
            myDiag.show(getSupportFragmentManager(), "altitude");
        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }

    @Override
    public void onDone(int value) {
//        iNextService = value;
//        valueStr = Integer.toString(iNextService);
//        data1.set(1,valueStr);
//        adapter1.resetAdapter(data1);
//        adapter1.notifyDataSetChanged();
    }
}
