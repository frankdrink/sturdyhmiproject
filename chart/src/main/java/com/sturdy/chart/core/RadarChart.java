package com.sturdy.chart.core;

import android.content.Context;
import android.util.AttributeSet;

import com.sturdy.chart.core.base.BaseRotateChart;
import com.sturdy.chart.data.RadarData;
import com.sturdy.chart.matrix.RotateHelper;
import com.sturdy.chart.provider.radar.RadarProvider;



public class RadarChart extends BaseRotateChart<RadarProvider,RadarData> implements RotateHelper.OnRotateListener{


    public RadarChart(Context context) {
        super(context);
    }

    public RadarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RadarChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




    @Override
    protected RadarProvider initProvider(RotateHelper rotateHelper) {
        RadarProvider provider =  new RadarProvider();
        provider.setRotateHelper(rotateHelper);
        return provider;
    }
}
