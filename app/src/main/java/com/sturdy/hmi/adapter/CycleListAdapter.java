package com.sturdy.hmi.adapter;

import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sturdy.hmi.R;
import com.sturdy.hmi.RadioButton.InertCheckBox;
import com.sturdy.hmi.RecyclerViewItem;

public class CycleListAdapter extends BaseAdapter {
	private Context ctx;

	private LayoutInflater lInflater;
	private final String[] itemName;
	private List<String> data;
	UpdateMainClass updateMainClass;
	private final Integer[] imageId;
	private String listKindTag;

	public CycleListAdapter(Context context,String[] itemName, List<String> data, Integer[] imageId) {
		ctx = context;
		this.data = data;
		this.itemName = itemName;
		this.imageId = imageId;
		lInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(context instanceof UpdateMainClass){
			updateMainClass = (UpdateMainClass)context;
		}
	}


	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	class ViewHolder {
		ImageView iconImg;
		InertCheckBox radioButton;
		TextView mMsg1, mMsg2;
	}

	public void resetAdapter(List<String> data) {
		this.data = data;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = lInflater.inflate(R.layout.cyclecount_edit_item, parent, false);
			holder = new ViewHolder();
			holder.iconImg = (ImageView) convertView.findViewById(R.id.img);
			holder.iconImg.setImageResource(imageId[position]);
			holder.mMsg1 = (TextView) convertView.findViewById(R.id.power_txt);
			holder.mMsg2 = (TextView) convertView.findViewById(R.id.text_view2);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.mMsg1.setText(itemName[position]);

		holder.mMsg2.setText(data.get(position));
		if (position==0) {
			holder.mMsg2.setBackgroundResource(R.color.trans);
		}else{
			holder.mMsg2.setBackgroundResource(R.drawable.edittext_border);
		}
		holder.mMsg2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				updateMainClass.updateItemList(position);
			}
		});

		holder.iconImg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				updateMainClass.updateListBackground(position,true);
			}
		});

		return convertView;
	}

	public interface UpdateMainClass{
		void updateItemList(int itemPostion);
		void updateListBackground(int position, boolean isChecked);
	}
}