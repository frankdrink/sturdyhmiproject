package com.sturdy.chart.provider;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.animation.Interpolator;

import com.sturdy.chart.core.base.BaseChart;
import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.ColumnData;
import com.sturdy.chart.listener.OnClickColumnListener;
import com.sturdy.chart.provider.component.mark.IMark;
import com.sturdy.chart.matrix.MatrixHelper;



public interface IProvider<C extends ColumnData> {

     boolean calculation( ChartData<C> chartData);

     void drawProvider(Canvas canvas, Rect rect, MatrixHelper helper, Paint paint);

     void clickPoint(PointF point);
     void startAnim(BaseChart chartView, int duration, Interpolator interpolator);

     void setMarkView(IMark markView);

     void setOnClickColumnListener(OnClickColumnListener<C> onClickColumnListener);

}
