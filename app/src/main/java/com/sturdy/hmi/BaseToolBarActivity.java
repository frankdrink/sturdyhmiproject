package com.sturdy.hmi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbDevice;
import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbManager;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.ACTION_TO_SERVICE;
import static com.sturdy.hmi.Constants.HMI_RMSLEEP_BROADCAST;
import static com.sturdy.hmi.Constants.HMI_SLEEP_BROADCAST;
import static com.sturdy.hmi.Constants.MACHINE_STATE.IDLE;
import static com.sturdy.hmi.Constants.saveDateFormat;
import static com.sturdy.hmi.Constants.saveRealPrint;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.saveTimeFormat;

public abstract class BaseToolBarActivity extends AppCompatActivity {
    Button rightTimeButton = null;
    UIAlphaImageButton leftButton1=null;
    UIAlphaImageButton leftButton2=null;
    UIAlphaImageButton leftButton3=null;
    UIAlphaImageButton leftButton4=null;
    UIAlphaImageButton leftButton5=null;
    TextView sleepTimeView = null;
    Activity mActivity;

    UIAlphaImageButton backButton = null;
    ImageView ivBack;
    Calendar calendar;
    CatCurrentTimeThread timeThread;
    boolean stillCat = true;
    boolean bServiceReceiver = false;
    public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
    private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
    public static final IntentFilter INTENT_FILTER = createIntentFilter();
    String programName = "";
    long programEndTime = 0;

    private BroadcastReceiver mServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x20) {
                    String className = mActivity.getLocalClassName();
                    if (className.contains("EditActivity")) {
                        if (!GlobalClass.getInstance().isStartSterillizationActivity) {
                            GlobalClass.getInstance().isStartSterillizationActivity = true;
                            System.arraycopy(recv, 2, GlobalClass.getInstance().globalStageList, 0, 20);
                            if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                                startActivity(new Intent(mActivity, StartSterilizationActivity.class));
                            }else{
                                startActivity(new Intent(mActivity, StartSterilizationActivity.class));
                            }
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                }else if (recv[1] == 0xf1) {

                }

            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        Constants.instance(this.getApplicationContext());
        mActivity = this;
    }

    public char getClassNameID() {
        char id = 0x00;
        int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            switch (menuSelectId) {
                case 0:
                    id = 0x0e;
                    break;
                case 1:
                    id = 0x0f;
                    break;
                case 2:
                    id = 0x10;
                    break;
                case 3:
                    id = 0x11;
                    break;
                case 4:
                    id = 0x12;
                    break;
                case 5:
                    id = 0x13;
                    break;
                case 6:
                    id = 0x14;
                    break;
                case 7:
                    id = 0x15;
                    break;
                case 8:
                    id = 0x16;
                    break;
                case 9:
                    id = 0x17;
                    break;
                case 10:
                    id = 0x0b;
                    break;
                case 11:
                    id = 0x09;
                    break;
                case 12:
                    id = 0x0d;
                    break;
                case 13:
                    id = 0x18;
                    break;
            }
        }else{
            switch (menuSelectId) {
                case 0:
                    id = 0x01;
                    break;
                case 1:
                    id = 0x02;
                    break;
                case 2:
                    id = 0x03;
                    break;
                case 3:
                    id = 0x04;
                    break;
                case 4:
                    id = 0x05;
                    break;
                case 5:
                    id = 0x06;
                    break;
                case 6:
                    id = 0x07;
                    break;
                case 7:
                    id = 0x09;
                    break;
                case 8:
                    id = 0x0a;
                    break;
                case 9:
                    id = 0x08;
                    break;
                case 10:
                    id = 0x0b;
                    break;
                case 11:
                    id = 0x0c;
                    break;
                case 12:
                    id = 0x0d;
                    break;
            }
        }

        return id;
    }

    public void setCustomTitleBar(CustomTitleBar mTopBar) {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitleBackColor));
        leftButton1 = mTopBar.addLeftImageButton(R.drawable.status_bar_icon_door_lock, R.id.topbar_right_1_button, 45,5);
        leftButton2 = mTopBar.addLeftImageButton(R.drawable.status_bar_icon_high_water_level, R.id.topbar_right_2_button, 45);
        leftButton3 = mTopBar.addLeftImageButton(R.drawable.status_bar_icon_usb, R.id.topbar_right_3_button, 45);
        leftButton4 = mTopBar.addLeftImageButton(R.drawable.status_bar_icon_barcode, R.id.topbar_right_4_button, 45);
        leftButton5 = mTopBar.addLeftImageButton(R.drawable.status_bar_icon_wifi, R.id.topbar_right_5_button, 45);
        leftButton1.setScaleType(ImageView.ScaleType.FIT_CENTER);
        leftButton2.setScaleType(ImageView.ScaleType.FIT_CENTER);
        leftButton3.setScaleType(ImageView.ScaleType.FIT_CENTER);
        leftButton4.setScaleType(ImageView.ScaleType.FIT_CENTER);
        leftButton5.setScaleType(ImageView.ScaleType.FIT_CENTER);
        leftButton4.setVisibility(View.GONE);
        rightTimeButton =         mTopBar.addRightTextButton("11:12 06/06/2019", R.id.topbar_right_time_button, Color.WHITE,5, 33);
        leftButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent;
//                intent = new Intent(mActivity, UartLogActivity.class);
//                startActivity(intent);
            }
        });
    }

    public void sendGetSterillzationStep() {
        char id = getClassNameID();
        char[] data = new char[]{0x02,0x20,id};
        sendToService(data);
        int menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));

        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            switch (menuSelectId) {
                case 0:
                    programName = "Liquid 1";
                    break;
                case 1:
                    programName = "Liquid 2";
                    break;
                case 2:
                    programName = "Solid 1";
                    break;
                case 3:
                    programName = "Solid 2";
                    break;
                case 4:
                    programName = "Agar";
                    break;
                case 5:
                    programName = "Dissolution";
                    break;
                case 6:
                    programName = "Dry Only";
                    break;
                case 7:
                    programName = "Waste";
                    break;
                case 8:
                    programName = "User 1";
                    break;
                case 9:
                    programName = "User 2";
                    break;
                case 10:
                    programName = "Leakage Test";
                    break;
                case 11:
                    programName = "Flash";
                    break;
                case 12:
                    programName = "B&D Test";
                    break;
                case 13:
                    programName = "Latex";
                    break;
            }
        }else{
            switch (menuSelectId){
                case 0:
                    programName = "Unwrapped 121";
                    break;
                case 1:
                    programName = "Unwrapped 134";
                    break;
                case 2:
                    programName = "Wrapped 121";
                    break;
                case 3:
                    programName = "Wrapped 134";
                    break;
                case 4:
                    programName = "Instrument 121";
                    break;
                case 5:
                    programName = "Instrument 134";
                    break;
                case 6:
                    programName = "Prion";
                    break;
                case 7:
                    programName = "Flash";
                    break;
                case 8:
                    programName = "Dry";
                    break;
                case 9:
                    programName = "Customization";
                    break;
                case 10:
                    programName = "Leakage test";
                    break;
                case 11:
                    programName = "Helix test";
                    break;
                case 12:
                    programName = "B&D test";
                    break;
            }
        }
        GlobalClass.getInstance().programStartTime = System.currentTimeMillis()/1000;
        programEndTime=3600;
//        new Thread(new Runnable(){
//            @Override
//            public void run() {
//                HttpPost myPost = new HttpPost("http://iot.opage.idv.tw/admin/base/sync_workflow");
//                Date date = new Date();
//                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss") ;
//                GlobalClass.getInstance().steriSN = dateFormat.format(date);
//                try {
//                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                    params.add(new BasicNameValuePair("programName",programName));
//                    params.add(new BasicNameValuePair("programStarttime",String.valueOf(GlobalClass.getInstance().programStartTime)));
//                    params.add(new BasicNameValuePair("programEndtime",String.valueOf(programEndTime)));
//                    params.add(new BasicNameValuePair("sn",GlobalClass.getInstance().CB_SN));
//                    params.add(new BasicNameValuePair("steriSN",GlobalClass.getInstance().steriSN));
//                    myPost.setEntity(new UrlEncodedFormEntity(params,  "utf-8"));
//                    HttpResponse response = new DefaultHttpClient().execute(myPost);
//                    String er = response.toString();
//                } catch (Exception e) {
//                    String ee = e.toString();
//                    e.printStackTrace();
//                }
//            }
//        }).start();
    }

    public void sendStartSterillzation() {
        char id = getClassNameID();
        char[] data = new char[]{0x02,0x1f,id};
        sendToService(data);
        Intent broadcast = new Intent(HMI_RMSLEEP_BROADCAST);
        sendBroadcast(broadcast);
    }

    public void startShowTitleClock(Button button) {
        rightTimeButton = button;
        timeThread = new CatCurrentTimeThread();
        timeThread.start();

        if (backButton!=null){
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //finish();
                    BaseToolBarActivity.super.onBackPressed();
                }
            });
        }
    }

    public void startShowTitleClock(TextView timeView) {
        sleepTimeView = timeView;
        timeThread = new CatCurrentTimeThread();
        timeThread.start();

        if (backButton!=null){
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //finish();
                    BaseToolBarActivity.super.onBackPressed();
                }
            });
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
        sendBroadcast(broadcast);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean superReturn = super.dispatchTouchEvent(ev);
        Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
        sendBroadcast(broadcast);
        return superReturn;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent;
        Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
        sendBroadcast(broadcast);
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK: // 開門
                if ((mActivity.getLocalClassName().equals("SteriRecordReaderActivity"))||(mActivity.getLocalClassName().equals("StartSterilizationActivity"))||(mActivity.getLocalClassName().equals("CalSensorActivity"))||(mActivity.getLocalClassName().equals("DynamicCalEditActivity"))||(mActivity.getLocalClassName().equals("DynamicCalViewActivity"))||(mActivity.getLocalClassName().equals("CleaningActivity"))) {

                }else{
                    char[] data = new char[]{0x01, 0x60};
                    sendToService(data);
                }
                break;
            case KeyEvent.KEYCODE_MENU:
//            case KeyEvent.KEYCODE_2:

                if ((mActivity.getLocalClassName().equals("CalSensorActivity"))||(mActivity.getLocalClassName().equals("DynamicCalEditActivity"))||(mActivity.getLocalClassName().equals("DynamicCalViewActivity"))) {
                    String contentStr = "Code 2\nEmergency stop";
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                            dialog.setContentImageRes(R.drawable.error_msg_icon_emergency_stop);
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                            dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                            dialog.setAnimationEnable(true);
                            dialog.setTitleText("Notification");
                            dialog.setContentText(contentStr);
                            dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    char[] data = new char[]{0x01, 0x50};
                                    sendToService(data);
                                    dialog.dismiss();
                                    closeAllActivities();
                                }
                            }).show();
                        }
                    });
                }else{
                    if ((mActivity.getLocalClassName().equals("CleaningActivity"))) {
                        String contentStr = "Code 2\nEmergency stop";
                        runOnUiThread(new Runnable() {
                            public void run()
                            {
                                NoticeDialog dialog = new NoticeDialog(getApplicationContext());
                                dialog.setContentImageRes(R.drawable.error_msg_icon_emergency_stop);
                                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
                                dialog.setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS);
                                dialog.setAnimationEnable(true);
                                dialog.setTitleText("Notification");
                                dialog.setContentText(contentStr);
                                dialog.setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                    @Override
                                    public void onClick(NoticeDialog dialog) {
                                        char[] data = new char[]{0x01, 0x50};
                                        sendToService(data);
                                        dialog.dismiss();
                                        closeAllActivities();
                                    }
                                }).show();
                            }
                        });
                    }else{
                        char[] data = new char[]{0x01, 0x50};
                        sendToService(data);
                        closeAllActivities();
                        if (Constants.instance().fetchValueInt(saveSelectMenuIdKey)==10) {
                            intent = new Intent(this, LeakageTestCompleteFailActivity.class);
                        }else{
                            intent = new Intent(this, CompleteFailedSterilizationActivity.class);
                        }
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    }

                }

                break;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
//        unRegisterBaseActivityReceiver();
        super.onDestroy();
        if(calendar!=null){
            calendar.clear();
        }
        if(timeThread!=null){
            stillCat = false;
            timeThread.interrupt();
        }
    }

    class CatCurrentTimeThread extends Thread{

        @Override
        public void run() {
            super.run();
            while(stillCat){
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Intent broadcast = new Intent(HMI_SLEEP_BROADCAST);
//                            sendBroadcast(broadcast);
                            calendar = Calendar.getInstance();
                            String hour = "";
                            String minute = "";
                            String second = "";
                            String month = "";
                            String date = "";
                            String hourStr = "";
                            int i_hour = calendar.get(Calendar.HOUR_OF_DAY);
                            int i_minute = calendar.get(Calendar.MINUTE);
                            int i_year = calendar.get(Calendar.YEAR);
                            int i_month = calendar.get(Calendar.MONTH)+1;
                            int i_date = calendar.get(Calendar.DAY_OF_MONTH);

                            String topTime = "";
                            String sleepTime = "";
                            if ((Constants.instance().fetchValueInt(saveTimeFormat))==0) {
                                if (i_hour<12) hourStr = "AM";
                                else {
                                    hourStr = "PM";
                                    if (i_hour>12)
                                    i_hour -= 12;
                                }
                            }

                            // 0:Y/M/D 1:D/M/Y 2:M/D/Y
                            if ((Constants.instance().fetchValueInt(saveDateFormat))==0) {
                                topTime = String.format("%02d:%02d %s %02d/%02d/%02d",i_hour, i_minute, hourStr, i_year, i_month, i_date);
                            }else if ((Constants.instance().fetchValueInt(saveDateFormat))==1) {
                                topTime = String.format("%02d:%02d %s %02d/%02d/%02d",i_hour, i_minute, hourStr, i_date, i_month, i_year);
                            }else if ((Constants.instance().fetchValueInt(saveDateFormat))==2) {
                                topTime = String.format("%02d:%02d %s %02d/%02d/%02d",i_hour, i_minute, hourStr, i_month, i_date, i_year);
                            }
                            if (rightTimeButton!=null)
                                rightTimeButton.setText(topTime);
                            else {
                                // 0:Y/M/D 1:D/M/Y 2:M/D/Y
                                if ((Constants.instance().fetchValueInt(saveDateFormat))==0) {
                                    sleepTime = String.format("%02d/%02d/%02d\n%02d:%02d %s", i_year, i_month, i_date,i_hour, i_minute, hourStr);
                                }else if ((Constants.instance().fetchValueInt(saveDateFormat))==1) {
                                    sleepTime = String.format("%02d/%02d/%02d\n%02d:%02d %s ", i_date, i_month, i_year,i_hour, i_minute, hourStr);
                                }else if ((Constants.instance().fetchValueInt(saveDateFormat))==2) {
                                    sleepTime = String.format("%02d/%02d/%02d\n%02d:%02d %s ", i_month, i_date, i_year,i_hour, i_minute, hourStr);
                                }
                                sleepTimeView.setText(sleepTime);
                            }
                            calendar.clear();
                            calendar = null;

                            if (leftButton2!=null) {
                                if (GlobalClass.getInstance().CB_CleanWaterLevel==0x00) {
                                    leftButton2.setImageResource(R.drawable.status_bar_icon_low_water_level);
                                }else if (GlobalClass.getInstance().CB_CleanWaterLevel==0x01) {
                                    leftButton2.setImageResource(R.drawable.status_bar_icon_middle_water_level);
                                }else if (GlobalClass.getInstance().CB_CleanWaterLevel==0x02) {
                                    leftButton2.setImageResource(R.drawable.status_bar_icon_high_water_level);
                                }
                            }
                            if (leftButton3!=null) {
                                if (GlobalClass.getInstance().HM_USBStatus == 0) {
                                    leftButton3.setVisibility(View.GONE);
                                } else {
                                    leftButton3.setVisibility(View.VISIBLE);
                                }
                            }
                            if (leftButton1!=null) {
                                if (GlobalClass.getInstance().CB_DoorLock==0x00) {
                                    leftButton1.setImageResource(R.drawable.status_bar_icon_door_lock);
                                }else {
                                    leftButton1.setImageResource(R.drawable.status_bar_icon_door_unlock);
                                }
                            }
                            if (leftButton4!=null){
                                if (GlobalClass.getInstance().HM_KeyboardStatus==0) {
                                    leftButton4.setVisibility(View.GONE);
                                }else{
                                    leftButton4.setVisibility(View.VISIBLE);
                                }
                            }
                            if (leftButton5!=null){
                                if (GlobalClass.getInstance().HM_WiFiStatus==0) {
                                    leftButton5.setVisibility(View.GONE);
                                }else{
                                    leftButton5.setImageResource(R.drawable.status_bar_icon_wifi_4);

                                    leftButton5.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static IntentFilter createIntentFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
        return filter;
    }

    protected void registerBaseActivityReceiver() {
        if (!mActivity.getLocalClassName().equals("StartSterilizationActivity")) {
            final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
            registerReceiver(mServiceReceiver, myFilter);
            bServiceReceiver = true;
        }
        registerReceiver(baseActivityReceiver, INTENT_FILTER);
    }

    protected void unRegisterBaseActivityReceiver() {
        if (bServiceReceiver) {
            unregisterReceiver(mServiceReceiver);
        }
        unregisterReceiver(baseActivityReceiver);
    }

    public class BaseActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)){
                finish();
            }
        }
    }

    protected void closeAllActivities(){

        GlobalClass.getInstance().MachineState = IDLE;
        sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
    }

    void sendToService(char[] sendData) {
        final Intent intent = new Intent(ACTION_TO_SERVICE);
        intent.putExtra("data", sendData);
        sendBroadcast(intent);
    }

    void sendToServicePrinter(char[] printData,String CSVData) {

        if (Constants.instance().fetchValueInt(saveRealPrint) > 0) {
            char[] buf = new char[printData.length + 3];
            System.arraycopy(printData, 0, buf, 2, printData.length);
            buf[0] = 0x00;
            buf[1] = 0x02;
            final Intent intent = new Intent(ACTION_TO_SERVICE);
            intent.putExtra("data", buf);
            sendBroadcast(intent);
        }
        for (int i=0;i<printData.length;i++) {
            if (printData[i]==0) {
                printData[i] = ' ';
            }
        }
        GlobalClass.getInstance().globalStdRecordSaveString += String.valueOf(printData);
        String jsonStr = String.valueOf(printData).replace("\r\n","<br/>");
        jsonStr = jsonStr.replace(" ","&nbsp;");
        GlobalClass.getInstance().globalCloudRecordSaveString += jsonStr;
        GlobalClass.getInstance().globalStdCSVRecordSaveString += CSVData;


//        GlobalClass.getInstance().saveRecordData(String.valueOf(printData));
    }

    void saveDtlToCache(char[] printData) {
        for (int i=0;i<printData.length;i++) {
            if (printData[i]==0) {
                printData[i] = ' ';
            }
        }
        GlobalClass.getInstance().globalDtlRecordSaveString += String.valueOf(printData);
    }

    void sendToServiceRecordPrinter(char[] printData) {
            char[] buf = new char[printData.length + 3];
            System.arraycopy(printData, 0, buf, 2, printData.length);
            buf[0] = 0x00;
            buf[1] = 0x02;
            final Intent intent = new Intent(ACTION_TO_SERVICE);
            intent.putExtra("data", buf);
            sendBroadcast(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
