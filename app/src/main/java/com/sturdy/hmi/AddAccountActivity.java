package com.sturdy.hmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.Notice2Dialog;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.NotificationDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.sql.ReturnMessage;
import com.sturdy.hmi.utils.FileUtils;
import com.sturdy.hmi.utils.KyEditText;
import com.sturdy.widget.SwitchButton;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import com.sturdy.hmi.adapter.SteriMenuList;

import java.util.List;

import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveSelectMenuIdKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;
import static com.sturdy.hmi.Constants.userLoginAccount;
import static com.sturdy.hmi.Constants.userLoginCheckPoint;
import static com.sturdy.hmi.Constants.userLoginLevel;
import static com.sturdy.hmi.Constants.userLoginPassword;

public class AddAccountActivity extends BaseToolBarActivity {
    private Activity mActivity;
    private DatabaseHelper mDatabaseHelper;
    private ListView list;
    private KyEditText userAccountEdit;
    private KyEditText userPasswordEdit;
    private String menuSelectName;
    private int menuSelectId;
    private String loginAccount;
    private String loginPassword;
    private int loginLevel;
    private boolean bChecked = true;
    private SwitchButton switchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_addaccount);
        Constants.instance(this.getApplicationContext());
        mActivity = this;
        mDatabaseHelper=new DatabaseHelper(mActivity);

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button mNavTitleView = mNavTopBar.addLeftTextButton("Add User", R.id.topbar_sterilization_right_title, Color.BLACK,38);
//        mNavTopBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
////            }
////        });
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        switchButton=(SwitchButton)findViewById(R.id.menu_setting_swich);
        switchButton.dontTouch = false;





        menuSelectName = (Constants.instance().fetchValueString(saveSelectMenuNameKey));
        menuSelectId = (Constants.instance().fetchValueInt(saveSelectMenuIdKey));
        loginAccount = (Constants.instance().fetchValueString(userLoginAccount));
        loginPassword = (Constants.instance().fetchValueString(userLoginPassword));
        loginLevel = (Constants.instance().fetchValueInt(userLoginLevel));
        int imageId = 0;
        switch (loginLevel) {
            case 0:
                imageId =  R.drawable.user_list_icon_top_administrator_normal;
                break;
            case 1:
                imageId =  R.drawable.user_list_icon_administrator_normal;
                break;
            case 2:
                imageId =  R.drawable.user_list_icon_user_normal;
                break;
        }
        userAccountEdit = (KyEditText) findViewById(R.id.user_account_edit);
        int maxLength = 12;
        userAccountEdit.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
        userPasswordEdit = (KyEditText)findViewById(R.id.password_edit);
        maxLength = 9;
        userPasswordEdit.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
        userPasswordEdit.setOnEditorActionListener(
                new KyEditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                                ) {

                            if (userPasswordEdit.getText().length()>1){
                                String input = userPasswordEdit.getText().toString();


                                return false;
                            }
                        }
                        return false;
                    }
                });
        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();

        ImageView showPasswordIV = (ImageView)this.findViewById(R.id.show_password);
        showPasswordIV.setTag(0);
        showPasswordIV.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                if ((int)showPasswordIV.getTag()==0) {
                    showPasswordIV.setTag(1);
                    userPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    showPasswordIV.setImageResource(R.drawable.text_field_icon_visibility);
                }else{
                    showPasswordIV.setTag(0);
                    userPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    showPasswordIV.setImageResource(R.drawable.text_field_icon_invisibility);
                }

            }});

        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        DrawMeButton mSaveButton = (DrawMeButton)findViewById(R.id.save_button);
        mSaveButton.setTextSize(30);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userAccountEdit.length()<1) {
                    new NoticeDialog(mActivity)
                            .setContentImageRes(R.drawable.icon_del)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Notification")
                            .setContentText("ID has not been entered")
                            .setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).show();
                    return;
                }
                if (userPasswordEdit.length()<4) {
                    new NoticeDialog(mActivity)
                            .setContentImageRes(R.drawable.icon_del)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Notification")
                            .setContentText("Password is 4-9 characters.")
                            .setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                @Override
                                public void onClick(NoticeDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).show();

                    return;
                }
                new NotificationDialog(mActivity)
                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                        .setAnimationEnable(true)
                        .setTitleText("Notification")
                        .setContentText("Confirm to add this account?")
                        .setCancelListener("Confirm", new NotificationDialog.OnCancelListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveListener("Cancle", new NotificationDialog.OnPositiveListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .setConfirmListener("Confirm", new NotificationDialog.OnConfirmListener() {
                            @Override
                            public void onClick(NotificationDialog dialog) {
                                User user = new User();
                                user.setUserOrder(mDatabaseHelper.getUserOrderNumber());
                                user.setName(userAccountEdit.getText().toString());
                                if (switchButton.isChecked())
                                    user.setUserLevel(1);
                                else
                                    user.setUserLevel(2);
                                user.setPassword(userPasswordEdit.getText().toString());
                                mDatabaseHelper.addUser(user);
                                dialog.dismiss();
                                new Notice2Dialog(mActivity)
                                        .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                        .setAnimationEnable(true)
                                        .setTitleText("")
                                        .setContentText("Save Success", Gravity.CENTER)
                                        .setConfirmListener("Confirm", new Notice2Dialog.OnConfirmListener() {
                                            @Override
                                            public void onClick(Notice2Dialog dialog) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        }).show();
                            }
                        }).show();
            }
        });



    }


}
