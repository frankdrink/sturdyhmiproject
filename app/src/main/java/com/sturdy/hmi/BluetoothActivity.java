package com.sturdy.hmi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.sturdy.hmi.Bluetooth.DataTransFragment;
import com.sturdy.hmi.Bluetooth.DeviceListFragment;
import com.sturdy.hmi.Bluetooth.Params;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by franklin on 2019/12/18.
 */


public class BluetoothActivity extends BaseToolBarActivity {

    final String TAG = "MainActivity";

    TabLayout tabLayout;
    ViewPager viewPager;
    MyPagerAdapter pagerAdapter;
    String[] titleList=new String[]{"設備列表","資料傳輸"};
    List<Fragment> fragmentList=new ArrayList<>();

    DeviceListFragment deviceListFragment;
    DataTransFragment dataTransFragment;

    BluetoothAdapter bluetoothAdapter;

    Handler uiHandler =new Handler(){
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case Params.MSG_REV_A_CLIENT:
                    Log.e(TAG,"--------- uihandler set device name, go to data frag");
                    BluetoothDevice clientDevice = (BluetoothDevice) msg.obj;
                    dataTransFragment.receiveClient(clientDevice);
                    viewPager.setCurrentItem(1);
                    break;
                case Params.MSG_CONNECT_TO_SERVER:
                    Log.e(TAG,"--------- uihandler set device name, go to data frag");
                    BluetoothDevice serverDevice = (BluetoothDevice) msg.obj;
                    dataTransFragment.connectServer(serverDevice);
                    viewPager.setCurrentItem(1);
                    break;
                case Params.MSG_SERVER_REV_NEW:
                    String newMsgFromClient = msg.obj.toString();
                    dataTransFragment.updateDataView(newMsgFromClient, Params.REMOTE);
                    break;
                case Params.MSG_CLIENT_REV_NEW:
                    String newMsgFromServer = msg.obj.toString();
                    dataTransFragment.updateDataView(newMsgFromServer, Params.REMOTE);
                    break;
                case Params.MSG_WRITE_DATA:
                    try {
                        String dataSend = msg.obj.toString();
                        dataTransFragment.updateDataView(dataSend, Params.ME);
                        deviceListFragment.writeData(dataSend);
                    }catch (Exception e){

                    }
                    break;

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            initUI();
        }catch(Exception e){
            finish();
        }
    }


    /**
     * 返回 uiHandler
     * @return
     */
    public Handler getUiHandler(){
        return uiHandler;
    }

    /**
     * 初始化界面
     */
    private void initUI() {
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Bluetooth", R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.text_field_icon_search, R.id.topbar_unwrapped_right_2_button, 64);
        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bluetoothAdapter.isDiscovering()) {
                    bluetoothAdapter.cancelDiscovery();
                }
//                if (Build.VERSION.SDK_INT >= 6.0) {
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                            Params.MY_PERMISSION_REQUEST_CONSTANT);
//                }

                bluetoothAdapter.startDiscovery();
            }
        });

        tabLayout= (TabLayout) findViewById(R.id.tab_layout);
        viewPager= (ViewPager) findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText(titleList[0]));
        tabLayout.addTab(tabLayout.newTab().setText(titleList[1]));

        deviceListFragment=new DeviceListFragment();
        dataTransFragment=new DataTransFragment();
        fragmentList.add(deviceListFragment);
        fragmentList.add(dataTransFragment);

        pagerAdapter=new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    /**
     * ViewPager 适配器
     */
    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList[position];
        }
    }

    /**
     * Toast 提示
     */
    public void toast(String str){
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }
}
