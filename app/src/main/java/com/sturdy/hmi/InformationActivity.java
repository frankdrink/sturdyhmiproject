package com.sturdy.hmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.drawme.delegate.DrawMe;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.item.StartSterInformationItemA;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecordStepClass;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.view.DashedLineView;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.widget.WheelView;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.SteriMenuList;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.CycleCounterKey;
import static com.sturdy.hmi.Constants.saveAltitudeKey;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class InformationActivity extends BaseToolBarActivity {
    Handler handler;
    TableLayout tableLayout;
    String[] infoItem = {
            "Model name",
            "Serial number",
            "Software version",
            "Firmware version",
            "cycle",
            "",
            "Temp. of Steam Generator",
            "Temp. of Chamber",
            "Temp. of Heater",
            "Pressure of chamber",
            "atmospheric pressure",
            "Altitude",
            "Door status",
            "",
//            "Fan speed",
            "Remaining storage",
            "USB status",
            "Water quality",
            "Clean water level",
            "Waste water level",
            "",
            "IP address(RJ45)",
            "IP address (Wifi)",
            "Mac address"
    };

    String[] infoValue = {
            "Prime ",
            "120413003-001",
            "1.0(HMI)",
            "1.0(Control)",
            "100",
            "",
            "121℃",
            "121℉",
            "105℃",
            "1.0 bar",
            "1.0 bar",
            "100 m",
            "Closed",
            "",
//            "2000 rpm",
            "2 GB",
            "Plug",
            "",
            "Normal",
            "Normal",
            "",
            "127.0.0.1",
            "127.0.0.1",
            "12:34:56:78:90:aa"
    };
    ListView list;
    int rowIndex;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                infoValue[1] = GlobalClass.getInstance().CB_SN;
                if (GlobalClass.getInstance().CB_DoorLock==0)
                    infoValue[12]="Close";
                else
                    infoValue[12]="Open";

                if (recv[1] == 0xf0) {
                    char[] ch10 = new char[10];
                    System.arraycopy(recv, 2, ch10, 0, 10);
                    infoValue[0] = CharUtils.charToString(ch10);
                    char[] ch8 = new char[8];
                    System.arraycopy(recv, 12, ch8, 0, 8);
                    infoValue[3] = CharUtils.charToString(ch8);
                }else if (recv[1] == 0xf5) {
                    char[] ch2 = new char[2];
                    System.arraycopy(recv, 2, ch2, 0, 2);
                    int SGTemp = CharUtils.charArrayToInt(ch2);
                    infoValue[6] = String.format("%.1f℃",(float)SGTemp/(float)10);
                    System.arraycopy(recv, 4, ch2, 0, 2);
                    int ChamberTemp = CharUtils.charArrayToInt(ch2);
                    infoValue[7] = String.format("%.1f℃",(float)ChamberTemp/(float)10);
                    System.arraycopy(recv, 6, ch2, 0, 2);
                    int BHTemp = CharUtils.charArrayToInt(ch2);
                    infoValue[8] = String.format("%.1f℃",(float)BHTemp/(float)10);
                    System.arraycopy(recv, 8, ch2, 0, 2);
                    int ChamberBar = CharUtils.charArrayToInt(ch2);
                    infoValue[9] = String.format("%.3fbar",(float)ChamberBar/(float)1000);
                    System.arraycopy(recv, 10, ch2, 0, 2);
                    int ATBar = CharUtils.charArrayToInt(ch2);
                    infoValue[10] = String.format("%.3fbar",(float)ATBar/(float)1000);
//                    System.arraycopy(recv, 12, ch2, 0, 2);

                    System.arraycopy(recv, 10, ch2, 0, 2);
                    double P = (((double)ATBar/(double)1013.25));
                    double pow = Math.pow(P,(double)0.190258751902588);
                    double H = 44300*(1-pow);
                    infoValue[11] = String.valueOf((int)H)+" m";
                    int FanSpeed = CharUtils.charArrayToInt(ch2);
//                    Random random = new Random();
//                    infoValue[14] = String.format("%d rpm",FanSpeed+random.nextInt(2000));
                    System.arraycopy(recv, 12, ch2, 0, 2);
                    int WaterQuality = CharUtils.charArrayToInt(ch2);
                    infoValue[16] = String.format("%d",WaterQuality);
                    char WasteWaterLevel = recv[14];
                    if (GlobalClass.getInstance().CB_CleanWaterLevel==0x00) {
                        infoValue[17] = "Low";
                    }else if (GlobalClass.getInstance().CB_CleanWaterLevel==0x01){
                        infoValue[17] = "Middle";
                    }else {
                        infoValue[17] = "High";
                    }
                    if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
                        if (WasteWaterLevel==0x00) {
                            infoValue[18] = "";
                        }else {
                            infoValue[18] = "";
                        }
                    }else{
                        if (WasteWaterLevel==0x00) {
                            infoValue[18] = "Low";
                        }else {
                            infoValue[18] = "High";
                        }
                    }

                    tableLayout.removeAllViews();
                    for (int i=0;i<infoItem.length;i++) {
                        TableRow row= new TableRow(mActivity);
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                                TableRow.LayoutParams.WRAP_CONTENT);
                        row.setLayoutParams(lp);
                        TextView tv1 = new TextView(mActivity);
                        tv1.setGravity(Gravity.LEFT);
                        tv1.setTextSize(32);
                        tv1.setTypeface(null, Typeface.BOLD);
                        tv1.setText(infoItem[i]);
                        TextView tv2 = new TextView(mActivity);
                        tv2.setGravity(Gravity.RIGHT);
                        tv2.setTextSize(28);
                        tv2.setText(infoValue[i]);
                        row.addView(tv1);
                        row.addView(tv2);
                        tableLayout.addView(row,new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.WRAP_CONTENT,
                                TableLayout.LayoutParams.WRAP_CONTENT));
                    }
//                    char[] ch7 = new char[7];
//                    CharUtils.charToString(ch7);
//                    CB_NowStage = recv[2];
//                    System.arraycopy(recv, 3, ch7, 0, 7);
//                    CB_StageName= CharUtils.charToString(ch7);
//                    char[] ch4 = new char[4];
//                    System.arraycopy(recv, 10, ch4, 0, 4);
//                    CB_TotalTime = CharUtils.charArrayToInt(ch4);
//                    ch4 = new char[4];
//                    System.arraycopy(recv, 14, ch4, 0, 4);
//                    CB_Time = CharUtils.charArrayToInt(ch4);
//                    char[] ch2 = new char[2];
//                    System.arraycopy(recv, 18, ch2, 0, 2);
//                    CB_Temp = CharUtils.charArrayToInt(ch2);
//                    System.arraycopy(recv, 20, ch2, 0, 2);
//                    CB_Bar = CharUtils.charArrayToInt(ch2);
//                    viewPager.setCurrentItem(CB_NowStage);
//                    int hour = CB_TotalTime/60/60;
//                    int min = CB_TotalTime/60;
//                    int sec = CB_TotalTime%60;
//                    String timeStr = String.format("%02d:%02d:%02d",hour, min, sec);
//                    StartSterInformationItemA item = new StartSterInformationItemA(startSterInformationItemA.get(0).getDrawableId(),startSterInformationItemA.get(0).getName(),timeStr);
//                    startSterInformationItemA.set(0,item);
//                    gridViewAdapter1.notifyDataSetChanged();
//                    float temp = (float)CB_Temp / (float)10;
//                    float bar = (float)CB_Bar / (float)1000;
//                    str = String.format("%.1f",temp);
//                    startSterInformationItemB.set(0, new StartSterGridRecycler("Temp.", "°C", str, R.drawable.parameter_icon_temp,true));
//                    str = String.format("%.3f",bar);
//                    startSterInformationItemB.set(1, new StartSterGridRecycler("Pres.", "kgf/cm", str, R.drawable.parameter_icon_time,false));
//                    gridViewAdapter2.notifyDataSetChanged();
//                    int stage = (int)GlobalClass.getInstance().globalStageList[CB_NowStage];
//                    progressBarTitleView.setText(getStageToString(stage));
//                    RecordStepClass step = new RecordStepClass();
//                    step.step = CB_StageName;
//                    step.time = CB_Time;
//                    step.temp = CB_Temp;
//                    step.pres = CB_Bar;
                }

            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            char[] data = new char[]{0x01, 0xf5};
            sendToService(data);
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_information);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if ((GlobalClass.getInstance().MachineModel.equals("30L"))||(GlobalClass.getInstance().MachineModel.equals("60L"))) {
            infoItem[18] = "";
        }
        startShowTitleClock(rightTimeButton);
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Information", R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        infoValue[2] = Constants.instance().getVersionName(mActivity);
        infoValue[4] = Integer.toString(Settings.System.getInt(this.getContentResolver(),CycleCounterKey,0));

        tableLayout = (TableLayout) findViewById(R.id.tableLayout1);
        for (int i=0;i<infoItem.length;i++) {
            TableRow row= new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            TextView tv1 = new TextView(this);
            tv1.setGravity(Gravity.LEFT);
            tv1.setTextSize(38);
            tv1.setTypeface(null, Typeface.BOLD);
            tv1.setText(infoItem[i]);
            TextView tv2 = new TextView(this);
            tv2.setGravity(Gravity.RIGHT);
            tv2.setTextSize(36);
            tv2.setText(infoValue[i]);
            row.addView(tv1);
            row.addView(tv2);
            tableLayout.addView(row,new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
        }
        char[] data = new char[]{0x01, 0xf0};
        sendToService(data);
        handler = new Handler();
        handler.postAtTime(runnable, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
