package com.sturdy.hmi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.Notice2Dialog;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.CustomAdapter;
import com.sturdy.hmi.adapter.StartSterGrid2Adapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.RecyclerViewDisabler;
import com.sturdy.hmi.utils.StartSterGridRecycler;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;
import com.sturdy.dotlibrary.DottedProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.chenupt.multiplemodel.viewpager.ModelPagerAdapter;
import github.chenupt.multiplemodel.viewpager.PagerModelManager;
import com.sturdy.springindicator.SpringIndicator;
import com.sturdy.springindicator.viewpager.ScrollerViewPager;
import com.sturdy.hmi.adapter.RecyclerTouchListener;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter1;
import com.sturdy.hmi.adapter.StartSterGridViewAdapter2;
import com.sturdy.hmi.item.StartSterInformationItemA;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.MACHINE_STATE.IDLE;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.postScanControl;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveSelectMenuNameKey;

public class CleaningActivity extends BaseToolBarActivity{
    Handler handler = new Handler();
    Runnable run;
    private int cleaningState=0;
    private ImageView mPromptimageview;
    private TextView mProgramTextView;
    private TextView mNextTextView;
    private DrawMeImageButton HomeButton;
    private DrawMeButton mStartSterButton;

//    0x01-清潔啟動訊號(HMI --> CB)
//    0x02-清潔完成訊號(CB --> HMI)
//    0x03-清水箱滿水位訊號(CB --> HMI)
//    0x04-清洗啟動訊號(HMI --> CB)
//    0x05-清洗完成訊號(CB --> HMI)
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x61) {
                    switch (recv[2]) {
                        case 0x02:
                            cleaningState++;
                            changeCleaningState();
                            mNextTextView.setVisibility(View.VISIBLE);
                            break;
                        case 0x03:
                            new NoticeDialog(mActivity)
                                    .setContentImageRes(R.drawable.msg_icon_water_full)
                                    .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                                    .setAnimationEnable(true)
                                    .setTitleText("Notification")
                                    .setContentText("Water tank full water level")
                                    .setConfirmListener("Confirm", new NoticeDialog.OnConfirmListener() {
                                        @Override
                                        public void onClick(NoticeDialog dialog) {
                                            mNextTextView.setVisibility(View.VISIBLE);
                                            dialog.dismiss();
                                        }
                                    }).show();
                            break;
                        case 0x05:
                            cleaningState++;
                            changeCleaningState();
                            mNextTextView.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_cleaning);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        Constants.instance(this.getApplicationContext());
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.titlebar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_sterilization_right_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveMainMenuSettting();
                finish();
            }
        });
        Button mNavTitleView = mNavTopBar.addLeftTextButton("Cleaning", R.id.topbar_sterilization_right_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });

        startShowTitleClock(rightTimeButton);
        registerBaseActivityReceiver();
        mPromptimageview = (ImageView)findViewById(R.id.prompt_imageview);
        mPromptimageview.setImageResource(R.drawable.msg_icon_cleaning_sg);
        mProgramTextView = (TextView)findViewById(R.id.program_title_view);
        changeCleaningState();
        HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleaningState++;
                changeCleaningState();
                mStartSterButton.setVisibility(View.GONE);
                HomeButton.setVisibility(View.GONE);

            }
        });

        mNextTextView = (TextView)findViewById(R.id.next_button);
        mNextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextTextView.setVisibility(View.GONE);
                cleaningState++;
                changeCleaningState();
            }
        });

    }

    private void changeCleaningState() {
        switch (cleaningState) {
            case 0:{
                // 清潔流程代號
                mPromptimageview.setImageResource(R.drawable.msg_icon_cleaning_sg);
                mProgramTextView.setText("Designed for cleaning steam generator.\n\nPlease put in detergent. If it is powdery, please dissolve it in water completely. Pour into chamber. Close the sterilizer door afterwards");
                mProgramTextView.setGravity(Gravity.LEFT);
            }
            break;
            case 1:{
                mPromptimageview.setImageResource(R.drawable.msg_icon_add_detergent);
                mProgramTextView.setText("Please put in detergent.");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.VISIBLE);
            }
            break;
            case 2:{
                char[] data = new char[]{0x02,0x61,0x01};
                sendToService(data);
                mPromptimageview.setImageResource(R.drawable.msg_icon_washing);
                mProgramTextView.setText("Cleaning..");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.GONE);
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        cleaningState++;
//                        changeCleaningState();
//                        mNextTextView.setVisibility(View.VISIBLE);
//                    }
//                }, 3000);
            }
            break;
            case 3:{
                mPromptimageview.setImageResource(R.drawable.msg_icon_clear_sewage_tank);
                mProgramTextView.setText("Cleaning complete\nPlease drain and clean the water tank");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.VISIBLE);
            }
            break;
            case 4:{
                mPromptimageview.setImageResource(R.drawable.msg_icon_add_water);
                mProgramTextView.setText("Please drain the water tank");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.GONE);
                char[] data = new char[]{0x02,0x61,0x02};
                sendToService(data);

            }
            break;
            case 5:{
                char[] data = new char[]{0x02,0x61,0x04};
                sendToService(data);
                mPromptimageview.setImageResource(R.drawable.msg_icon_washing);
                mProgramTextView.setText("Cleaning");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.GONE);
            }
            break;
            case 6:{
                mPromptimageview.setImageResource(R.drawable.msg_icon_clear_sewage_tank);
                mProgramTextView.setText("清洗完成_請清潔廢水箱");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.VISIBLE);
            }
            break;
            case 7:{
                mPromptimageview.setImageResource(R.drawable.msg_icon_cleaning_sg);
                mProgramTextView.setText("Cleaning completed");
                mProgramTextView.setGravity(Gravity.CENTER);
                mNextTextView.setVisibility(View.GONE);
                HomeButton.setVisibility(View.VISIBLE);
            }
            break;
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
