package com.sturdy.hmi.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.sturdy.hmi.R;


public class PayFragment extends DialogFragment implements View.OnClickListener {
    public static final String EXTRA_CONTENT = "extra_content";    //提示框内容
    private PayPasswordView mPasswordView;
    private PayPasswordView.InputCallBack mInputCallBack;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.BottomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_pay);
        dialog.setCanceledOnTouchOutside(false);

        final Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.AinmBottom);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);

        initView(dialog);
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        Dialog d = builder.create();
////        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        if(getActivity().getCurrentFocus()!=null &&getActivity().getCurrentFocus().getWindowToken() != null) {
//            System.out.println("getCurrentFocus() in frag");
//            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//
//            IBinder binder = getActivity().getCurrentFocus().getWindowToken();
//            inputManager.hideSoftInputFromWindow(binder,
//                    InputMethodManager.HIDE_NOT_ALWAYS);
//        }
//        getActivity().getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        //this line below does NOT work, it does not hide the keyboard
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onDismiss(dialog);
    }

    private void initView(Dialog dialog) {
        Bundle bundle = getArguments();

        mPasswordView = (PayPasswordView) dialog.findViewById(R.id.payPwdView);
        hideKeyboard();
        PayKeyboardView payKeyboardView = (PayKeyboardView) dialog.findViewById(R.id.inputMethodView);
        mPasswordView.setInputMethodView(payKeyboardView);
        mPasswordView.setInputCallBack(mInputCallBack);
        dialog.findViewById(R.id.iv_close).setOnClickListener(this);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
        }
    }

    public static void hideKeyboardInAndroidFragment(View view){
        final InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
        }
    }

    /**
     * 设置输入回调
     */
    public void setPaySucessCallBack(PayPasswordView.InputCallBack inputCallBack) {
        this.mInputCallBack = inputCallBack;
    }
}
