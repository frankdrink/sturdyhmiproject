package com.sturdy.hmi.STM32;

public interface STM32OperationProgressListener {
    public void completed(boolean successfull);
    public void progress(long current, long total);
}
