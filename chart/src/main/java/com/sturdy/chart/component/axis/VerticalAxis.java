package com.sturdy.chart.component.axis;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;

import com.sturdy.chart.data.ChartData;
import com.sturdy.chart.data.BarData;
import com.sturdy.chart.data.ScaleData;
import com.sturdy.chart.data.ScaleSetData;
import com.sturdy.chart.exception.ChartException;

import java.util.List;

/**
 * 圖表豎軸
 */

public class VerticalAxis extends BaseAxis<Double> {

    /**
     * 圖表刻度設置數據
     * <p>用於保存用戶設置最大，最小等數據</p>
     */
    private ScaleSetData scaleSetData = new ScaleSetData();

    /**
     * 橫軸構造方法
     * <p>可以設置橫軸方向 左右方位</p>
     * @param direction 方位
     */
    public VerticalAxis(int direction) {
        this.direction = direction;
    }

    /**
     * 小數點格式化
     */
    public java.text.DecimalFormat df = new java.text.DecimalFormat("0.00");
    public String unitLabel;
    /**
     * 計算刻度大小
     * <p>通過計算刻度的寬高得到軸的大小，然後保存到scaleData對像中，以便後面的計算</p>
     * @param chartData 圖表數據
     * @param rect 圖表原始範圍
     * @param paint 畫筆
     */
    @Override
    public void computeScale(ChartData<? extends BarData> chartData, Rect rect, Paint paint) {
        if(isDisplay()) {
            ScaleData scaleData = chartData.getScaleData();
            scaleData.resetScale(scaleSetData, direction);
            scaleStyle.fillPaint(paint);
            int length = Math.max(formatVerticalAxisData(scaleData.getMaxScaleValue(direction)).length(),
                    formatVerticalAxisData(scaleData.getMinScaleValue(direction)).length());
            int textHeight = (int) (paint.measureText("1", 0, 1) * length);
            int dis = (int) (textHeight + scaleStyle.getPadding() + axisStyle.getWidth());
            if (direction == AxisDirection.LEFT) {
                scaleData.scaleRect.left = dis;
            } else {
                scaleData.scaleRect.right = dis;
            }
        }
    }
    /**
     * 繪製刻度
     * <p>通過zoomRect計算出每個刻度的寬度，迭代繪製刻度</p>
     * @param canvas 畫布
     * @param zoomRect 縮放之後範圍
     * @param rect  原始範圍
     * @param paint  畫筆
     * @param chartData 圖表數據
     */
    @Override
    protected void drawScale(Canvas canvas, Rect zoomRect, Rect clipRect, Paint paint,  ChartData<? extends BarData> chartData) {
        ScaleData scaleData = chartData.getScaleData();
        List<Double> scaleList = scaleData.getScaleList(direction);
        float startX;
        if (direction == AxisDirection.LEFT) {
            startX = zoomRect.left + scaleStyle.getPadding();
        } else {
            startX = zoomRect.right - scaleData.scaleRect.right + scaleStyle.getPadding();
        }
        int bottom = zoomRect.bottom;
        int height = zoomRect.height();
        float textHeight = paint.measureText("1", 0, 1);
        int perHeight = height / (scaleList.size()-1);
        for (int i = 0; i < scaleList.size(); i++) {
            double value = scaleList.get(i);
            float startY = bottom - i * perHeight;
            if (clipRect.contains(clipRect.centerX(), (int) startY-1)) {
                if (i==0) {
                    drawText(canvas, startX+5, 12 + textHeight / 2, unitLabel, paint);
                }
                drawText(canvas, startX, startY + textHeight / 2, value, paint);
                drawGrid(canvas, startY, zoomRect, scaleData.scaleRect, paint);
                if (i>0) {
                    drawGridDot(canvas, startY, zoomRect, scaleData.scaleRect, paint);
                }
            }
        }

    }

    /**
     * 繪製文字
     */
    private void drawText(Canvas canvas, float startX, float startY, double value, Paint paint) {
        scaleStyle.fillPaint(paint);
        String content = formatVerticalAxisData(value);
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(content, startX, startY, paint);
    }

    private void drawText(Canvas canvas, float startX, float startY, String strValue, Paint paint) {
        scaleStyle.fillPaint(paint);
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(strValue, startX, startY, paint);
    }

    /**
     * 繪製網格
     */
    private void drawGrid(Canvas canvas, float startY, Rect rect, Rect scaleRect, Paint paint) {
        if (gridStyle != null && isDrawGrid) {
            gridStyle.fillPaint(paint);
            Path path = new Path();
            path.moveTo(rect.left + scaleRect.left, startY);
            path.lineTo(rect.right - scaleRect.right, startY);
            canvas.drawPath(path, paint);
        }
    }
    private void drawGridDot(Canvas canvas, float startY, Rect rect, Rect scaleRect, Paint paint) {
        if (gridStyle != null && isDrawGrid) {
            gridStyle.fillPaint(paint);
            Path path = new Path();
            path.moveTo(rect.left + scaleRect.left, startY+22);
            path.lineTo(rect.left + scaleRect.left+5, startY+22);
            canvas.drawPath(path, paint);
            path = new Path();
            path.moveTo(rect.right - scaleRect.right-5, startY+22);
            path.lineTo(rect.right - scaleRect.right, startY+22);
            canvas.drawPath(path, paint);
        }
    }

    /**
     * 計算出裁切軸之後的範圍
     * @param rect 原始範圍
     * @param scaleRect 縮放範圍
     * @return 上下左右的大小
     */
    protected int[] calculation(Rect rect, Rect scaleRect) {

        int startY = rect.top + scaleRect.top;
        int endY = rect.bottom - scaleRect.bottom;
        int startX, endX;
        if (direction == AxisDirection.LEFT) {
            startX = rect.left + scaleRect.left;
        } else {
            startX = rect.right - scaleRect.right;
        }
        endX = startX;
        return new int[]{startX, startY, endX, endY};
    }
    /**
     * 設置軸方位
     * <p>豎軸只能設置左右方位</p>
     * @param axisDirection 軸方位
     *
     */
    @Override
    public void setAxisDirection(int axisDirection) {
        if (axisDirection == AxisDirection.LEFT || axisDirection == AxisDirection.RIGHT) {
            this.direction = axisDirection;
        } else throw new ChartException("Can only set LEFT, RIGHT direction");
    }

    /**
     * 格式化豎軸數據
     * @param value 數值
     * @return 格式化豎軸之後數據
     */
    private String formatVerticalAxisData(double value) {
        if(getFormat() != null){
            return getFormat().format(value);
        }
        return df.format(value);
    }
    /**
     * 設置刻度是否從0開始
     */
    public void setStartZero(boolean isStartZero){
        this.scaleSetData.setStartZoom(isStartZero);
    }

    /**
     * 設置刻度最大值
     * @param maxValue 最大值
     */
    public void setMaxValue(double maxValue) {
        this.scaleSetData.setMaxValue(maxValue);
    }
    /**
     * 設置刻度最小值
     * @param minValue 最大值
     */
    public void setMinValue(double minValue) {
        this.scaleSetData.setMinValue(minValue);
    }

}
