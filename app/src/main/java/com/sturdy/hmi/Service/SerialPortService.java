package com.sturdy.hmi.Service;

import android.app.Dialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.sturdy.hmi.Constants;
import com.sturdy.hmi.Dialog.NoticeDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.GlobalClass;
import com.sturdy.hmi.MainActivity;
import com.sturdy.hmi.R;
import com.sturdy.hmi.USBList.ui.main.tabs.TabController;
import com.sturdy.hmi.USBList.ui.main.tabs.TabViewHolder;
import com.sturdy.hmi.utils.CharUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dma.xch.hmi.api.HmiClient;
import dma.xch.hmi.api.UntiTools;
import dma.xch.hmi.api.callback.UartDataCallBack;
import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbDevice;
import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbManager;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.ACTION_TO_SERVICE;
import static com.sturdy.hmi.Constants.AutoAddWaterKey;
import static com.sturdy.hmi.Constants.ExhaustTempKey;
import static com.sturdy.hmi.Constants.HMI_ERROR_BROADCAST;
import static com.sturdy.hmi.Constants.PressuizedcoolingKey;
import static com.sturdy.hmi.Constants.saveRealPrint;

/**
 * Created by franklin on 2019/9/26.
 */

public class SerialPortService extends Service implements UartDataCallBack {
    private Handler handler = new Handler();

    String[] comNameList = new String[]{"串口1"};//"串口1"
    char[] cmdData = new char[]{0xd1, 0xd2 ,0xd3, 0xd4 ,0xd5, 0xd6 ,0xd7, 0xd8 ,0xd9, 0xda ,0xdb, 0xdc ,0xdd, 0xde ,0xdf, 0xe0 ,0xe1, 0xe2 ,0xe3, 0xe4 ,0xe5, 0xe6 ,0xe7};
    int cmdNum = 0;
    private int port=1;
    int[] highArray;
    int high;
    Random random;
    private int LEDFlicker=0;

    String[] USBDeviceArray;
    String[] USBDeviceSortArray;
    String[] USBDeviceNameArray;

    private SysBusUsbManager mUsbManagerLinux;
    private Map<String, SysBusUsbDevice> mLinuxDeviceMap;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (GlobalClass.getInstance().MachineState!= Constants.MACHINE_STATE.CB_DFU) {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[0]==0x00) {
                    if (recv.length>2) {
                        switch (recv[1]) {
                            case 0x00:                  // 三色燈
                                if (recv[2]==0x01) {
                                    handler.postDelayed(showTime, 100);
                                }else{
                                    setGpio(11, 0);
                                    setGpio(12, 0);
                                    setGpio(13, 0);
                                    handler.removeCallbacks(showTime);
                                }
                                break;
                            case 0x01:                  // 印表機
                                if (recv[2]==0x01) {

                                    sendUartMsg(0,"Step   Time   ts   Temp   Pres.\r\n");
                                    sendUartMsg(0,"     mmm:ss  mm:ss  'C     bar \r\n");
                                    sendUartMsg(0,"Start  000:00 00:00 xxx.x x.xx\r\n");

                                    handler.postDelayed(printerTime, 5000);
                                }else{
                                    handler.removeCallbacks(printerTime);
                                }
                                break;
                            case 0x02:
                                char[] buf = new char[recv.length-2];
                                System.arraycopy(recv, 2, buf, 0, recv.length-2);
                                sendUartHex(0,buf);
                                break;
                        }
                    }
                } else {
                    sendUartHex(1,recv);
                }
            }
        }
    };

    private Runnable mBleSendCmdRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (GlobalClass.getInstance().MachineState != Constants.MACHINE_STATE.CB_DFU) {
                try {
                    if (GlobalClass.getInstance().mSendCmdQueue != null && GlobalClass.getInstance().mSendCmdQueue.size() > 0) {
                        port = 1;
                        char[] sendData = GlobalClass.getInstance().mSendCmdQueue.get(0);
                        GlobalClass.getInstance().client.sendUartByte(GlobalClass.getInstance().getKeyFromMap(GlobalClass.getInstance().portMap, port + 1), sendData, sendData.length);
                        GlobalClass.getInstance().mSendCmdQueue.remove(0);
                        char[] buf = new char[sendData.length + 1];
                        System.arraycopy(sendData, 0, buf, 1, sendData.length);
                        buf[0] = 0xff;
                        if (port == 1) {
                            GlobalClass.getInstance().globalArrayList.add(buf);
                            String receviceMsg = UntiTools.CharToString(sendData, sendData.length);
                            Calendar calendar = Calendar.getInstance();
                            String topTime = String.format("%02d:%02d:%02d:", calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
                            appendLog("H->C" + topTime + receviceMsg);
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            handler.postDelayed(mBleSendCmdRunnable, 50);
        }
    };

    private Runnable updateWaterStatus = new Runnable() {
        public void run() {
            char[] data = new char[]{0x01,0xf1};
            GlobalClass.getInstance().mSendCmdQueue.add(data);
            refreshUsbDevices();
            handler.postDelayed(updateWaterStatus, 10000);
        }
    };

    private void refreshUsbDevices() {
        mLinuxDeviceMap = mUsbManagerLinux.getUsbDevices();
        updateList(mLinuxDeviceMap);
    }

    private void updateList(final Map<String, ?> map) {
        USBDeviceArray = map.keySet().toArray(new String[map.keySet().size()]);
        USBDeviceNameArray = map.keySet().toArray(new String[map.keySet().size()]);
        boolean bFindKeyboard=false;
        for (int i=0;i<USBDeviceArray.length;i++) {
            if (USBDeviceArray[i].equals("2-1.2")) {
//                GlobalClass.getInstance().HM_KeyboardStatus = 1;
                bFindKeyboard = true;
                break;
            }
        }
        if (!bFindKeyboard) {
            GlobalClass.getInstance().HM_KeyboardStatus = 0;
        }
        boolean bFindWifi=false;
        for (int i=0;i<map.keySet().size();i++) {
            SysBusUsbDevice usb = (SysBusUsbDevice)map.get(USBDeviceArray[i]);
            USBDeviceArray[i] = usb.getReportedProductName();
            if (USBDeviceArray[i].length()>0 && (USBDeviceArray[i].contains("WLAN")||USBDeviceArray[i].contains("USB"))) {
                if (USBDeviceArray[i].contains("WLAN")){
                    GlobalClass.getInstance().HM_WiFiStatus = 1;
                }
                GlobalClass.getInstance().HM_USBStatus = 1;
                bFindWifi = true;
                break;
            }
            if (!bFindWifi) {
                GlobalClass.getInstance().HM_WiFiStatus = 0;
                GlobalClass.getInstance().HM_USBStatus = 0;
            }
        }
    }

    private Runnable showLEDTime = new Runnable() {
        public void run() {
            try {
                LEDFlicker++;
                if (LEDFlicker==4) LEDFlicker = 0;
                Constants.MACHINE_STATE state = GlobalClass.getInstance().MachineState;
                if (state == Constants.MACHINE_STATE.STERI_RUN) {
                    if (LEDFlicker==0) {
                        setGpio(11, 0);
                        setGpio(12, 0);
                        setGpio(13, 0);
                    }else{
                        setGpio(11, 0);
                        setGpio(12, 0);
                        setGpio(13, 1);
                    }
                }else if (state== Constants.MACHINE_STATE.STERI_COMPLETE) {
                    setGpio(11, 0);
                    setGpio(12, 1);
                    setGpio(13, 0);
                }else if (state== Constants.MACHINE_STATE.STERI_FAIL) {
                    setGpio(11, 1);
                    setGpio(12, 0);
                    setGpio(13, 0);
                }else  {
                    setGpio(11, 0);
                    setGpio(12, 0);
                    setGpio(13, 1);
                }
                handler.postDelayed(this, 1000);
            }catch (Exception e){

            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    void sendToActivity(char[] sendData) {
        final Intent intent = new Intent(ACTION_FROM_SERVICE);
        intent.putExtra("data", sendData);
        sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        final IntentFilter myFilter = new IntentFilter(ACTION_TO_SERVICE);
        registerReceiver(mReceiver, myFilter);
        GlobalClass.getInstance().isSerialServiceRun = true;
        GlobalClass.getInstance().mSendCmdQueue = new ArrayList<char[]>();
        handler.postDelayed(mBleSendCmdRunnable, 500);
        handler.postDelayed(updateWaterStatus, 1000);
        mUsbManagerLinux = new SysBusUsbManager(com.sturdy.hmi.USBList.util.Constants.PATH_SYS_BUS_USB);

        try {
            if (GlobalClass.getInstance().client == null) {
                GlobalClass.getInstance().client = HmiClient.getInstance();
                GlobalClass.getInstance().client.initSDK();
                GlobalClass.getInstance().client.setBootPackage(this.getApplicationContext(),"com.sturdy.hmi");
                random = new Random();
                highArray = new int[3];
                openUart();
                handler.postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        char[] f0_data = new char[]{0x01, 0xf0};
                        sendUartHex(1,f0_data);
                        char[] f4_data = new char[]{0x01, 0xf4};
                        sendUartHex(1,f4_data);
//                        char[] f3_data = new char[5];
//                        f3_data[0] = 0x04;
//                        f3_data[1] = 0xf3;
//                        f3_data[2] = Constants.instance().fetchValueInt(AutoAddWaterKey) > 0?(char)1:0;
//                        f3_data[3] = Constants.instance().fetchValueInt(PressuizedcoolingKey) > 0?(char)1:0;
//                        f3_data[4] = Constants.instance().fetchValueInt(ExhaustTempKey) > 0?(char)1:0;
//                        sendUartHex(1,f3_data);
                    }}, 10000);

                handler.postDelayed(showLEDTime, 100);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onDestroy() {
        GlobalClass.getInstance().isSerialServiceRun = false;
        handler.removeCallbacks(updateWaterStatus);
        handler.removeCallbacks(showTime);
        handler.removeCallbacks(showLEDTime);
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private Runnable showTime = new Runnable() {
        public void run() {
//            Log.i("time:", new Date().toString());
            try {
//                char[] data = new char[]{0x02, cmdData[cmdNum],(char)random.nextInt(2)};
//                cmdNum++;
//                client.sendUartByte(data, data.length);
//                if (cmdNum>=cmdData.length)
//                    cmdNum = 0;
//                client.sendUartString("Model:Prime 60B\r\n");
                for (int i = 0; i < highArray.length; i++) {
                    highArray[i] = random.nextInt(2);
                    Log.i("highArray:", Integer.toString(highArray[i]));
                }
                setGpio(11, highArray[0]);
                setGpio(12, highArray[1]);
                setGpio(13, highArray[2]);

//                setGpio(17,high);           // Boot 0
//                setGpio(18,high);           // Reset

                if (high==0) {
                    high = 1;
                }else {
                    high = 0;
                }
                handler.postDelayed(this, 1000);
            }catch (Exception e){

            }
        }
    };

    private Runnable printerTime = new Runnable() {
        public void run() {
//            Log.i("time:", new Date().toString());
            try {

                sendUartMsg(0,"Step   Time   ts   Temp   Pres.\r\n");
                sendUartMsg(0,"     mmm:ss  mm:ss  'C     bar \r\n");
                sendUartMsg(0,"Start  000:00 00:00 xxx.x x.xx\r\n");

                handler.postDelayed(this, 5000);
            }catch (Exception e){

            }
        }
    };

    public void openUart(){
        int count =0;
        int openCount = 0;
        StringBuffer sbHint = new StringBuffer();
        GlobalClass.getInstance().portMap = new HashMap<>();
        sbHint.append("串口");
        boolean firstAppend = true;
        for(int i=0;i<2;i++){
            boolean chooice = true;
            if(chooice){
                boolean isOpen = GlobalClass.getInstance().client.openUart(count,this,i+1,HmiClient.BAUD_RATE_115200);
                int port = i+1;
                GlobalClass.getInstance().portMap.put(count,port);
                if(isOpen){
                    openCount++;
                    if(firstAppend){
                        firstAppend = false;
                        sbHint.append(""+Integer.valueOf(i));
                    }else{
                        sbHint.append(","+Integer.valueOf(i));
                    }
                }
                count++;
            }
        }

//        setGpio(17,0);           // Boot 0
//        setGpio(18,1);           // Reset

    }

    public void sendUartMsg(int select_port,String sendMsg){
        if (GlobalClass.getInstance().MachineState!= Constants.MACHINE_STATE.CB_DFU) {
            GlobalClass.getInstance().client.sendUartString(GlobalClass.getInstance().getKeyFromMap(GlobalClass.getInstance().portMap, select_port + 1), sendMsg);
        }
    }

    public void sendUartHex(int select_port,char[] sendMsg){
        if (GlobalClass.getInstance().MachineState!= Constants.MACHINE_STATE.CB_DFU) {
            if (select_port==1){                // CB
                GlobalClass.getInstance().mSendCmdQueue.add(sendMsg);
            }else{                              // Printer
                //if (Constants.instance().fetchValueInt(saveRealPrint) > 0) {
                    GlobalClass.getInstance().client.sendUartByte(GlobalClass.getInstance().getKeyFromMap(GlobalClass.getInstance().portMap, select_port + 1), sendMsg, sendMsg.length);
                //}
            }
        }

    }



//    public void onOpenOrCloseUart(boolean open)
//    {
//        if(open)
//        {
//            boolean ret=false;
//            ret = client.openUart(this, port, HmiClient.BAUD_RATE_115200, 0, 0);
//            if(ret)
//            {
//            }
//        }
//        else
//        {
//            client.closeUart();
//        }
//    }

    public void setGpio(int gpio_num, int high) {
        GlobalClass.getInstance().client.setGpioState(gpio_num, high);
    }

    public void setGpioL(int gpio_num)
    {
        GlobalClass.getInstance().client.setGpioState(gpio_num, 0);
    }

    public void setGpioH(int gpio_num)
    {
    }

    public void getGpioStatus(int gpio_num)
    {
//        for(int i=0;i<allState.length;i++)
//        {
//            //allState[i].setText("State");
//            //allState[i].setTextColor(Color.BLACK);
//            allState[i].setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
//            int index = Integer.valueOf(allState[i].getTag().toString()) ;
//            if(index==gpio_num)
//            {
//                int ret =client.getGpiosState(gpio_num);
//                if(ret!=0) {
//                    allState[i].setText("High");
//                } else {
//                    allState[i].setText("Low");
//                }
//                allState[i].setTextColor(Color.RED);
//                allState[i].setTypeface(Typeface.DEFAULT,Typeface.BOLD);
//            }
//        }

    }

    @Override
    public void UartData(final int i, final char[] chars) {
        if (chars!=null && chars.length>0) {
            if (GlobalClass.getInstance().MachineState!= Constants.MACHINE_STATE.CB_DFU) {
                sendToActivity(chars);
                char[] buf = new char[chars.length+1];
                System.arraycopy(chars, 0, buf, 1, chars.length);
                buf[0] = 0x00;
                GlobalClass.getInstance().globalArrayList.add(buf);
                Calendar calendar = Calendar.getInstance();
                String topTime = String.format("%02d:%02d:%02d:",calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
                String receviceMsg = UntiTools.CharToString(chars,chars.length);
                appendLog("C->H"+topTime+receviceMsg);
                if (chars.length>1) {
                    if (chars[1] == 0xf1) {
                        GlobalClass.getInstance().CB_DoorLock = chars[2];
                        GlobalClass.getInstance().CB_CleanWaterLevel = chars[3];
                    }
                    if (chars[1] == 0x51) {
                        int Error = 2;
                        Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
                        broadcast.putExtra("Error",Error);
                        sendBroadcast(broadcast);
                    }
                    if (chars[1] == 0x52) {
                        int Error = 3;
                        Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
                        broadcast.putExtra("Error",Error);
                        sendBroadcast(broadcast);
                    }
                    if (chars[1] == 0x53) {
                        char[] ch2 = new char[2];
                        System.arraycopy(chars, 2, ch2, 0, 2);
                        int Error = CharUtils.charArrayToInt(ch2);
                        Intent broadcast = new Intent(HMI_ERROR_BROADCAST);
                        broadcast.putExtra("Error",Error);
                        sendBroadcast(broadcast);
                    }
                }
            }else{
                GlobalClass.getInstance().writeDfuBuffer(chars);
                String receviceMsg = UntiTools.CharToString(chars,chars.length);
                GlobalClass.getInstance().appendLog("U:"+receviceMsg);
            }

        }

//        final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
//        globalVariable.getGlobalArrayList().add(chars);
//        Log.i(TAG,"接收到的数据："+chars);
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                String receviceMsg = "";
//                if(isReceviceHex){
//                    receviceMsg = UntiTools.CharToString(chars,chars.length);
//                }else{
//                    receviceMsg = new String(chars);
//                }
//                if(portMap!=null){
//                    int port = portMap.get(i);
//                    tvMsg.append("串口 "+Integer.valueOf(port-1)+" 收到的信息："+receviceMsg+"\n");
//                }else{
//                    tvMsg.append("收到的信息："+receviceMsg+"\n");
//                }
//                //滑动到底部
//                svMsg.scrollTo(0,svMsg.getScrollY()+svMsg.getMeasuredHeight());
//            }
//        });
    }

    public void appendLog(String text)
    {
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "uart_log.txt");
        if (!logFile.exists())
        {
			try
			{
				logFile.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
            return;
        }
        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


//    @Override
//    public void UartData(final char[] data)
//    {
//        // TODO Auto-generated method stub
//        String s = String.valueOf(data);
//        Log.d("uart", "cmd"+s);
////        this.runOnUiThread(new Runnable()
////        {
////
////            @Override
////            public void run()
////            {
////                // TODO Auto-generated method stub
////
////                Log.e("TAG", new String(data));
////                tvMsg.append(new String(data));
////
////                int offset=tvMsg.getLineCount()*tvMsg.getLineHeight();
////                if(offset>tvMsg.getHeight())
////                {
////                    tvMsg.scrollTo(0,offset-tvMsg.getHeight());
////                }
////
////            }
////        });
//    }

}
