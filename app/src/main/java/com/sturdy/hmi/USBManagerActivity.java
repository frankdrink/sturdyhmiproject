/*******************************************************************************
 * Copyright 2011 Alexandros Schillings
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.sturdy.hmi;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.R;
import com.sturdy.hmi.USBList.data.DataProviderCompanyInfo;
import com.sturdy.hmi.USBList.data.DataProviderCompanyLogo;
import com.sturdy.hmi.USBList.data.DataProviderUsbInfo;
import com.sturdy.hmi.USBList.ui.common.DialogFactory;
import com.sturdy.hmi.USBList.ui.common.Navigation;
import com.sturdy.hmi.USBList.ui.main.tabs.TabController;
import com.sturdy.hmi.USBList.ui.main.tabs.TabViewHolder;
import com.sturdy.hmi.USBList.ui.progress.ProgressDialogControl;
import com.sturdy.hmi.USBList.ui.usbinfo.fragments.FragmentFactory;
import com.sturdy.hmi.USBList.util.Constants;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbDevice;
import com.sturdy.usbdeviceenumerator.sysbususb.SysBusUsbManager;

public class USBManagerActivity extends BaseToolBarActivity {
    final String TAG = this.getClass().getName();
    String[] USBDeviceArray;
    String[] USBDeviceSortArray;
    String[] USBDeviceNameArray;

    private UsbManager mUsbManAndroid;
    private SysBusUsbManager mUsbManagerLinux;
    private ViewPager viewPager;

    private DataProviderUsbInfo mDbUsb;
    private DataProviderCompanyInfo mDbComp;
    private DataProviderCompanyLogo mZipComp;

    private Map<String, UsbDevice> mAndroidDeviceMap;
    private Map<String, SysBusUsbDevice> mLinuxDeviceMap;

    private ProgressDialogControl progressDialogControl;
    private Navigation mNavigation;

    private TabController mTabController;

    private void checkIfDbPresent() {
        // Prompt user to DL db if it is missing.
//        if (!new File(mDbUsb.getDataFilePath()).exists()) {
//            DialogFactory.createOkDialog(this,
//                    R.string.alert_db_not_found_title,
//                    R.string.alert_db_not_found_instructions)
//                    .show();
//            Log.w(TAG, "^ Database not found: " + mDbUsb.getDataFilePath());
//        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_usbmanager);
        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Button mNavTitleView = mNavTopBar.addLeftTextButton("USB Device", R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.wi_fi_icon_search, R.id.topbar_unwrapped_right_2_button, 64);
        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshUsbDevices();
            }
        });

        mTabController = new TabController(this);
        mNavigation = new Navigation(this);

        mUsbManAndroid = (UsbManager) getSystemService(Context.USB_SERVICE);
        mUsbManagerLinux = new SysBusUsbManager(Constants.PATH_SYS_BUS_USB);

        mDbUsb = new DataProviderUsbInfo(this);
        mDbComp = new DataProviderCompanyInfo(this);
        mZipComp = new DataProviderCompanyLogo(this);

        mTabController.setup(new TabController.OnTabChangeListener() {
            @Override
            public void onTabChangeListener(String tag, TabViewHolder holder) {
                onTabChanged(tag, holder);
            }
        });

        // Setup android list - tab1;
        mTabController.getHolderForTag(TabController.TAB_ANDROID_INFO)
                .getList().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((ListView) parent).setItemChecked(position, true);
                mNavigation.showLinuxUsbDeviceInfo(mLinuxDeviceMap.get(((TextView) view).getText().toString()));
            }
        });


//        // Setup linux list - tab2
//        mTabController.getHolderForTag(TabController.TAB_LINUX_INFO)
//                .getList().setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ((ListView) parent).setItemChecked(position, true);
//                mNavigation.showLinuxUsbDeviceInfo(mLinuxDeviceMap.get(((TextView) view).getText().toString()));
//            }
//        });
        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        checkIfDbPresent();
        refreshUsbDevices();
    }

    /**
     * Creates the menu items
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Handles item selections
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_about:
//                AboutDialogFactory.createAboutDialog(this).show();
//                return true;
//            case R.id.menu_debug:
//                final Intent intent = new Intent(this, DebugActivity.class);
//                ActivityCompat.startActivity(this, intent, null);
//                return true;
//            case R.id.menu_update_db:
//                final ProgressDialogControl control = new ProgressDialogControl(getSupportFragmentManager());
//                final DatabaseUpdater databaseUpdater = new DatabaseUpdater(control, mDbComp, mDbUsb, mZipComp);
//
//                databaseUpdater.start(this);
//                return true;
//            case R.id.menu_refresh:
//                refreshUsbDevices();
//                return true;
        }

        return false;
    }

    private void onTabChanged(String tabId, TabViewHolder tabViewHolder) {
        if (mNavigation.isSmallScreen()) {
            return;
        }

        final ListView listView = tabViewHolder.getList();
        final int checkedItemPosition = listView.getCheckedItemPosition();
        final Fragment fragment;

        if (checkedItemPosition == ListView.INVALID_POSITION) {
            fragment = null;
        } else {
            final String text = (String) listView.getItemAtPosition(checkedItemPosition);

            switch (tabId) {
                case TabController.TAB_ANDROID_INFO:
                    fragment = FragmentFactory.getFragment(text);
                    break;
//                case TabController.TAB_LINUX_INFO:
//                    fragment = FragmentFactory.getFragment(mLinuxDeviceMap.get(text));
//                    break;
                default:
                    fragment = null;
                    break;
            }
        }

        if (fragment == null) {
            mNavigation.removeFragmentsFromContainer();
        } else {
            mNavigation.stackFragment(fragment);
        }
    }


    private void refreshUsbDevices() {
//        mAndroidDeviceMap = mUsbManAndroid.getDeviceList();
        mLinuxDeviceMap = mUsbManagerLinux.getUsbDevices();

        updateList(mTabController.getHolderForTag(TabController.TAB_ANDROID_INFO), mLinuxDeviceMap);
//        updateList(mTabController.getHolderForTag(TabController.TAB_LINUX_INFO), mLinuxDeviceMap);
    }

    private void updateList(final TabViewHolder holder, final Map<String, ?> map) {
        USBDeviceArray = map.keySet().toArray(new String[map.keySet().size()]);
        USBDeviceNameArray = map.keySet().toArray(new String[map.keySet().size()]);
        int totalUSB=0;
        for (int i=0;i<map.keySet().size();i++) {
            SysBusUsbDevice usb = (SysBusUsbDevice)map.get(USBDeviceArray[i]);
            USBDeviceArray[i] = usb.getReportedProductName();
            if (USBDeviceArray[i].length()>0 && !USBDeviceArray[i].contains("Freescale")) {
                totalUSB++;
            }
        }
        USBDeviceSortArray = new String[totalUSB];
        int index=0;
        for (int i=0;i<USBDeviceArray.length;i++) {
            if (USBDeviceArray[i].length()>0 && !USBDeviceArray[i].contains("Freescale")) {
                USBDeviceSortArray[index] = USBDeviceArray[i];
                index++;
            }
        }
//        Arrays.sort(USBDeviceArray);

        final ListAdapter adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_item, USBDeviceSortArray);
        holder.getList().setAdapter(adapter);

        final String count = getString(R.string.text_number_of_devices, USBDeviceArray.length);
//        holder.getCount().setText(count);
    }
}
