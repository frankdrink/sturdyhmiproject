package com.sturdy.hmi.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sturdy.hmi.R;
import com.sturdy.hmi.item.StartSterInformationItemA;

import java.util.List;

public class CleaningFailedGridViewAdapter1 extends RecyclerView.Adapter<CleaningFailedGridViewAdapter1.ViewHolder> {
    private List<StartSterInformationItemA> items;
    private Activity activity;

    public CleaningFailedGridViewAdapter1(Activity activity, List<StartSterInformationItemA> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.cleaningfailed_grid1, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CleaningFailedGridViewAdapter1.ViewHolder viewHolder,final int position) {

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView cleaning_textView;


        public ViewHolder(View view) {
            super(view);
            cleaning_textView = (TextView)view.findViewById(R.id.cleaning_text);
            cleaning_textView.setText("Cleaning");
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
