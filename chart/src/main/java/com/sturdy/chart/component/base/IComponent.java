package com.sturdy.chart.component.base;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/** 圖表組件
 */

public interface IComponent<T> {

    int LEFT = 0;
    int TOP = 1;
    int RIGHT =2;
    int BOTTOM = 3;
    /**
     * 計算組件Rect
     * @param chartRect
     */
    void computeRect(Rect chartRect);

    /**
     * 繪製組件
     * @param canvas 畫布
     * @param  t 數據
     * @param paint 畫筆
     */
    void draw(Canvas canvas, T t, Paint paint);
}
