package com.sturdy.hmi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.sturdy.drawme.DrawMeButton;
import com.sturdy.drawme.DrawMeImageButton;
import com.sturdy.hmi.Dialog.NumberDialog;
import com.sturdy.hmi.Dialog.PreScanDialog;
import com.sturdy.hmi.adapter.SteriEditAdapter;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.sql.DatabaseHelper;
import com.sturdy.hmi.utils.CharUtils;
import com.sturdy.hmi.utils.RecyclerViewClass;
import com.sturdy.hmi.utils.RecyclerViewDecorator;
import com.sturdy.hmi.utils.UnitConvert;
import com.stx.xhb.commontitlebar.CustomTitleBar;
import com.stx.xhb.commontitlebar.widget.UIAlphaImageButton;

import java.util.ArrayList;
import java.util.List;

import com.sturdytheme.framework.picker.OptionPicker;
import com.sturdytheme.framework.picker.TimePicker;
import com.sturdytheme.framework.util.ConvertUtils;
import com.sturdytheme.framework.widget.WheelView;

import static com.sturdy.hmi.Constants.ACTION_FROM_SERVICE;
import static com.sturdy.hmi.Constants.VacuumpumpKey;
import static com.sturdy.hmi.Constants.preScanControl;
import static com.sturdy.hmi.Constants.saveTempFormat;

public class LatexEditActivity extends BaseToolBarActivity implements NumberDialog.OnNumberDialogDoneListener,SteriEditAdapter.UpdateMainClass{
    ListView list;
    private Activity mActivity;


    private String[] switchArray = {"ON","OFF"};

    private int highTemp;
    private int lowTemp;
    private RecyclerView mRecyclerView;
    private SteriEditAdapter mAdapter;
    private List<RecyclerViewClass> mItems;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawMeButton mStartSterButton;
    private boolean bFieldmodified = false;
    private String[] pickerTempArrayList;
    private String[] pickerDryTimeArrayList;

    private String str;

    private boolean isFirstPrescan = true;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                char[] recv = intent.getCharArrayExtra("data");
                if (recv[1] == 0x14) { // ID(0~20); 預抽真空(ON/OFF); 滅菌溫度(1050~1350,Word,0.1℃); 滅菌時間(60~18000,Word,秒); 乾燥時間(0~3600,Word,秒)
                    char[] ch2 = new char[2];
                    GlobalClass.getInstance().CB_PreVacuum = recv[2]!=0;
                    System.arraycopy(recv, 3, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTemp = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 5, ch2, 0, 2);
                    GlobalClass.getInstance().CB_SteriTime = CharUtils.charArrayToInt(ch2);
                    System.arraycopy(recv, 7, ch2, 0, 2);
                    GlobalClass.getInstance().CB_DryTime = CharUtils.charArrayToInt(ch2);
                    if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
                        str = GlobalClass.getInstance().CB_PreVacuum == true ? "ON" : "OFF";
                        mItems.set(0, new RecyclerViewClass("Pre-Vacuum", str, R.drawable.parameter_icon_prevacuum, true));
                    }else{
                        mItems.set(0, new RecyclerViewClass("", "", R.drawable.parameter_icon_prevacuum,true));
                    }
                    str = UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_SteriTemp);
                    mItems.set(1, new RecyclerViewClass("Ster. Temp.", str, R.drawable.parameter_icon_temp,true));
                    str = UnitConvert.formatTimeHM(GlobalClass.getInstance().CB_SteriTime);
                    mItems.set(2, new RecyclerViewClass("Ster. Time", str, R.drawable.parameter_icon_time,true));
                    str = String.format("%d m",GlobalClass.getInstance().CB_DryTime/60);
                    mItems.set(3, new RecyclerViewClass("Dry Time", str, R.drawable.parameter_icon_time,true));
                    mAdapter.notifyDataSetChanged();
                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_waste_edit);
        mActivity = this;
        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);
        GlobalClass.getInstance().isFloatingTemp = false;

        CustomTitleBar mTopBar = (CustomTitleBar) findViewById(R.id.status_bar);
        setCustomTitleBar(mTopBar);
        CustomTitleBar mNavTopBar = (CustomTitleBar) findViewById(R.id.navigation_bar);
        mNavTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mNavTopBar.setBackgroundDividerEnabled(true);
        backButton = mNavTopBar.addLeftImageButton(R.drawable.arrow_icon_back, R.id.topbar_unwrapped_left_1_button, 64);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        startShowTitleClock(rightTimeButton);
        Intent intent = this.getIntent();
        final String name = intent.getStringExtra("name");
        final String sterTemp = intent.getStringExtra("temp");
        Button mNavTitleView = mNavTopBar.addLeftTextButton(name, R.id.topbar_unwrapped_left_title, Color.BLACK,38);
        mNavTitleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
                }
                return false;
            }
        });
        UIAlphaImageButton rightToolsButton1 = mNavTopBar.addRightImageButton(R.drawable.toolbar_barcode_dark, R.id.topbar_unwrapped_right_2_button, 64);
        UIAlphaImageButton rightToolsButton2 = mNavTopBar.addRightImageButton(R.drawable.toolbar_information, R.id.topbar_unwrapped_right_3_button, 64);

        highTemp = UnitConvert.TempConvert(135);
        lowTemp = UnitConvert.TempConvert(105);
        pickerTempArrayList = new String[highTemp-lowTemp+1];
        for (int i=0;i<=(highTemp-lowTemp);i++) {
            pickerTempArrayList[i] = Integer.toString(i+lowTemp)+((Constants.instance().fetchValueInt(saveTempFormat)==0)?" °C":" °F");
        }
        pickerDryTimeArrayList = new String[61];
        for (int i=0;i<=60;i++) {
            pickerDryTimeArrayList[i] = Integer.toString(i)+" m";
        }

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new RecyclerViewDecorator(this));
        mItems = new ArrayList<>();
        if (Constants.instance().fetchValueInt(VacuumpumpKey) > 0) {
            mItems.add(0, new RecyclerViewClass("Pre-Vacuum", "", R.drawable.parameter_icon_prevacuum, true));
        }else{
            mItems.add(0, new RecyclerViewClass("", "", R.drawable.parameter_icon_prevacuum,true));
        }
        mItems.add(1, new RecyclerViewClass("Ster. Temp.", "", R.drawable.parameter_icon_temp,true));
        mItems.add(2, new RecyclerViewClass("Ster. Time", "", R.drawable.parameter_icon_time,true));
        mItems.add(3, new RecyclerViewClass("Dry Time", "", R.drawable.parameter_icon_time,true));

        mAdapter = new SteriEditAdapter(this, mItems, true);
        mRecyclerView.setAdapter(mAdapter);

        rightToolsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LatexEditActivity.this,BarCodeScanActivity.class);
                intent.putExtra("StartSteri",0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        rightToolsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LatexEditActivity.this,SolidInformationActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("temp",sterTemp);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });


        registerBaseActivityReceiver();
        DrawMeImageButton HomeButton =(DrawMeImageButton) findViewById(R.id.main_menu_button);
        HomeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    closeAllActivities();
                }
                return false;
            }
        });

        mStartSterButton = (DrawMeButton)findViewById(R.id.startster_button);
        mStartSterButton.setTextSize(30);
        mStartSterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper helper = new DatabaseHelper(mActivity);
                List<BarCode> allBarCodeList = helper.getCustomerDao().getAllBarCode();
                if (((Constants.instance().fetchValueInt(preScanControl))==1) && (isFirstPrescan) && allBarCodeList.size()==0){
                    isFirstPrescan = false;
                    new PreScanDialog(mActivity)
                            .setDialogType(PreScanDialog.DIALOG_TYPE_SUCCESS)
                            .setAnimationEnable(true)
                            .setTitleText("Pre scan")
                            .setContentText("Pre scan is activated, please scan first and then will be run program")
                            .setScanListener("Scan", new PreScanDialog.OnScanListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(LatexEditActivity.this, BarCodeScanActivity.class);
                                    intent.putExtra("StartSteri",1);
                                    startActivity(intent);
                                }
                            })
                            .setPositiveListener("Cancle", new PreScanDialog.OnPositiveListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendGetSterillzationStep();
                                }
                            })
                            .setSkipListener("Skip", new PreScanDialog.OnSkipListener() {
                                @Override
                                public void onClick(PreScanDialog dialog) {
                                    dialog.dismiss();
                                    sendCustomizationSetting();
                                    sendGetSterillzationStep();
                                }
                            }).show();
                }else {
                    sendCustomizationSetting();
                    sendGetSterillzationStep();
                }
            }
        });
        // 取得消毒行程資(Solid 1)
        char[] data = new char[]{0x01,0x14};
        sendToService(data);

    }

    void sendCustomizationSetting() {   // 滅菌溫度(1050~1350,Word,0.1℃); 滅菌時間(60~18000,Word,秒); 乾燥時間(0~3600,Word,秒)
        char[] data = new char[9];
        char[] ch22 = new char[2];
        data[0] = 0x08;
        data[1] = 0x28;
        data[2] = GlobalClass.getInstance().CB_PreVacuum ? (char)1:(char)0;
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_SteriTemp);
        data[3] = ch22[0];
        data[4] = ch22[1];
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_SteriTime);
        data[5] = ch22[0];
        data[6] = ch22[1];
        ch22 = CharUtils.fromShort((short)GlobalClass.getInstance().CB_DryTime);
        data[7] = ch22[0];
        data[8] = ch22[1];
        sendToService(data);
    }



    @Override
    public void updateItemList(final int position) {
//        if (position==0) {
//            OptionPicker picker = new OptionPicker(mActivity, switchArray);
//            picker.setCanceledOnTouchOutside(false);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setSelectedItem(mItems.get(position).getMessage2());
//            picker.setTitleTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
//                @Override
//                public void onOptionPicked(int index, String item) {
//                    GlobalClass.getInstance().CB_PreVacuum = index==0?true:false;
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
//        }else if (position==1) {
//            OptionPicker picker = new OptionPicker(LatexEditActivity.this, pickerTempArrayList);
//            picker.setSelectedItem(UnitConvert.sTempConvert(1,GlobalClass.getInstance().CB_SteriTemp));
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setTitleTextSize(40);
//            picker.setCanceledOnTouchOutside(true);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setTitleTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
//                @Override
//                public void onOptionPicked(int index, String item) {
//                    str = item;
//                    str = str.replaceAll("[^\\d]+", "");
//                    GlobalClass.getInstance().CB_SteriTemp = UnitConvert.TempConvertF(Integer.valueOf(str)) * 10;
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
//        }else if (position==2) {
//            TimePicker picker = new TimePicker(this, TimePicker.HOUR_24);
//            picker.setTitleText(mItems.get(position).getMessage1());
//            picker.setTitleTextSize(40);
//            picker.setDividerVisible(true);
//            picker.setUseWeight(false);
//            picker.setCycleDisable(false);
//            picker.setRangeStart(0, 1);//00:00
//            picker.setRangeEnd(6, 59);//23:59
//            int currentHour = GlobalClass.getInstance().CB_SteriTime/3600;
//            int currentMinute = (GlobalClass.getInstance().CB_SteriTime%3600)/60;
//            picker.setSelectedItem(currentHour, currentMinute);
//            picker.setTopLineVisible(true);
//            picker.setCanceledOnTouchOutside(true);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setTextPadding(ConvertUtils.toPx(this, 15));
//            picker.setOnTimePickListener(new TimePicker.OnTimePickListener() {
//                @Override
//                public void onTimePicked(String hour, String minute, String timeAt) {
//                    GlobalClass.getInstance().CB_SteriTime = Integer.valueOf(hour)*60*60+Integer.valueOf(minute)*60;
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), hour+" h "+minute+" m", mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
//        }else if (position==3) {
//            OptionPicker picker = new OptionPicker(LatexEditActivity.this, pickerDryTimeArrayList);
//            picker.setSelectedItem(Integer.toString(GlobalClass.getInstance().CB_DryTime/60)+" m");
//            picker.setCanceledOnTouchOutside(true);
//            picker.setTopHeight(50);
//            picker.setSubmitTextSize(40);
//            picker.setDividerRatio(WheelView.DividerConfig.FILL);
//            picker.setShadowColor(Color.WHITE, 40);
//            picker.setCycleDisable(true);
//            picker.setTextSize(32);
//            picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
//                @Override
//                public void onOptionPicked(int index, String item) {
////                        showToast("index=" + index + ", item=" + item);
//                    str = item;
//                    str = str.replaceAll("[^\\d]+", "");
//                    GlobalClass.getInstance().CB_DryTime = Integer.valueOf(str) * 60;
//                    RecyclerViewClass recycler = new RecyclerViewClass(mItems.get(position).getMessage1(), item, mItems.get(position).getmImage(),mItems.get(position).getmIsTextViewEditable());
//                    mItems.set(position,recycler);
//                    mAdapter.notifyDataSetChanged();
//                }
//            });
//            picker.show();
//        }
    }

    @Override
    public void updateListBackground(int position, boolean isChecked) {
//        try {
//            mItems.get(position).setmIsChecked(isChecked);
//            mAdapter.notifyItemChanged(position);
//        }catch(IllegalStateException e){
//        }
    }

    @Override
    public void onDone(int value) {
//        iNextService = value;
//        valueStr = Integer.toString(iNextService);
//        data1.set(1,valueStr);
//        adapter1.resetAdapter(data1);
//        adapter1.notifyDataSetChanged();
    }
}
