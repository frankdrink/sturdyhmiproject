package com.sturdy.hmi.adapter;
/**
 * Created by franklin on 2019/8/8.
 */
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sturdy.hmi.R;



public class EditUserListAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    EditUserListAdapter.UpdateMainClass updateMainClass;

    public EditUserListAdapter(Activity context,
                      String[] web, Integer[] imageId) {
        super(context, R.layout.steri_menu_list, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;
        if(context instanceof EditUserListAdapter.UpdateMainClass){
            updateMainClass = (EditUserListAdapter.UpdateMainClass)context;
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.edit_user_list, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(web[position]);

        imageView.setImageResource(imageId[position]);

        ImageButton delete_btn = (ImageButton)  rowView.findViewById(R.id.ibDeleteCustomer);
        if (position==0) {
            delete_btn.setVisibility(View.INVISIBLE);
        }

        txtTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    updateMainClass.updateItemList(position);
                }
                return false;
            }
        });

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    updateMainClass.updateItemList(position);
                }
                return false;
            }
        });

        delete_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    updateMainClass.deleteItemList(position);
                }
                return false;
            }
        });
        return rowView;
    }

    public interface UpdateMainClass{
        void deleteItemList(int position);
        void updateItemList(int position);
    }
}
