package com.sturdy.chart.provider.component.grid;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;


public interface IGrid {
   /**
    * 繪製網格
    * @param canvas
    * @param x
    * @param rect
    * @param perWidth
    * @param paint
    */
   void drawGrid(Canvas canvas, float x, Rect rect, int perWidth, Paint paint);

   /**
    * 繪製點擊背景
    * @param canvas
    * @param pointF
    * @param rect
    * @param perWidth
    * @param paint
    */
   void drawClickBg(Canvas canvas, PointF pointF, Rect rect, int perWidth, Paint paint);

   /**
    * 繪製列內容
    * @param canvas
    * @param position
    * @param rect
    * @param perWidth
    * @param paint
    */
   void drawColumnContent(Canvas canvas, int position, Rect zoomRect, Rect rect, int perWidth, Paint paint);
}
