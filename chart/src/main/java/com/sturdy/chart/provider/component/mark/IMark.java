package com.sturdy.chart.provider.component.mark;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.sturdy.chart.data.ColumnData;

/**
 * Created by Frank on 2019/9/28.
 */

public  interface IMark<C extends ColumnData> {
    void drawMark(Canvas canvas,float x, float y,Rect rect, String content, C data, int position);
}
