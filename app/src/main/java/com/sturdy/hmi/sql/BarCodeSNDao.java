package com.sturdy.hmi.sql;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sturdy.hmi.R;
import com.sturdy.hmi.model.BarCode;
import com.sturdy.hmi.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by franklin on 2019/8/14.
 */

public class BarCodeSNDao {
    public static final String TABLE_NAME = "customer" ;
    public static final String COL_ID = "_id" ;
    public static final String COL_FIRST_NAME = "firstName" ;
    public static final String COL_LAST_NAME = "lastName" ;
    private static DatabaseHelper healper = null ;
    private static String TAG ="CustomerDao" ;
    /**
     * I dont want that anyone can create an instance of CustomerDAo except SqliteHelper
     */


    protected static BarCodeSNDao getInstance(DatabaseHelper helper){
        healper = helper;
        return new BarCodeSNDao();
    }

    /**
     * This method called once version of SQLiteHelper get updated
     * @param db is writable database passed from SQLiteHelper
     */
    public static void createCustomerTable(SQLiteDatabase db){
        String createQuery = "Create Table " +
                TABLE_NAME + " ( " +
                COL_ID + " Integer Primary key AutoIncrement , " +
                COL_FIRST_NAME + " text not null," +
                COL_LAST_NAME + " text not null )" ;
        db.execSQL(createQuery);

    }

    /**
     * Method called from SqliteHelper in case of upgrade
     * @param db
     */
    public static void upgradeCustomerTable(SQLiteDatabase db){
        // write upgrade query if any
        String upgradeQuery = "" ;
        db.execSQL(upgradeQuery);
        // db.close();
    }

    /**
     * Method used to add customer in custoemr table
     * @param customer
     * @return
     */
    public ReturnMessage addCustomer(BarCode customer){

        ReturnMessage rm = new ReturnMessage();
        ContentValues cv = new ContentValues();
        cv.put(COL_FIRST_NAME,customer.getSerialNumber());
        cv.put(COL_LAST_NAME,customer.getLastName());
        long i =healper.getWritableDatabase().insert(TABLE_NAME, null,cv) ;
        Log.v(TAG,"Row Id: " + i);
        if (i>0)
        {
            rm.message = healper.getContext().getString(R.string.customer_added_successfully)  ;
        }
        else
        {

            rm.success = false ;
            rm.message = healper.getContext().getString(R.string.customer_could_not_be_added)  ;
        }
        return rm ;
    }

    public List<BarCode> getAllBarCode() {
        // array of columns to fetch
        String []columns = new String[]{COL_ID,COL_FIRST_NAME,COL_LAST_NAME} ;

        // sorting orders
        String orderBy =  COL_FIRST_NAME + "," +COL_LAST_NAME ;

        List<BarCode> userList = new ArrayList<BarCode>();

        SQLiteDatabase db = healper.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                orderBy); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                BarCode barCode = new BarCode();
                barCode.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_ID))));
                barCode.setSerialNumber(cursor.getString(cursor.getColumnIndex(COL_FIRST_NAME)));
                barCode.setLastName(cursor.getString(cursor.getColumnIndex(COL_LAST_NAME)));
                // Adding user record to list
                userList.add(barCode);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }

    public void deleteAllBarCode() {
        SQLiteDatabase db = healper.getReadableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
        db.close();
    }

    /**
     * Method used to update customer
     * @param barCode
     * @return
     */
    public ReturnMessage updateCustomer(BarCode barCode){
        ReturnMessage rm = new ReturnMessage();
        ContentValues cv = new ContentValues();
        cv.put(COL_FIRST_NAME, barCode.getSerialNumber());
        cv.put(COL_LAST_NAME, barCode.getLastName());
        String whereClause = COL_ID + "=?" ;
        int i = healper.getWritableDatabase().update(TABLE_NAME ,cv, whereClause , new String[]{barCode.getId()+""} );
        if(i>0){
//            rm.message = healper.getContext().getString(R.string.customer_updated_successfully)  ;
        }
        else{
            rm.success = false ;
//            rm.message = healper.getContext().getString(R.string.customer_couldnt_be_updated)  ;
        }
        return rm ;
    }

    /**
     * Method used to delete customer record
     * @param id
     * @return
     */
    public ReturnMessage deleteCustomer(int id){
        ReturnMessage rm = new ReturnMessage();
        int i = healper.getWritableDatabase().delete(TABLE_NAME,COL_ID + "=?" , new String[]{id+""});
        if(i>0){
            rm.message = healper.getContext().getString(R.string.customer_deleted_successfully)  ; ;
        }
        else{
            rm.success = false ;
            rm.message = healper.getContext().getString(R.string.customer_couldnt_be_deleted)  ;
        }
        return rm ;
    }

    public Cursor getCustomers(BarCode customer){

        Cursor cursor = null ;
        String []columns = new String[]{COL_ID,COL_FIRST_NAME,COL_LAST_NAME} ;
        String selection = "" ;
        String selectionArg[] = null ;
        String orderBy =  COL_FIRST_NAME + "," +COL_LAST_NAME ;
        if( customer.getId() > 0 ){
            selection = COL_ID + "=?" ;
            selectionArg =new String[]{customer.getId()+""};
            cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, selection, selectionArg, null,null,orderBy);

        }
        else if(!customer.getSerialNumber().equals("") && !customer.getLastName().equals("")){
            selection = COL_FIRST_NAME + " like ?" +
                    " AND "+  COL_LAST_NAME + " like ?" ;
            selectionArg =new String[]{"%" + customer.getSerialNumber() + "%" , "%" + customer.getLastName() + "%"};

            cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, selection, selectionArg, null,null,orderBy);
        }
        else if(!customer.getSerialNumber().equals("") && customer.getLastName().equals("")){
            selection = COL_FIRST_NAME + " like ?"   ;
            selectionArg =new String[]{"%" + customer.getSerialNumber() + "%"};
            cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, selection, selectionArg, null,null,orderBy);
            // cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, selection, selectionArg, null,null,orderBy);
        }
        else if(customer.getSerialNumber().equals("") && !customer.getLastName().equals("")){
            selection = COL_LAST_NAME + " like ?" ;
            selectionArg = new String[]{"%" + customer.getLastName() + "%"};

            cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, selection, selectionArg, null,null,orderBy);
        }
        else if(customer.getSerialNumber().equals("") && customer.getLastName().equals("")){
            cursor = healper.getReadableDatabase().query(TABLE_NAME, columns, null, null, null,null,orderBy);
        }
        return cursor ;

    }
}
