package com.sturdy.hmi;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.sturdy.hmi.item.StartSterInformationItemA;

public class StartSterInformationFragmentB extends Fragment {
    Handler handler;
    RecyclerView recyclerView;

    Runnable runnable=new Runnable() {
        @Override
        public void run() {

            handler.postDelayed(this, 1000);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.operationalinfofragment, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handler = new Handler();
        handler.postAtTime(runnable, 1000);

        TableLayout tableLayout = (TableLayout) view.findViewById(R.id.tableLayout1);
        int textSize = 20;
        TableRow head_row= new TableRow(getContext());
        TableRow.LayoutParams head_lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        head_row.setLayoutParams(head_lp);
        TextView head_tv1 = new TextView(getContext());
        head_tv1.setGravity(Gravity.CENTER);
        head_tv1.setTextSize(textSize);
        head_tv1.setText("Step");
        TextView head_tv2 = new TextView(getContext());
        head_tv2.setGravity(Gravity.CENTER);
        head_tv2.setTextSize(textSize);
        head_tv2.setText("Time");
        TextView head_tv3 = new TextView(getContext());
        head_tv3.setGravity(Gravity.CENTER);
        head_tv3.setTextSize(textSize);
        head_tv3.setText("ts");
        TextView head_tv4 = new TextView(getContext());
        head_tv4.setGravity(Gravity.CENTER);
        head_tv4.setTextSize(textSize);
        head_tv4.setText("Temp.");
        TextView head_tv5 = new TextView(getContext());
        head_tv5.setGravity(Gravity.CENTER);
        head_tv5.setTextSize(textSize);
        head_tv5.setText("Pres.");
        head_row.addView(head_tv1);
        head_row.addView(head_tv2);
        head_row.addView(head_tv3);
        head_row.addView(head_tv4);
        head_row.addView(head_tv5);
        tableLayout.addView(head_row,0);

//        for (int i=0;i<10;i++) {
//            TableRow row= new TableRow(getContext());
//            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
//            row.setLayoutParams(lp);
//            TextView tv1 = new TextView(getContext());
//            tv1.setGravity(Gravity.CENTER);
//            tv1.setTextSize(textSize);
//            tv1.setText("Step");
//            TextView tv2 = new TextView(getContext());
//            tv2.setGravity(Gravity.CENTER);
//            tv2.setTextSize(textSize);
//            tv2.setText("Time");
//            TextView tv3 = new TextView(getContext());
//            tv3.setGravity(Gravity.CENTER);
//            tv3.setTextSize(textSize);
//            tv3.setText("ts");
//            TextView tv4 = new TextView(getContext());
//            tv4.setGravity(Gravity.CENTER);
//            tv4.setTextSize(textSize);
//            tv4.setText("Temp.");
//            TextView tv5 = new TextView(getContext());
//            tv5.setGravity(Gravity.CENTER);
//            tv5.setTextSize(textSize);
//            tv5.setText("Pres.");
//            row.addView(tv1);
//            row.addView(tv2);
//            row.addView(tv3);
//            row.addView(tv4);
//            row.addView(tv5);
//            tableLayout.addView(row,i+1);
//        }

        TableRow row1= new TableRow(getContext());
        TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row1.setLayoutParams(lp1);
        TextView tv1_1 = new TextView(getContext());
        tv1_1.setGravity(Gravity.CENTER);
        tv1_1.setTextSize(textSize);
        tv1_1.setText("");
        TextView tv1_2 = new TextView(getContext());
        tv1_2.setGravity(Gravity.CENTER);
        tv1_2.setTextSize(textSize);
        tv1_2.setText("mmm:ss");
        TextView tv1_3 = new TextView(getContext());
        tv1_3.setGravity(Gravity.CENTER);
        tv1_3.setTextSize(textSize);
        tv1_3.setText("mm:ss");
        TextView tv1_4 = new TextView(getContext());
        tv1_4.setGravity(Gravity.CENTER);
        tv1_4.setTextSize(textSize);
        tv1_4.setText("°C");
        TextView tv1_5 = new TextView(getContext());
        tv1_5.setGravity(Gravity.CENTER);
        tv1_5.setTextSize(textSize);
        tv1_5.setText("bar");
        row1.addView(tv1_1);
        row1.addView(tv1_2);
        row1.addView(tv1_3);
        row1.addView(tv1_4);
        row1.addView(tv1_5);
        tableLayout.addView(row1,1);

        TableRow row2= new TableRow(getContext());
        TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row2.setLayoutParams(lp2);
        TextView tv2_1 = new TextView(getContext());
        tv2_1.setGravity(Gravity.CENTER);
        tv2_1.setTextSize(textSize);
        tv2_1.setText("Start");
        TextView tv2_2 = new TextView(getContext());
        tv2_2.setGravity(Gravity.CENTER);
        tv2_2.setTextSize(textSize);
        tv2_2.setText("000:00");
        TextView tv2_3 = new TextView(getContext());
        tv2_3.setGravity(Gravity.CENTER);
        tv2_3.setTextSize(textSize);
        tv2_3.setText("00:00");
        TextView tv2_4 = new TextView(getContext());
        tv2_4.setGravity(Gravity.CENTER);
        tv2_4.setTextSize(textSize);
        tv2_4.setText("xxx.x");
        TextView tv2_5 = new TextView(getContext());
        tv2_5.setGravity(Gravity.CENTER);
        tv2_5.setTextSize(textSize);
        tv2_5.setText("x.xx");
        row2.addView(tv2_1);
        row2.addView(tv2_2);
        row2.addView(tv2_3);
        row2.addView(tv2_4);
        row2.addView(tv2_5);
        tableLayout.addView(row2,2);
        String[] stepStrings = {"PV1", "H1", "PV2", "H2", "PV3", "H3", "PV4", "H4", "S00-00", "EX"};
        for (int i=0;i<50;i++) {
            TableRow row= new TableRow(getContext());
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            TextView tv1 = new TextView(getContext());
            tv1.setGravity(Gravity.CENTER);
            tv1.setTextSize(textSize);
            tv1.setText(stepStrings[i%10]);
            TextView tv2 = new TextView(getContext());
            tv2.setGravity(Gravity.CENTER);
            tv2.setTextSize(textSize);
            tv2.setText(tv2_2.getText());
            TextView tv3 = new TextView(getContext());
            tv3.setGravity(Gravity.CENTER);
            tv3.setTextSize(textSize);
            tv3.setText(tv2_3.getText());
            TextView tv4 = new TextView(getContext());
            tv4.setGravity(Gravity.CENTER);
            tv4.setTextSize(textSize);
            tv4.setText(tv2_4.getText());
            TextView tv5 = new TextView(getContext());
            tv5.setGravity(Gravity.CENTER);
            tv5.setTextSize(textSize);
            tv5.setText(tv2_5.getText());
            row.addView(tv1);
            row.addView(tv2);
            row.addView(tv3);
            row.addView(tv4);
            row.addView(tv5);
            tableLayout.addView(row,i+3);
        }

    }
}